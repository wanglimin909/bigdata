package com.atwlm1.java;

/*
 *一、数组的概述
 *	1.数组的理解:数组(Array)，是多个相同类型数据按一定顺序排列的集合，
 *			并使用一个名字命名，并通过编号的方式对这些数据进行统一管理
 *	2.数组相关概念
 *			>数组名		>元素  	>角标(下标、索引)		>数组的长度(元素的个数)
 *	3.数组的特点：
 *		1）数组是有序排列的
 *		2）数组属于引用数据类型,数组中的元素可以是任何数据类型，包括基本数据类型和引用数据类型
 *		3）创建数组对象会在内存中开辟一整块连续的空间
 *		4）数组的长度一旦确定，就不能修改
 *	4.数组的分类
 *		按照维度：一维数组、二维数组、三维数组、…
 *		按照元素的数据类型分：基本数据类型元素的数组、引用数据类型元素的数组(即对象数组)
 *	5.一维数组的使用
 *		①一维数组的声明和初始化
 *		② 如何调用数组的指定位置的元素
 *  	③ 如何获取数组的长度
 * 	    ④ 如何遍历数组
 *      ⑤ 数组元素的默认初始化值
 *      	>数组元素是整型：0
 *      	>数组元素是浮点型：0.0
 *      	>数组元素是char型：0或'\u0000',而非'0'
 *      	>数组元素是boolean型：false(计算机底层二进制拿0充当false)
 *      
 *      	>数组元素是引用数据类型时：null(空值)(不是"null")
 *  	⑥ 数组的内存解析
 */
public class ArrayTest1 {
	public static void main(String[] args) {
		
		//①一维数组的声明和初始化
		int num;//声明
		num = 10;//初始化
		int id = 1001;//声明 + 初始化
			
		//1.1静态初始化:数组的初始化和数组元素的复制操作同时进行
		int[] ids = new int[]{1001,1002,1003,1004};
		//1.2动态初始化:数组的初始化和数组元素的复制操作分开进行
		String[] names = new String[5];
		
		//不标准但是正确
		int[]arr5 = {1,2,3,4};//类型推断
		//总结：数组一旦初始化完成，其长度就确定了
		
		//② 如何调用数组的指定位置的元素:通过角标的方式调用
		//数组的角标(或索引)从0开始，到数组的长度-1结束
		names[0] = "张三";
		names[1] = "李四";
		names[2] = "王五";
		names[3] = "赵六";
		names[4] = "孙七";//charAt(0)获取“孙”  {字符串也是从0开始}
		//names[5] = "老八";
		
		//③ 如何获取数组的长度。
		//属性：length
		System.out.println(names.length);//5
		System.out.println(ids.length);//4
		
		//④ 如何遍历数组
		/*System.out.println(names[0]);
		System.out.println(names[1]);
		System.out.println(names[2]);
		System.out.println(names[3]);
		System.out.println(names[4]);
		*/
		for(int i = 0;i < names.length;i++){
			System.out.println(names[i]);			
		}
		
		//⑤ 数组元素的默认初始化值
		int[] arr = new int[4];
		for(int i = 0;i < arr.length;i++){
			System.out.println(arr[i]);
		}
		
		System.out.println("*********************");
		
		double[] arr1 = new double[4];
		for(int i = 0;i < arr1.length;i++){
			System.out.println(arr1[i]);
		}
		
		System.out.println("*********************");
		
		char[] arr2 = new char[4];
		for(int i = 0;i < arr2.length;i++){
			System.out.println("----" + arr2[i] + "****");
		}
		//输出空格效果，但其实是0
		if(arr2[0] == 0){
			System.out.println("你好！");
		}
		
		System.out.println("*********************");
		
		boolean[] arr3 = new boolean[4];
		for(int i = 0;i < arr3.length;i++){
			System.out.println(arr3[i]);
		}
		
		System.out.println("*********************");
		
		String[] arr4 = new String[4];
		for(int i = 0;i < arr4.length;i++){
			System.out.println(arr4[i]);
		}
		if(arr4[0] == null){
			System.out.println("你好！");
		}
		
		//⑥ 数组的内存解析
		
	}
}
