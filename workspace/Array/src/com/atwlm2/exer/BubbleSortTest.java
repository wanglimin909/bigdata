package com.atwlm2.exer;
/*
 * 数组的冒泡排序的实现
 * 冒泡排序的基本思想：通过对待排序序列从前向后，依次比较相邻元素的排序码，
 * 若发现逆序则交换，使排序码较大的元素逐渐从前部移向后部。
 * 若序列中有 n 个元素，通常进行 n - 1 趟
 */
public class BubbleSortTest {
	public static void main(String[] args) {
		int[] arr = new int[]{43,32,76,-98,0,64,33,-21,32,99};
		//冒泡排序
		for(int i = 0;i<arr.length-1;i++){
			for(int j = 0;j < arr.length - 1 - i;j++){
				if(arr[j] > arr[j + 1]){
					int temp = arr[j];
					arr[j] = arr [j + 1];
					arr[j + 1] = temp;
				}
			}
		}
		for(int i = 0;i<arr.length;i++){
			System.out.print(arr[i] + "\t");
		}
		
		
	}
}
