package com.atwlm2.exer;
/*数组中算法的考察2：求数值型数组中元素的最大值、最小值、平均数、总和等
 * 	定义一个int型的一维数组，包含10个元素，分别赋一些随机整数，
 *  然后求出所有元素的最大值，最小值，和值，平均值，并输出出来。
 * 		要求：所有随机数都是两位数。
 * 	
 *  随机两位数[10,99]
 *  公式：(int)(Math.random() * (99 - 10 + 1) + 10)
 *  
*/
public class ArrayDemo2 {
	public static void main(String[] args) {
		int[] arr = new int[10];
		//赋值随机数
		for(int i = 0;i<arr.length;i++){
			arr[i] = (int)(Math.random() * (99 - 10 + 1) + 10);
		}
		//遍历随机数（有个参考）
		for(int i = 0;i<arr.length;i++){
			System.out.print(arr[i] + "\t");
		}
		System.out.println();
		//数组元素的最大值
		int maxValue = arr[0];//可先暂时赋值为数组中的任意一个元素
		for(int i = 1;i<arr.length;i++){//第一个元素已经赋给maxValue,所以从第二个元素开始
			if(maxValue<arr[i]){
				maxValue = arr[i];
			}
		}
		System.out.println("最大值：" + maxValue);
		
		//数组元素的最小值
		int minValue = arr[0];//可先暂时赋值为数组中的任意一个元素
		for(int i = 1;i<arr.length;i++){//第一个元素已经赋给maxValue,所以从第二个元素开始
			if(minValue>arr[i]){
				minValue = arr[i];
			}
		}
		System.out.println("最小值：" + minValue);
		
		//数组元素的和值
		int sum = 0;
		for(int i = 0;i<arr.length;i++){
			sum += arr[i];
		}
		System.out.println("总和为:" + sum);
		//数组元素的平均值
		int avgValue = sum / arr.length;//一般用double接收（更精确）
		System.out.println("平均数为:" + avgValue);
	
		
	}
}
