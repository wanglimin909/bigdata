package com.atwlm2.exer;
/*数组中算法的考察3：数组的复制、反转、查找(线性查找、二分法查找)
 * 
 * (练习)使用简单数组
 * 	(1)创建一个名为ArrayDemo3的类，在main()方法中声明arr1和arr2两个变量，他们是int[]类型的数组。
 * 	(2)使用大括号{}，把arr1初始化为8个素数：2,3,5,7,11,13,17,19。
 * 	(3)显示arr1的内容。
 * 	(4)赋值arr2变量等于arr1，修改arr2中的偶索引元素，使其等于索引值(如arr[0]=0,arr[2]=2)。打印出arr1。
 * 		思考：arr1和arr2是什么关系？
 * 				arr1和arr2地址值相同，都指向了堆空间中唯一的一个数组实体
 * 		拓展：修改题目，实现arr2对arr1数组的复制
 */
public class ArrayDemo3 {
	public static void main(String[] args) {
		
/*		int[]arr1,arr2;
		arr1 = new int[]{2,3,5,7,11,13,17,19};
		
		//显示arr1的内容
		for(int i = 0;i<arr1.length;i++){
			System.out.print(arr1[i] + "\t");
		}
		System.out.println();//2,3,5,7,11,13,17,19
		
		//赋值arr2变量等于arr1
		//不能称作数组的复制(看做建桌面快捷方式)
		arr2 = arr1;
		
		//修改arr2中的偶索引元素，使其等于索引值(如arr[0]=0,arr[2]=2)
		for(int i = 0;i<arr2.length;i++){
			if(i % 2 == 0){
				arr2[i] = i;
			}
			System.out.print(arr2[i] + "\t");
		}
		System.out.println();//0,3,2,7,4,13,6,19
		
		//打印出arr1
		for(int i = 0;i<arr1.length;i++){
			System.out.print(arr1[i] + "\t");//0,3,2,7,4,13,6,19
		}
*/		
	
/*
	//实现arr2对arr1数组的复制
		
		int[]arr1,arr2;
		arr1 = new int[]{2,3,5,7,11,13,17,19};
		//显示arr1的内容
		for(int i = 0;i<arr1.length;i++){
			System.out.print(arr1[i] + "\t");
		}
		System.out.println();//2,3,5,7,11,13,17,19
		
		//赋值arr2变量等于arr1
		//数组的复制(看做复制了一份文件到桌面)
		arr2 = new int[arr1.length];
		for(int i = 0;i<arr2.length;i++){
			arr2[i] = arr1[i];
		}
		
		//修改arr2中的偶索引元素，使其等于索引值(如arr[0]=0,arr[2]=2)
		for(int i = 0;i<arr2.length;i++){
			if(i % 2 == 0){
				arr2[i] = i;
			}
			System.out.print(arr2[i] + "\t");
		}
		System.out.println();//0,3,2,7,4,13,6,19
		
		//打印出arr1
		for(int i = 0;i<arr1.length;i++){
			System.out.print(arr1[i] + "\t");//2,3,5,7,11,13,17,19
		}
*/		
		
		String[] arr1 = new String[]{"JJ","DD","AA","BB"};
		//数组的复制(区别于数组变量的赋值)
		String[] arr2 = new String[arr1.length];
		for(int i = 0;i<arr2.length;i++){
			arr2[i] = arr1[i];
			System.out.print(arr2[i] + "\t");
		}
		System.out.println();
		
		
		//数组的反转
		//方法一
			/*
			for(int i = 0;i<arr1.length / 2;i++){
				String temp = arr1[i];
				arr1[i] = arr1[arr1.length - i - 1];
				arr1[arr1.length - i - 1] = temp;
			}
			*/
		//方法二
			/*
			for(int i = 0,j = arr1.length-1;i<j;i++,j--){
				String temp = arr1[i];
				arr1[i] = arr1[j];
				arr1[j] = temp;
			}
			for(int i = 0;i<arr1.length;i++){
				System.out.print(arr1[i] + "\t");
			}
			*/
		
		//查找(或搜索)
		//线性查找：
		String dest1 = "BB";
		boolean isFlag1 = true;
		for(int i = 0;i<arr1.length;i++){
			if(dest1.equals(arr1[i])){
				System.out.println("找到了指定的元素，位置为：" + i);
				isFlag1 = false;
				break;
			}
		}
		if(isFlag1){
			System.out.println("没找到");
		}
		//二分法查找:
		//前提：所要查找的数组必须有序
		int[] arr3 = new int[]{-98,-65,4,54,63,84,466,534,536};
		int dest2 = 84;
		int head = 0;//初始的首索引
		int end = arr3.length - 1;//初始的末索引
		boolean isFlag2 = true;
		while(head <= end){
			int middle = (head + end)/2;
			if(dest2 == arr3[middle]){
				System.out.println("找到了指定的元素，位置为：" + middle);
				isFlag2 = false;
				break;
			}else if(arr3[middle] > dest2){
				end = middle - 1;
			}else{//arr3[middle] < dest2
				head = middle + 1;
			}
		}
		if(isFlag2){
			System.out.println("没找到");
		}
		
	
	}
}
