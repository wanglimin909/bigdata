package practice02;

import java.util.Scanner;
/*
 * 模拟电脑出题10道(两位数加减乘除)
 * 做错不通过，继续做直到正确为止，并显示做题时间
 */
/*
 * 随机生成两位数(随机函数类)
 * 如何获取一个随机数：10 - 99
			double value = Math.random();  //[0.0,1.0)
			double value = Math.random(); * 90 + 10;  //[10.0,100.0)
			int value = (int)(Math.random() * 90 + 10);  //[10,99]
			公式：[a,b] : (int)(Math.random() * (b - a + 1) + a);
 */
/*
 * java时间复杂度(如何统计运行时间)
 *		获取当前时间的毫秒数，距离1970-01-01 00:00:00到现在的毫秒数
 *		long start = System.currentTimeMillis();
 *		获取当前时间的毫秒数，距离1970-01-01 00:00:00到现在的毫秒数
 *		long end = System.currentTimeMillis();
 *			System.out.println("所花费的时间为：" + (end - start));
 * 
 */
public class p6 {
	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		long start = System.currentTimeMillis();
		for(int k = 1;k<=10;k++){
			boolean isFlag = true;
			int i,j;
			i = (int)(Math.random() * 90 + 10);
			j = (int)(Math.random() * 90 + 10);
				do{
					System.out.print(i+"+"+j+"=");
					int result01=input.nextInt();
					if(result01==(i+j)) {
						System.out.println("回答正确");
						isFlag =false;
					}
					else {
						System.out.println("回答错误");
						continue;
					}
				}while(isFlag);
		}
		long end = System.currentTimeMillis();
		System.out.println("所花费的时间为：" + (end - start));
	} 
}