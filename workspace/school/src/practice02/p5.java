package practice02;
import java.util.Scanner;
/*
 * 递归方法的调用举例(函数求和)
 */
public class p5 {
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("请输入一个正整数n");
		int n = scan.nextInt();
		System.out.println("sum:"+sumOdd(n));
		System.out.printf("sum:%.2f\n",sumOdd(n));
	}
	public static double sumOdd(int n){
		if(n == 1)
			return 1.0/3;
		else
			return (1.0*n/(2*n+1) +sumOdd(n-1));
	}
}











