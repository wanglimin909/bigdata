package practice02;
import java.util.Scanner;
/*
 * 字符串逆序输出
 */
public class p3 {
	
	 public static void main(String args[]) {
	  Scanner in=new Scanner(System.in);
	  System.out.println("input a string:");
	  String s = in.nextLine();
	  System.out.println("inverse:"+inverseString(s));
	 }
	 private static String inverseString(String s){
		 if(s.length()==1){
			 return "" + s.charAt(0);
		 }
		 else
			 return ""+s.charAt(s.length()-1)+inverseString(s.substring(0,s.length()-1));
	 }
}

