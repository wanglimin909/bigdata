package practice02;
import java.util.Scanner;
/*
 * 一串数字的逆序输出
 */
public class p2 {
	     public static void main(String[] args) {
	         Scanner sc = new Scanner(System.in);
	         System.out.println("请你输入一个数字：");
	         int num = sc.nextInt();
	         String ss = way(num);
	         System.out.println(num+"的逆序为："+ss);
	     }
	     public static String way(int num){
	         String s="";
	         if (num<10){
	             return num+s;
	         }else {
	             return s+way(num%10)+way(num/10);
	         }
	     }
}

