package practice03;
import java.util.Scanner;

/*
 * 输入正整数n，计算并输出1！+2！+...+n!
 */
public class p4 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("请输入正整数n：");
		int n = scan.nextInt();
		int a,sum = 0;
		for(int j=n;j>0;j--){
			a=1;
			for(int i=1;i<=j;i++){
				a*=i;
			}
			sum+=a;
		}
		System.out.println("结果为："+sum);
	}
}
