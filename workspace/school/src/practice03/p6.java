package practice03;
import java.util.Scanner;

/*
 * 输入正整数n，将其转换为八进制数
 */
public class p6 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("请输入正整数n:");
		int n = in.nextInt();
		System.out.println("n的八进制为："+f(n));
	}
	private static String f(int n){
		if(n<8){
			return ""+n;
		}else{
			return f(n/8) + n%8;
		}
	}
}
