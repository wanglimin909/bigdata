package practice03;
import java.util.Scanner;

/*
 * 输入一个整数，利用递归方法计算该整数各位数字之和
 */
public class p5{
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("请输入一个整数：");
		int n = in.nextInt();
		System.out.println("整数各位数字之和为："+way(n));
	}
	 public static int way(int n){
         if (n==0){
             return 0;
         }else{
            return n%10+way(n/10);
         }
     }
}
