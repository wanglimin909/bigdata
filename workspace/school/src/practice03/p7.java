package practice03;
import java.util.Scanner;
/*
 * 随机生成10道两位数的加法，只要输入答案，程序会自动判断是否正确
 */
public class p7 {
	public static void main(String[] args){
	Scanner input=new Scanner(System.in);
	for(int k = 1;k<=10;k++){
		boolean isFlag = true;
		int i,j;
		i = (int)(Math.random() * 90 + 10);
		j = (int)(Math.random() * 90 + 10);
			do{
				System.out.print(i+"+"+j+"=");
				int result01=input.nextInt();
				if(result01==(i+j)) {
					System.out.println("回答正确");
					isFlag =false;
				}
				else {
					System.out.println("回答错误");
					continue;
				}
			}while(isFlag);
	}
	} 
}
