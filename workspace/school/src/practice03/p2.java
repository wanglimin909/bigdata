package practice03;
/*
	九九乘法表
	1 * 1 = 1
	2 * 1 = 2  2 * 2 = 4
	...

	9 * 1 = 9 ................. 9 * 9 = 81
 */

public class p2 {
		public static void main(String[] args) {
			
			for(int i = 1;i <= 9;i++){//控制行数
				for(int j = 1;j <= i;j++){//控制列数
					System.out.print(i + " * " + j + " = " + (i * j) + "  ");			
				}
				System.out.println();	
			}
		}
	}

