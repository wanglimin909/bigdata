package practice01;
/**
 * @Description 课堂练习
 * @author atwlm Email:3478344074@qq.com 
 * @version 
 * @date 2023年3月6日上午10:17:17 
 */
public class InitVar {
	public static void main(String[] args) {
		byte a = 10;
		short b = 20;
		int c = 30;
		long d = 40L;
		float e = 50F;
		double f = 60;
		char g = 'A';
		boolean h = true;
		double PI = 3.14;
		System.out.println("字节型，a = " + a);
		System.out.println("短整型，a = " + b);
		System.out.println("整型，a = " + c);
		System.out.println("长整型，a = " + d);
		System.out.println("单精度型，a = " + e);
		System.out.println("单精度型，a = " + f);
		System.out.println("字符型，a = " + g);
		System.out.println("布尔型，a = " + h);
		System.out.println("双精度型，a = " + PI);
	}
}
