package practice01;

import java.util.Scanner;

public class ScannerTest {
	public static void main(String[] args) {
		int num1;
		double num2;
		double result;
		Scanner scan = new Scanner(System.in);
		System.out.println("input num1:");
		num1 = scan.nextInt();
		System.out.println("input num2:");
		num2 = scan.nextDouble();
		result = num1 * num2;
		System.out.println("result:" + result);
	}
}
