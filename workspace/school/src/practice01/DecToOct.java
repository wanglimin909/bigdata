package practice01;

import java.util.Scanner;

public class DecToOct {
	public static void main(String[] args) {
		System.out.println("请输入八进制整数：");
		int n = new Scanner(System.in).nextInt();
		System.out.println(n+"的八进制为："+dec20ct(n));
	}
	private static String dec20ct(int n){
		if(n<8){
			return n + "";
		}
		else
			return dec20ct(n/8)+n%8;
	}
}
