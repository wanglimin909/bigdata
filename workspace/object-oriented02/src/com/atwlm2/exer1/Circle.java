package com.atwlm2.exer1;

public class Circle extends GeometricObject{
	private double radius;
	protected Circle(double radius,String color, double weight) {
		super(color, weight);
		this.radius = radius;
	}
	public void setRadius(double radius){
		this.radius = radius;
	}
	public double getRadius(){
		return radius;
	}
	@Override
	public double findArea() {
		return Math.PI * radius *radius;
	}
}
