package com.atwlm2.exer1;

//考查多态的笔试题目：
public class InterviewTest2 {

	public static void main(String[] args) {
		Base2 base = new Sub2();
		base.add(1, 2, 3);

//		Sub2 s = (Sub2)base;
//		s.add(1,2,3);
	}
}

class Base2 {
	public void add(int a, int... arr) {
		System.out.println("base2");
	}
}

class Sub2 extends Base2 {

	public void add(int a, int[] arr) {//可以看做对父类方法的重写(int[] arr == int...arr)
		System.out.println("sub_1");
	}

//	public void add(int a, int b, int c) {//不能看做对父类方法的重写
//		System.out.println("sub_2");
//	}

}