package com.atwlm2.java1;
/*
 * 有了对象的多态性以后，内存中实际上是加载了子类特有的属性和方法的，但是由于变量声明为父类类型,
 * 导致编译时，只能调用父类中声明的属性和方法，子类中特有的属性和方法不能调用
 * 
 * 如何才能调用子类特有的属性和方法?
 * 		向下转型：使用强制类型转换符（多态性就相当于向上转型）
 * 注意：使用强转时，可能出现ClassCastException的异常
 * 		 因此引入关键字instanceof
 * instanceof关键字的使用
 * 			a instanceof A:判断对象a是否是类A的实例。如果是，返回true、如果不是，返回false
 * 使用情境：为了避免在向下转型时出现ClassCastException的异常，我们在向下转型之前、先进行
 * 			instanceof的判断，返回true就向下转型，返回false就不进行向下转型
 * 		如果  a instanceof A 返回true，则  a intsanceof B 也返回true
 * 		其中，类B是类A的父类
 * 
 */
public class PersonTest {
	public static void main(String[] args){
		Person p1 = new Man();
		p1.eat();
		p1.walk();
		//不能调用子类所特有的方法、属性：编译时，p1是Person类型。
		p1.name = "Tom";
//		p1.isSmoking = true;
//		p1.earnMoney();
		//有了对象的多态性以后，内存中实际上是加载了子类特有的属性和方法的，但是由于变量声明
		//为父类类型,导致编译时，只能调用父类中声明的属性和方法，子类中特有的属性和方法不能调用
		
		//如何才能调用子类特有的属性和方法
		Man m1 = (Man)p1;//使用强制类型转换符
		m1.isSmoking = true;
		m1.earnMoney();
		
		//使用强转时，可能出现ClassCastException的异常
//		Woman w1 = (Woman)p1;
//		w1.goShopping();
		
		//instanceof:
		if(p1 instanceof Woman){
			Woman w1 = (Woman)p1;
			w1.goShopping();
			System.out.println("********woman*********");
		}
		if(p1 instanceof Man){
			Man m2 = (Man)p1;
			m2.earnMoney();
			System.out.println("********man*********");
		}
		
		if(p1 instanceof Person){
			System.out.println("******person********");
		}
		if(p1 instanceof Object){
			System.out.println("******object********");
		}
		
		//问题一：编译时通过，运行时不通过
//		1.
//		Person p2 = new Woman();
//		Man m3 = (Man)p2;
//		2.
//		Person p4 = new Person();
//		Man m4 = (Man)p4;
		
		//问题二：编译时通过，运行时也通过
//		Object obj = new Woman();
//		Person p = (Person)obj;
		
		//问题三：编译时不通过
//		Man m5 = new Woman();
		
	}
}
