package com.atwlm2.java2;
import java.util.Date;
/*
 * 面试题：==和equals的区别
 * 1.== 既可以比较基本类型也可以比较引用类型。对于基本类型就是比较值，对于引用类型就是比较内存地址
 * 2.equals的话，它是属于java.lang.Object类里面的方法，如果该方法没有被重写过默认也是==;
 *   我们可以看到String等类的equals方法是被重写过的，而且String类在日常开发中用的比较多，久而久之，形成了equals是比较值的错误观点
 * 3.具体要看自定义类里有没有重写Object的equals方法来判断
 * 4.通常情况下，重写equals方法，会比较类中的相应属性是否都相等
 * 一、回顾  == 的使用
 * == ：运算符
 * 		1.可以使用在基本数据类型变量和引用数据类型变量中
 * 		2.如果比较的是基本数据类型变量：比较两个变量保存的数据是否相等(不一定类型要相同)
 * 		    如果比较的是引用数据类型变量：比较两个对象的地址值是否相同(即两个引用是否指向同一个对象实体)
 * 		补充： == 符号使用时，必须保证符号左右两边的变量类型一致
 * 二、equals()方法的使用
 * 		1.是一个方法，而非运算符
 * 		2.只能适用于引用数据类型
 * 		3.Object类中equals的定义
 * 			 public boolean equals(Object obj) {
 *       		return (this == obj);
 *  		 }
 * 			  说明：Object类中定义的equals()和==的作用是相同的(比较两个对象的地址值是否相同)
 * 		4.像String、Date、File、包装类等都重写了Object类中的equals()方法
 * 		    重写以后，比较的不是两个引用的地址是否相同，而是比较两个对象的“实体内容”是否相同
 * 		5.通常情况下，我们自定义的类如果使用equals()的话，也通常是比较两个对象的“实体内容”是否相同
 * 		    就需要对Object类中的equals()进行重写
 * 		    重写的原则：比较两个对象的实体内容是否相同
 */
public class EqualsTest {
	public static void main(String[] args) {
		//基本数据类型
		int i = 10;
		int j = 10;
		double d = 10.0;
		System.out.println(i==j);//true
		System.out.println(i==d);//true(类型提升)
		
		boolean b = true;
//		System.out.println(i==b);//不能比
		
		char c1 = 10;
		System.out.println(i==c1);//true
		char c2 = 'A';
		char c3 = 65;
		System.out.println(c2==c3);//true
		//**************************************
		//引用数据类型
		Customer cust1 = new Customer("Tom",21);
		Customer cust2 = new Customer("Tom",21);
		System.out.println(cust1==cust2);//false
		
		String str1 = new String("atwlm");
		String str2 = new String("atwlm");
		System.out.println(str1==str2);//false
		
		System.out.println("**************************************");
		
		System.out.println(cust1.equals(cust2));//false--(重写equals())-->true
		System.out.println(str1.equals(str2));//true  //equals被String类重写过
		
		Date date1 = new Date(24973283);
		Date date2 = new Date(24973283);
		System.out.println(date1.equals(date2));//true  //equals被String类重写过
	}
}

class Customer{
	private String name;
	private int age;
	public Customer(){
		
	}
	public Customer(String name,int age){
		this.name = name;
		this.age = age;
	}
	public void setName(String name){
		this.name = name;
	}
	public String getName(){
		return name;
	}
	public void setAge(int age){
		this.age = age;
	}
	public int getAge(){
		return age;
	}
	//重写原则：比较两个对象的“实体内容”(即name和age)是否相同
	//手动实现equals()的重写
//	@Override
//	public boolean equals(Object obj) {
//		 if (this == obj) {
//	         return true;
//	     }
//		 if(obj instanceof Customer){
//			 Customer cust = (Customer)obj;
//			 //比较两个对象的属性是否都相同
////			 if(this.age == cust.age && this.name.equals(cust.name)){
////				 return true;
////			 }else{
////				 return false;
////			 }
//			 //或
//			 return this.age == cust.age && this.name.equals(cust.name);
//		 }
//		 return false;
//	}
	//自动生成equals()
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (age != other.age)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	//手动实现toString()的重写
//	@Override
//	public String toString() {
//		return "Customer[name =" + name + ",age = " + age + "]";
//	}
	//自动生成toString()
	@Override
	public String toString() {
		return "Customer [name=" + name + ", age=" + age + "]";
	}
	
}
