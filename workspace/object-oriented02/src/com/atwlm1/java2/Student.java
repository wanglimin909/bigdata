package com.atwlm1.java2;

public class Student extends Person{
	String major;
	public Student(){
		
	}
	public Student(String major){
		this.major = major;
	}
	public void study(){
		System.out.println("学习专业是" + major);
	}
	//对父类中的eat()进行重写
	public void eat(){
		System.out.println("学生吃饭！");
	}
	public void show(){
		System.out.println("我是一个学生");
	}
	public String info(){
		return null;
	}
//	public int info1(){//报错
//		return 1;
//	}
}
