package com.atwlm1.exer1;

public class CylinderTest {
	public static void main(String[] args) {
		Cylinder cy = new Cylinder();
		cy.setRadius(2);
		cy.setLength(2);
		double volume = cy.findVolume();
		System.out.println("圆柱体积为：" + volume);
	}
}
