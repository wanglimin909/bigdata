package com.atwlm1.java6;

import java.util.Random;

//面试题：多态是编译时行为还是运行时行为？
//		   运行时行为
//证明如下：
class Animals{
	protected void eat() {
		System.out.println("animal eat food");
	}
}
class Cat1 extends Animals{
	protected void eat() {
		System.out.println("cat eat fish");
	}
}
class Dog1 extends Animals  {
	public void eat() {
		System.out.println("Dog eat bone");
	}
}
class Sheep extends Animals  {
	public void eat() {
		System.out.println("Sheep eat grass");
	}
}
public class InterviewTest {
	public static Animals getInstance(int key) {
		switch (key) {
		case 0:
			return new Cat1 ();
		case 1:
			return new Dog1 ();
		default:
			return new Sheep();
		}
	}
	public static void main(String[] args) {
		int key = new Random().nextInt(3);
		System.out.println(key);
		Animals animals = getInstance(key);
		animals.eat();
	}
}
