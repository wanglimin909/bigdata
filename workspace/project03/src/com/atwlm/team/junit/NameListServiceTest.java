package com.atwlm.team.junit;

import org.junit.Test;

import com.atwlm.team.domain.Employee;
import com.atwlm.team.service.NameListService;
import com.atwlm.team.service.TeamException;

/**
 * @Description 对NameListService类的测试
 * @author atwlm Email:3478344074@qq.com 
 * @version 
 * @date 2023年2月13日下午4:22:27 
 */
public class NameListServiceTest {
	
	@Test
	public void testGetAllEmployees(){
		NameListService service = new NameListService();
		Employee[] employees = service.getAllEmployees();
		for(int i = 0;i < employees.length;i++){
			System.out.println(employees[i]);
		}
	}
	
	@Test
	public void testgetEmployee(){
		NameListService service = new NameListService();
		int id =10;
		try {
			Employee employee = service.getEmployee(id);
			System.out.println(employee.toString());
		} catch (TeamException e) {
			System.out.println(e.getMessage());
		}
	}
	
	
}
