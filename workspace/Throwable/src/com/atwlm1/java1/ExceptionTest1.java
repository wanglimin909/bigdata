package com.atwlm1.java1;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.junit.Test;
/*
 * 一、异常的处理：抓抛模型
 * 		过程一：“抛”：程序在正常执行的过程中，一旦出现异常，就会在异常代码处生成一个对应异常类的
 * 					  对象并将此对象抛出。
 * 					  一旦抛出对象以后，其后的代码就不再执行。
 * 			关于异常对象的产生：①系统自动生成的异常对象
 * 							  ②手动的生成一个异常对象，并抛出(throw)
 * 
 * 		过程二：“抓”：可以理解为异常的处理方式：①try-catch-finally ②throws
 * 二、try-catch-finally的使用
 * 		try{
 * 			//可能出现异常的代码
 * 		}catch(异常类型1 变量名1){
 * 			//处理异常的方式1
 * 		}catch(异常类型2 变量名2){
 * 			//处理异常的方式2
 * 		}catch(异常类型3 变量名3){
 * 			//处理异常的方式3
 * 		}
 * 		......
 * 		finally{
 * 			//一定会执行的代码
 * 		}
 * 	说明：
 * 		1.finally是可选的
 * 		2.使用try将可能出现异常的代码包装起来，在执行过程中，一旦出现异常，就会生成一个对应异常类
 * 		    的对象，根据此对象的类型，去catch中进行匹配
 * 		3.一旦try中的异常对象匹配到某一个catch时，就进入catch中进行异常的处理。一旦处理完成，就跳
 * 		    出当前的try-catch结构(在没有写finally的情况下)，继续执行其后的代码
 * 		4.catch中的异常类型如果没有子父类关系，则声明顺序无所谓
 * 		  catch中的异常类型如果满足子父类关系，则要求子类一定声明在父类的上面。否则报错
 * 		5.常用的异常对象处理的方式：①String getMessage()  ②printStackTrace()
 * 		6.在try结构中声明的变量，在出了try结构以后，就不能在被调用
 * 		7.try-catch-finally结构可以嵌套
 * try-catch-finally中finally的使用
 * 		1.finally是可选的
 * 		2.finally中声明的是一定会被执行的代码。即使catch中又出现异常了，try中有return
 * 		    语句，catch中有return语句等情况
 * 		3.像数据库连接、输入输出流、网络编程Socket等资源，JVM是不能自动回收的，我们需要
 * 		    手动的进行资源的释放。此时的资源释放，就需要声明在finally中
 * 
 * 
 * 体会1：使用try-catch-finally处理编译时异常，使得程序在编译时不再报错，但运行时仍可能报错
 * 		   相当于我们使用try-catch-finally将一个编译时可能出现的异常，延迟到运行时出现
 * 体会2：开发中，由于运行时异常比较常见，所以我们通常不针对运行时异常编写try-catch-finally。
 * 		   针对编译时异常，我们一定要考虑异常的处理
 */
public class ExceptionTest1 {

	@Test
	public void test1(){
		String str = "123";
		str = "abc";
		int num = 0;
		try{
			num = Integer.parseInt(str);
			System.out.println("hello---1");
		}catch(NullPointerException e){
			System.out.println("出现空指针异常了!");
		}catch(NumberFormatException e){
//			System.out.println("出现数值转换异常了!");
			//常用1
			//String getMessage();
//			System.out.println(e.getMessage());
			//常用2
			//printStackTrace();
//			e.printStackTrace();
		}catch(Exception e){
			System.out.println("出现异常了!");
		}
		System.out.println(num);
		System.out.println("hello---2");
	}
	
	@Test
	public void test2(){
		try{
		File file = new File("hello.txt");
		FileInputStream fis = new FileInputStream(file);
		int data = fis.read();
		while(data != -1){
			System.out.print((char)data);
			data = fis.read();
		}
		fis.close();
		}catch(FileNotFoundException e){
			e.printStackTrace();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	//*********************************************************
	
	
	@Test
	public void test3(){
		try{
			int a = 10;
			int b = 0;
			System.out.println(a / b);
		}catch(ArithmeticException e){
//			e.printStackTrace();
			int[] arr = new int[10];
			System.out.println(arr[10]);
		}catch(Exception e){
			e.printStackTrace();
		}
//		System.out.println("hello!!");
		finally{
			System.out.println("hello~~");
		}
	}
	
	@Test
	public void testMethod(){
		int num = method();
		System.out.println(num);
	}
	public int method(){
		try{
			int[] arr = new int[10];
			System.out.println(arr[10]);
			return 1;
		}catch(ArrayIndexOutOfBoundsException e){
			e.printStackTrace();
			return 2;
		}finally{
			System.out.println("我一定会被执行");
		}
	}
	
	@Test
	public void test4(){
		FileInputStream fis = null;
			try {
				File file = new File("hello.txt");
				fis = new FileInputStream(file);
				int data = fis.read();
				while(data != -1){
					System.out.print((char)data);
					data = fis.read();
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}finally{
				try{
					if(fis != null)
						fis.close();
				}catch (IOException e){
					e.printStackTrace();
				}
			}
	}
	
}
