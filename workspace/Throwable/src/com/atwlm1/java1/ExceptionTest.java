package com.atwlm1.java1;
import java.io.File;
import java.io.FileInputStream;
import java.util.Date;
import java.util.Scanner;
import org.junit.Test;

/*
 * 一、异常体系结构
 * 		java.lang.Throwable
 * 			|---java.lang.Error:一般不编写针对性的代码进行处理
 * 			|---java.lang.Exception:可以进行异常处理
 * 				|---编译时异常(checked)
 * 					|---IOException
 * 						|---FileNotFoundException
 * 					|---ClassNotFoundException
 * 				|---运行时异常(unchecked)
 * 					|---NullPointerException
 * 					|---ArrayIndexOutOfBoundsException
 * 					|---ClassCastException
 * 					|---NumberFormatException
 * 					|---InputMismatchException
 * 					|---ArithmeticException
 * 面试题：常见的异常都有哪些？举例说明
 * 
 * 
 */
public class ExceptionTest {
	//NullPointerException(空指针异常)
	@Test
	public void test1(){
		//举例一
		int[] arr = null;
		System.out.println(arr[3]);
		//举例二
		String str = "abc";
		str = null;
		System.out.println(str.charAt(0));
	}
	
	//IndexOutOfBoundsException(越界异常)
	@Test
	public void test2(){
		//ArrayIndexOutOfBoundsException(数组下标越界异常)
		int arr[] = new int[10];
		System.out.println(arr[10]);
		//StringIndexOutOfBoundsException
		String str = "abc";
		System.out.println(str.charAt(3));
	}
	
	//ClassCastException(类型转换异常)
	@Test
	public void test3(){
		Object obj = new Date();
		String str = (String)obj;
	}
	
	//NumberFormatException(数值转换异常)
	@Test
	public void test4(){
		String str = "123";
		str = "abc";
		Integer.parseInt(str);		
	}
	
	//InputMismatchException(输入不匹配)
	@Test
	public void test5(){
		Scanner scanner = new Scanner(System.in);
		int score = scanner.nextInt();
		System.out.println(score);
		scanner.close();
	}
	
	//ArithmeticException(算术异常)
	@Test
	public void test6(){
		int a = 10;
		int b = 0;
		System.out.println(a / b);
	}
	
	//***************************************************
	
	@Test
	public void test7(){
//		File file = new File("hello.txt");
//		FileInputStream fis = new FileInputStream(file);
//		int data = fis.read();
//		while(data != -1){
//			System.out.print((char)data);
//			data = fis.read();
//		}
//		fis.close();
	}
	
}
