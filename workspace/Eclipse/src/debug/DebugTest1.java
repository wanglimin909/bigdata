package debug;
/*
 * 如何调试程序
 * 	 1.system.out.println();
 * 	 2.Eclipse - Debug调试
 */
public class DebugTest1 {
	public static void main(String[] args) {
		int i = 10;
		int j = 20;
		System.out.println("i = " + i + ", j = " + j);
		
		DebugTest1 test = new DebugTest1();
		int max = test.getMax(i, j);
		
		System.out.println("max = " + max);
	}

	private int getMax(int k, int m) {
		int max = 0;
		if (k < m) {
			max = k;
		} else {
			max = m;
		}
		return max;
	}

}
