package com.atwlm2.java4;
/*
 * 类的内部成员之五：内部类
 * 	1.Java中允许将一个类A声明在另一个类B中，则类A就是内部类，类B称为外部类
 * 	2.内部类的分类：成员内部类(静态、非静态) vs 局部内部类(方法内、代码块内、构造器内)
 * 	3.成员内部类：
 * 		一方面：作为外部类的成员：
 * 			>调用外部类的结构
 * 			>可以被static修饰
 * 			>可以被4中不同的权限修饰
 * 		另一方面：作为一个类：
 * 			>类内可以定义属性、方法、构造器等
 * 			>可以被final修饰：表示此类不能被继承/即不用final修饰的话可以被继承
 * 			>可以被abstract修饰
 * 	4.注意点
 * 		4.1：如何实例化成员内部类的对象
 * 				//创建Dog实例(静态的成员内部类)
				Person.Dog dog = new Person.Dog();
				dog.show();
				//创建Bird实例(非静态的成员内部类)
				Person p = new Person();
				Person.Bird bird = p.new Bird();
				bird.sing();
 * 		4.2：如何在成员内部类中区分调用外部类的结构
 * 				name//方法的形参
 * 				this.name//内部类的属性
 * 				Person.this.name//外部类的属性
 * 		4.3：开发中局部内部类的使用
 * 				//返回一个实现了Comparable接口的类的对象
				public Comparable getComparable(){
		
				//创建一个实现了Comparable接口的类:局部内部类
				//方式一：
				class MyComparable implements Comparable{

					@Override
					public int compareTo(Object o) {
					return 0;
					}
			
				}
				return new MyComparable();
		
				//方式二：
				return new Comparable(){

					@Override
					public int compareTo(Object o) {
					return 0;
					}
			
				};
 * 
 */
public class InnerClassTest1 {
	public static void main(String[] args) {
		//创建Dog实例(静态的成员内部类)
		Person.Dog dog = new Person.Dog();
		dog.show();
		//创建Bird实例(非静态的成员内部类)
//		Person.Bird bird = new Person.Bird();//错误的
		Person p = new Person();
		Person.Bird bird = p.new Bird();
		bird.sing();
		System.out.println("*******************************");
		bird.disPlay("黄鹂");
		
	}
}

class Person{
	
	String name = "张三";
	int age;
	public void eat(){
		System.out.println("人吃饭");
	}
	
	//静态成员内部类
	static class Dog{
		String name;
		int age;
		public void show(){
			System.out.println("狗");
//			eat();
		}
	}
	//非静态成员内部类
	class Bird{
		String name = "杜鹃";
		public Bird(){
			
		}
		public void sing(){
			System.out.println("鸟唱歌");
			Person.this.eat();//调用外部类的非静态属性
			eat();
		}
		public void disPlay(String name){
			System.out.println(name);//方法的形参
			System.out.println(this.name);//内部类的属性
			System.out.println(Person.this.name);//外部类的属性
		}
	}
	
	public void method(){
		//局部内部类
		class AA{
			
		}
	}
	
	{
		//局部内部类
		class BB{
			
		}
	}
	
	public Person(){
		//局部内部类
		class CC{
			
		}
	}
}
