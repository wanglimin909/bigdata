package com.atwlm2.java5;

public class InnerClassTest {
	/*
	 * 在局部内部类的方法中(比如：show)如果调用外部类所声明的方法(比如method)中的局部变量(比如：num)的话
	 * 要求此局部变量声明为final的。
	 * java7及之前的版本：要求此局部变量显式的声明为final的
	 * java8及之后的版本：可以省略final声明
	 * 
	 */
	public void method(){
		//局部变量
		final int num = 10;//java7及以前，必须显式声明final。java8及以后可省略final
		
		class AA{
			
			public void show(){
//				num = 20;
				System.out.println(num);
			}
			
		}
		
	}
	
}
