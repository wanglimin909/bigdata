package com.atwlm2.java3;
/*
 * JDK8：全局常量、抽象方法、静态方法、默认方法
 * 
 * 
 * 
 */
public interface CompareA {
	//静态方法
	public static void method1(){
		System.out.println("CompareA:北京");
	}
	//默认方法
	public default void method2(){
		System.out.println("CompareA:上海");
	}
	default void method3(){//可省略public
		System.out.println("CompareA:上海");
	}
	
	
}
