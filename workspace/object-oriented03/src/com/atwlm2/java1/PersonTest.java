package com.atwlm2.java1;
/*
 * 抽象类的匿名子类
 */
public class PersonTest {
	public static void main(String[] args) {
		
		method1(new Student());//匿名对象
		
		Worker worker = new Worker();
		method2(worker);//非匿名的类、非匿名的对象
		
		method2(new Worker());//非匿名的类、匿名的对象
		
		System.out.println("****************************");
		
		//创建了匿名子类的对象：p
		Person p = new Person(){
			@Override
			public void eat() {
				System.out.println("吃东西1");
			}
			@Override
			public void breath() {
				System.out.println("呼吸1");
			}
		};
		method2(p);
		
		System.out.println("*******************************");
		
		//创建匿名子类的匿名对象
		method2(new Person(){
			@Override
			public void eat() {
				System.out.println("吃东西2");
			}
			@Override
			public void breath() {
				System.out.println("呼吸2");
			}
		});
		
		
	}
	
	public static void method2(Person p){
		p.eat();
		p.breath();
	}

	public static void method1(Student s){
		
	}

}

class Worker extends Person{

	@Override
	public void eat() {
		
	}
	@Override
	public void breath() {
		
	}
	
}
