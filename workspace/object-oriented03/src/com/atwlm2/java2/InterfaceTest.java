package com.atwlm2.java2;
/*
 * 接口的使用：
 * 	1.接口使用interface来定义
 * 	2.Java中，接口和类是并列的两个结构
 * 	3.如何定义接口：定义接口中的成员
 * 		3.1 JDK7及以前：只能定义全局常量和抽象方法
 * 				>全局常量：public static final的,但是省略不写
 * 				>抽象方法：public abstract的
 * 
 * 		3.2 JDK8：全局常量、抽象方法、静态方法、默认方法
 * 
 * 	4.接口中不能定义构造器！(意味着接口不可以实例化)
 * 	5.Java开发中，接口都通过让类去实现(implements)的方式来使用
 * 	    如果实现类覆盖了接口中的所有抽象方法，则此实现类就可以实例化
 * 	    如果实现类没有覆盖接口中所有的抽象方法，则此实现类任为一个抽象类
 * 	6.Java类可以实现多个接口 ---> 弥补了Java类的单继承性的局限性
 * 	    格式：class AA extends BB implements CC,DD,EE
 * 	7.接口与接口之间可以继承，而且可以多继承
 *******************************************************************
 * 	8.接口的具体使用，体现多态性
 * 	9.接口，实际上可以看做是一种规范
 * 
 * 面试题：抽象类和接口有哪些异同？
 * 		相同点：不能实例化，都可以被继承
 * 		不同点：抽象类：有构造器。 接口：不能声明构造器
 * 			       单继承 vs 多继承 
 * 
 */
public class InterfaceTest {
	public static void main(String[] args) {
		System.out.println(Flyable.MAX_SPEED);
		System.out.println(Flyable.MIN_SPEED);
//		Flyable.MIN_SPEED = 2;
		
		Plane plane = new Plane();
		plane.fly();
		plane.stop();
		
	}
}

interface Flyable{
	//全局常量(省略了public static final)
	public static final int MAX_SPEED = 7900;//第一宇宙速度
	int MIN_SPEED = 1;//省略了public static final
	
	//抽象方法(省略了public abstract)
	public abstract void fly();
	void stop();//省略了public abstract 
	
}

interface Attackable{
	
	void attack();
	
}

class Plane implements Flyable{
	@Override
	public void fly() {
		System.out.println("通过引擎起飞");
	}
	@Override
	public void stop() {
		System.out.println("驾驶员减速停止");
	}
}

abstract class Kite implements Flyable{
	@Override
	public void fly() {
		System.out.println("飞");
	}
}

class Bullet extends Object implements Flyable,Attackable{
	@Override
	public void attack() {
	}
	@Override
	public void fly() {
	}
	@Override
	public void stop() {
	}
}
//*************************************
interface AA{
	void method1();
}
interface BB{
	void method2();
}
interface cc extends AA,BB{
	
}
