package com.atwlm2.exer1;
/*
 * 编写一个Employee类，声明为抽象类，
 * 包含如下三个属性：name，id，salary。
 * 提供必要的构造器和抽象方法：work()。
 */
public abstract class Employee {
	
	private String name;
	private int id;
	private double salary;
	public Employee() {
		super();
	}
	public Employee(String name, int id, double salary) {
		super();
		this.name = name;
		this.id = id;
		this.salary = salary;
	}
	public abstract void work();
}
//使用继承的思想，设计CommonEmployee类和Manager类，要求类中提供必要的方法进行属性访问
//对于Manager类来说，他既是员工，还具有奖金(bonus)的属性
class Manager extends Employee{
	private double bonus;//奖金
	public Manager(double bonus) {
		super();
		this.bonus = bonus;
	}
	public Manager(String name, int id, double salary, double bonus) {
		super(name, id, salary);
		this.bonus = bonus;
	}
	@Override
	public void work() {
		System.out.println("管理员工");
	}
}
class CommonEmployee extends Employee{
	@Override
	public void work() {
		System.out.println("员工在一线车间生产产品");
	}
}

