package com.atwlm1.exer1;
//static关键字的应用

public class CircleTest {
	public static void main(String[] args) {
		
		Circle c1 = new Circle();
		Circle c2 = new Circle();
		Circle c3 = new Circle(3.4);
		System.out.println("c1的ID：" + c1.getId());//1001
		System.out.println("c2的ID：" + c2.getId());//1002
		System.out.println("c3的ID：" + c3.getId());//1003
		System.out.println("创建的圆的个数为：" + Circle.getTotal());
	}
}

class Circle{
	private double radius;
	private int id;//自动赋值
	
	public Circle(){
		id = init++;
		total++;
	}
	public Circle(double radius){
		this();
//		id = init++;
//		total++;
		this.radius = radius;
	}
	
	private static int total;//记录创建的圆的个数
	private static int init = 1001;//static声明的属性被所有对象所共享
	
	public double findArea(){
		return 3.14 * radius;
	}

	public double getRadius() {
		return radius;
	}
	public int getId() {
		return id;
	}
	public static int getTotal() {
		return total;
	}
	
}









