package com.atwlm1.java1;

public class ArrayUtilTest {
	public static void main(String[] args) {
		
//		ArrayUtil util = new ArrayUtil();
		int[] arr = new int[]{1,2,34,325,35,3465,756,765,34};
		System.out.println("最大值为：" + ArrayUtil.getMax(arr));//最大值为：3465
		System.out.println("最小值为：" + ArrayUtil.getMin(arr));//最小值为：1
		System.out.println("总和为：" + ArrayUtil.getSum(arr));//总和为：5383
		System.out.println("平均值为：" + ArrayUtil.getAvg(arr));//平均值为：672

		System.out.println("排序前");
		ArrayUtil.pritn(arr);//遍历
		ArrayUtil.sort(arr);//排序
		System.out.println("排序后");
		ArrayUtil.pritn(arr);//遍历
		
		System.out.println("查找：");
		int index = ArrayUtil.getIndex(arr, 34);
		if(index >= 0){
			System.out.println("找到了，索引地址为：" + index);
		}else{
			System.out.println("没找到");
		}
		
		System.out.println("反转前");
		ArrayUtil.pritn(arr);//遍历
		ArrayUtil.reverse(arr);//反转
		System.out.println("反转后");
		ArrayUtil.pritn(arr);//遍历
		
		System.out.println("复制数组" + ArrayUtil.copy(arr));
	}
}
