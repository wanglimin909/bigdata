package com.atwlm.p2.service;
import com.atwlm.p2.bean.Customer;
/**
 * @Description CustomerList为Customer对象的管理模块，
 * 				内部用数组管理一组Customer对象，并提供相应的添加、修改、删除和遍历方法，
 * 				供CustomerView调用
 * @author atwlm Email:3478344074@qq.com 
 * @version 
 * @date 2023年1月16日下午3:24:18 
 */
public class CustomerList {
	private Customer[] customers;//用来保存客户对象的数组
	private int total;//记录已保存客户对象的数量
	/**
	 * 用来初始化customers数组的构造器
	 * @param totalCustomer
	 */
	public CustomerList(int totalCustomer){
		customers = new Customer[totalCustomer];
	}
	/**
	 * @Description 将指定的客户添加到数组中
	 * @author atwlm 
	 * @date 2023年1月16日下午3:54:23 
	 * @param customer
	 * @return true：添加成功    false：添加失败
	 */
	public boolean addCustomer(Customer customer){
		if(total >= customers.length){
			return false;
		}else{
//			customers[total] = customer;
//			total++;
			customers[total++] = customer;
			return true;
		}
	}
	/**
	 * @Description 修改指定索引位置上的客户信息
	 * @author atwlm 
	 * @date 2023年1月16日下午4:00:53 
	 * @param index
	 * @param cust
	 * @return true：修改成功    false：修改失败
	 */
	public boolean replaceCustomer(int index, Customer cust){
		if(index < 0 || index >= total){
			return false;
		}else{
			customers[index] = cust;
			return true;
		}
	}
	/**
	 * @Description 删除指定索引位置上的元素
	 * @author atwlm 
	 * @date 2023年1月16日下午4:10:28 
	 * @param index
	 * @return true：删除成功    false：删除失败
	 */
	public boolean deleteCustomer(int index){
		if(index < 0 || index >= total){
			return false;
		}else{
			for(int i = index;i < total - 1;i++){
				customers[i] = customers[i+1];
			}
			//最后有数据的元素需要置空
//			customers[total - 1] = null;
//			total--;
			customers[--total] = null;
			return true;
		}
	}
	/**
	 * @Description 获取客户所有的信息
	 * @author atwlm 
	 * @date 2023年1月16日下午4:21:14 
	 * @return
	 */
	public Customer[] getAllCustomers(){
		Customer[] custs = new Customer[total];
		for(int i = 0;i < custs.length;i++){
			custs[i] = customers[i];
		}
		return custs;
	}
	/**
	 * @Description 获取指定索引位置上的客户
	 * @author atwlm 
	 * @date 2023年1月16日下午4:57:25 
	 * @param index
	 * @return 如果找到了元素，则返回	  如果没有找到，则返回null
	 */
	public Customer getCustomer(int index){
		if(index < 0 || index >= total){
			return null;
		}else{
			return customers[index];
		}
	}
	/**
	 * @Description 获取存储的客户的数量
	 * @author atwlm 
	 * @date 2023年1月16日下午5:01:18 
	 * @return
	 */
	public int getTotal(){
		return total;
	}

}
