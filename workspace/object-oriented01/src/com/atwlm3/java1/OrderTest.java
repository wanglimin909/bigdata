package com.atwlm3.java1;

public class OrderTest {
	public static void main(String[] args) {
		Order order = new Order();
		
		order.orderDefault = 1;//同包不同类，可调。出了包就不能被调用了
		order.orderPublic = 2;
		
		//order.orderPrivate = 3;//出了Order类之后，私有的结构就不可以被调用了
		
		order.methodDefault();//同包不同类，可调。出了包就不能被调用了
		order.methodPublic();
		
		//order.methodPrivate();//出了Order类之后，私有的结构就不可以被调用了
	}
}
