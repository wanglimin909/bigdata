package com.atwlm3.java1;
/*
 * 面向对象的特征一：封装与隐藏
 * 一、问题的引入
 * 		当我们创建一个类的对象以后，我们可以通过“对象.属性”的方式，对对象的属性进行赋值。
 * 		这里，赋值操作要受到属性的数据类型和存储范围的制约，除此之外，没有其他制约条件
 * 		但是，在实际问题中，我们往往需要给属性的赋值加入额外的限制条件，这个条件就不能在
 * 		属性声明时体现，我们只能通过方法进行限制条件的添加。（比如：setLegs()）
 * 		同时，我们需要避免用户再使用“对象.属性”的方式对属性进行赋值，则需要将属性声明为
 * 		私有的(private)---此时，针对于属性就体现了封装性。
 * 二、封装性的体现
 *	我们将类的属性私有化(private),同时提供公共的(public)方法来获取(get..)和设置(set..)此属性的值
 * 	拓展：封装性的体现
 * 			不对外暴露的私有的方法
 * 			单例模式
 * 			...
 * 三、封装性的体现，需要权限修饰符来配合
 * 	1.Java规定的4种权限(从小到大排列)：private、缺省(default)、protected、public
 * 	2.4种权限可以用来修饰类及类的内部结构：属性、方法、构造器、内部类
 * 	3.具体的：4种权限都可以用来修饰类的内部结构：属性、方法、构造器、内部类
 * 		           修饰类的话，只能使用：缺省(default)、public
 * 总结封装性：Java提供了4种权限修饰符来修饰类及类的内部结构，体现类及类的内部结构在被调用时
 * 			    的可见性的大小
 * 
 */
public class AnimalTest {
	public static void main(String[] args) {
		Animal test = new Animal();
		test.name ="大黄";
		//test.age = 1;
		//test.legs = -4;（属性被封装，无法直接调用）
		
		test.show();
		
		test.setLegs(6);
		test.show();
		test.setLegs(-6);
		test.show();
		
		//System.out.println(test.legs);//此时无法查看该属性，则提供另外方法获取属性
		System.out.println(test.getLegs());//利用方法获取属性
	}
}

class Animal{
	String name;
	private int age;
	private int legs;//禁用直接调用，避免用户直接调用属性进行赋值（相当于封装该属性）
	
	//提供关于属性legs的get和set方法
	//对属性legs的设置（set）
	public void setLegs(int l){
		if(l >= 0 && l % 2 == 0){
			legs = l;
		}else{
			legs = 0;
			//或者抛出一个异常
		}
	}
	//对属性legs的获取（get）
	public int getLegs(){
		return legs;
	}
	
	//提供关于属性age的get和set方法
	//对属性age的获取（get）
	public int getAge(){
		return age;
	}
	//对属性legs的设置（set）
	public void setAge(int a){
		if(a >= 0){
			age = a;
		}else{
			age = -1;
		}
	}
	
	public void eat(){
		System.out.println("动物进食");
	}
	public void show(){
		System.out.println("name:" + name + "age:" + age + "legs:" + legs);
	}
	
}