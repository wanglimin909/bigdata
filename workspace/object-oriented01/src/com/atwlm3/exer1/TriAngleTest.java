package com.atwlm3.exer1;

public class TriAngleTest {
	public static void main(String[] args) {
		TriAngle t1 = new TriAngle();
		t1.setBase(2.0);
		t1.setHeight(2.4);
		System.out.println("base:"+t1.getBase()+",height"+t1.getHeight());
		
		double area1;
		area1 = t1.getBase() * t1.getHeight() / 2;
		System.out.println(area1);
		
		TriAngle t2 = new TriAngle(5.1, 5.6);
		System.out.println("base:"+t2.getBase()+",height"+t2.getHeight());
		
		
		double area2;
		area2 = t2.getBase() * t2.getHeight() / 2;
		System.out.println(area2);
	}
}
