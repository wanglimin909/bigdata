package com.atwlm3.exer1;

public class PersonTest {
	public static void main(String[] args) {
		Person a = new Person();
		System.out.println("年龄为：" + a.getAge());
		a.setAge(89);
		System.out.println("年龄为：" + a.getAge());
		
		Person b = new Person("Tom", 21);
		System.out.println("name:"+b.getName()+",age:"+b.getAge());
	}
}
