package com.atwlm3.exer1;
/*
 * 1.创建程序,在其中定义两个类：Person和PersonTest类。定义如下：
 * 用setAge()设置人的合法年龄(0~130),用getAge()返回人的年龄。
 * 在 PersonTest类中实例化Person类的对象 b,调用setAge()和getAge()方法,体会Java的封装性。
 * 2.在Person类中添加构造器，利用构造器设置所有人的age属性初始值都为18
 *   修改类和构造器，增加name属性,使得每次创建Person对象的同时初始化对象的age属性值和name属性值
 */
public class Person {
	private int age;
	private String name;
	
	public Person(){
		age = 18;
	}
	public Person(String n,int a){
		name = n;
		age = a;
	}
	
	public void setAge(int i){
		if(i>0 && i<130){
			age = i;
		}else{
			System.out.println("输入年龄不合理");
			//throw new RuntimeException("传入数据非法！");
		}
	}
	public int getAge(){
		return age;
	}
	public void setName(String n){
		name = n;
	}
	public String getName(){
		return name;
	}
	
	//set和get不要写成一个方法
	
}
