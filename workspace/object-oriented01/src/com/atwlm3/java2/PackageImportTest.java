package com.atwlm3.java2;

import java.lang.reflect.Field;
import java.text.FieldPosition;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.HashMap;
import java.util.*;

import com.atwlm3.exer2.Account;

import com.atwlm3.exer2.Bank;

import static java.lang.System.*;

import static java.lang.Math.*;

/*
 * 一、package关键字的使用
 * 	 1.为了更好的实现项目中类的管理，提供包的概念
 * 	 2.使用package声明类或接口所属的包，声明在源文件的首行
 * 	 3.包，属于标识符，遵循标识符的命名规则、规范(xxxyyyzzz)、“见名知意”
 * 	 4.每“.”一次，就代表一层文件目录
 * 	  补充：同一个包下，不能命名同名的接口、类
 * 		     不同的包下，可以命名同名的接口、类
 * 二、import关键字的使用
 * 	 1.在源文件中显式的使用import结构导入指定包下的类、接口
 * 	 2.声明在包的声明和类的声明之间
 * 	 3.如果需要导入多个结构，则并列的写出即可
 * 	 4.可以使用“ xxx.* ”的方式，表示可以导入xxx包下的所有结构
 * 	 5.如果使用的类或接口是java.lang包下定义(String、System)的，则可以省略import结构
 * 	 6.如果使用的类或接口是本包下定义的，则可以省略import结构
 * 	 7.如果在源文件中使用了不同包下同名的类，则至少有一个类需要以全类名的方式显示
 * 	 8.使用“ xxx.* ”方式表明可以调用xxx包下的所有结构。但是如果使用的是xxx子包下的结构，则任需导入
 * 
 * 	 9.import static：导入指定类或接口中的静态结构:属性或方法
 */
public class PackageImportTest {
	public static void main(String[] args) {
		
		String info = Arrays.toString(new int[]{1,2,3});
		
		Bank bank = new Bank();
		bank.getClass();
		
		ArrayList list = new ArrayList();
		HashMap map = new HashMap();
		Scanner s = null;
		
		System.out.println("hello");
		
		Person p = new Person();//同一个包下定义的类，不用import结构
		
		Account acct = new Account(1000);//import com.atwlm3.exer2.Account;
		//全类名的方式显示
		com.atwlm3.exer1.Account acct1 = new com.atwlm3.exer1.Account(1000,2000,0.123);
		
		Date date = new Date();//import java.util.*;
		java.sql.Date date1 = new java.sql.Date(536348);//全类名显示
		
		Field field = null;//import java.lang.reflect.Field;
		//lang包可以不用导，但是lang包下的子包还需导入显示
		
		out.println("hello");//import static java.lang.System.*;//属性
		
		long num = round(123.422);//import static java.lang.Math.*;//方法
		
	}
}
