package com.atwlm1.exer;
/*
 * 利用面向对象的编程方法，设计类Circle计算圆的面积
 */
public class Circle {
	//属性
	double radius;
	
	//求圆的面积
	//方法一
	public double findArea(){
		double area = Math.PI * radius * radius;
		return area; 
	}
	
	//方法二
	//public void findArea(){
	//	double area = Math.PI * radius * radius;
	//	System.out.println("面积为：" + area);
	//}
	
}
