package com.atwlm1.exer;
/*
 * 4. 对象数组题目
 * 		定义类Student，包含三个属性：学号number(int)，年级state(int)，成绩score(int)
 * 		创建20个学生对象，学号为1到20，年级和成绩都由随机数确定
 * 		问题一：打印出3年级(state值为3）的学生信息
 * 		问题二：使用冒泡排序按学生成绩排序，并遍历所有学生信息
 * 提示： 1) 生成随机数：Math.random()，返回值类型double;  
 * 		 2) 四舍五入取整：Math.round(double d)，返回值类型long
 * 
 * 此代码是对StudentTest.java的改进：将操作数组的功能分装到方法中
 * 
 */
public class StudentTest2 {
	public static void main(String[] args) {
		//声明Student类型的数组
		Student1[] stus = new Student1[20];
		
		for(int i = 0;i<stus.length;i++){
			//给数组元素赋值
			stus[i] = new Student1();
			//给Student对象的属性赋值
			//学号[1,20]
			stus[i].number = (i + 1);
			//年级[1,6]
			stus[i].state = (int)(Math.random() * (6 - 1 + 1) + 1);
			//成绩[0,100]
			stus[i].score = (int)(Math.random() * (100 - 0 + 1) + 0);
		}
	
		Student1 test = new Student1();
		test.print(stus);//遍历
		System.out.println();
		test.search(stus, 3);//查找三年级学生
		System.out.println();
		test.sort(stus);//排序
		test.print(stus);//遍历
	}
}
