package com.atwlm1.exer;

public class StudentTest1 {
	public static void main(String[] args) {
		//声明Student类型的数组
		Student[] stus = new Student[20];//对象数组
		for(int i = 0;i<stus.length;i++){
			//给数组元素赋值
			stus[i] = new Student();
			//给Student对象的属性赋值
			//学号[1,20]
			stus[i].number = (i + 1);
			//年级[1,6]
			stus[i].state = (int)(Math.random() * (6 - 1 + 1) + 1);
			//成绩[0,100]
			stus[i].score = (int)(Math.random() * (100 - 0 + 1) + 0);
		}
	//遍历学生数组
	for(int i = 0;i<stus.length;i++){
//		System.out.println(stus[i].number + "," + stus[i].state + "," + stus[i].score);
//	}
		System.out.println(stus[i].info());
	}
	
	System.out.println();
	
	//问题一：打印出3年级(state值为3）的学生信息
	for(int i = 0;i<stus.length;i++){
		if(stus[i].state == 3){
			System.out.println(stus[i].info());
		}
	}
	System.out.println();
	
	//问题二：使用冒泡排序按学生成绩排序，并遍历所有学生信息		
	for(int i = 0;i<stus.length;i++){
		for(int j = 0;j<stus.length - 1 - i;j++){
			if(stus[j].score > stus[j + 1].score){
				//如果需要换序，交换的是数组的元素，Student对象！！！
				Student temp = stus[j];
				stus[j] = stus[j+1];
				stus[j+1] = temp;
			}
		}
	}
	//遍历学生数组
		for(int i = 0;i<stus.length;i++){
			System.out.println(stus[i].info());
		}
		
	}
}
