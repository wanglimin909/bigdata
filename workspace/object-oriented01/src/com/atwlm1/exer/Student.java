package com.atwlm1.exer;
/*
 * 4. 对象数组题目
 * 		定义类Student，包含三个属性：学号number(int)，年级state(int)，成绩score(int)
 * 		创建20个学生对象，学号为1到20，年级和成绩都由随机数确定
 * 		问题一：打印出3年级(state值为3）的学生信息
 * 		问题二：使用冒泡排序按学生成绩排序，并遍历所有学生信息
 * 提示： 1) 生成随机数：Math.random()，返回值类型double;  
 * 		 2) 四舍五入取整：Math.round(double d)，返回值类型long
 */

//StudentTest.java
public class Student {
	int number;//学号
	int state;//年级
	int score;//成绩
	
	//显示学生信息的方法
	public String info(){
		return"学号：" + number + ",年级：" + state + ",成绩：" + score;
	}
}


//StudentTest2.java
class Student1 {
	int number;//学号
	int state;//年级
	int score;//成绩
	
	//显示学生信息的方法
		public String info(){
			return"学号：" + number + ",年级：" + state + ",成绩：" + score;
		}
	
	/**
	 * @Description 遍历Student1[]数组的操作
	 * @author atwlm 
	 * @date 2023年1月13日下午8:43:40 
	 * @param stus
	 */
	public void print(Student1[] stus){
		//遍历学生数组
		for(int i = 0;i<stus.length;i++){
			System.out.println(stus[i].info());
		}
	}
	/**
	 * @Description 查找Student数组中指定年级的学生信息
	 * @author atwlm 
	 * @date 2023年1月13日下午8:39:33 
	 * @param stus要查找的数组
	 * @param state要查找的年级
	 */
	public void search(Student1[] stus,int state){
		//问题一：打印出某年级的学生信息
		for(int i = 0;i<stus.length;i++){
			if(stus[i].state == state){
				System.out.println(stus[i].info());
			}
		}
	}
	/**
	 * @Description 给Student1数组排序
	 * @author atwlm 
	 * @date 2023年1月13日下午8:42:45 
	 * @param stus
	 */
	public void sort(Student1[] stus){
		for(int i = 0;i<stus.length;i++){
			for(int j = 0;j<stus.length - 1 - i;j++){
				if(stus[j].score > stus[j + 1].score){
					//如果需要换序，交换的是数组的元素，Student对象！！！
					Student1 temp = stus[j];
					stus[j] = stus[j+1];
					stus[j+1] = temp;
				}
			}
		}
	}
	
}
