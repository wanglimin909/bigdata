package com.atwlm1.java;
/*
 * 类中方法的声明和使用
 * 
 * 方法：描述类应该具有的功能
 * 比如：Math类：sqrt()\random()\...
 * 		Scanner类：nextXxx()...
 * 		Arrays类：sort()\binarySearch()\toString()\equals()\...
 * 1.举例：
 * 		无返回值
 * 			无形参public void eat(){}
 * 			有形参public void sleep(int hour){}
 * 		有返回值
 * 			无形参public String getName(){}
 * 			有形参public String getNation(String nation){}
 * 2.方法的声明：权限修饰符  返回值类型  方法名(形参列表){
 * 					方法体
 * 				}
 * 		注意：static、final、abstract来修饰的方法（后面会讲）
 * 3.说明
 *		3.1权限修饰符（目前默认使用public）
 *			Java中常用的权限修饰符：private、public、缺省、protected --->封装性细说
 * 		3.2返回值类型：有返回值 vs 无返回值
 * 			3.2.1如果方法有返回值，则必须在方法声明时，指定返回值的类型
 *				      同时，方法中，需要使用return关键字来返回指定类型的变量或常量：“return 数据”
 * 				  如果方法没有返回值，则方法声明时，使用void来表示
 * 				      通常，没有返回值的方法中就不需要使用return
 * 				      但是如果使用的话，只能“return;”表示结束此方法的意思
 * 			3.2.2什么时候需要返回值？
 * 				 ①题目要求
 * 				 ②具体问题具体分析
 * 		3.3方法名：属于标识符，遵循标识符的规则和规范“见名知意”
 * 		3.4形参列表:方法可以声明0个、1个、或多个形参
 * 			3.4.1格式：数据类型1  形参1,数据类型2  形参2,...
 * 			3.4.2什么时候需要定义形参？
 * 				 ①题目要求
 * 				 ②具体问题具体分析
 * 		3.5方法体：方法功能的体现
 * 4.return关键字的使用:
 * 		4.1使用范围：使用在方法体中
 * 		4.2作用： ①结束方法
 * 			    ②针对于有返回值类型的方法，使用“return 数据”方法返回所要的数据
 * 		4.3注意：return关键字后面不可以声明执行语句
 * 5.方法的使用：可以调用当前类的属性或方法
 * 				特别的：方法a中又调用了方法a：递归调用
 * 				方法中不可以定义方法
 */
public class CustomerTest {
	public static void main(String[] args){
		Customer cust1 = new Customer();
		cust1.sleep(8);
		
		
	}
}
	
//客户类
class Customer{
	//属性
	String name;
	int age;
	boolean isMail;
	//方法
	public void eat(){
		System.out.println("客户吃饭");
		return;
		//return后不可以声明表达式
		//System.out.println("hello");
				
	}
	public void sleep(int hour){
		System.out.println("休息了" + hour + "个小时");
		
		eat();//方法中可以调用方法
	}
	public String getName(){
		//return name;//变量
		//return "Tom";//常量
		if(age > 18){
			return name;//方法中可以调用属性
		}else{
			return "Tom";
		}
	}
	public String getNation(String nation){
		String info = "我的国籍是：" + nation;
		return info;
	}
}	
	
	
	

