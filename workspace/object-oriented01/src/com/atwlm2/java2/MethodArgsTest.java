package com.atwlm2.java2;
/*
 * 可变个数形参的方法
 * 允许直接定义能和多个实参相匹配的形参。从而，可以用一种更简单的方式，来传递个数可变的实参
 * 1.jdk5.0新增的内容
 * 2.具体使用：
 * 		2.1:可变个数形参的格式：数据类型...变量名
 * 		2.2:当调用可变个数形参的方法时，传入的参数个数可以是：0个，1个，多个
 * 		2.3:可变个数形参的方法与本类中方法名相同，形参不同的方法之间构成重载
 * 		2.4:可变个数形参的方法与本类中方法名相同，形参类型也相同的数组之间不构成重载（不能共存）
 * 		2.5:可变个数形参在方法的形参中，必须声明在末尾，且最多只能声明一个可变形参
 * 
 */
public class MethodArgsTest {
	public static void main(String[] args) {
		MethodArgsTest test = new MethodArgsTest();
		test.show();//3
		test.show("hello");//2
		test.show("hello","world");//3
		
//		test.show(new String[]{"AA","BB","CC"});//4
	}
	
	public void show(int i){
		System.out.println("1");
	}
	public void show(String s){
		System.out.println("2");
	}
	public void show(String... strs){
		System.out.println("3");
		for(int i = 0;i<strs.length;i++){
			System.out.println(strs[i]);
		}
	}
	//与3不能共存
//	public void show(String[] strs){
//		System.out.println("4");
//	}
	public void show(int i,String... strs){
	
	}
	//报错The variable argument type String of the method show must be the last parameter
//	public void show(String... strs,int i){
//		
//	}
	
	
	
}
