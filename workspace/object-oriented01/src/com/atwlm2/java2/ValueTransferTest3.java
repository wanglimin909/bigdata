package com.atwlm2.java2;

public class ValueTransferTest3 {
	public static void main(String[] args) {
		Data data = new Data();
		data.m = 10;
		data.n = 20;
		System.out.println("m:"+ data.m + ",n:" + data.n);//m:10,n:20
		//交换m和n的值
//		int temp = data.m;
//		data.m = data.n;
//		data.n = temp;
//		System.out.println("m:"+ data.m + ",n:" + data.n);//m:20,n:10
		
		ValueTransferTest3 test = new ValueTransferTest3();
		test.swap(data);
		System.out.println("m:"+ data.m + ",n:" + data.n);//m:20,n:10
	}
	
	public void swap(Data data){
		int temp = data.m;
		data.m = data.n;
		data.n = temp;
	}
	
}
class Data{
	int m;
	int n;
	
}