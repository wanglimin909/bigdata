package com.atwlm2.java2;
/*
 * 递归方法的使用（了解）
 * 1.递归方法：一个方法体内调用它自身
 * 		方法递归包含了一种隐式的循环，它会重复执行某段代码，但这种重复执行无须循环控制。
 * 		递归一定要向已知方向递归，否则这种递归就变成了无穷递归，类似于死循环。
 * 
 */
public class RecursionTest {
	public static void main(String[] args) {
		//例一：计算1-100之间所有自然数的和
		//方法一
		int sum = 0;
		for(int i = 1;i<=100;i++){
			sum += i;
		}
		System.out.println(sum);//5050
		//方法二
		RecursionTest test1 = new RecursionTest();
		System.out.println(test1.getSum(100));//5050
		//例二：计算1-100之间所有自然数的乘积
		RecursionTest test2 = new RecursionTest();
		System.out.println(test2.getSum1(10));//3628800
		//例三：已知有一个数列：f(0) = 1,f(1) = 4,f(n+2)=2*f(n+1) + f(n),
		//其中n是大于0的整数，求f(10)的值
		RecursionTest test3 = new RecursionTest();
		System.out.println(test3.f(10));//10497
		//例四：斐波那契数列
		//输入一个数据n，计算斐波那契数列(Fibonacci)的第n个值
		//1 1 2 3 5 8 13 21 34 55
		//规律：一个数等于前两个数之和
		//要求：计算斐波那契数列(Fibonacci)的第n个值，并将整个数列打印出来
		//例五：汉诺塔问题
		//例六：快排
	}
	//例一：计算1-n之间所有自然数的和
	public int getSum(int n){
		if(n == 1){
			return 1;
		}else{
			return n + getSum(n - 1);
		}
	}
	//例二：计算1-n之间所有自然数的乘积(n!)
	public int getSum1(int n){
		if(n == 1){
			return 1;
		}else {
			return n * getSum1(n - 1);
		}
	}
	//例三：计算f(n)
	public int f(int n){
		if(n == 0){
			return 1;
		}else if(n == 1){
			return 4;
		}else{
			return 2 * f(n - 1) + f(n - 2);
		}
	}
	
}
