/*
do-while循环
一、循环结构的4个要素
				①初始化条件;
				②循环条件;--->是boolean类型
				③循环体;
				④迭代条件；
二、do-while循环结构

①
do{
	③;
	④;
}while(②);

执行过程：①-③-④-②-③-④-②-③-④-...②

说明：
do-while循环至少执行一次循环体。
开发中，基本使用for和while更多，较少使用do-while
*/
class DoWhileTest {
	public static void main(String[] args) {
		
		//遍历100以内的偶数,并计算所有偶数的和即偶数的个数
		int num = 1;
		int sum = 0;//记录总和
		int count = 0;//记录个数
		do{
			if(num % 2 == 0){
			System.out.println(num);
			sum += num;
			count++;
			}
			num++;		
		}while(num <= 100);
		System.out.println("所有偶数的和为：" + sum);
		System.out.println("所有偶数的个数为：" + count);

		//*******************体会do-while至少执行一次*************
		int number1 = 10;
		while(number1 > 10){
			System.out.println("hello:while");
			number1--;
		}

		
		int number2 = 10;
		do{
			System.out.println("hello:do-while");
			number2--;		
		}while(number2 > 10);
		
	}
}
