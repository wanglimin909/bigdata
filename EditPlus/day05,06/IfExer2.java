/*
练习
	假设你想开发一个玩彩票的游戏，程序随机地产生一个两位数的彩票，提示用户输入一个两位数，然后按照下面的规则判定用户是否能赢。
		1)如果用户输入的数匹配彩票的实际顺序，奖金10 000美元。
		2)如果用户输入的所有数字匹配彩票的所有数字，但顺序不一致，奖金 3 000美元。
		3)如果用户输入的一个数字仅满足顺序情况下匹配彩票的一个数字，奖金1 000美元。
		4)如果用户输入的一个数字仅满足非顺序情况下匹配彩票的一个数字，奖金500美元。
		5)如果用户输入的数字没有匹配任何一个数字，则彩票作废。
		提示：使用(int)(Math.random() * 90 + 10)产生随机数。
		Math.random() : [0,1) * 90 ?[0,90) + 10 ?[10,100) ? [10,99]
注意
	如何获取一个随机数：10 - 99
	double value = Math.random();  //[0.0,1.0)
	double value = Math.random(); * 90 + 10;  //[10.0,100.0)
	int value = (int)(Math.random() * 90 + 10);  //[10,99]

	公式：[a,b] : (int)(Math.random() * (b - a + 1) + a);

*/

import java.util.Scanner;
class IfExer2 
{
	public static void main(String[] args) 
	{
		//1、随机产生一个两位数
		//System.out.println(Math.random());//产生[0,1)
		int number = (int)(Math.random()*90 + 10);//得到[10,99]，即[10,100)
		//System.out.println(number);
		
		int numberShi = number/10;
		int numberGe = number%10;
		
		//2、用户输入一个两位数
		Scanner input = new Scanner(System.in);
		System.out.print("请输入一个两位数：");
		int guess = input.nextInt();
		
		int guessShi = guess/10;
		int guessGe = guess%10;
		
		if(number == guess){
			System.out.println("奖金10 000美元");
		}else if(numberShi == guessGe && numberGe == guessShi){
			System.out.println("奖金3 000美元");
		}else if(numberShi==guessShi || numberGe == guessGe){
			System.out.println("奖金1 000美元");
		}else if(numberShi==guessGe || numberGe == guessShi){
			System.out.println("奖金500美元");
		}else{
			System.out.println("没中奖");
		}
		
		System.out.println("中奖号码是：" + number);

	}
}
