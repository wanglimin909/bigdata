/*
对PrimeNumberTest.java文件中质数输出问题的优化

	100000以内的所有质数
	质数（素数）：只能被1和它本身整除的自然数
				  -->从2开始，到n-1结束为止，都不能被这个数本身整除
	最小的质数是2
*/
class PrimeNumberTest1 {
	public static void main(String[] args) {		
		
		boolean isFlag = true;		//标识i是否被j除尽，一旦除尽，修改其值
		
		int count = 0;//记录质数的个数

		//获取当前时间的毫秒数，距离1970-01-01 00:00:00到现在的毫秒数
		long start = System.currentTimeMillis();

		for(int i = 2;i <= 100000;i++){       //遍历100000以内的自然数
			
			//优化2:改成j<根号i(对本身是质数的自然数更有效)
			for(int j = 2;j <= Math.sqrt(i);j++){//for(int j = 2;j < i;j++){     //j:被i去除
					
				if(i % j == 0){		//i被j除尽
				isFlag = false;
				break;		//优化1：只对本身非质数的自然数是有效的
				}			
			}
			if (isFlag == true){
				//System.out.println(i);
				count++;
			}
			isFlag = true;	//重制isFlag
		}


		//获取当前时间的毫秒数，距离1970-01-01 00:00:00到现在的毫秒数
		long end = System.currentTimeMillis();
		System.out.println("质数的个数为：" + count);
		System.out.println("所花费的时间为：" + (end - start));//优化之前:8168(约8秒)
															   //优化1之后:988(约1秒)
															   //优化2之后:405
															   //优化1,2后:298
															   //优化1,2,3后:0
	}
}

