/*
对PrimeNumberTest.java文件中质数输出问题的优化

	100000以内的所有质数
	质数（素数）：只能被1和它本身整除的自然数
				  -->从2开始，到n-1结束为止，都不能被这个数本身整除
	最小的质数是2
*/
class PrimeNumberTest2 {
	public static void main(String[] args) {		
				
		int count = 0;//记录质数的个数

		lable:for(int i = 2;i <= 100000;i++){       //遍历100000以内的自然数
						
			for(int j = 2;j <= Math.sqrt(i);j++){
					
				if(i % j == 0){		//i被j除尽
				continue lable;
				}			
			}	
			//执行到此步骤的都是质数
			count++;
		}
		System.out.println("质数的个数：" + count);
	}
}


