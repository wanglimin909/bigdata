/*
对学生成绩大于60分的，输出“合格”。低于60分的，输出“不合格”
说明：如果switch-case语句中多个相同语句，可以进行合并。
*/
import java.util.Scanner;
class SwitchCaseExer2 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int score = scan.nextInt();

		/*方案一
		switch(score / 10){
		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
			...
		case 59:
			System.out.println("不合格");
			break;
		case 60:
		case 61:
		case 61:
		case 63:
			...
		case 100:
			System.out.println("合格");
			break;
		}
		*/

		//方法2
		switch(score / 10){
		case 0:
			System.out.println("不及格");
			break;
		case 1:
			System.out.println("不及格");
			break;
		case 2:
			System.out.println("不及格");
			break;
		case 3:
			System.out.println("不及格");
			break;
		case 4:
			System.out.println("不及格");
			break;
		case 5:
			System.out.println("不及格");
			break;
		case 6:
			System.out.println("及格");
			break;
		case 7:
			System.out.println("及格");
			break;
		case 8:
			System.out.println("及格");
			break;
		case 9:
			System.out.println("及格");
			break;
		case 10:
			System.out.println("及格");
			break;		
		}
		//方法二也可以合并
		switch(score / 10){
		case 0:
		case 1:			
		case 2:			
		case 3:			
		case 4:
		case 5:
			System.out.println("不及格");
			break;
		case 6:			
		case 7:			
		case 8:			
		case 9:			
		case 10:
			System.out.println("及格");
			break;		
		}
		//方法3
		//更优的解法
		switch(score / 60){
		case 0:
			System.out.println("不及格");
			break;
		case 1:
			System.out.println("合格");
			break;
		}

	}
}
