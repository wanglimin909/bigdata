/*
For循环结构的使用
	一、循环结构的4个要素
		①初始化条件;
		②循环条件;--->是boolean类型
		③循环体;
		④迭代条件；
	二、for循环的结构
			for(初始化条件;循环条件;迭代条件){
				循环体
			}
执行过程：①-②-③-④-②-③-④-②-③-④-.....-②


说明：
②循环条件部分为boolean类型表达式，当值为false时，退出循环
①初始化部分可以声明多个变量，但必须是同一个类型，用逗号分隔
④可以有多个变量更新，用逗号分隔

*/

class ForTest  {
	public static void main(String[] args) {
		for(int i = 1;i <= 5;i++){
			System.out.println("HelloWorld!");
		}
		
	//练习
	int num = 1;
	for(System.out.print('a');num <= 3;System.out.print('c'),num++){
		System.out.print('b');
	}
	//输出结果：abcbcbc

	//例题：遍历100以内的偶数,输出所有偶数的和,输出偶数的个数
	int sum = 0;//接收所有偶数的和
	int count = 0;//接收偶数的个数
	for(int i = 1;i <= 100;i++){
		if(i % 2 == 0){
			System.out.println(i);
			sum += i;
			count++;
		}
	}
	System.out.println("总和为：" + sum);
	System.out.println("偶数个数为：" + count);





	}
}