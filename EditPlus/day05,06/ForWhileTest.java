/*
循环语句综合例题

	从键盘读入个数不确定的整数，并判断读入的正数和负数的个数，输入为0时结束程序

*/

import java.util.Scanner;

class ForWhileTest {
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		int positiveNumber = 0;//记录正数的个数
		int negativeNumber = 0;//记录负数的个数
		
		while(true){    //for(;;){
			int number = scan.nextInt();
			
			//判断number的正负情况
			if(number > 0){
			positiveNumber++;
			}else if(number < 0){
			negativeNumber++;
			}else{
				//一旦执行break，就跳出循环
				break;
			}			
		}
		System.out.println("正数的个数：" + positiveNumber);
		System.out.println("负数的个数：" + negativeNumber);



	}
}
