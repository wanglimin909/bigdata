/*
While循环的使用
一、循环结构的4个要素
		①初始化条件;
		②循环条件;--->是boolean类型
		③循环体;
		④迭代条件；
二、while循环的结构
		①
		while(②){
			③;
			④;
		}
	注意：
		不要忘记声明④迭代部分。否则，循环将不能结束，变成死循环。 
?		for循环和while循环可以相互转换
*/

class WhileTest{
	public static void main(String[] args) {
		
		//遍历100以内的所有偶数
		/*int i = 1;
		while (i <= 100)
		{
			if(i % 2 == 0){
				System.out.println(i);
				}
			i++;
		}
		//出了whlie循环以后，仍可以调用
		System.out.println(i);//101
		*/
		int result = 0;
        int i= 1;
        while(i<= 100) {
            result += i;
            i++;
        }
        System.out.println("result="+ result);
	
	}
}
