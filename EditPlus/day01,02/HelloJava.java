class HelloJava{
	/*
	多行注释：
	如下的main方法是程序的入口！
	main的格式是固定的！
	*/
	public static void main(String[] args) {
	//单行注释：如下的语句表示输出到控制台！
		System.out.println("Hello World!");
	}
}
