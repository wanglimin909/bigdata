/*
基本数据类型之间的运算规则：
前提：这里讨论只是7种基本数据类型变量间的运算，不包含boolean类型
	一、自动类型提升：
		结论：当容量小的数据类型变量与容量大的数据类型变量做运算时，结果自动提升为容量大的数据类型
		      byte、char、short --> int --> long --> float --> double
			  特别的：当byte、char、short三种类型的变量做运算时，结果为int型
说明：此时的容量大小指的是，表示数的范围的大和小（比如：float容量要大于long的容量）
*/
class VariableTest2 {
	public static void main(String[] args) {
		byte b1 = 2;
		int i1 = 12;
		//编译不通过
		//byte b2 = b1 + i1;
		int i2 = b1 + i1;
		System.out.println(i2);
		//long l1 = b1 + i1;
		//System.out.println(l1);
		float f1 = b1 + i1;
		System.out.println(f1);
		//short s1 = 123;
		//double d1 = s1;
		//System.out.println(d1);

		//*********************************************
		char c1 = 'a';//97
		int i3 = 10;
		int i4 = c1 + i3;
		System.out.println(i4);
		//char --> int

		char c2 = 'a';//97
		short s2 = 10;
		//short s3 = c2 + s2;//编译不通过
		//System.out.println(s3);//编译不通过
		//不能证明char --> short
		char c3 = 'a';//97
		short s4 = 10;
		//char c4 = c3 + s4;//编译不通过
		//System.out.println(c4);//编译不通过
		//不能证明short --> char

		char c4 = 'a';//97
		byte b2 = 10 ;
		//char c5 = c4 + b2;//编译不通过
		short s5 = 10;
		byte b3 = 15;
		//short s6 = s5 + b3;//编译不通过

		//short s7 = b2 + b3;//编译不通过
		//char c5 = b2 + b3;//编译不通过
		//*********************************************
		




	
		



		
	}
}
