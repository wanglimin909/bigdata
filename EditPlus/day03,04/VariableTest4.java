class VariableTest4 {
	public static void main(String[] args) {
//1.编码情况1
	long l1 = 123213;//{没加L}编译成功，可理解为long的值附给int
	System.out.println(l1);

	//long l2 = 479274297429374; //{没加L}编译失败，容量超过int容量的范围(过大的整数)，无法自动提升为int，故出错
	//System.out.println(l2);    //即不加L，用int(4个字节)去存。加上L，用long(8个字节)去存

	long l2 = 479274297429374L;//编译成功
	System.out.println(l2);

	//float f1 = 12.3;  //{没加F}编译失败
	float f1 = (float)12.3;   //(用了一下强制转换编译成功，还不如直接加个F)
//*************************************************************

//2.编码情况2
	//整型常量，默认类型为int型
	//浮点型常量，默认类型为double型
	byte b1 = 12;
	//byte b2 = b1 + 1;  //编译失败

	//float f1 = b1 + 12.3;  //编译失败
	}
}
