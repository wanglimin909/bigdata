/*
String类型变量的使用
	1.Sring属于引用数据类型，翻译为：字符串
	2.声明String类型变量时，使用一对“”
	3.String可以和8种基本数据类型变量做运算，且运算只能是连接运算：+
	4.运算的结果仍然是Spring类型
*/
class StringTest{
	public static void main(String[] args) {
		System.out.println("HelloWorld");

		String s1 = "HelloWorld";
		System.out.println(s1);

		String s2 = "a";
		String s3 = "";

		//char c1 = '';//编译错误

		//*************************************

		int number = 1001;
		String numberStr = "学号：";
		String info1 = numberStr + number;  // +:连接运算
		boolean b1 = true;
		System.out.println(info1);
		
		String info2= info1 + b1;  // +:连接运算
		System.out.println(info2); 

		//*************************************

		//练习1
		char c1 = 'a';//97  A:65
		int num = 10;
		String str = "hello";
		System.out.println(c1 + num + str);//107hello
		System.out.println(c1 + str + num);//ahello10
		System.out.println(c1 + (num + str));//a10hello
		System.out.println((c1 + num) + str);//107hello
		System.out.println(str + num + c1);//hello10a

		//练习2
		//*	*
		System.out.println("*	*");//*	*
		System.out.println('*' + '\t' +'*');//93
		System.out.println('*' + "\t" +'*');//*	*
		System.out.println('*' + '\t' +"*");//51*
		System.out.println('*' + ('\t' +"*"));//* *

		//*************************************************

		//String str1 = 123;  //编译不通过
		String str1 = 123 + "";
		System.out.println(str1); //"123"(String型)

		//int num1 = str1;	//编译不通过
		//int num1 = (int)str1;	 //编译不通过

		int num1 = Integer.parseInt(str1);
		System.out.println(num1);//123(int型)   //String型返回int型，不用记，作为了解
	}
}
