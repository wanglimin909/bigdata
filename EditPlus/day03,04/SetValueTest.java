/*
赋值运算符
=  +=  -=  *=  /=  %=
*/
class SetValueTest {
	public static void main(String[] args) {
		//赋值符号：=
		int i1 = 10;
		int j1 = 10;
		
		int i2,j2;
		//连续赋值
		i2 = j2 = 10;

		int i3 = 10,j3 = 20;

		//****************************
		int num1 = 10;
		num1 += 2;//num1 = num1 + 2;
		System.out.println(num1);//12

		int num2 = 12;
		num2 %= 5;//num2 = num2 % 5;
		System.out.println(num2);//2

		short s1 = 10;
		//s1 = s1 + 2;//编译失败
		s1 += 2;//不会改变本身的数据类型
		System.out.println(s1);//12

		//开发中，如果希望变量实现+1的操作，有几种方法(前提：int num = 10;)
		//方法一：num = num + 1;
		//方法二：num +=1;(增加其他数值时推荐)
		//方法三：num++;(++num;){只能自增1}{增1时推荐}

		//练习
		int i = 1;
		i *= 0.1;
		System.out.println(i);//0
		i++;
		System.out.println(i);//1

		int m = 2;
		int n = 3;
		n *= m++; //n = n * m++;
		System.out.println("m=" + m);//m = 3
		System.out.println("n=" + n);//n = 6

		int n1 = 10;
		n1 += (n1++) + (++n1);//n1 = n1 + (n1++) + (++n1);{n1 = n1[10] + (n1++)[10] + (++n1)[12]}
		System.out.println(n1);//32

	}
}
