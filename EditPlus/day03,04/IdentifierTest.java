/*
标识符的使用
1.标识符：凡是可以自己起名字的地方都叫标识符
	比如：类名，变量名，方法名，接口名，包名...
2.标识符的命名规则
	由26个英文字母大小写，0-9 ，_或 $ 组成?
	数字不可以开头?
	不可以使用关键字和保留字，但能包含关键字和保留字?
	Java中严格区分大小写，长度无限制?
	标识符不能包含空格。
*/
class IdentifierTest{
 
	public static void main(String[] args){

			//int myNumber = 1001;
			//System.out.println(myNumber);
			int mynumber = 1002;
			System.out.println(mynumber);
 
 }

}

