/*
逻辑运算符
& && | || ！ ^

说明：
	逻辑与运算符操作的都是boolean类型的变量

*/
class LogicTest{
	public static void main(String[] args){
		//区分& 与 &&
		//相同点1：& 与 && 的运算结果都相同
		//相同点2：当符号左边是true时，二者都会执行符号右边的运算
		//不同点：当符号左边是false时，&继续执行符号,&&在不执行运算
		//开发中，推荐使用&&
		boolean b1 = true;
		b1 = false;
		int num1 = 10;
		if(b1 & (num1++ > 0)){
			System.out.println("我现在在南京");
		}else{
			System.out.println("我现在在北京");
		}
		System.out.println("num1 = " + num1);

		boolean b2 = true;
		b2 = false;
		int num2 = 10;
		if(b2 && (num2++ > 0)){
			System.out.println("我现在在南京");
		}else{
			System.out.println("我现在在北京");
		}
		System.out.println("num2 = " + num2);

		//区分：| 与 ||
		//相同点1：| 与 || 的运算结果都相同
		//相同点2：当符号左边是false时，二者都会执行符号右边的运算
		//不同点3：当符号左边是true时，|继续执行符号右边的运算，而||不再执行符号右边的运算
		//开发中，推荐使用||
		boolean b3 = false;
		int num3 = 10;
		b3 = true;
		if(b3 | (num3++ > 0)){
			System.out.println("我现在在南京");
		}else{
			System.out.println("我现在在北京");
		}
		System.out.println("num3 = " + num3);

		boolean b4 = false;
		b4 = true;
		int num4 = 10;
		if(b4 || (num4++ > 0)){
			System.out.println("我现在在南京");
		}else{
			System.out.println("我现在在北京");
		}
		System.out.println("num4 = " + num4);


		//练习
		int x1 = 1,y1 = 1;if(x1++==2 && ++y1==2){
			x1 =7;
		}System.out.println("x1="+x1+",y1="+y1);//x1 = 2,y1 = 1

		int x2 = 1,y2 = 1;if(x2++==1 | ++y2==1){
			x2 =7;
		}System.out.println("x2="+x2+",y2="+y2);//x2 = 7,y2 = 2

		//面试题
		boolean x=true; 
		boolean y=false;
		short z=42;
		// if(y == true)  //加上该语句输出z = 43
			if((z++==42)&&(y=true))
				z++;
		 if((x=false) || (++z==45))
				z++;
		System.out.println("z="+z);//z = 46
	
	
	}
}
