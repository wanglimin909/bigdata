package com.atwlm;

import org.junit.Test;

/**
 * ClassName: HelloMavenTest
 * Package: com.atwlm
 * Description:
 *
 * @Author wlm.java
 * @Create 2023/7/20 - 16:28
 * @Version: v1.0
 */
public class HelloMavenTest {
    @Test
    public void helloMavenTest(){
        HelloMaven helloMaven = new HelloMaven();
        System.out.println(helloMaven.show("你好，Maven"));
    }
}
