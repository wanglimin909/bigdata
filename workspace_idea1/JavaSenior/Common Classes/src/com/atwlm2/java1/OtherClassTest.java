package com.atwlm2.java1;

import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * 其他常用类的使用
 * 1.System
 * 2.Math
 * 3.BigInteger 和 BigDecimal
 *
 * @author wlm.java
 * @create 2023-04-05 17:28
 */
public class OtherClassTest {
    /*
    System类内部包含in、out和err三个成员变量，
    分别代表标准输入流(键盘输入)，标准输出流(显示器)和标准错误输出流(显示器)
    native long currentTimeMillis()
        该方法的作用是返回当前的计算机时间
    void exit(int status)
        该方法的作用是退出程序,使用该方法可以在图形界面编程中实现程序的退出功能
    void gc()
        该方法的作用是请求系统进行垃圾回收
    String getProperty(String key)
        该方法的作用是获得系统中属性名为key的属性对应的值
     */
    @Test
    public void tets1(){
        String javaVersion= System.getProperty("java.version");
        System.out.println("java的version:"+ javaVersion);

        String javaHome= System.getProperty("java.home");
        System.out.println("java的home:"+ javaHome);

        String osName= System.getProperty("os.name");
        System.out.println("os的name:"+ osName);

        String osVersion= System.getProperty("os.version");
        System.out.println("os的version:"+ osVersion);

        String userName= System.getProperty("user.name");
        System.out.println("user的name:"+ userName);

        String userHome= System.getProperty("user.home");
        System.out.println("user的home:"+ userHome);

        String userDir= System.getProperty("user.dir");
        System.out.println("user的dir:"+ userDir);
    }

    /*
    abs     绝对值
    acos,asin,atan,cos,sin,tan  三角函数
    sqrt     平方根
    pow(double a,doble b)     a的b次幂
    log    自然对数
    exp    e为底指数
    max(double a,double b)
    min(double a,double b)
    random()      返回0.0到1.0的随机数
    long round(double a)     double型数据a转换为long型（四舍五入）
    toDegrees(double angrad)     弧度—>角度
    toRadians(double angdeg)     角度—>弧度
     */

    /*
    java.math包的BigInteger可以表示不可变的任意精度的整数。
    BigInteger 提供所有 Java 的基本整数操作符的对应物，并提供 java.lang.Math 的所有相关方法。
    另外，BigInteger 还提供以下运算：模算术、GCD 计算、质数测试、素数生成、位操作以及一些其他操作

    一般的Float类和Double类可以用来做科学计算或工程计算，但在商业计算中，要求数字精度比较高，
    故用到java.math.BigDecimal类。 BigDecimal类支持不可变的、任意精度的有符号十进制定点数
     */
    @Test
    public void test3(){
            BigInteger bi= new BigInteger("12433241123");
            BigDecimal bd= new BigDecimal("12435.351");
            BigDecimal bd2= new BigDecimal("11");
            System.out.println(bi);
            // System.out.println(bd.divide(bd2));
            System.out.println(bd.divide(bd2, BigDecimal.ROUND_HALF_UP));
            System.out.println(bd.divide(bd2, 15, BigDecimal.ROUND_HALF_UP));

    }
}
