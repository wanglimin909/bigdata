package com.atwlm2.java1;

import org.junit.Test;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.TemporalAccessor;
import java.util.Date;

/**
 *
 * jdk 8 中时间日期API的测试
 *
     java.time–包含值对象的基础包
     java.time.chrono–提供对不同的日历系统的访问
     java.time.format–格式化和解析时间和日期
     java.time.temporal–包括底层框架和扩展特性
     java.time.zone–包含时区支持的类
 *
 * @author wlm.java
 * @create 2023-04-03 19:49
 */
public class JDK8DateTimeTest {

    @Test
    public void testDate(){
        //偏移量
        Date date1 = new Date(2023,4,3);
        System.out.println(date1);//Thu May 03 00:00:00 CST 3923
        Date date2 = new Date(2023-1900,4-1,3);
        System.out.println(date2);//Mon Apr 03 00:00:00 CST 2023
    }

    /*
    LocalDate、LocalTime、LocalDateTime的使用
    说明：
        1.LocalDateTime相较于LocalDate、LocalTime,使用频率要高
     */
    @Test
    public void test1(){

        //now():获取当前的日期、时间、日期+时间
        LocalDate localDate = LocalDate.now();
        LocalTime localTime = LocalTime.now();
        LocalDateTime localDateTime = LocalDateTime.now();

        System.out.println(localDate);//2023-04-03
        System.out.println(localTime);//20:05:51.991
        System.out.println(localDateTime);//2023-04-03T20:05:51.991

        //of():设置指定的年、月、日、时、分、秒。没有偏移量
        LocalDateTime localDateTime1 = LocalDateTime.of(2023, 4, 3, 20, 34, 55);
        System.out.println(localDateTime1);//2023-04-03T20:34:55

        //getXxx()：获取相关的属性
        System.out.println(localDateTime.getDayOfMonth());
        System.out.println(localDateTime.getDayOfWeek());
        System.out.println(localDateTime.getMonth());
        System.out.println(localDateTime.getMonthValue());
        System.out.println(localDateTime.getMinute());

        //体现不可变性
        //withXxx():设置相关的属性
        LocalDateTime localDateTime2 = localDateTime.withDayOfMonth(22);
        System.out.println(localDateTime);//2023-04-03T20:20:05.398(当前)
        System.out.println(localDateTime2);//2023-04-22T20:19:29.972

        LocalDateTime localDateTime3 = localDateTime.withHour(22);
        System.out.println(localDateTime);//2023-04-03T20:25:38.808
        System.out.println(localDateTime3);//2023-04-03T22:26:25.055

        //不可变性

        LocalDateTime localDateTime4 = localDateTime.plusMonths(3);
        System.out.println(localDateTime);//2023-04-03T20:29:34.998
        System.out.println(localDateTime4);//2023-07-03T20:29:34.998

        LocalDateTime localDateTime5 = localDateTime.minusDays(2);
        System.out.println(localDateTime);//2023-04-03T20:31:34.003
        System.out.println(localDateTime5);//2023-04-01T20:31:34.003

    }

    /*
    Instant的使用：
        类似于java.util.Date类

     */
    @Test
    public void test2(){
        //now():获取本初子午线对应的标准时间
        Instant instant = Instant.now();
        System.out.println(instant);//2023-04-03T12:37:15.534Z(本初子午线的那个时间，和北京时间差8个小时)

        //添加时间的偏移量
        OffsetDateTime offsetDateTime = instant.atOffset(ZoneOffset.ofHours(8));
        System.out.println(offsetDateTime);//2023-04-03T20:41:36.459+08:00(北京时间)

        //toEpochMilli():获取自1970年1月1日0时0分0秒（UTC）开始的毫秒数 ---> Date类的getTime()
        long milli = instant.toEpochMilli();
        System.out.println(milli);//1680528600669

        //ofEpochMilli():通过给定的毫秒数获取Instant实例 ---> Date(Long millis)
        Instant instant1 = Instant.ofEpochMilli(1680528600669L);
        System.out.println(instant1);//2023-04-03T13:30:00.669Z

    }

    /*
    DateTimeFormatter:格式化或解析日期、时间
    类似于SimpleDateFormat

     */
    @Test
    public void test3(){
//        方式一：预定义的标准格式。如：ISO_LOCAL_DATE_TIME;ISO_LOCAL_DATE;ISO_LOCAL_TIME
        DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
        //格式化：日期 ---> 字符串
        LocalDateTime localDateTime = LocalDateTime.now();
        String str1 = formatter.format(localDateTime);
        System.out.println(localDateTime);//2023-04-05T14:35:38.659
        System.out.println(str1);//2023-04-05T14:35:38.659
        //解析：字符串 ---> 日期
        TemporalAccessor parse = formatter.parse("2023-04-05T14:35:38.659");
        System.out.println(parse);//{},ISO resolved to 2023-04-05T14:35:38.659

//        方式二：本地化相关的格式。
//          本地化相关的格式。如：ofLocalizedDateTime()
//          FormatStyle.LONG/FormatStyle.MEDIUM/FormatStyle.SHORT:适用于LocalDateTime
        DateTimeFormatter formatter1 = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG);
        //格式化
        String str2 = formatter1.format(localDateTime);
        System.out.println(str2);//2023年4月5日 下午02时49分16秒

//          本地化相关的格式。如：ofLocalizedDate()
//          FormatStyle.FULL/FormatStyle.LONG/FormatStyle.MEDIUM/FormatStyle.SHORT:适用于LocalDate
        DateTimeFormatter formatter2 = DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL);
        //格式化
        String str3 = formatter2.format(LocalDate.now());
        System.out.println(str3);//2023年4月5日 星期三

//        方式三：自定义的格式。如：ofPattern(“yyyy-MM-dd hh:mm:ss”)
        DateTimeFormatter formatter3 = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss");
        //格式化
        String str4 = formatter3.format(LocalDateTime.now());
        System.out.println(str4);//2023-04-05 02:56:44
        //解析
        TemporalAccessor accessor = formatter3.parse("2023-04-05 02:56:44");
        System.out.println(accessor);//{NanoOfSecond=0, MicroOfSecond=0, HourOfAmPm=2, MinuteOfHour=56, MilliOfSecond=0, SecondOfMinute=44},ISO resolved to 2023-04-05

    }

}
