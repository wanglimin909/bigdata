package com.atwlm1.exer1;

/**
 *
 * 面试题
 *
 * @author wlm.java
 * @create 2023-03-22 20:47
 */
public class StringDemo1 {

        String str = new String("good");

        char[] ch = { 't', 'e', 's', 't' };

        public void change(String str, char ch[]) {

            str = "test ok";//不可变性，只给地址值

            ch[0] = 'b';

        }

        public static void main(String[] args) {
            StringDemo1 ex = new StringDemo1();

            ex.change(ex.str, ex.ch);//str给了个地址值

            System.out.println(ex.str);//good

            System.out.println(ex.ch);//best
        }
}
