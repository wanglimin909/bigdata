package com.atwlm.practice;

import org.junit.Test;

import java.sql.SQLException;

/**
 *
 * 使用preparedStatement进行t_user的curd操作
 *
 * @author wlm.java
 * @create 2023-06-21 16:16
 */
public class PreparedStatementCURD extends BaseDao{
    /**
     * 增
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    @Test
    public void testInsert() throws ClassNotFoundException, SQLException {

        String sql = "INSERT INTO student(sid,sno,sname,ssex,sbirth,saddr,sclass)VALUES(?,?,?,?,?,?,?);";

        int rows = executeUpdate(sql, 5, "1006", "李四",1,"2021-09-08","兰州","大数据2班");
        System.out.println("rows = " + rows);

    }

    /**
     * 改
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    @Test
    public void testUpdate() throws ClassNotFoundException, SQLException {

        String sql = "UPDATE student SET sname = ? WHERE sid = ?";

        int rows = executeUpdate(sql, "王", 2);
        System.out.println("rows = " + rows);
    }

    /**
     * 删
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    @Test
    public void testDelete() throws ClassNotFoundException, SQLException {

        String sql = "DELETE FROM student WHERE sid = ? ";

        int rows = executeUpdate(sql, 1);
        System.out.println("rows = " + rows);
    }


    @Test
    public void testSelect() throws Exception {
        String sql = "select * FROM student";
    }

}
