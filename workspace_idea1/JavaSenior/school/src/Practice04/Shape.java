package Practice04;

/**
 *
 *3.定义形状抽象类Shape，以该抽象类为父类派生出圆形子类Circle和矩形子类Rectangle，求出具体形状的周长和面积。
 *
 * @author wlm.java
 * @create 2023-05-05 17:12
 */
public abstract class Shape {
     public abstract double perimeter();
     public abstract double area();

}

class Circle extends Shape{

     private double radius;

     public Circle(double radius){
          this.radius = radius;
     }

     @Override
     public double perimeter() {
          return 2 * Math.PI * radius;
     }

     @Override
     public double area() {
          return Math.PI * radius * radius;
     }
}

class rectangle extends Shape{

     private double length;
     private double wide;

     public rectangle(double length,double wide){
          this.length = length;
          this.wide = wide;
     }

     @Override
     public double perimeter() {
          return 2 * length * wide;
     }

     @Override
     public double area() {
          return length * wide;
     }
}

class Test{
     public static void main(String[] args) {
          Circle c1 = new Circle(3);
          System.out.println("圆周长："+c1.perimeter()+"面积："+c1.area());
          rectangle r1 = new rectangle(4,5);
          System.out.println("矩形周长："+r1.perimeter()+"面积："+r1.area());
     }
}