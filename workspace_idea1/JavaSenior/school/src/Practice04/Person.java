package Practice04;

/**
 *
 * 2.创建个人类Person，再以该类为父类创建一个学生子类Student，实现子类对象调用父类的方法和子类中的方法覆盖父类的方法。
 *
 * @author wlm.java
 * @create 2023-05-05 16:57
 */
public class Person {
    String name;
    int age;
    public Person(){}
    public Person(String name,int age){
        this.name = name;
        this.age = age;
    }
    public void eat(){
        System.out.println("人吃饭");
    }
    public void sleep(){
        System.out.println("人走路");
    }
}

class Student extends Person{
    int id;
    public Student (String name,int age,int id){
        super(name,age);
        this.id = id;
    }
    @Override
    public void eat(){
        System.out.println("学生吃饭");
    }
}

class StudentTest{
    public static void main(String[] args) {
        Student s1 = new Student("Tom", 19, 1002);
        s1.sleep();//子类对象调用父类方法
        s1.eat();//子类对象调用重写的父类的方法

    }
}

