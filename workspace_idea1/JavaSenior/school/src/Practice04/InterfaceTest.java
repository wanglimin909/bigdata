package Practice04;

/**
 *
 * 4.自定义一个接口和类，并利用接口实现类的多重继承。
 *
 * @author wlm.java
 * @create 2023-05-05 17:34
 */

interface animals{
    void eat();
}

class dog implements animals{

    @Override
    public void eat() {
        System.out.println("狗吃饭");
    }
}

interface car{
    void run();
}

class aodi implements car{

    @Override
    public void run() {
        System.out.println("汽车跑");
    }
}

class dogcar implements animals,car{

    @Override
    public void eat() {
        System.out.println("狗在汽车上吃饭");
    }

    @Override
    public void run() {
        System.out.println("狗在汽车上跑");
    }
}

public class InterfaceTest {
    public static void main(String[] args) {
        dogcar dc = new dogcar();
        dc.eat();
        dc.run();
    }
}