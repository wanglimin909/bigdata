package Practice04;

/**
 *
 * 1.定义圆柱体类Cylinder，利用方法重载来设置成员变量颜色、半径和高，最后输出其颜色和体积。
 *
 * @author wlm.java
 * @create 2023-05-05 14:24
 */
public class Cylinder {
    private String color;
    private double radius;
    private double height;

    public void setcolor(String color){
        this.color = color;
    }
    public void setradiusheight(double radius,double height){
        this.radius = radius;
        this.height = height;
    }
    public void setradiusheight(double radius,double height,String color){
        this.radius = radius;
        this.height = height;
        this.color = color;
    }
    public String getColor(){
        return color;
    }
    public double volume(){
        return Math.PI * radius * radius * height;
    }
}

class CylinderTest{
    public static void main(String[] args) {

        Cylinder c1 = new Cylinder();
        c1.setcolor("红色");
        c1.setradiusheight(3,4);
        System.out.println("c1的颜色是"+c1.getColor()+",c1的体积"+c1.volume());

        Cylinder c2 = new Cylinder();
        c2.setradiusheight(5,6,"蓝色");
        System.out.println("c2的颜色是"+c2.getColor()+",c2的体积"+c2.volume());

    }
}