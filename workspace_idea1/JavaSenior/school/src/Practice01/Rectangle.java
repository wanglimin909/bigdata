package Practice01;

import java.util.Scanner;

/**
 * @author wlm.java
 * @create 2023-03-30 9:12
 */
public class Rectangle {

    private double width;
    private double height;

    public void setRectangle(int w,int h){
        if(w > 0 && h > 0){
            this.width = w;
            this.height = h;
        }else{
            System.out.println("输入错误");
        }
    }

    public  double getArea(){
        return width * height;
    }

}

class RectangleTest{
    public static void main(String[] args) {
        Rectangle rect = new Rectangle();
        int w,h;
        Scanner in = new Scanner(System.in);
        System.out.println("输入宽度：");
        w = in.nextInt();
        System.out.println("输入长度：");
        h = in.nextInt();
        rect.setRectangle(w,h);
        double area = rect.getArea();
        System.out.println("面积为：" + area);
    }
}
