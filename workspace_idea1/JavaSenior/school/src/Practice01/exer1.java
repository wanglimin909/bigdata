package Practice01;
import java.util.Scanner;

/**
 * @author wlm.java
 * @create 2023-03-28 17:04
 */
public class exer1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("请输入二维数组的行数：");
        int m = scanner.nextInt();
        System.out.print("请输入二维数组的列数：");
        int n = scanner.nextInt();
        int[][] arr = new int[m][n];
        System.out.println("请输入二维数组的元素：");
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                arr[i][j] = scanner.nextInt();
            }
        }
        System.out.println("该二维数组为： ");
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++){
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
        int[] sum = getSum(arr);
        System.out.println("该二维数组各列之和为：");
        for (int i = 0; i < sum.length; i++) {
            System.out.print(sum[i] + " ");
        }
    }

    public static int[] getSum(int[][] arr) {
        int m = arr.length;
        int n = arr[0].length;
        int[] sum = new int[n];
        for (int j = 0; j < n; j++) {
            for (int i = 0; i < m; i++) {
                sum[j] += arr[i][j];
            }
        }
        return sum;
    }
}
