package Practice01;

/**
 * @author wlm.java
 * @create 2023-03-29 21:46
 */
import java.util.*;
public class exer2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("[请输入M*N的维度]");
        System.out.print("请输入维数M：");
        int M = sc.nextInt();
        System.out.print("请输入维数N：");
        int N = sc.nextInt();
        int[][] arr = new int[M][N];
        int[][] a = new int[N][M];
        System.out.println("[请输入矩阵的内容]");
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                arr[i][j] = sc.nextInt();
            }
        }
        System.out.println("[转置矩阵前的内容为：]");
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                System.out.print(arr[i][j] + "\t");
            }
            System.out.println();
        }


        System.out.println("[转置矩阵的内容为：]");
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                a[i][j] = arr[j][i];
                System.out.print(a[i][j] + "\t");
            }
            System.out.println();
        }
    }
}

