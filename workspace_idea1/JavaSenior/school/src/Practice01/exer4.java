package Practice01;

/**
 * @author wlm.java
 * @create 2023-04-04 11:00
 */
public class exer4 {

    private int i,j;
    private double x,y,z;

    public exer4(int i,int j){
        this.i = i;
        this.j = j;
        System.out.println(max(this.i, this.j));
    }
    public exer4(double x,double y,double z){
        this.x = x;
        this.y = y;
        this.z = z;
        System.out.println(ji(this.x, this.y, this.z));
    }

    public int max(int i,int j){
        if(i > j){
            return i;
        }else{
            return j;
        }
    }
    public double ji(double x,double y,double z){
        return x*y*z;
    }

}

class exer4Test{
    public static void main(String[] args) {

        exer4 x = new exer4(3, 6);

        exer4 y = new exer4(3.2, 4.5, 5);

    }
}
