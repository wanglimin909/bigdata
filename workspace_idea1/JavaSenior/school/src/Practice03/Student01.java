package Practice03;

/**
 *
 * 1.定义一个Student类，包含以下内容：
 成员变量：学号，姓名，性别，班干部否，数学，语文，英语
 成员方法：求三门课总成绩、求三门课平均成绩、输出学生信息。
 编程实现这个类，并调用相应的方法输入数据，计算总分和平均分。

 * @author wlm.java
 * @create 2023-04-13 8:11
 */
public class Student01 {
    private int id;
    private String name;
    private char sex;//男：'1'  女：'0'
    private boolean leader;
    private int math;
    private int chinese;
    private int english;

    public Student01() {
    }
    public Student01(int id,String name,char sex,boolean leader,int math,int chinese,int english){
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.leader = leader;
        this.math = math;
        this.chinese = chinese;
        this.english = english;
    }

    public int getMath() {
        return math;
    }
    public int getChinese() {
        return chinese;
    }
    public int getEnglish() {
        return english;
    }

    public int sum(){
        return getMath() + getChinese() + getEnglish();
    }
    public double avg(){
        return (getMath() + getChinese() + getEnglish())/3;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sex=" + sex +
                ", leader=" + leader +
                ", math=" + math +
                ", chinese=" + chinese +
                ", english=" + english +
                '}';
    }
}

class StudentTest{
    public static void main(String[] args) {
        Student01 s1 = new Student01(1001, "张三", '1', false, 88, 94, 79);
        System.out.println("总成绩：" + s1.sum() + "\n" + "平均成绩：" + s1.avg() + "\n" + s1.toString());
    }
}
