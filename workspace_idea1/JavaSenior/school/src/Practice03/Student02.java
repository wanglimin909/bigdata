package Practice03;

/**
 *
 * 3.定义一个Student类，根据需要添加静态成员变量和静态方法，在主类中测试静态成员变量和静态方法的使用。
 *
 * @author wlm.java
 * @create 2023-04-13 9:04
 */
public class Student02 {
        private String name;
        private int age;
        private static int studentCount = 0;//记录学生人数

        public Student02(String name, int age) {
            this.name = name;
            this.age = age;
            studentCount++;
        }

        public String getName() {
            return name;
        }

        public int getAge() {
            return age;
        }

        public static int getStudentCount() {
            return studentCount;
        }
}

class Test {
    public static void main(String[] args) {
        Student02 s1 = new Student02("Tom", 18);
        Student02 s2 = new Student02("Jerry", 19);
        Student02 s3 = new Student02("Peter", 20);
        System.out.println("Total students: " + Student02.getStudentCount()); //3
    }
}

