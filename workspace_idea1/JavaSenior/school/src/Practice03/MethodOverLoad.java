package Practice03;

/**
 *
 * 2.定义一个MethodOverload类，并在该类中定义几个重载方法，在主类中实现方法的重载。
 *
 * @author wlm.java
 * @create 2023-04-13 9:01
 */
public class MethodOverLoad {
    public static void print() {
        System.out.println("无参数的print方法");
    }

    public static void print(int n) {
        System.out.println("整数参数的print方法，参数值为：" + n);
    }

    public static void print(double d) {
        System.out.println("浮点数参数的print方法，参数值为：" + d);
    }

    public static void print(String s) {
        System.out.println("字符串参数的print方法，参数值为：" + s);
    }
}

class OverLoadTest{
    public static void main(String[] args) {
        MethodOverLoad.print();
        MethodOverLoad.print(123);
        MethodOverLoad.print(3.14);
        MethodOverLoad.print("Hello, world!");
    }
}