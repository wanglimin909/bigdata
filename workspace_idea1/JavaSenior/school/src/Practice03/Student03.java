package Practice03;

import java.util.Scanner;

/**
 *
 * 4. 定义一个Student类并建立Student类数组， 要求从键盘输入学生信息，找出总成绩最高的学生并输出该学生的信息。
 *
 * @author wlm.java
 * @create 2023-04-13 9:06
 */
public class Student03 {
    private String name;  // 学生姓名
    private int score;  // 学生成绩

    public Student03(String name, int score) {
        this.name = name;
        this.score = score;
    }

    // 获取学生姓名
    public String getName() {
        return name;
    }

    // 获取学生成绩
    public int getScore() {
        return score;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("请输入学生人数：");
        int n = scanner.nextInt();
        Student03[] students = new Student03[n];
        // 从键盘输入学生信息并构建Student对象数组
        for (int i = 0; i < n; i++) {
            System.out.print("请输入学生姓名：");
            String name = scanner.next();
            System.out.print("请输入学生成绩：");
            int score = scanner.nextInt();
            students[i] = new Student03(name, score);
        }
        // 找出总成绩最高的学生
        Student03 maxStudent = students[0];
        for (int i = 1; i < n; i++) {
            if (students[i].getScore() > maxStudent.getScore()) {
                maxStudent = students[i];
            }
        }
        // 输出该学生的信息
        System.out.println("总成绩最高的学生是：" + maxStudent.getName() + "，成绩为：" + maxStudent.getScore());
    }
}