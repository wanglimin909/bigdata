package Practice02;

import java.util.Scanner;

/**
 * 4. 编写程序，要求自定义一个二维数组，从键盘输入数组元素，实现矩阵的转置
 * @author wlm.java
 * @create 2023-03-28 17:34
 */
public class Demo4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("请输入矩阵的行数：");
        int row = scanner.nextInt();
        System.out.print("请输入矩阵的列数：");
        int col = scanner.nextInt();
        int[][] matrix = new int[row][col];
        System.out.println("请输入矩阵的元素：");
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                matrix[i][j] = scanner.nextInt();
            }
        }
        System.out.println("原矩阵为：");
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
        int[][] transposeMatrix = new int[col][row];
        for (int i = 0; i < col; i++) {
            for (int j = 0; j < row; j++) {
                transposeMatrix[i][j] = matrix[j][i];
            }
        }
        System.out.println("转置后的矩阵为：");
        for (int i = 0; i < col; i++) {
            for (int j = 0; j < row; j++) {
                System.out.print(transposeMatrix[i][j] + " ");
            }
            System.out.println();
        }
    }
}
