package Practice02;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 7. 利用正则表达式判断输入的IP地址是否合法，IP地址以x.x.x.x的形式表示，其中每个x都是一个0～255的十进制数
 * @author wlm.java
 * @create 2023-03-28 17:39
 */
public class Demo7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入IP地址：");
        String ip = scanner.nextLine();
        String regex = "^((25[0-5]|2[0-4]\\d|[01]?\\d\\d?)\\.){3}(25[0-5]|2[0-4]\\d|[01]?\\d\\d?)$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(ip);
        if (matcher.matches()) {
            System.out.println("IP地址合法");
        } else {
            System.out.println("IP地址不合法");
        }
    }
}
