package Practice02;

/**
 * 1. 编写程序，要求从键盘输入n个数，求这n个数中的最大数与最小数并输出，利用数组实现
 * @author wlm.java
 * @create 2023-03-28 17:09
 */
import java.util.Scanner;

public class Demo1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("请输入n的值：");
        int n = input.nextInt();
        int[] arr = new int[n];
        System.out.print("请输入" + n + "个数：");
        for (int i = 0; i < n; i++) {
            arr[i] = input.nextInt();
        }
        int max = arr[0];
        int min = arr[0];
        for (int i = 1; i < n; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
            if (arr[i] < min) {
                min = arr[i];
            }
        }
        System.out.println("最大值为：" + max);
        System.out.println("最小值为：" + min);
    }
}



