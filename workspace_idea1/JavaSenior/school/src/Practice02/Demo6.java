package Practice02;

import java.util.Scanner;

/**
 * 6. 从键盘输入一个字符串和一个字符，从该字符串中删除给定的字符
 * @author wlm.java
 * @create 2023-03-28 17:37
 */
public class Demo6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("请输入一个字符串：");
        String str = scanner.nextLine();
        System.out.print("请输入一个字符：");
        char ch = scanner.next().charAt(0);
        String newStr = str.replace(String.valueOf(ch), "");
        System.out.println("删除后的字符串为：" + newStr);
    }
}
