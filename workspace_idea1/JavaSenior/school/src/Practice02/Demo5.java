package Practice02;

import java.util.Scanner;

/**
 * 5. 编写程序，要求从键盘输入若干个字符串（比如国家英文名称），并利用冒泡（或选择）排序法将其升序后输出
 * @author wlm.java
 * @create 2023-03-28 17:35
 */
public class Demo5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("请输入字符串个数：");
        int n = scanner.nextInt();
        String[] arr = new String[n];
        System.out.println("请输入字符串：");
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.next();
        }
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (arr[j].compareTo(arr[j + 1]) > 0) {
                    String temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
        System.out.println("排序后的字符串为：");
        for (String s : arr) {
            System.out.print(s + " ");
        }
    }
}
