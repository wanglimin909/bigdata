package Practice02;

import java.util.Scanner;

/**
 * 3. 编写程序，要求统计键盘输入的字符串中大写字母的个数、小写字母的个数和其它字符的个数
 * @author wlm.java
 * @create 2023-03-28 17:31
 */

public class Demo3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("请输入字符串：");
        String str = scanner.nextLine();
        int upperCount = 0;
        int lowerCount = 0;
        int otherCount = 0;
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (c >= 'A' && c <= 'Z') {
                upperCount++;
            } else if (c >= 'a' && c <= 'z') {
                lowerCount++;
            } else {
                otherCount++;
            }
        }
        System.out.println("大写字母个数：" + upperCount);
        System.out.println("小写字母个数：" + lowerCount);
        System.out.println("其他字符个数：" + otherCount);
    }
}
