package Practice02;

import java.util.Scanner;

/**
 *
 * 1!+2!+...+n!
 *
 * @author wlm.java
 * @create 2023-05-09 21:42
 */
public class Demo8 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("输入n:");
        int n = scan.nextInt();
        System.out.println(c(n));


    }
    public static int s(int n){
        if(n == 0){
            return 1;
        }else{
            return n * s(n-1);
        }
    }
    public static int c(int n){
        if(n == 1){
            return 1;
        }else{
            return s(n)+c(n-1);
        }
    }
}
