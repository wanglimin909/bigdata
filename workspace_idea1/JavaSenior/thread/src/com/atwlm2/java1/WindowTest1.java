package com.atwlm2.java1;

/**
 *例子：创建三个窗口卖票，总票数为100张(使用继承Thread类的方式)
 * 存在线程的安全问题，待解决。
 *
 * 使用同步代码块来解决继承Thread类的方式的线程安全问题
 *
 * 说明：在继承Thread类创建多线程的方式中，慎用this充当同步监视器，考虑使用当前类充当同步监视器
 *
 *
 *
 * @author wlm.java
 * @create 2023-03-18 19:22
 */
class window1 extends Thread{

    private static int ticket = 100;//思考：ticket必须声明为静态的

    private static Object obj = new Object();//因为需要共用一把锁，所以要声明为static

    @Override
    public void run() {

        while(true){
            //正确的1
            //synchronized(obj) {
            ////正确的2
            synchronized(window1.class) {//Class c = window.class,window.class只会加载一次
                //错误的:this代表着t1,t2,t3三个对象
                //synchronized(this) {
                if (ticket > 0) {

                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    System.out.println(getName() + ":卖票，票号为" + ticket);
                    ticket--;
                } else {
                    break;
                }
            }
        }

    }
}

public class WindowTest1 {
    public static void main(String[] args) {
        window1 t1 = new window1();
        window1 t2 = new window1();
        window1 t3 = new window1();
        t1.setName("窗口一");
        t2.setName("窗口二");
        t3.setName("窗口三");
        t1.start();
        t2.start();
        t3.start();
    }
}
