package com.atwlm2.exer2;

/**
 * 多线程的创建方式一：继承Thread类
 *
 * 处理线程安全问题的三种方法
 *      1.同步代码块
 *      2.同步方法
 *      3.Lock锁
 *
 * @author wlm.java
 * @create 2023-03-21 9:58
 */
class Window1 extends Thread{

    private int ticket = 100;

    @Override
    public void run() {
        while(true){
                if(ticket > 0){
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(Thread.currentThread().getName() + "成功售票，票号为：" + ticket);
                    ticket--;
                }else{
                    break;
                }
        }
    }

}

public class WindowTest1 {
    public static void main(String[] args) {
        Window1 w1 = new Window1();
        Window1 w2 = new Window1();
        Window1 w3 = new Window1();

        w1.setName("窗口一");
        w2.setName("窗口二");
        w3.setName("窗口三");

        w1.start();
        w2.start();
        w3.start();

    }
}

//同步代码块解决继承Thread类创建多线程所出现的线程安全问题

//    private static int ticket = 100;
//
//    @Override
//    public void run() {
//        while(true){
//            synchronized (Window1.class){
//                if(ticket > 0){
//                    try {
//                        Thread.sleep(100);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                    System.out.println(Thread.currentThread().getName() + "成功售票，票号为：" + ticket);
//                    ticket--;
//                }
//            }
//        }
//    }


//同步方法解决继承Thread类创建多线程所出现的线程安全问题

//    private static int ticket = 100;
//
//    @Override
//    public void run() {
//        while(true){
//            show();
//        }
//    }
//
//    private synchronized static void show(){
//        if(ticket > 0){
//            try {
//                Thread.sleep(100);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            System.out.println(Thread.currentThread().getName() + "成功售票，票号为：" + ticket);
//            ticket--;
//        }
//    }


//Lock锁解决继承Thread类创建多线程所出现的线程安全问题

//    private static int ticket = 100;
//    private static ReentrantLock lock = new ReentrantLock();//因为是继承方式创建的多线程，所以要声明为static的
//
//    @Override
//    public void run() {
//        while(true){
//            try {
//
//                lock.lock();
//
//                if(ticket > 0){
//                    try {
//                        Thread.sleep(100);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                    System.out.println(Thread.currentThread().getName() + "成功售票，票号为：" + ticket);
//                    ticket--;
//                }
//            }finally{
//                lock.unlock();
//            }
//
//        }
//    }








