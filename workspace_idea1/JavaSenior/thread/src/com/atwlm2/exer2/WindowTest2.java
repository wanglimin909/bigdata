package com.atwlm2.exer2;

/**
 *
 * 多线程的创建方式二：实现Runnable
 *
 * 处理线程安全问题的三种方法
 *      1.同步代码块
 *      2.同步方法
 *      3.Lock锁
 *
 * @author wlm.java
 * @create 2023-03-21 10:08
 */

class Window2 implements Runnable{

    private int ticket = 100;

    @Override
    public void run() {
        while(true){
                if (ticket > 0){
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(Thread.currentThread().getName() + "售票成功，票号为：" + ticket);
                    ticket--;
                }else{
                    break;
                }
        }
    }

}

public class WindowTest2 {
    public static void main(String[] args) {
        Window2 w = new Window2();

        Thread t1 = new Thread(w);
        Thread t2 = new Thread(w);
        Thread t3 = new Thread(w);

        t1.setName("窗口一");
        t2.setName("窗口二");
        t3.setName("窗口三");

        t1.start();
        t2.start();
        t3.start();

    }
}

//同步代码块解决实现Runnable创建多线程所出现的线程安全问题

//    private int ticket = 100;
//
//    @Override
//    public void run() {
//        while(true){
//            synchronized (this){
//                if (ticket > 0){
//                    try {
//                        Thread.sleep(100);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                    System.out.println(Thread.currentThread().getName() + "售票成功，票号为：" + ticket);
//                    ticket--;
//                }else{
//                    break;
//                }
//            }
//        }
//    }


//同步方法解决实现Runnable创建多线程所出现的线程安全问题

//    private int ticket = 100;
//
//    @Override
//    public void run() {
//        while(true){
//            show();
//        }
//    }
//    private synchronized void show(){
//        if (ticket > 0){
//            try {
//                Thread.sleep(100);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            System.out.println(Thread.currentThread().getName() + "售票成功，票号为：" + ticket);
//            ticket--;
//        }
//    }


//Lock锁解决实现Runnable创建多线程所出现的线程安全问题

//    private int ticket = 100;
//
//    private ReentrantLock lock = new ReentrantLock();
//
//    @Override
//    public void run() {
//        while(true){
//            try {
//
//                lock.lock();
//
//                if (ticket > 0){
//                    try {
//                        Thread.sleep(100);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                    System.out.println(Thread.currentThread().getName() + "售票成功，票号为：" + ticket);
//                    ticket--;
//                }else{
//                    break;
//                }
//            }finally{
//                lock.unlock();
//            }
//
//        }
//    }

