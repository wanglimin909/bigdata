package com.atwlm2.java2;

import java.util.concurrent.locks.ReentrantLock;

/**
 * 解决线程安全问题的方式三：Lock锁 ---> JDK5.0新增
 *  注1.//如果用继承方式创建的多线程，则用该方法解决安全问题时要声明为static
 *
 * 1.面试题：synchronized与Lock的异同
 *      相同点：二者都可以解决线程安全问题
 *      不同点：synchronized机制在执行完相应的同步代码以后，自动的释放同步监视器
 *             Lock需要手动的启动同步（lock()），同时结束同步也需要手动的实现（unlock()）
 * 优先使用顺序：Lock  同步代码块（已经进入了方法体，分配了相应资源）  同步方法（在方法体之外）
 *
 * 2.面试题：如何解决线程安全问题 ? 有几种方式！
 *
 * @author wlm.java
 * @create 2023-03-20 20:46
 */
class Window1 implements Runnable{

    private int ticket = 100;

    //1.实例化ReentrantLock
    private ReentrantLock lock = new ReentrantLock();//如果用继承方式创建的多线程，则用该方法解决安全问题时要声明为static

    @Override
    public void run() {
        while(true){
            try{

                //2.调用锁定方法：lock()
                lock.lock();

                if(ticket > 0){
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(Thread.currentThread().getName() + ":售票，票号为：" + ticket);
                    ticket--;
                }else {
                    break;
                }

            }finally {

                //3.调用解锁方法:unlock()
                lock.unlock();

            }
        }
    }
}

public class LockTest1 {
    public static void main(String[] args) {
        Window1 w = new Window1();

        Thread t1 = new Thread(w);
        Thread t2 = new Thread(w);
        Thread t3 = new Thread(w);

        t1.setName("窗口1");
        t2.setName("窗口2");
        t3.setName("窗口3");

        t1.start();
        t2.start();
        t3.start();

    }
}
