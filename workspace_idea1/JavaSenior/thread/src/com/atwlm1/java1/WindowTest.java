package com.atwlm1.java1;

/**
 *例子：创建三个窗口卖票，总票数为100张(使用继承Thread类的方式)
 * 存在线程的安全问题，待解决。
 *
 * @author wlm.java
 * @create 2023-03-18 19:22
 */
class window extends Thread{

    private static int ticket = 100;//思考：ticket必须声明为静态的

    @Override
    public void run() {

        while(true){

            if(ticket > 0){
                System.out.println(getName() + ":卖票，票号为" + ticket);
                ticket--;
            }else{
                break;
            }

        }

    }
}
public class WindowTest {
    public static void main(String[] args) {
        window t1 = new window();
        window t2 = new window();
        window t3 = new window();
        t1.setName("窗口一");
        t2.setName("窗口二");
        t3.setName("窗口三");
        t1.start();
        t2.start();
        t3.start();
    }
}
