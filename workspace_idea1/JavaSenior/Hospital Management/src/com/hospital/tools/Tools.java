package com.hospital.tools;



import java.util.Scanner;

/**
 * @ClassName Tools
 * @Description 提供了 Tools.java类，可用来方便的实现键盘访问
 * @Author Snoopy
 * @Date 2022/3/3 16:31
 * @Version 1.0
 */
public class Tools {

    private static Scanner input = new Scanner(System.in);

    /**
     * @author Snoopy
     * @Description 该方法读取键盘，如果用户键入 ‘1’-‘4’中的任意字符，则方法返回，返回值为用户键入的字符
     * @Date 21:13 2022/3/3
     * @Param
     * @Return
     */

    public static char readRegisterMenuSelection() {
        char c;
        while (true) {
            String str = readKeyBoard(1, false);
            c = str.charAt(0);
            if (c != '1' && c != '2' && c != '3' && c != '4') {
                System.out.println("选择错误，请重新输入：");
            } else break;
        }
        return c;
    }

    public static char readMedicine() {
        char c;
        while (true) {
            String str = readKeyBoard(1, false);
            c = str.charAt(0);
            if (c != '1' && c != '2' && c != '3' && c != '4' && c != '5') {
                System.out.println("选择错误，请重新输入：");
            } else break;
        }
        return c;
    }


    /**
     * @author Snoopy
     * @Description 该方法读取键盘，如果用户键入 ‘1’-‘3’中的任意字符，则方法返回，返回值为用户键入的字符
     * @Date 21:13 2022/3/3
     * @Param
     * @Return
     */

    public static char readAdminMenuSelection() {
        char c;
        while (true) {
            String str = readKeyBoard(1, false);
            c = str.charAt(0);
            if (c != '1' && c != '2' && c != '3') {
                System.out.println("选择错误，请重新输入：");
            } else break;
        }
        return c;
    }

    public static char readPatientMenu() {
        char c;
        while (true) {
            String str = readKeyBoard(1, false);
            c = str.charAt(0);
            if (c != '1' && c != '2' && c != '3' && c != '4' && c != '5') {
                System.out.println("选择错误，请重新输入：");
            } else break;
        }
        return c;
    }

    /**
     * @author Snoopy
     * @Description 该方法读取键盘，如果用户键入 ‘1’-‘2’中的任意字符，则方法返回，返回值为用户键入的字符
     * @Date 21:13 2022/3/3
     * @Param
     * @Return
     */

    public static char readPatientMenuSelection() {
        char c;
        while (true) {
            String str = readKeyBoard(1, false);
            c = str.charAt(0);
            if (c != '1' && c != '2' ) {
                System.out.println("选择错误，请重新输入：");
            } else break;
        }
        return c;
    }


    /**
     * @author Snoopy
     * @Description 该方法读取键盘，如果用户键入 ‘1’-‘7’中的任意字符，则方法返回，返回值为用户键入的字符
     * @Date
     * @Param
     * @Return
     */

    public static char readMainMenuSelection() {
        char c;
        while (true) {
            String str = readKeyBoard(1, false);
            c = str.charAt(0);
            if (c != '0' && c != '1' && c != '2' && c != '3' && c != '4' && c != '5' && c != '6' && c != '7' && c != '8' && c != '9') {
                System.out.println("选择错误，请重写输入：");
            } else break;
        }
        return c;
    }

    /**
     * @author Snoopy
     * @Description 该方法读取键盘，如果用户键入 ‘1’-‘9’中的任意字符，则方法返回，返回值为用户键入的字符
     * @Date
     * @Param
     * @Return
     */

    public static char readAdminInfo() {
        char c;
        while (true) {
            String str = readKeyBoard(1, false);
            c = str.charAt(0);
            if ( c != '1' && c != '2' && c != '3' && c != '4' && c != '5' && c != '6' && c != '7' && c != '8' && c != '9') {
                System.out.println("选择错误，请重写输入：");
            } else break;
        }
        return c;
    }

    public static char readDoctorInfo() {
        char c;
        while (true) {
            String str = readKeyBoard(1, false);
            c = str.charAt(0);
            if ( c != '1' && c != '2' && c != '3' && c != '4' && c != '5' && c != '6' && c != '7' && c != '8' ) {
                System.out.println("选择错误，请重写输入：");
            } else break;
        }
        return c;
    }

    public static char readInfo() {
        char c;
        while (true) {
            String str = readKeyBoard(1, false);
            c = str.charAt(0);
            if (c != '1' && c != '2' && c != '3' && c != '4' && c != '5' && c != '6' && c != '7') {
                System.out.println("选择错误，请重写输入：");
            } else break;
        }
        return c;
    }

    public static char readInforoom() {
        char c;
        while (true) {
            String str = readKeyBoard(1, false);
            c = str.charAt(0);
            if ( c != '1' && c != '2' && c != '3' && c != '4' && c != '5' && c != '6'  ) {
                System.out.println("选择错误，请重写输入：");
            } else break;
        }
        return c;
    }

    /**
     * @author Snoopy
     * @Description 该方法提示并等待，直到用户按回车键后返回
     * @Date
     * @Param
     * @Return
     */

    public static void readReturn() {
        System.out.println("按回车键继续....");
        readKeyBoard(100, true);
    }

    /**
     * @author
     * @Description 该方法从键盘读取一个长度不超过 2 位的整数，并将其作为方法的返回值
     * @Date
     * @Param
     * @Return
     */

    public static int readLimitInt() {
        int n;
        while (true) {
            String str = readKeyBoard(2, false);
            try {
                n = Integer.parseInt(str);
                break;
            } catch (NumberFormatException e) {
                System.out.println("数字输入错误，请重新输入：");
            }
        }
        return n;
    }

    /**
     * @author Snoopy
     * @Description 该方法从键盘读取一个长度不超过 18 位的字符串，并将其作为方法的返回值
     * @Date
     * @Param
     * @Return
     */

    public static String readString() {
        String str = readKeyBoard(18, false);
        return str;
    }

    /**
    * @author Snoopy
    * @Description 该方法从键盘读取一个整型数据
    * @Date
    * @Param
    * @Return
    */

    public static int readInt(){
        return input.nextInt();
    }

    /**
     * @author Snoopy
     * @Description 该方法从键盘读取一个Double类型数据
     * @Date
     * @Param
     * @Return
     */

    public static double readDouble(){
        return input.nextDouble();
    }

    /**
     * @author Snoopy
     * @Description 对齐输出的学生信息（由 com.student.bean.student 来调用）,默认指定字段长度为18
     * @Date
     * @Param
     * @Return
     */
    public static String alignment(String str){
        int length = 18;
        if(str.length() < 18){
            while(str.length() < length){
                str += " ";
            }
        }
        return str;
    }

    /**
     * @author Snoopy
     * @Description 从键盘读取‘Y’ 或 ‘N’，并将其作为方法的返回值
     * @Date
     * @Param
     * @Return
     */
    public static char readConfirmSelection() {
        char c;
        while (true) {
            String str = readKeyBoard(1, false).toUpperCase();
            c = str.charAt(0);
            if (c == 'Y' || c == 'N') {
                break;
            } else {
                System.out.println("选择错误，请重写输入：");
            }
        }
        return c;
    }

    /**
     * @author Snoopy
     * @Description 从键盘获取数据
     * @Date
     * @Param
     * @Return
     */

    private static String readKeyBoard(int limit, boolean blankReturn) {
        String line = "";
        while (input.hasNextLine()) {
            line = input.nextLine();
            if (line.length() == 0) {
                if (blankReturn) {
                    return line;
                } else {
                    continue;
                }
            }
            if (line.length() < 1 || line.length() > limit) {
                System.out.println("输入长度（不大于" + limit + "）错误，请重新输入：");
                continue;
            }
            break;
        }
        return line;
    }
}
