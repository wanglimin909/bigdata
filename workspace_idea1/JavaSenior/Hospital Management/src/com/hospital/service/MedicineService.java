package com.hospital.service;

import com.hospital.bean.Medicine;
import com.hospital.dao.MedicineDAOImpl;
import com.hospital.dao.util.JDBCUtils;

import java.sql.*;
import java.util.List;

/**
 * @author wlm.java
 * @create 2023-06-26 20:07
 */
public class MedicineService {
    MedicineDAOImpl medicineDAO = new MedicineDAOImpl();

    /**
     * 添加药品信息
     * @param medicine
     * @return
     */
    public int addMedicine(Medicine medicine) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int insert = medicineDAO.insert(connection, medicine);
            return insert;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }

    /**
     * 删除药品信息
     * @param medicineId
     * @return
     */
    public int deleteMedicine(String medicineId) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int delete = medicineDAO.delete(connection, medicineId);
            return delete;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }

    /**
     * 根据药品id，修改药品部分信息
     * @param medicineId
     * @param key
     * @param value
     * @return
     */

    public int update(String medicineId, String key, Object value) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int update = medicineDAO.update(connection, medicineId, key, value);
            return update;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }

    /**
     * 根据药品id修改全部信息
     * @param oldMedicineId
     * @param medicine
     * @return
     */

    public int updateAll(String oldMedicineId, Medicine medicine) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int updateAll = medicineDAO.updateAll(connection, oldMedicineId, medicine);
            return updateAll;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }

    /**
     * 根据id查询药品信息
     * @param medicineId
     * @return
     */

    public Object search(String medicineId) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Medicine medicine = medicineDAO.getMedicineByMedicineId(connection, medicineId);
            return medicine;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return null;
    }

    /**
     * 查找全部药品信息
     * @return
     */

    public List<Medicine> searchAll() {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            List<Medicine> medicineAll = medicineDAO.getMedicineAll(connection);
            return medicineAll;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return null;
    }

    /**
     * 获取药品总数
     * @return
     */

    public Long getCount() {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Long count = medicineDAO.getCount(connection);
            return count;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return null;
    }


    /**
     * 清空所有药品的信息
     * @return
     */

    public int clearAll() {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int clearAll = medicineDAO.clearAll(connection);
            return clearAll;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }

    /**
     * @author Snoopy
     * @Description 检查用户输入的字段是否存在
     * @Date
     * @Param
     * @Return
     */

    public boolean checkExist(String value) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            String sql = "select student_id,student_name,student_age,student_sex,student_phone,student_card,student_location," +
                    "student_english,student_math,student_java from student";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            // 获取结果集
            ResultSet resultSet = preparedStatement.executeQuery();
            // 获取结果集的元数据
            ResultSetMetaData metaData = resultSet.getMetaData();
            // 获取每一行的列数
            int columnCount = metaData.getColumnCount();
            String columnLabel = "";
            for (int i = 0; i < columnCount; i++) {
                // 获取每一行的每一列的别名
                columnLabel = metaData.getColumnLabel(i + 1);
                if (value.equals(columnLabel)) {
                    return true;
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return false;
    }

}
