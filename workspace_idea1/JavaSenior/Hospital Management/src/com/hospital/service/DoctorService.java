package com.hospital.service;

import com.hospital.bean.Doctor;
import com.hospital.dao.DoctorDAOImpl;
import com.hospital.dao.util.JDBCUtils;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @author wlm.java
 * @create 2023-06-25 16:50
 */
public class DoctorService {
    DoctorDAOImpl doctorDAO = new DoctorDAOImpl();

    /**
     * 注册功能
     * @param doctor
     * @return
     */

    public int register(Doctor doctor) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int register = doctorDAO.register(connection, doctor);
            return register;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }

    /**
     * 登录功能
     * @param doctorId
     * @param doctorPassword
     * @return
     */

    public Doctor login(String doctorId, String doctorPassword) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Doctor login = doctorDAO.login(connection, doctorId, doctorPassword);
            return login;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return null;
    }


    /**
     * 找回密码
     * @param doctorId
     * @param doctorPhone
     * @return
     */

    public String recover(String doctorId, String doctorPhone) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            String recover = doctorDAO.recover(connection, doctorId, doctorPhone);
            return recover;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return null;
    }

    /**
     * 注销账户
     * @param doctorId
     * @param doctorPassword
     * @return
     */

    public int unsubscribe(String doctorId, String doctorPassword) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int unsubscribe = doctorDAO.unsubscribe(connection, doctorId, doctorPassword);
            return unsubscribe;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }

    /**
     * 根据id修改全部信息
     * @param oldDoctorId
     * @param doctor
     * @return
     */

    public int updateAll(String oldDoctorId, Doctor doctor) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int updateAll = doctorDAO.updateAll(connection, oldDoctorId, doctor);
            return updateAll;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }

    /**
     * 通过id查询
     * @param doctorId
     * @return
     */

    public Doctor getDoctorById(String doctorId) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Doctor doctor = doctorDAO.getDoctorById(connection, doctorId);
            return doctor;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return null;
    }

    /**
     * 根据手机号，获取医生信息
     * @param doctorPhone
     * @return
     */

    public Doctor getDoctorByPhone(String doctorPhone) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Doctor doctor = doctorDAO.getDoctorByPhone(connection, doctorPhone);
            return doctor;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return null;
    }

    /**
     * 删除医生信息
     * @param doctorId
     * @return
     */
    public int deleteDoctor(String doctorId) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int delete = doctorDAO.delete(connection, doctorId);
            return delete;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }

    /**
     * 查找全部医生信息
     * @return
     */
    public List<Doctor> searchAll() {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            List<Doctor> doctorAll = doctorDAO.getDoctorAll(connection);
            return doctorAll;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return null;
    }


    /**
     * 清空所有医生的信息
     * @return
     */

    public int clearAll() {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int clearAll = doctorDAO.clearAll(connection);
            return clearAll;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }


    /**
     * 检查医生账号是否重复
     *
     * @param doctorId
     * @return
     */

    public boolean checkId(String doctorId) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Doctor doctorById = doctorDAO.getDoctorById(connection, doctorId);
            if (doctorById != null) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return false;
    }


    /**
     * 检查手机号是否有重复
     * @param doctorPhone
     * @return
     */

    public boolean checkPhone(String doctorPhone) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Doctor phone = doctorDAO.getDoctorById(connection, doctorPhone);
            if (phone != null) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return false;
    }
}