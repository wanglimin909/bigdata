package com.hospital.service;

import com.hospital.bean.Pay;
import com.hospital.dao.PayDAOImpl;
import com.hospital.dao.util.JDBCUtils;

import java.sql.*;
import java.util.List;

/**
 * @author wlm.java
 * @create 2023-06-26 20:14
 */
public class PayService {
    PayDAOImpl payDAO = new PayDAOImpl();

    /**
     * 添加支付信息
     * @param pay
     * @return
     */
    public int addPay(Pay pay) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int insert = payDAO.insert(connection, pay);
            return insert;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }

    /**
     * 删除缴费记录
     * @param payId
     * @return
     */
    public int deletePay(String payId) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int delete = payDAO.delete(connection, payId);
            return delete;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }


    /**
     * 根据id查询缴费记录
     * @param payId
     * @return
     */

    public Object search(String payId) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Pay pay = payDAO.getPayByPayId(connection, payId);
            return pay;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return null;
    }

    /**
     * 查找全部缴费记录
     * @return
     */

    public List<Pay> searchAll() {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            List<Pay> payAll = payDAO.getPayAll(connection);
            return payAll;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return null;
    }

    /**
     * 获取支付记录总数
     * @return
     */

    public Long getCount() {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Long count = payDAO.getCount(connection);
            return count;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return null;
    }


    /**
     * 清空所有缴费信息
     * @return
     */
    public int clearAll() {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int clearAll = payDAO.clearAll(connection);
            return clearAll;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }

    /**
     * 根据id修改全部信息
     * @param oldPayId
     * @param pay
     * @return
     */

    public int updateAll(String oldPayId, Pay pay) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int updateAll = payDAO.updateAll(connection, oldPayId, pay);
            return updateAll;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }


    /**
     * 检查用户输入的字段是否存在
     * @param value
     * @return
     */

    public boolean checkExist(String value) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            String sql = "select student_id,student_name,student_age,student_sex,student_phone,student_card,student_location," +
                    "student_english,student_math,student_java from student";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            // 获取结果集
            ResultSet resultSet = preparedStatement.executeQuery();
            // 获取结果集的元数据
            ResultSetMetaData metaData = resultSet.getMetaData();
            // 获取每一行的列数
            int columnCount = metaData.getColumnCount();
            String columnLabel = "";
            for (int i = 0; i < columnCount; i++) {
                // 获取每一行的每一列的别名
                columnLabel = metaData.getColumnLabel(i + 1);
                if (value.equals(columnLabel)) {
                    return true;
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return false;
    }

}
