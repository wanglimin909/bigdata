package com.hospital.service;

import com.hospital.bean.Department;
import com.hospital.dao.DepartmentDAOImpl;
import com.hospital.dao.util.JDBCUtils;

import java.sql.*;
import java.util.List;

/**
 * @author wlm.java
 * @create 2023-06-26 19:55
 */
public class DepartmentService {

    DepartmentDAOImpl departmentDAO = new DepartmentDAOImpl();

    /**
     * 添加科室信息
     * @param department
     * @return
     */
    public int addDepartment(Department department) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int insert = departmentDAO.insert(connection, department);
            return insert;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }

    /**
     * 删除科室信息
     * @param departmentId
     * @return
     */
    public int deleteDepartment(String departmentId) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int delete = departmentDAO.delete(connection, departmentId);
            return delete;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }

    /**
     * 根据科室id，修改科室部分信息
     * @param departmentId
     * @param key
     * @param value
     * @return
     */

    public int update(String departmentId, String key, Object value) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int update = departmentDAO.update(connection, departmentId, key, value);
            return update;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }

    /**
     * 根据id修改科室全部信息
     * @param oldDepartmentId
     * @param department
     * @return
     */

    public int updateAll(String oldDepartmentId, Department department) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int updateAll = departmentDAO.updateAll(connection, oldDepartmentId, department);
            return updateAll;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }

    /**
     * 根据科室id查询科室信息
     * @param departmentId
     * @return
     */

    public Object search(String departmentId) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Department department = departmentDAO.getDepartmentByDepartmentId(connection, departmentId);
            return department;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return null;
    }

    /**
     * 查找全部科室信息
     * @return
     */

    public List<Department> searchAll() {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            List<Department> departmentAll = departmentDAO.getDepartmentAll(connection);
            return departmentAll;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return null;
    }

    /**
     * 获取科室总数
     * @return
     */

    public Long getCount() {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Long count = departmentDAO.getCount(connection);
            return count;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return null;
    }


    /**
     * 清空所有科室的信息
     * @return
     */

    public int clearAll() {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int clearAll = departmentDAO.clearAll(connection);
            return clearAll;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }


    /**
     * 检查用户输入的字段是否存在
     * @param value
     * @return
     */

    public boolean checkExist(String value) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            String sql = "select department_id,department_name,department_address,department_phone,department_chairman from department";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            // 获取结果集
            ResultSet resultSet = preparedStatement.executeQuery();
            // 获取结果集的元数据
            ResultSetMetaData metaData = resultSet.getMetaData();
            // 获取每一行的列数
            int columnCount = metaData.getColumnCount();
            String columnLabel = "";
            for (int i = 0; i < columnCount; i++) {
                // 获取每一行的每一列的别名
                columnLabel = metaData.getColumnLabel(i + 1);
                if (value.equals(columnLabel)) {
                    return true;
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return false;
    }


}
