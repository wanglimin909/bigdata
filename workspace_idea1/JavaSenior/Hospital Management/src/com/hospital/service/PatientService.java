package com.hospital.service;

import com.hospital.bean.Patient;
import com.hospital.dao.PatientDAOImpl;
import com.hospital.dao.util.JDBCUtils;

import java.sql.*;
import java.util.List;

/**
 * @author wlm.java
 * @create 2023-06-26 19:35
 */
public class PatientService {

    PatientDAOImpl patientDAO = new PatientDAOImpl();

    /**
     * 添加病人信息
     * @param patient
     * @return
     */
    public int register(Patient patient) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int register = patientDAO.register(connection, patient);
            return register;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }

    /**
     * 删除病人信息
     * @param patientId
     * @param patientName
     * @return
     */
    public int deletePatient(String patientId,String patientName) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int delete = patientDAO.unsubscribe(connection, patientId,patientName);
            return delete;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }


    /**
     * 根据病人id查询病人信息
     * @param patientId
     * @return
     */

    public Object searchById(String patientId) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Patient patient = patientDAO.getPatientById(connection, patientId);
            return patient;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return null;
    }

    /**
     * 根据病人name查询病人信息
     * @param patientName
     * @return
     */

    public Object searchByName(String patientName) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Patient patient = patientDAO.getPatientByName(connection, patientName);
            return patient;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return null;
    }

    /**
     * 查找全部病人信息
     * @return
     */

    public List<Patient> searchAll() {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            List<Patient> patientAll = patientDAO.getPatientAll(connection);
            return patientAll;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return null;
    }

    /**
     * 获取病人总数
     * @return
     */

    public Long getCount() {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Long count = patientDAO.getCount(connection);
            return count;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return null;
    }


    /**
     * 清空所有病人的信息
     * @return
     */

    public int clearAll() {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int clearAll = patientDAO.clearAll(connection);
            return clearAll;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }

    /**
     * 根据id修改全部信息
     * @param oldPatientId
     * @param patient
     * @return
     */

    public int updateAll(String oldPatientId, Patient patient) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int updateAll = patientDAO.updateAll(connection, oldPatientId, patient);
            return updateAll;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }

}
