package com.hospital.service;

import com.hospital.bean.Admin;
import com.hospital.dao.AdminDAOImpl;
import com.hospital.dao.util.JDBCUtils;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @author wlm.java
 * @create 2023-06-26 13:37
 */
public class AdminService { //service就相当于controller
    AdminDAOImpl adminDAO = new AdminDAOImpl();

    /**
     * 注册功能
     * @param admin
     * @return
     */

    public int register(Admin admin) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int register = adminDAO.register(connection, admin);
            return register;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }

    /**
     * 登录功能
     * @param adminId
     * @param password
     * @return
     */

    public Admin login(String adminId, String password) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Admin login = adminDAO.login(connection, adminId, password);
            return login;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return null;
    }


    /**
     * 找回密码
     * @param card
     * @param phone
     * @return
     */

    public String recover(String card, String phone) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            String recover = adminDAO.recover(connection, card, phone);
            return recover;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return null;
    }

    /**
     * 删除管理员信息
     * @param adminId
     * @return
     */
    public int deleteAdmin(String adminId) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int delete = adminDAO.delete(connection, adminId);
            return delete;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }


    public Admin getAdminById(String adminId) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Admin admin = adminDAO.getAdminById(connection, adminId);
            return admin;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return null;
    }

    /**
     * 根据id修改全部信息
     * @param oldAdminId
     * @param admin
     * @return
     */

    public int updateAll(String oldAdminId, Admin admin) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int updateAll = adminDAO.updateAll(connection, oldAdminId, admin);
            return updateAll;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }

    /**
     * 注销账户
     * @param card
     * @param phone
     * @return
     */

    public int unsubscribe(String card, String phone) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int unsubscribe = adminDAO.unsubscribe(connection, card, phone);
            return unsubscribe;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }

    /**
     * 通过身份证号，获取管理员信息
     * @param card
     * @return
     */

    public Admin getAdminByCard(String card) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Admin admin = adminDAO.getAdminByCard(connection, card);
            return admin;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return null;
    }

    /**
     * 根据手机号，获取管理员信息
     * @param phone
     * @return
     */

    public Admin getAdminByPhone(String phone) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Admin admin = adminDAO.getAdminByPhone(connection, phone);
            return admin;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return null;
    }


    /**
     * 查找全部学生信息
     * @return
     */

    public List<Admin> searchAll() {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            List<Admin> adminAll = adminDAO.getAdminAll(connection);
            return adminAll;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return null;
    }


    /**
     * 清空所有管理员的信息
     * @return
     */

    public int clearAll() {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int clearAll = adminDAO.clearAll(connection);
            return clearAll;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }


    /**
     * 检查管理员账号是否重复
     * @param adminId
     * @return
     */

    public boolean checkId(String adminId) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Admin adminById = adminDAO.getAdminById(connection, adminId);
            if (adminById != null) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return false;
    }


    /**
     * 检查身份证号是否重复
     * @param card
     * @return
     */

    public boolean checkCard(String card) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Admin admin = adminDAO.getAdminById(connection, card);
            if (admin != null) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return false;
    }

    /**
     * 检查手机号是否有重复
     * @param phone
     * @return
     */

    public boolean checkPhone(String phone) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Admin admin = adminDAO.getAdminById(connection, phone);
            if (admin != null) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return false;
    }

}
