package com.hospital.service;

import com.hospital.bean.Sickroom;
import com.hospital.dao.SickroomDAOImpl;
import com.hospital.dao.util.JDBCUtils;

import java.sql.*;
import java.util.List;

/**
 * @author wlm.java
 * @create 2023-06-26 20:21
 */
public class SickroomService {

    SickroomDAOImpl sickroomDAO = new SickroomDAOImpl();

    /**
     * 添加病房信息
     * @param sickroom
     * @return
     */
    public int addSickroom(Sickroom sickroom) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int insert = sickroomDAO.insert(connection, sickroom);
            return insert;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }

    /**
     * 删除病房信息
     * @param sickroomId
     * @return
     */
    public int deleteSickroom(String sickroomId) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int delete = sickroomDAO.delete(connection, sickroomId);
            return delete;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }

    /**
     * 根据病房号，修改病房部分信息
     * @param sickroomId
     * @param key
     * @param value
     * @return
     */

    public int update(String sickroomId, String key, Object value) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int update = sickroomDAO.update(connection, sickroomId, key, value);
            return update;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }

    /**
     * 根据id修改全部信息
     * @param oldSickroomId
     * @param sickroom
     * @return
     */

    public int updateAll(String oldSickroomId, Sickroom sickroom) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int updateAll = sickroomDAO.updateAll(connection, oldSickroomId, sickroom);
            return updateAll;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }

    /**
     * 根据id查询病房信息
     * @param sickroomId
     * @return
     */

    public Object search(String sickroomId) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Sickroom sickroom = sickroomDAO.getSickroomBySickroomId(connection, sickroomId);
            return sickroom;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return null;
    }

    /**
     * 查找全部病房信息
     * @return
     */

    public List<Sickroom> searchAll() {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            List<Sickroom> sickroomAll = sickroomDAO.getSickroomAll(connection);
            return sickroomAll;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return null;
    }


    /**
     * 清空所有病房的信息
     * @return
     */

    public int clearAll() {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int clearAll = sickroomDAO.clearAll(connection);
            return clearAll;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }


    /**
     * 检查用户输入的字段是否存在
     * @param value
     * @return
     */

    public boolean checkExist(String value) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            String sql = "select student_id,student_name,student_age,student_sex,student_phone,student_card,student_location," +
                    "student_english,student_math,student_java from student";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            // 获取结果集
            ResultSet resultSet = preparedStatement.executeQuery();
            // 获取结果集的元数据
            ResultSetMetaData metaData = resultSet.getMetaData();
            // 获取每一行的列数
            int columnCount = metaData.getColumnCount();
            String columnLabel = "";
            for (int i = 0; i < columnCount; i++) {
                // 获取每一行的每一列的别名
                columnLabel = metaData.getColumnLabel(i + 1);
                if (value.equals(columnLabel)) {
                    return true;
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return false;
    }

}
