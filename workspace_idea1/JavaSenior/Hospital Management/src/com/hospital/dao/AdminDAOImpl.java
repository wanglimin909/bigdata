package com.hospital.dao;

import com.hospital.bean.Admin;
import com.hospital.dao.util.BaseDAO;

import java.sql.Connection;
import java.util.List;

/**
 * @author wlm.java
 * @create 2023-06-25 18:51
 */
public class AdminDAOImpl extends BaseDAO<Admin> implements AdminDAO {

    @Override
    public int register(Connection connection, Admin admin) {
        String sql = "insert into admin(admin_id,admin_password,admin_name,admin_card,admin_phone) values(?,?,?,?,?)";
        int register = update(connection, sql, admin.getAdminId(), admin.getAdminPassword(), admin.getAdminName(), admin.getAdminCard(), admin.getAdminPhone());
        return register;
    }

    @Override
    public Admin login(Connection connection, String adminId, String adminPassword) {
        String sql = "select admin_id adminId,admin_password adminPassword,admin_name adminName,admin_card adminCard,admin_phone adminPhone from admin where admin_id = ? and admin_password = ?";
        Admin login = getInstance(connection, sql, adminId, adminPassword);
        return login;
    }



    @Override
    public int delete(Connection connection, String adminId) {
        String sql = "delete from admin where admin_id = ?";
        int delete = update(connection, sql, adminId);
        return delete;
    }

    @Override
    public String recover(Connection connection, String adminCard, String adminPhone) {
        String sql = "select admin_password password from admin where admin_card = ? and admin_phone = ?";
        String recover = getValue(connection, sql, adminCard, adminPhone);
        return recover;
    }

    @Override
    public int unsubscribe(Connection connection, String adminCard, String adminPhone) {
        String sql = "delete from admin where admin_card = ? and admin_phone = ?";
        int unsubscribe = update(connection, sql, adminCard, adminPhone);
        return unsubscribe;
    }

    @Override
    public int updateAll(Connection connection, String oldAdminId, Admin admin) {
        String sql = "update admin set admin_id = ? ,admin_password = ? ,admin_name = ? ,admin_card = ? ,admin_phone = ? where admin_id = ?";
        int updateAll = update(connection, sql, admin.getAdminId(),admin.getAdminPassword(),admin.getAdminName(),admin.getAdminCard(),admin.getAdminPhone(), oldAdminId);
        return updateAll;
    }


    @Override
    public Admin getAdminById(Connection connection, String adminId) {
        String sql = "select admin_id adminId,admin_password adminPassword,admin_name adminName,admin_card adminCrd,admin_phone adminPhone from admin where admin_id = ?";
        Admin admin = getInstance(connection, sql, adminId);
        return admin;
    }

    @Override
    public Admin getAdminByCard(Connection connection, String adminCard) {
        String sql = "select admin_id adminId,admin_password adminPassword,admin_name adminName,admin_card adminCard,admin_phone adminPhone from admin where admin_card = ?";
        Admin admin = getInstance(connection, sql, adminCard);
        return admin;
    }

    @Override
    public Admin getAdminByPhone(Connection connection, String adminPhone) {
        String sql = "select admin_id adminId,admin_password adminPassword,admin_name adminName,admin_card adminCard,admin_phone adminPhone from admin where admin_phone = ?";
        Admin admin = getInstance(connection, sql, adminPhone);
        return admin;
    }



    @Override
    public List<Admin> getAdminAll(Connection connection) {
        String sql = "select admin_id adminId,admin_password adminPassword,admin_name adminName,admin_card adminCard,admin_phone adminPhone from admin";
        List<Admin> forList = getForList(connection, sql);
        return forList;
    }


    @Override
    public int clearAll(Connection connection) {
        String sql = "delete from admin";
        int clearAll = update(connection, sql);
        return clearAll;
    }
}
