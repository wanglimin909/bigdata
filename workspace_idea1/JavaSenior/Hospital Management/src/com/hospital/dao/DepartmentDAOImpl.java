package com.hospital.dao;

import com.hospital.bean.Department;
import com.hospital.dao.util.BaseDAO;

import java.sql.Connection;
import java.util.List;

/**
 * @author wlm.java
 * @create 2023-06-26 14:01
 */
public class DepartmentDAOImpl extends BaseDAO<Department> implements DepartmentDAO{


    @Override
    public int insert(Connection connection, Department department) {
        String sql = "insert into department(department_id,department_name,department_address,department_phone,department_chairman) values(?,?,?,?,?)";
        int insert = update(connection, sql, department.getDepartmentId(),department.getDepartmentName(),department.getDepartmentAddress(),department.getDepartmentPhone(),department.getDepartmentChairman());
        return insert;
    }


    @Override
    public int delete(Connection connection, String departmentId) {
        String sql = "delete from department where department_id = ?";
        int delete = update(connection, sql, departmentId);
        return delete;
    }


    @Override
    public int update(Connection connection, String departmentId, String key, Object value) {
        String sql = "update department set " + key + "= ? where department_id = ?";
        int update = update(connection, sql, value, departmentId);
        return update;
    }


    @Override
    public int updateAll(Connection connection, String oldDepartmentId, Department department) {
        String sql = "update department set department_id = ?,department_name = ?,department_address = ?,department_phone = ?,department_chairman = ? where department_id = ?";
        int updateAll = update(connection, sql, department.getDepartmentId(),department.getDepartmentName(),department.getDepartmentAddress(),department.getDepartmentPhone(),department.getDepartmentChairman(),oldDepartmentId);
        return updateAll;
    }



    @Override
    public Department getDepartmentByDepartmentId(Connection connection, String departmentId) {
        String sql = "select department_id departmentId,department_name departmentName,department_address departmentAddress,department_phone departmentPhone,department_chairman departmentChairman from department where department_id = ?";
        Department department = getInstance(connection, sql, departmentId);
        return department;
    }




    @Override
    public List<Department> getDepartmentAll(Connection connection) {
        String sql = "select department_id departmentId,department_name departmentName,department_address departmentAddress,department_phone departmentPhone,department_chairman departmentChairman from department";
        List<Department> forList = getForList(connection, sql);
        return forList;
    }


    @Override
    public Long getCount(Connection connection) {
        String sql = "select count(*) from department";
        Long value = getValue(connection, sql);
        return value;
    }


    @Override
    public int clearAll(Connection connection) {
        String sql = "delete from department";
        int clearAll = update(connection, sql);
        return clearAll;
    }
}
