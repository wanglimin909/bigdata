package com.hospital.dao;

import com.hospital.bean.Pay;

import java.sql.Connection;
import java.util.List;

/**
 * @author wlm.java
 * @create 2023-06-26 14:01
 */
public interface PayDAO {
    /**
     * 将 pay 对象添加到数据库中
     * @param connection
     * @param pay
     * @return
     */

    public abstract int insert(Connection connection, Pay pay);

    /**
     * 根据缴费号，删除缴费记录
     * @param connection
     * @param payId
     * @return
     */

    public abstract int delete(Connection connection, String payId);

    /**
     * 针对内存中的 pay 对象，去修改数据库中指定的缴费记录
     * @param connection
     * @param oldPayId
     * @param pay
     * @return
     */

    public abstract int updateAll(Connection connection, String oldPayId, Pay pay);



    /**
     * 根据缴费号，查询缴费记录
     * @param connection
     * @param payId
     * @return
     */

    public abstract Pay getPayByPayId(Connection connection, String payId);


    /**
     * 查询表中所有记录构成的集合
     * @param connection
     * @return
     */

    public abstract List<Pay> getPayAll(Connection connection);


    /**
     * 查询数据库中 pay 数据总数目
     * @param connection
     * @return
     */

    public abstract Long getCount(Connection connection);

    /**
     * @author Snoopy
     * @Description 清空所有缴费记录
     * @Date
     * @Param
     * @Return
     */

    public abstract int clearAll(Connection connection);
}
