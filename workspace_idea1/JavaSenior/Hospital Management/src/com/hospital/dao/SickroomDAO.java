package com.hospital.dao;

import com.hospital.bean.Sickroom;

import java.sql.Connection;
import java.util.List;

/**
 * @author wlm.java
 * @create 2023-06-26 14:02
 */
public interface SickroomDAO {
    /**
     * 将 sickroom 对象添加到数据库中
     * @param connection
     * @param sickroom
     * @return
     */

    public abstract int insert(Connection connection, Sickroom sickroom);

    /**
     * 根据病房的房号，删除病房信息
     * @param connection
     * @param sickroomId
     * @return
     */

    public abstract int delete(Connection connection, String sickroomId);

    /**
     * 根据房号和字段修改指定病房的指定字段信息
     * @param connection
     * @param sickroomId
     * @param key
     * @param value
     * @return
     */

    public abstract int update(Connection connection, String sickroomId, String key, Object value);

    /**
     * 针对内存中的 sickroom 对象，去修改数据库中指定的病房的全部数据
     * @param connection
     * @param oldSickroomId
     * @param sickroom
     * @return
     */

    public abstract int updateAll(Connection connection, String oldSickroomId, Sickroom sickroom);


    /**
     * 根据病房的房号，查询病房信息
     * @param connection
     * @param sickroomId
     * @return
     */

    public abstract Sickroom getSickroomBySickroomId(Connection connection, String sickroomId);


    /**
     * 查询表中所有记录构成的集合
     * @param connection
     * @return
     */

    public abstract List<Sickroom> getSickroomAll(Connection connection);


    /**
     * 清空所有病房信息
     * @param connection
     * @return
     */

    public abstract int clearAll(Connection connection);}
