package com.hospital.dao;

import com.hospital.bean.Department;

import java.sql.Connection;
import java.util.List;

/**
 * @author wlm.java
 * @create 2023-06-26 14:01
 */
public interface DepartmentDAO {
    /**
     * 将 department 对象添加到数据库中
     * @param connection
     * @param department
     * @return
     */

    public abstract int insert(Connection connection, Department department);

    /**
     * 根据科室的科号，删除科室信息
     * @param connection
     * @param departmentId
     * @return
     */

    public abstract int delete(Connection connection, String departmentId);

    /**
     * 根据科号和字段修改指定科室的指定字段信息
     * @param connection
     * @param departmentId
     * @param key
     * @param value
     * @return
     */

    public abstract int update(Connection connection, String departmentId, String key, Object value);

    /**
     * 针对内存中的 department 对象，去修改数据库中指定的科室的全部数据
     * @param connection
     * @param oldDepartmentId
     * @param department
     * @return
     */

    public abstract int updateAll(Connection connection, String oldDepartmentId, Department department);


    /**
     * 根据科室科号，查询科室信息
     * @param connection
     * @param departmentId
     * @return
     */

    public abstract Department getDepartmentByDepartmentId(Connection connection, String departmentId);


    /**
     * 查询表中所有记录构成的集合
     * @param connection
     * @return
     */

    public abstract List<Department> getDepartmentAll(Connection connection);


    /**
     * 查询数据库中 department 数据总数目
     * @param connection
     * @return
     */

    public abstract Long getCount(Connection connection);

    /**
     * 清空所有科室信息
     * @param connection
     * @return
     */
    public abstract int clearAll(Connection connection);

}
