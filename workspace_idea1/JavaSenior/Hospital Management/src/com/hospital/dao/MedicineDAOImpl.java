package com.hospital.dao;

import com.hospital.bean.Medicine;
import com.hospital.dao.util.BaseDAO;

import java.sql.Connection;
import java.util.List;

/**
 * @author wlm.java
 * @create 2023-06-26 14:04
 */
public class MedicineDAOImpl extends BaseDAO<Medicine> implements MedicineDAO{

    @Override
    public int insert(Connection connection, Medicine medicine) {
        String sql = "insert into medicine(medicine_id,medicine_name,medicine_price,medicine_drugs,medicine_precautions,medicine_use,Patient_id) values(?,?,?,?,?,?,?)";
        int insert = update(connection, sql, medicine.getMedicineId(),medicine.getMedicineName(),medicine.getMedicinePrice(),medicine.getMedicineDrugs(),medicine.getMedicinePrecautions(),medicine.getMedicineUse(),medicine.getPatientId());
        return insert;
    }


    @Override
    public int delete(Connection connection, String medicineId) {
        String sql = "delete from medicine where medicine_id = ?";
        int delete = update(connection, sql, medicineId);
        return delete;
    }


    @Override
    public int update(Connection connection, String medicineId, String key, Object value) {
        String sql = "update medicine set " + key + "= ? where medicine_id = ?";
        int update = update(connection, sql, value, medicineId);
        return update;
    }


    @Override
    public int updateAll(Connection connection, String oldMedicineId, Medicine medicine) {
        String sql = "update medicine set medicine_id = ?,medicine_name = ?,medicine_price = ?,medicine_drugs = ?,medicine_precautions = ?,medicine_use = ?,Patient_id = ? where medicine_id = ?";
        int updateAll = update(connection, sql, medicine.getMedicineId(),medicine.getMedicineName(),medicine.getMedicinePrice(),medicine.getMedicineDrugs(),medicine.getMedicinePrecautions(),medicine.getMedicineUse(),medicine.getPatientId(),oldMedicineId);
        return updateAll;
    }



    @Override
    public Medicine getMedicineByMedicineId(Connection connection, String medicineId) {
        String sql = "select medicine_id medicineId,medicine_name medicineName,medicine_price medicinePrice,medicine_drugs medicineDrugs,medicine_precautions medicinePrecautions,medicine_use medicineUse,Patient_id patientId from medicine where medicine_id = ?";
        Medicine medicine = getInstance(connection, sql, medicineId);
        return medicine;
    }




    @Override
    public List<Medicine> getMedicineAll(Connection connection) {
        String sql = "select medicine_id medicineId,medicine_name medicineName,medicine_price medicinePrice,medicine_drugs medicineDrugs,medicine_precautions medicinePrecautions,medicine_use medicineUse,Patient_id patientId from medicine";
        List<Medicine> forList = getForList(connection, sql);
        return forList;
    }


    @Override
    public Long getCount(Connection connection) {
        String sql = "select count(*) from medicine";
        Long value = getValue(connection, sql);
        return value;
    }


    @Override
    public int clearAll(Connection connection) {
        String sql = "delete from medicine";
        int clearAll = update(connection, sql);
        return clearAll;
    }
}
