package com.hospital.dao;

import com.hospital.bean.Patient;
import com.hospital.dao.util.BaseDAO;

import java.sql.Connection;
import java.util.List;

/**
 * @author wlm.java
 * @create 2023-06-25 19:09
 */
public class PatientDAOImpl extends BaseDAO<Patient> implements PatientDAO {

    @Override
    public int register(Connection connection, Patient patient) {
        String sql = "insert into patient(patient_id,patient_name,patient_sex,patient_age,patient_room,patient_doctor,patient_blood,patient_results,Department_id,Sickroom_id) values(?,?,?,?,?,?,?,?,?,?)";
        int register = update(connection, sql, patient.getPatientId(),patient.getPatientName(),patient.getPatientSex(),patient.getPatientAge(),patient.getPatientRoom(),patient.getPatientDoctor(),patient.getPatientBlood(),patient.getPatientResules(),patient.getDepartmentId(),patient.getSickroomId());
        return register;
    }


    @Override
    public int unsubscribe(Connection connection, String patientId, String patientName) {
        String sql = "delete from patient where patient_id = ? and patient_name = ?";
        int unsubscribe = update(connection, sql, patientId, patientName);
        return unsubscribe;
    }

    @Override
    public int updateAll(Connection connection, String oldPatientId, Patient patient) {
        String sql = "update patient set patient_id = ? ,patient_name = ? ,patient_sex = ? ,patient_age = ? ,patient_room = ? ,patient_doctor = ? ,patient_blood = ? ,patient_results = ? ,Department_id = ? ,Sickroom_id = ?  where patient_id = ?";
        int updateAll = update(connection, sql, patient.getPatientId(),patient.getPatientName(),patient.getPatientSex(),patient.getPatientAge(),patient.getPatientRoom(),patient.getPatientDoctor(),patient.getPatientBlood(),patient.getPatientResules(),patient.getDepartmentId(),patient.getSickroomId(), oldPatientId);
        return updateAll;
    }

    @Override
    public Patient getPatientById(Connection connection, String patientId) {
        String sql = "select patient_id patientId,patient_name patientName,patient_sex patientSex,patient_age patientAge,patient_room patientRoom,patient_doctor patientDoctor,patient_blood patientBlood,patient_results patientResults,Department_id departmentId,Sickroom_id sickroomId from patient where patient_id = ?";
        Patient patient = getInstance(connection, sql, patientId);
        return patient;
    }

    @Override
    public Patient getPatientByName(Connection connection, String patientName) {
        String sql = "select patient_id patientId,patient_name patientName,patient_sex patientSex,patient_age patientAge,patient_room patientRoom,patient_doctor patientDoctor,patient_blood patientBlood,patient_results patientResults,Department_id departmentId,Sickroom_id sickroomId from patient where patient_name = ?";
        Patient patient = getInstance(connection, sql, patientName);
        return patient;
    }

    @Override
    public List<Patient> getPatientAll(Connection connection) {
        String sql = "select patient_id patientId,patient_name patientName,patient_sex patientSex,patient_age patientAge,patient_room patientRoom,patient_doctor patientDoctor,patient_blood patientBlood,patient_results patientResults,Department_id departmentId,Sickroom_id sickroomId from patient";
        List<Patient> forList = getForList(connection, sql);
        return forList;
    }

    @Override
    public Long getCount(Connection connection) {
        String sql = "select count(*) from patient";
        Long value = getValue(connection, sql);
        return value;
    }

    @Override
    public int clearAll(Connection connection) {
        String sql = "delete from patient";
        int clearAll = update(connection, sql);
        return clearAll;
    }


}
