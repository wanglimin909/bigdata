package com.hospital.dao;

import com.hospital.bean.Medicine;

import java.sql.Connection;
import java.util.List;

/**
 * @author wlm.java
 * @create 2023-06-26 14:03
 */
public interface MedicineDAO {
    /**
     * 将 medicine 对象添加到数据库中
     * @param connection
     * @param medicine
     * @return
     */

    public abstract int insert(Connection connection, Medicine medicine);

    /**
     * 根据药品的药品号，删除药品信息
     * @param connection
     * @param medicineId
     * @return
     */

    public abstract int delete(Connection connection, String medicineId);

    /**
     * 根据药品号和字段修改指定药品的指定字段信息
     * @param connection
     * @param medicineId
     * @param key
     * @param value
     * @return
     */

    public abstract int update(Connection connection, String medicineId, String key, Object value);

    /**
     * 针对内存中的 medicine 对象，去修改数据库中指定的药品的全部数据
     * @param connection
     * @param oldMedicineId
     * @param medicine
     * @return
     */

    public abstract int updateAll(Connection connection, String oldMedicineId, Medicine medicine);


    /**
     * 根据药品号，查询药品信息
     * @param connection
     * @param medicineId
     * @return
     */

    public abstract Medicine getMedicineByMedicineId(Connection connection, String medicineId);


    /**
     * 查询表中所有记录构成的集合
     * @param connection
     * @return
     */

    public abstract List<Medicine> getMedicineAll(Connection connection);


    /**
     * 查询数据库中 medicine 数据总数目
     * @param connection
     * @return
     */

    public abstract Long getCount(Connection connection);

    /**
     * @author Snoopy
     * @Description 清空所有药品信息
     * @Date
     * @Param
     * @Return
     */

    public abstract int clearAll(Connection connection);
}
