package com.hospital.dao;

import com.hospital.bean.Patient;

import java.sql.Connection;
import java.util.List;

/**
 * @author wlm.java
 * @create 2023-06-25 19:08
 */
public interface PatientDAO {

    /**
     * 注册功能（向数据库插入患者信息）
     * @param connection
     * @param patient
     * @return
     */

    public abstract int register(Connection connection, Patient patient);


    /**
     * 注销账户
     * @param connection
     * @param patientId
     * @param patientName
     * @return
     */

    public abstract int unsubscribe(Connection connection, String patientId, String patientName);


    /**
     * 针对内存中的 patient 对象，去修改数据库中指定的患者的全部数据
     * @param connection
     * @param oldPatientId
     * @param patient
     * @return
     */

    public abstract int updateAll(Connection connection, String oldPatientId, Patient patient);


    /**
     * 根据id，获取病人的信息
     * @param connection
     * @param patientId
     * @return
     */

    public abstract Patient getPatientById(Connection connection, String patientId);

    /**
     * 根据name，获取病人的信息
     * @param connection
     * @param patientName
     * @return
     */

    public abstract Patient getPatientByName(Connection connection, String patientName);

    /**
     * 查询表中所有记录构成的集合
     * @param connection
     * @return
     */

    public abstract List<Patient> getPatientAll(Connection connection);

    /**
     * 查询数据库中 patient 数据总数目
     * @param connection
     * @return
     */

    public abstract Long getCount(Connection connection);

    /**
     * 清空所有病人信息
     * @param connection
     * @return
     */

    public abstract int clearAll(Connection connection);




}