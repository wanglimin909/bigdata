package com.hospital.dao;

import com.hospital.bean.Admin;

import java.sql.Connection;
import java.util.List;

/**
 * @author wlm.java
 * @create 2023-06-25 18:44
 */
public interface AdminDAO {

    /**
     * 注册功能（向数据库插入管理者信息）
     * @param connection
     * @param admin
     * @return
     */

    public abstract int register(Connection connection, Admin admin);

    /**
     * 登录功能 （验证密码和账号）
     * @param connection
     * @param adminId
     * @param adminPassword
     * @return
     */


    public abstract Admin login(Connection connection, String adminId, String adminPassword);


    /**
     * 找回密码
     * @param connection
     * @param adminCard
     * @param adminPhone
     * @return
     */

    public abstract String recover(Connection connection, String adminCard, String adminPhone);

    /**
     * 针对内存中的 admin 对象，去修改数据库中指定的管理员的全部数据
     * @param connection
     * @param oldAdminId
     * @param admin
     * @return
     */

    public abstract int updateAll(Connection connection, String oldAdminId, Admin admin);

    /**
     * 注销账户
     * @param connection
     * @param adminCard
     * @param adminPhone
     * @return
     */

    public abstract int unsubscribe(Connection connection, String adminCard, String adminPhone);

    /**
     * 根据管理员id，删除管理员信息
     * @param connection
     * @param adminId
     * @return
     */

    public abstract int delete(Connection connection, String adminId);


    /**
     * 根据管理员id，获取管理员的信息
     * @param connection
     * @param adminId
     * @return
     */

    public abstract Admin getAdminById(Connection connection, String adminId);

    /**
     * 根据管理员身份证号，获取管理员的信息
     * @param connection
     * @param adminCard
     * @return
     */

    public abstract Admin getAdminByCard(Connection connection, String adminCard);

    /**
     * 根据管理员手机号，获取管理员的信息
     * @param connection
     * @param adminPhone
     * @return
     */

    public abstract Admin getAdminByPhone(Connection connection, String adminPhone);

    /**
     * 查询表中所有记录构成的集合
     * @param connection
     * @return
     */

    public abstract List<Admin> getAdminAll(Connection connection);

    /**
     * 清空所有系统管理员信息
     * @param connection
     * @return
     */

    public abstract int clearAll(Connection connection);

}

