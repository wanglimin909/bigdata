package com.hospital.dao;

import com.hospital.bean.Doctor;
import com.hospital.dao.util.BaseDAO;

import java.sql.Connection;
import java.util.List;

/**
 * @author wlm.java
 * @create 2023-06-25 16:24
 */
public class DoctorDAOImpl extends BaseDAO<Doctor> implements DoctorDAO {

    @Override
    public int register(Connection connection, Doctor doctor) {
        String sql = "insert into doctor(doctor_id,doctor_password,doctor_name,doctor_sex,doctor_age,doctor_phone,doctor_title,doctor_department,Department_id) values(?,?,?,?,?,?,?,?,?)";
        int register = update(connection, sql, doctor.getDoctorId(),doctor.getDoctorPassword(),doctor.getDoctorName(),doctor.getDoctorSex(),doctor.getDoctorAge(),doctor.getDoctorPhone(),doctor.getDoctorTitle(),doctor.getDoctorDepartment(),doctor.getDepartmentId());
        return register;
    }

    @Override
    public Doctor login(Connection connection, String doctorId, String doctorPassword) {
        String sql = "select doctor_id doctorId,doctor_password doctorPassword,doctor_name doctorName,doctor_sex doctorSex,doctor_age doctorAge,doctor_phone doctorPhone,doctor_title doctorTitle,doctor_department doctorDepartment,Department_id departmentId from doctor where doctor_id = ? and doctor_password = ?";
        Doctor login = getInstance(connection, sql, doctorId, doctorPassword);
        return login;
    }

    @Override
    public String recover(Connection connection,String doctorId, String doctorPhone) {
        String sql = "select doctor_password doctorPassword from doctor where doctor_id = ? and doctor_phone = ?";
        String recover = getValue(connection, sql, doctorId, doctorPhone);
        return recover;
    }

    @Override
    public int delete(Connection connection, String doctorId) {
        String sql = "delete from doctor where doctor_id = ?";
        int delete = update(connection, sql, doctorId);
        return delete;
    }

    @Override
    public int updateAll(Connection connection, String oldDoctorId, Doctor doctor) {
        String sql = "update doctor set doctor_id = ? ,doctor_password = ? ,doctor_name = ? ,doctor_sex = ? ,doctor_age = ? ,doctor_phone = ? ,doctor_title = ? ,doctor_department = ? ,Department_id = ?  where doctor_id = ?";
        int updateAll = update(connection, sql, doctor.getDoctorId(),doctor.getDoctorPassword(),doctor.getDoctorName(),doctor.getDoctorSex(),doctor.getDoctorAge(),doctor.getDoctorPhone(),doctor.getDoctorTitle(),doctor.getDoctorDepartment(),doctor.getDepartmentId(), oldDoctorId);
        return updateAll;
    }


    @Override
    public int unsubscribe(Connection connection, String doctorId, String doctorPassword) {
        String sql = "delete from doctor where doctor_id = ? and doctor_password = ?";
        int unsubscribe = update(connection, sql, doctorId, doctorPassword);
        return unsubscribe;
    }

    @Override
    public Doctor getDoctorById(Connection connection, String doctorId) {
        String sql = "select doctor_id doctorId,doctor_password doctorPassword,doctor_name doctorName,doctor_sex doctorSex,doctor_age doctorAge,doctor_phone doctorPhone,doctor_title doctorTitle,doctor_department doctorDepartment,Department_id departmentId from doctor where doctor_id = ?";
        Doctor doctor = getInstance(connection, sql, doctorId);
        return doctor;
    }

    @Override
    public Doctor getDoctorByPhone(Connection connection, String doctorPhone) {
        String sql = "select doctor_id doctorId,doctor_password doctorPassword,doctor_name doctorName,doctor_sex doctorSex,doctor_age doctorAge,doctor_phone doctorPhone,doctor_title doctor Title,doctor_department doctorDepartment,Department_id departmentId from doctor where doctor_phone = ?";
        Doctor doctor = getInstance(connection, sql, doctorPhone);
        return doctor;
    }

    @Override
    public List<Doctor> getDoctorAll(Connection connection) {
        String sql = "select doctor_id doctorId,doctor_password doctorPassword,doctor_name doctorName,doctor_sex doctorSex,doctor_age doctorAge,doctor_phone doctorPhone,doctor_title doctorTitle,doctor_department doctorDepartment,Department_id departmentId from doctor";
        List<Doctor> forList = getForList(connection, sql);
        return forList;
    }


    @Override
    public int clearAll(Connection connection) {
        String sql = "delete from doctor";
        int clearAll = update(connection, sql);
        return clearAll;
    }
}