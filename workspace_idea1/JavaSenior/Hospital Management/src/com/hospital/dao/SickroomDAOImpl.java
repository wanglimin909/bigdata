package com.hospital.dao;

import com.hospital.bean.Sickroom;
import com.hospital.dao.util.BaseDAO;

import java.sql.Connection;
import java.util.List;

/**
 * @author wlm.java
 * @create 2023-06-26 14:02
 */
public class SickroomDAOImpl extends BaseDAO<Sickroom> implements SickroomDAO{


    @Override
    public int insert(Connection connection, Sickroom sickroom) {
        String sql = "insert into sickroom(sickroom_id,sickroom_bed,sickroom_address,sickroom_state,Department_id) values(?,?,?,?,?)";
        int insert = update(connection, sql,sickroom.getSickroomId(),sickroom.getSickroomBed(),sickroom.getSickroomAddress(),sickroom.getSickroomState(),sickroom.getDepartmentId());
        return insert;
    }


    @Override
    public int delete(Connection connection, String sickroomId) {
        String sql = "delete from sickroom where sickroom_id = ?";
        int delete = update(connection, sql, sickroomId);
        return delete;
    }


    @Override
    public int update(Connection connection, String sickroomId, String key, Object value) {
        String sql = "update sickroom set " + key + "= ? where sickroom_id = ?";
        int update = update(connection, sql, value, sickroomId);
        return update;
    }


    @Override
    public int updateAll(Connection connection, String oldSickroomId, Sickroom sickroom) {
        String sql = "update sickroom set sickroom_id = ?,sickroom_bed = ?,sickroom_address = ?,sickroom_state = ?,Department_id = ? where sickroom_id = ?";
        int updateAll = update(connection, sql,sickroom.getSickroomId(),sickroom.getSickroomBed(),sickroom.getSickroomAddress(),sickroom.getSickroomState(),sickroom.getDepartmentId(),oldSickroomId);
        return updateAll;
    }



    @Override
    public Sickroom getSickroomBySickroomId(Connection connection, String sickroomId) {
        String sql = "select sickroom_id sickroomId,sickroom_bed sickroomBed,sickroom_address sickroomAddress,sickroom_state sickroomState,Department_id departmentId from sickroom where sickroom_id = ?";
        Sickroom sickroom = getInstance(connection, sql, sickroomId);
        return sickroom;
    }




    @Override
    public List<Sickroom> getSickroomAll(Connection connection) {
        String sql = "select sickroom_id sickroomId,sickroom_bed sickroomBed,sickroom_address sickroomAddress,sickroom_state sickroomState,Department_id departmentId from sickroom";
        List<Sickroom> forList = getForList(connection, sql);
        return forList;
    }



    @Override
    public int clearAll(Connection connection) {
        String sql = "delete from sickroom";
        int clearAll = update(connection, sql);
        return clearAll;
    }
}
