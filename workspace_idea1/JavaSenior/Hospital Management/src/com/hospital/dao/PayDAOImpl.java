package com.hospital.dao;

import com.hospital.bean.Pay;
import com.hospital.dao.util.BaseDAO;

import java.sql.Connection;
import java.util.List;

/**
 * @author wlm.java
 * @create 2023-06-26 14:02
 */
public class PayDAOImpl extends BaseDAO<Pay> implements PayDAO{

    @Override
    public int insert(Connection connection, Pay pay) {
        String sql = "insert into pay(pay_id,pay_name,pay_standard,pay_actual,pay_type,Department_id) values(?,?,?,?,?,?)";
        int insert = update(connection, sql, pay.getPayId(),pay.getPayName(),pay.getPayStandard(),pay.getPayActual(),pay.getPayType(),pay.getDepartmentId());
        return insert;
    }


    @Override
    public int delete(Connection connection, String payId) {
        String sql = "delete from pay where pay_id = ?";
        int delete = update(connection, sql, payId);
        return delete;
    }

    @Override
    public int updateAll(Connection connection, String oldPayId, Pay pay) {
        String sql = "update pay set pay_id = ? ,pay_name = ? ,pay_standard = ? ,pay_actual = ? ,pay_type = ? ,Department_id = ? where pay_id = ?";
        int updateAll = update(connection, sql, pay.getPayId(),pay.getPayName(),pay.getPayStandard(),pay.getPayActual(),pay.getPayType(),pay.getDepartmentId(), oldPayId);
        return updateAll;
    }



    @Override
    public Pay getPayByPayId(Connection connection, String payId) {
        String sql = "select pay_id payId,pay_name payName,pay_standard payStandard,pay_actual payActual,pay_type payType,Department_id departmentId from pay where pay_id = ?";
        Pay pay = getInstance(connection, sql, payId);
        return pay;
    }




    @Override
    public List<Pay> getPayAll(Connection connection) {
        String sql = "select pay_id payId,pay_name payName,pay_standard payStandard,pay_actual payActual,pay_type payType,Department_id departmentId from pay";
        List<Pay> forList = getForList(connection, sql);
        return forList;
    }


    @Override
    public Long getCount(Connection connection) {
        String sql = "select count(*) from pay";
        Long value = getValue(connection, sql);
        return value;
    }


    @Override
    public int clearAll(Connection connection) {
        String sql = "delete from pay";
        int clearAll = update(connection, sql);
        return clearAll;
    }
}

