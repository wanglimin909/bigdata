package com.hospital.dao;

import com.hospital.bean.Doctor;

import java.sql.Connection;
import java.util.List;

/**
 * @author wlm.java
 * @create 2023-06-25 16:08
 */
public interface DoctorDAO {

    /**
     * 注册功能（向数据库插入医生信息）
     * @param connection
     * @param doctor
     * @return
     */

    public abstract int register(Connection connection, Doctor doctor);

    /**
     * 登录功能 医生（验证密码和账号）
     * @param connection
     * @param doctorId
     * @param doctorPassword
     * @return
     */

    public abstract Doctor login(Connection connection, String doctorId, String doctorPassword);


    /**
     * 找回密码
     * @param connection
     * @param doctorId
     * @param doctorPhone
     * @return
     */

    public abstract String recover(Connection connection, String doctorId, String doctorPhone);

    /**
     * 针对内存中的 doctor 对象，去修改数据库中指定的医生的全部数据
     * @param connection
     * @param oldDoctorId
     * @param doctor
     * @return
     */

    public abstract int updateAll(Connection connection, String oldDoctorId, Doctor doctor);

    /**
     * 注销账户
     * @param connection
     * @param doctorId
     * @param doctorPhone
     * @return
     */

    public abstract int unsubscribe(Connection connection, String doctorId, String doctorPhone);


    /**
     * 根据医生id号，获取医生的信息
     * @param connection
     * @param doctorId
     * @return
     */

    public abstract Doctor getDoctorById(Connection connection, String doctorId);

    /**
     * 根据医生id，删除医生信息
     * @param connection
     * @param doctorId
     * @return
     */
    public abstract int delete(Connection connection, String doctorId);

    /**
     * 根据医生电话号号，获取医生的信息
     * @param connection
     * @param doctorPhone
     * @return
     */

    public abstract Doctor getDoctorByPhone(Connection connection, String doctorPhone);

    /**
     * 查询表中所有记录构成的集合
     * @param connection
     * @return
     */

    public abstract List<Doctor> getDoctorAll(Connection connection);

    /**
     * 清空所有医生信息
     * @param connection
     * @return
     */

    public abstract int clearAll(Connection connection);


}
