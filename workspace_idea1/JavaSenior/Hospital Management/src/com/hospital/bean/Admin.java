package com.hospital.bean;

/**
 * @author wlm.java
 * @create 2023-06-25 18:34
 */
public class Admin {
    private String id;
    private String adminId;
    private String adminPassword;
    private String adminName;
    private String adminCard;
    private String adminPhone;

    public Admin() {
    }

    public Admin(String id, String adminId, String adminPassword, String adminName, String adminCard, String adminPhone) {
        this.id = id;
        this.adminId = adminId;
        this.adminPassword = adminPassword;
        this.adminName = adminName;
        this.adminCard = adminCard;
        this.adminPhone = adminPhone;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getAdminPassword() {
        return adminPassword;
    }

    public void setAdminPassword(String adminPassword) {
        this.adminPassword = adminPassword;
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    public String getAdminCard() {
        return adminCard;
    }

    public void setAdminCard(String adminCard) {
        this.adminCard = adminCard;
    }

    public String getAdminPhone() {
        return adminPhone;
    }

    public void setAdminPhone(String adminPhone) {
        this.adminPhone = adminPhone;
    }

    @Override
    public String toString() {
        return "Admin{" +
                "id='" + id + '\'' +
                ", adminId='" + adminId + '\'' +
                ", adminPassword='" + adminPassword + '\'' +
                ", adminName='" + adminName + '\'' +
                ", adminCard='" + adminCard + '\'' +
                ", adminPhone='" + adminPhone + '\'' +
                '}';
    }
}
