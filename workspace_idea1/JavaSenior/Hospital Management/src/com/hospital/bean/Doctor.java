package com.hospital.bean;

/**
 * @author wlm.java
 * @create 2023-06-25 13:46
 */
public class Doctor {
    private String doctorId;
    private String doctorPassword;
    private String doctorName;
    private String doctorSex;
    private String doctorAge;
    private String doctorPhone;
    private String doctorTitle;
    private String doctorDepartment;
    private String DepartmentId;

    public Doctor() {
    }

    public Doctor(String doctorId, String doctorPassword, String doctorName, String doctorSex, String doctorAge, String doctorPhone, String doctorTitle, String doctorDepartment, String departmentId) {
        this.doctorId = doctorId;
        this.doctorPassword = doctorPassword;
        this.doctorName = doctorName;
        this.doctorSex = doctorSex;
        this.doctorAge = doctorAge;
        this.doctorPhone = doctorPhone;
        this.doctorTitle = doctorTitle;
        this.doctorDepartment = doctorDepartment;
        this.DepartmentId = departmentId;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getDoctorPassword() {
        return doctorPassword;
    }

    public void setDoctorPassword(String doctorPassword) {
        this.doctorPassword = doctorPassword;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getDoctorSex() {
        return doctorSex;
    }

    public void setDoctorSex(String doctorSex) {
        this.doctorSex = doctorSex;
    }

    public String getDoctorAge() {
        return doctorAge;
    }

    public void setDoctorAge(String doctorAge) {
        this.doctorAge = doctorAge;
    }

    public String getDoctorPhone() {
        return doctorPhone;
    }

    public void setDoctorPhone(String doctorPhone) {
        this.doctorPhone = doctorPhone;
    }

    public String getDoctorTitle() {
        return doctorTitle;
    }

    public void setDoctorTitle(String doctorTitle) {
        this.doctorTitle = doctorTitle;
    }

    public String getDoctorDepartment() {
        return doctorDepartment;
    }

    public void setDoctorDepartment(String doctorDepartment) {
        this.doctorDepartment = doctorDepartment;
    }

    public String getDepartmentId() {
        return DepartmentId;
    }

    public void setDepartment(String departmentId) {
        DepartmentId = departmentId;
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "doctorId='" + doctorId + '\'' +
                ", doctorPassword='" + doctorPassword + '\'' +
                ", doctorName='" + doctorName + '\'' +
                ", doctorSex='" + doctorSex + '\'' +
                ", doctorAge='" + doctorAge + '\'' +
                ", doctorPhone='" + doctorPhone + '\'' +
                ", doctorTitle='" + doctorTitle + '\'' +
                ", doctorDepartment='" + doctorDepartment + '\'' +
                ", Department='" + DepartmentId + '\'' +
                '}';
    }
}