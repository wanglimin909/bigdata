package com.hospital.bean;

/**
 * @author wlm.java
 * @create 2023-06-25 13:52
 */
public class Patient {
    private String patientId;
    private String patientName;
    private String patientSex;
    private String patientAge;
    private String patientRoom;
    private String patientDoctor;
    private String patientBlood;
    private String patientResules;
    private String DepartmentId;
    private String SickroomId;

    public Patient() {
    }

    public Patient(String patientId, String patientName, String patientSex, String patientAge, String patientRoom, String patientDoctor, String patientBlood, String patientResules, String departmentId, String sickroomId) {
        this.patientId = patientId;
        this.patientName = patientName;
        this.patientSex = patientSex;
        this.patientAge = patientAge;
        this.patientRoom = patientRoom;
        this.patientDoctor = patientDoctor;
        this.patientBlood = patientBlood;
        this.patientResules = patientResules;
        this.DepartmentId = departmentId;
        this.SickroomId = sickroomId;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientSex() {
        return patientSex;
    }

    public void setPatientSex(String patientSex) {
        this.patientSex = patientSex;
    }

    public String getPatientAge() {
        return patientAge;
    }

    public void setPatientAge(String patientAge) {
        this.patientAge = patientAge;
    }

    public String getPatientRoom() {
        return patientRoom;
    }

    public void setPatientRoom(String patientRoom) {
        this.patientRoom = patientRoom;
    }

    public String getPatientDoctor() {
        return patientDoctor;
    }

    public void setPatientDoctor(String patientDoctor) {
        this.patientDoctor = patientDoctor;
    }

    public String getPatientBlood() {
        return patientBlood;
    }

    public void setPatientBlood(String patientBlood) {
        this.patientBlood = patientBlood;
    }

    public String getPatientResules() {
        return patientResules;
    }

    public void setPatientResules(String patientResules) {
        this.patientResules = patientResules;
    }

    public String getDepartmentId() {
        return DepartmentId;
    }

    public void setDepartmentId(String departmentId) {
        DepartmentId = departmentId;
    }

    public String getSickroomId() {
        return SickroomId;
    }

    public void setSickroomId(String sickroomId) {
        SickroomId = sickroomId;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "patientId='" + patientId + '\'' +
                ", patientName='" + patientName + '\'' +
                ", patientSex='" + patientSex + '\'' +
                ", patientAge='" + patientAge + '\'' +
                ", patientRoom='" + patientRoom + '\'' +
                ", patientDoctor='" + patientDoctor + '\'' +
                ", patientBlood='" + patientBlood + '\'' +
                ", patientResules='" + patientResules + '\'' +
                ", DepartmentId='" + DepartmentId + '\'' +
                ", SickroomId='" + SickroomId + '\'' +
                '}';
    }
}
