package com.hospital.bean;

/**
 * @author wlm.java
 * @create 2023-06-25 13:40
 */
public class Department {
    private String departmentId;
    private String departmentName;
    private String departmentAddress;
    private String departmentPhone;
    private String departmentChairman;

    public Department() {
    }

    public Department(String departmentId, String departmentName, String departmentAddress, String departmentPhone, String departmentChairman) {
        this.departmentId = departmentId;
        this.departmentName = departmentName;
        this.departmentAddress = departmentAddress;
        this.departmentPhone = departmentPhone;
        this.departmentChairman = departmentChairman;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getDepartmentAddress() {
        return departmentAddress;
    }

    public void setDepartmentAddress(String departmentAddress) {
        this.departmentAddress = departmentAddress;
    }

    public String getDepartmentPhone() {
        return departmentPhone;
    }

    public void setDepartmentPhone(String departmentPhone) {
        this.departmentPhone = departmentPhone;
    }

    public String getDepartmentChairman() {
        return departmentChairman;
    }

    public void setDepartmentChairman(String departmentChairman) {
        this.departmentChairman = departmentChairman;
    }

    @Override
    public String toString() {
        return "Department{" +
                "departmentId='" + departmentId + '\'' +
                ", departmentName='" + departmentName + '\'' +
                ", departmentAddress='" + departmentAddress + '\'' +
                ", departmentPhone='" + departmentPhone + '\'' +
                ", departmentChairman='" + departmentChairman + '\'' +
                '}';
    }
}
