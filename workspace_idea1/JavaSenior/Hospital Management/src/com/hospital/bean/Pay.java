package com.hospital.bean;

/**
 * @author wlm.java
 * @create 2023-06-25 18:37
 */
public class Pay {
    private String payId;
    private String payName;
    private String payStandard;
    private String payActual;
    private String payType;
    private String DepartmentId;

    public Pay() {
    }

    public Pay(String payId, String payName, String payStandard, String payActual, String payType, String departmentId) {
        this.payId = payId;
        this.payName = payName;
        this.payStandard = payStandard;
        this.payActual = payActual;
        this.payType = payType;
        DepartmentId = departmentId;
    }

    public String getPayId() {
        return payId;
    }

    public void setPayId(String payId) {
        this.payId = payId;
    }

    public String getPayName() {
        return payName;
    }

    public void setPayName(String payName) {
        this.payName = payName;
    }

    public String getPayStandard() {
        return payStandard;
    }

    public void setPayStandard(String payStandard) {
        this.payStandard = payStandard;
    }

    public String getPayActual() {
        return payActual;
    }

    public void setPayActual(String payActual) {
        this.payActual = payActual;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getDepartmentId() {
        return DepartmentId;
    }

    public void setDepartmentId(String departmentId) {
        DepartmentId = departmentId;
    }

    @Override
    public String toString() {
        return "Pay{" +
                "payId='" + payId + '\'' +
                ", payName='" + payName + '\'' +
                ", payStandard='" + payStandard + '\'' +
                ", payActual='" + payActual + '\'' +
                ", payType='" + payType + '\'' +
                ", DepartmentId='" + DepartmentId + '\'' +
                '}';
    }
}
