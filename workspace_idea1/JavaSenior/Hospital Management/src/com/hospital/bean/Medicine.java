package com.hospital.bean;

/**
 * @author wlm.java
 * @create 2023-06-26 13:20
 */
public class Medicine {
    private String medicineId;
    private String medicineName;
    private String medicinePrice;
    private String medicineDrugs;
    private String medicinePrecautions;
    private String medicineUse;
    private String patientId;

    public Medicine() {
    }

    public Medicine(String medicineId, String medicineName, String medicinePrice, String medicineDrugs, String medicinePrecautions, String medicineUse, String patientId) {
        this.medicineId = medicineId;
        this.medicineName = medicineName;
        this.medicinePrice = medicinePrice;
        this.medicineDrugs = medicineDrugs;
        this.medicinePrecautions = medicinePrecautions;
        this.medicineUse = medicineUse;
        this.patientId = patientId;
    }

    public String getMedicineId() {
        return medicineId;
    }

    public void setMedicineId(String medicineId) {
        this.medicineId = medicineId;
    }

    public String getMedicineName() {
        return medicineName;
    }

    public void setMedicineName(String medicineName) {
        this.medicineName = medicineName;
    }

    public String getMedicinePrice() {
        return medicinePrice;
    }

    public void setMedicinePrice(String medicinePrice) {
        this.medicinePrice = medicinePrice;
    }

    public String getMedicineDrugs() {
        return medicineDrugs;
    }

    public void setMedicineDrugs(String medicineDrugs) {
        this.medicineDrugs = medicineDrugs;
    }

    public String getMedicinePrecautions() {
        return medicinePrecautions;
    }

    public void setMedicinePrecautions(String medicinePrecautions) {
        this.medicinePrecautions = medicinePrecautions;
    }

    public String getMedicineUse() {
        return medicineUse;
    }

    public void setMedicineUse(String medicineUse) {
        this.medicineUse = medicineUse;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    @Override
    public String toString() {
        return "Medicine{" +
                "medicineId='" + medicineId + '\'' +
                ", medicineName='" + medicineName + '\'' +
                ", medicinePrice='" + medicinePrice + '\'' +
                ", medicineDrugs='" + medicineDrugs + '\'' +
                ", medicinePrecautions='" + medicinePrecautions + '\'' +
                ", medicineUse='" + medicineUse + '\'' +
                ", patientId='" + patientId + '\'' +
                '}';
    }
}
