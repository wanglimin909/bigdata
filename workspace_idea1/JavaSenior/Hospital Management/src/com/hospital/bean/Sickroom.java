package com.hospital.bean;

/**
 * @author wlm.java
 * @create 2023-06-25 13:55
 */
public class Sickroom {
    private String sickroomId;
    private String sickroomBed;
    private String sickroomAddress;
    private String sickroomState;
    private String DepartmentId;

    public Sickroom() {
    }

    public Sickroom(String sickroomId, String sickroomBed, String sickroomAddress, String sickroomState, String departmentId) {
        this.sickroomId = sickroomId;
        this.sickroomBed = sickroomBed;
        this.sickroomAddress = sickroomAddress;
        this.sickroomState = sickroomState;
        DepartmentId = departmentId;
    }

    public String getSickroomId() {
        return sickroomId;
    }

    public void setSickroomId(String sickroomId) {
        this.sickroomId = sickroomId;
    }

    public String getSickroomBed() {
        return sickroomBed;
    }

    public void setSickroomBed(String sickroomBed) {
        this.sickroomBed = sickroomBed;
    }

    public String getSickroomAddress() {
        return sickroomAddress;
    }

    public void setSickroomAddress(String sickroomAddress) {
        this.sickroomAddress = sickroomAddress;
    }

    public String getSickroomState() {
        return sickroomState;
    }

    public void setSickroomState(String sickroomState) {
        this.sickroomState = sickroomState;
    }

    public String getDepartmentId() {
        return DepartmentId;
    }

    public void setDepartmentId(String departmentId) {
        DepartmentId = departmentId;
    }

    @Override
    public String toString() {
        return "Sickroom{" +
                "sickroomId='" + sickroomId + '\'' +
                ", sickroomBed='" + sickroomBed + '\'' +
                ", sickroomAddress='" + sickroomAddress + '\'' +
                ", sickroomState='" + sickroomState + '\'' +
                ", DepartmentId='" + DepartmentId + '\'' +
                '}';
    }
}
