package com.hospital.view;

/**
 * @author wlm.java
 * @create 2023-06-26 20:30
 */


import com.hospital.bean.*;
import com.hospital.service.*;
import com.hospital.tools.Tools;

import java.util.List;


public class HospitalView {

    /*
    临时保存当前登录的管理员的信息
     */
    private String tempAdminId = null;
    private String tempAdminName = null;
    private String tempAdminCard = null;
    private String tempAdminPhone = null;

    /*
     临时保存当前登录的医生的信息
     */
    private String tempDoctorId = null;
    private String tempDoctorName = null;
    private String tempDoctorSex = null;
    private String tempDoctorAge = null;
    private String tempDoctorPhone = null;
    private String tempDoctorTitle = null;
    private String tempDoctorDepartment = null;
    private String tempDepartmentId = null;

    /*
    临时保存当前的病人的信息
    */
    private String tempPatientId = null;
    private String tempPatientName = null;



    AdminService adminService = new AdminService();
    DoctorService doctorService = new DoctorService();
    PatientService patientService = new PatientService();
    DepartmentService departmentService = new DepartmentService();
    MedicineService medicineService = new MedicineService();
    SickroomService sickroomService = new SickroomService();
    PayService payService = new PayService();

    /**
     * 登录主菜单
     */
    public void enterRegisterMenu() {
        // 是否退出系统的标识
        boolean flag = true;
        while (flag) {
            System.out.println("------------------------------------【医院管理系统】---------------------------------------");
            System.out.println("请输入您的身份：");
            System.out.println("             1:系统管理员       2：医生          3：患者       4：关闭系统                   ");
            System.out.println("------------------------------------------------------------------------------------------");
            System.out.println("请输入你的选择：");
            char choice = Tools.readRegisterMenuSelection();
            switch (choice) {
                case '1':
                    enterAdminMenu();
                    break;
                case '2':
                    enterDoctorMenu();
                    break;
                case '3':
                    enterPatientMenu();
                    break;
                case '4':
                    System.out.println("是否要关闭系统(Y/N)？");
                    char exit = Tools.readConfirmSelection();
                    if (exit == 'Y' || exit == 'y') {
                        flag = false;
                        System.out.println("成功关闭系统");
                    }
                    break;
            }
        }
    }

    public void enterAdminMenu() {
        // 是否退出系统的标识
        boolean flag = true;
        while (flag) {
            System.out.println("------------------------------------【欢迎系统管理员】-------------------------------------");
            System.out.println("                   1:登录        2：注册          3：忘记密码     4:退出                     ");
            System.out.println("------------------------------------------------------------------------------------------");
            System.out.println("请输入你的选择：");
            char choice = Tools.readRegisterMenuSelection();
            switch (choice) {
                case '1':
                    Adminlogin();
                    break;
                case '2':
                    Adminregister();
                    break;
                case '3':
                    Adminrecover();
                    break;
                case '4':
                    enterRegisterMenu();
                    break;
            }
        }
    }


    public void enterAdminMainMenu() {
        boolean flag = true;
        while (flag) {
            System.out.println("------------------------------------【系统管理员】---------------------------------------");
            System.out.println("\t\t\t\t\t\t\t\t\t1.管理员信息管理");
            System.out.println("\t\t\t\t\t\t\t\t\t2.医生信息管理");
            System.out.println("\t\t\t\t\t\t\t\t\t3.科室信息管理");
            System.out.println("\t\t\t\t\t\t\t\t\t4.退出登录系统");
            System.out.println("-------------------------------------------------------------------------------------------");
            System.out.println("请输入你的选择：");
            char choice = Tools.readRegisterMenuSelection();
            switch (choice) {
                case '1':
                    enterAdminInfo();
                    break;
                case '2':
                    System.out.println("---------------安旭文------------------");
                    enterDoctorInfo();
                    break;
                case '3':
                    System.out.println("---------------田玉洁------------------");
                    enterDepartmentInfo();
                    break;
                case '4':
                    System.out.println("是否要退出登录状态(Y/N)？");
                    char exit = Tools.readConfirmSelection();
                    if (exit == 'Y' || exit == 'y') {
                        enterAdminMenu();
                    }
                    break;
            }
        }
    }

    public void enterAdminInfo() {
        boolean flag = true;
        while (flag) {
            System.out.println("------------------------------------【管理员信息管理】---------------------------------------");
            System.out.println("\t\t\t\t\t\t\t\t\t1.添加管理员信息");
            System.out.println("\t\t\t\t\t\t\t\t\t2.删除管理员信息");
            System.out.println("\t\t\t\t\t\t\t\t\t3.修改管理员信息(所有信息)");
            System.out.println("\t\t\t\t\t\t\t\t\t4.查找管理员信息");
            System.out.println("\t\t\t\t\t\t\t\t\t5.显示所有管理员信息");
            System.out.println("\t\t\t\t\t\t\t\t\t6.清空管理员信息");
            System.out.println("\t\t\t\t\t\t\t\t\t7.注销账户");
            System.out.println("\t\t\t\t\t\t\t\t\t8.退出");
            System.out.println("-------------------------------------------------------------------------------------------");
            System.out.println("请输入你的选择：");
            char choice = Tools.readDoctorInfo();
            switch (choice) {
                case '1':
                    addAdmin();
                    break;
                case '2':
                    deleteAdmin();
                    break;
                case '3':
                    updateAdmin();
                    break;
                case '4':
                    searchAdminById();
                    break;
                case '5':
                    showAdminAll();
                    break;
                case '6':
                    clearAdminAll();
                    break;
                case '7':
                    unsubscribe();
                    break;
                case '8':
                    enterAdminMainMenu();
                    break;
            }
        }

    }

    public void enterDoctorInfo() {
        boolean flag = true;
        while (flag) {
            System.out.println("------------------------------------【医生信息管理】---------------------------------------");
            System.out.println("---------------安旭文------------------");
            System.out.println("\t\t\t\t\t\t\t\t\t1.添加医生信息");
            System.out.println("\t\t\t\t\t\t\t\t\t2.删除医生信息");
            System.out.println("\t\t\t\t\t\t\t\t\t3.修改医生信息(所有信息)");
            System.out.println("\t\t\t\t\t\t\t\t\t4.查找医生信息");
            System.out.println("\t\t\t\t\t\t\t\t\t5.显示所有医生信息");
            System.out.println("\t\t\t\t\t\t\t\t\t6.清空医生信息");
            System.out.println("\t\t\t\t\t\t\t\t\t7.退出");
            System.out.println("-------------------------------------------------------------------------------------------");
            System.out.println("请输入你的选择：");
            char choice = Tools.readDoctorInfo();
            switch (choice) {
                case '1':
                    addDoctor();
                    break;
                case '2':
                    deleteDoctor();
                    break;
                case '3':
                    updateDoctor();
                    break;
                case '4':
                    searchDoctorById();
                    break;
                case '5':
                    showDoctorAll();
                    break;
                case '6':
                    clearDoctorAll();
                    break;
                case '7':
                    enterAdminMainMenu();
                    break;
            }
        }

    }

    public void enterDepartmentInfo() {
            boolean flag = true;
            while (flag) {
                System.out.println("------------------------------------【科室信息管理】---------------------------------------");
                System.out.println("---------------田玉洁------------------");
                System.out.println("\t\t\t\t\t\t\t\t\t1.添加科室信息");
                System.out.println("\t\t\t\t\t\t\t\t\t2.删除科室信息");
                System.out.println("\t\t\t\t\t\t\t\t\t3.修改科室信息(所有信息)");
                System.out.println("\t\t\t\t\t\t\t\t\t4.查找科室信息");
                System.out.println("\t\t\t\t\t\t\t\t\t5.显示所有科室信息");
                System.out.println("\t\t\t\t\t\t\t\t\t6.清空科室信息");
                System.out.println("\t\t\t\t\t\t\t\t\t7.退出");
                System.out.println("-------------------------------------------------------------------------------------------");
                System.out.println("请输入你的选择：");
                char choice = Tools.readInfo();
                switch (choice) {
                    case '1':
                        addDepartment();
                        break;
                    case '2':
                        deleteDepartment();
                        break;
                    case '3':
                        updateDepartment();
                        break;
                    case '4':
                        searchDepartmentById();
                        break;
                    case '5':
                        showDepartmentAll();
                        break;
                    case '6':
                        clearDepartmentAll();
                        break;
                    case '7':
                        enterAdminMainMenu();
                        break;
                }
            }

    }




    public void enterDoctorMenu() {
        // 是否退出系统的标识
        boolean flag = true;
        while (flag) {
            System.out.println("------------------------------------【欢迎医生】-------------------------------------");
            System.out.println("              1:登录        2：注册         3：忘记密码      4:退出                    ");
            System.out.println("------------------------------------------------------------------------------------------");
            System.out.println("请输入你的选择：");
            char choice = Tools.readRegisterMenuSelection();
            switch (choice) {
                case '1':
                    Doctorlogin();
                    break;
                case '2':
                    Doctorregister();
                    break;
                case '3':
                    Doctorrecover();
                    break;
                case '4':
                    enterRegisterMenu();
                    break;
            }
        }
    }




    public void enterDoctorMainMenu() {
        boolean flag = true;
        while (flag) {
            System.out.println("------------------------------------【医生】---------------------------------------");
            System.out.println("\t\t\t\t\t\t\t\t\t1.医生信息管理");
            System.out.println("\t\t\t\t\t\t\t\t\t2.患者信息管理");
            System.out.println("\t\t\t\t\t\t\t\t\t3.病房信息管理");
            System.out.println("\t\t\t\t\t\t\t\t\t4.药品信息管理");
            System.out.println("\t\t\t\t\t\t\t\t\t5.退出登录系统");
            System.out.println("-------------------------------------------------------------------------------------------");
            System.out.println("请输入你的选择：");
            char choice = Tools.readMedicine();
            switch (choice) {
                case '1':
                    System.out.println("---------------安旭文------------------");
                    enterDoctorInfoself();
                    break;
                case '2':
                    System.out.println("---------------王利民------------------");
                    enterPatientInfo();
                    break;
                case '3':
                    System.out.println("---------------赵方刚------------------");
                    enterSickroomInfo();
                    break;
                case '4':
                    System.out.println("---------------张  婷------------------");
                    enterMedicineInfo();
                    break;
                case '5':
                    System.out.println("是否要退出登录状态(Y/N)？");
                    char exit = Tools.readConfirmSelection();
                    if (exit == 'Y' || exit == 'y') {
                        enterDoctorMenu();
                    }
                    break;
            }
        }

    }

    public void enterDoctorInfoself() {
        boolean flag = true;
        while (flag) {
            System.out.println("------------------------------------【医生信息管理】---------------------------------------");
            System.out.println("---------------安旭文------------------");
            System.out.println("\t\t\t\t\t\t\t\t\t1.查找医生信息");
            System.out.println("\t\t\t\t\t\t\t\t\t2.显示所有医生信息");
            System.out.println("\t\t\t\t\t\t\t\t\t3.退出");
            System.out.println("-------------------------------------------------------------------------------------------");
            System.out.println("请输入你的选择：");
            char choice = Tools.readAdminMenuSelection();
            switch (choice) {
                case '1':
                    searchDoctorById();
                    break;
                case '2':
                    showDoctorAll();
                    break;
                case '3':
                    enterDoctorMainMenu();
                    break;

            }
        }

    }

    public void enterPatientInfo() {
        boolean flag = true;
        while (flag) {
            System.out.println("------------------------------------【患者信息管理】---------------------------------------");
            System.out.println("---------------王利民------------------");
            System.out.println("\t\t\t\t\t\t\t\t\t1.添加患者信息");
            System.out.println("\t\t\t\t\t\t\t\t\t2.删除患者信息");
            System.out.println("\t\t\t\t\t\t\t\t\t3.修改患者信息(所有信息)");
            System.out.println("\t\t\t\t\t\t\t\t\t4.查找患者信息");
            System.out.println("\t\t\t\t\t\t\t\t\t5.显示所有患者信息");
            System.out.println("\t\t\t\t\t\t\t\t\t6.清空患者信息");
            System.out.println("\t\t\t\t\t\t\t\t\t7.退出");
            System.out.println("-------------------------------------------------------------------------------------------");
            System.out.println("请输入你的选择：");
            char choice = Tools.readInfo();
            switch (choice) {
                case '1':
                    addPatient();
                    break;
                case '2':
                    deletePatient();
                    break;
                case '3':
                    updatePatient();
                    break;
                case '4':
                    searchPatientById();
                    break;
                case '5':
                    showPatientAll();
                    break;
                case '6':
                    clearPatientAll();
                    break;
                case '7':
                    enterDoctorMainMenu();
                    break;
            }
        }

    }

    public void enterSickroomInfo() {
        boolean flag = true;
        while (flag) {
            System.out.println("------------------------------------【病房信息管理】---------------------------------------");
            System.out.println("---------------赵方刚------------------");
            System.out.println("\t\t\t\t\t\t\t\t\t1.添加病房信息");
            System.out.println("\t\t\t\t\t\t\t\t\t2.删除病房信息");
            System.out.println("\t\t\t\t\t\t\t\t\t3.修改病房信息(所有信息)");
            System.out.println("\t\t\t\t\t\t\t\t\t4.查找病房信息");
            System.out.println("\t\t\t\t\t\t\t\t\t5.显示所有病房信息");
            System.out.println("\t\t\t\t\t\t\t\t\t6.退出");
            System.out.println("-------------------------------------------------------------------------------------------");
            System.out.println("请输入你的选择：");
            char choice = Tools.readInforoom();
            switch (choice) {
                case '1':
                    addSickroom();
                    break;
                case '2':
                    deleteSickroom();
                    break;
                case '3':
                    updateSickroom();
                    break;
                case '4':
                    searchSickroomById();
                    break;
                case '5':
                    showSickroomAll();
                    break;
                case '6':
                    enterDoctorMainMenu();
                    break;
            }
        }

    }

    public void enterMedicineInfo() {
        boolean flag = true;
        while (flag) {
            System.out.println("------------------------------------【药品信息管理】---------------------------------------");
            System.out.println("---------------张  婷------------------");
            System.out.println("\t\t\t\t\t\t\t\t\t1.添加药品信息");
            System.out.println("\t\t\t\t\t\t\t\t\t2.删除药品信息");
            System.out.println("\t\t\t\t\t\t\t\t\t3.修改药品信息(所有信息)");
            System.out.println("\t\t\t\t\t\t\t\t\t4.查找药品信息");
            System.out.println("\t\t\t\t\t\t\t\t\t5.显示所有药品信息");
            System.out.println("\t\t\t\t\t\t\t\t\t6.退出");
            System.out.println("-------------------------------------------------------------------------------------------");
            System.out.println("请输入你的选择：");
            char choice = Tools.readInforoom();
            switch (choice) {
                case '1':
                    addMedicine();
                    break;
                case '2':
                    deleteMedicine();
                    break;
                case '3':
                    updateMedicine();
                    break;
                case '4':
                    searchMedicineById();
                    break;
                case '5':
                    showMedicineAll();
                    break;
                case '6':
                    enterDoctorMainMenu();
                    break;
            }
        }

    }


    public void enterPatientMenu() {
        // 是否退出系统的标识
        boolean flag = true;
        while (flag) {
            System.out.println("------------------------------------【患者】---------------------------------------");
            System.out.println("\t\t\t\t\t\t\t\t\t1.患者信息管理");
            System.out.println("\t\t\t\t\t\t\t\t\t2.医生信息查询");
            System.out.println("\t\t\t\t\t\t\t\t\t3.缴费信息管理");
            System.out.println("\t\t\t\t\t\t\t\t\t4.是否退出登录系统");
            System.out.println("-------------------------------------------------------------------------------------------");
            System.out.println("请输入你的选择：");
            char choice = Tools.readRegisterMenuSelection();
            switch (choice) {
                case '1':
                    System.out.println("---------------王利民------------------");
                    enterPatientInfoself();
                    break;
                case '2':
                    System.out.println("---------------安旭文------------------");
                    enterDoctor();
                    break;
                case '3':
                    System.out.println("---------------关  馨------------------");
                    enterPayInfo();
                    break;
                case '4':
                    enterRegisterMenu();
                    break;
            }
        }
    }

    public void enterPatientInfoself() {
        boolean flag = true;
        while (flag) {
            System.out.println("------------------------------------【患者信息管理】---------------------------------------");
            System.out.println("---------------王利民------------------");
            System.out.println("\t\t\t\t\t\t\t\t\t1.查找患者信息");
            System.out.println("\t\t\t\t\t\t\t\t\t2.添加患者信息");
            System.out.println("\t\t\t\t\t\t\t\t\t3.删除患者信息");
            System.out.println("\t\t\t\t\t\t\t\t\t4.显示所有患者信息");
            System.out.println("\t\t\t\t\t\t\t\t\t5.退出");
            System.out.println("-------------------------------------------------------------------------------------------");
            System.out.println("请输入你的选择：");
            char choice = Tools.readPatientMenu();
            switch (choice) {
                case '1':
                    searchPatientById();
                    break;
                case '2':
                    addPatient();
                    break;
                case '3':
                    deletePatient();
                    break;
                case '4':
                    showPatientAll();
                    break;
                case '5':
                    enterPatientMenu();
                    break;

            }
        }

    }

    public void enterDoctor() {
        boolean flag = true;
        while (flag) {
            System.out.println("------------------------------------【医生信息查询】---------------------------------------");
            System.out.println("---------------安旭文------------------");
            System.out.println("\t\t\t\t\t\t\t\t\t1.查找医生信息");
            System.out.println("\t\t\t\t\t\t\t\t\t2.显示所有医生信息");
            System.out.println("\t\t\t\t\t\t\t\t\t3.退出");
            System.out.println("-------------------------------------------------------------------------------------------");
            System.out.println("请输入你的选择：");
            char choice = Tools.readAdminMenuSelection();
            switch (choice) {
                case '1':
                    searchDoctorById();
                    break;
                case '2':
                    showDoctorAll();
                    break;
                case '3':
                    enterPatientMenu();
                    break;

            }
        }

    }

    public void enterPayInfo() {
        boolean flag = true;
        while (flag) {
            System.out.println("------------------------------------【缴费信息管理】---------------------------------------");
            System.out.println("---------------关  馨------------------");
            System.out.println("\t\t\t\t\t\t\t\t\t1.添加缴费记录");
            System.out.println("\t\t\t\t\t\t\t\t\t2.删除缴费记录");
            System.out.println("\t\t\t\t\t\t\t\t\t3.修改缴费记录(所有记录)");
            System.out.println("\t\t\t\t\t\t\t\t\t4.查找缴费记录");
            System.out.println("\t\t\t\t\t\t\t\t\t5.显示所有缴费记录");
            System.out.println("\t\t\t\t\t\t\t\t\t6.清空缴费记录");
            System.out.println("\t\t\t\t\t\t\t\t\t7.退出");
            System.out.println("-------------------------------------------------------------------------------------------");
            System.out.println("请输入你的选择：");
            char choice = Tools.readInfo();
            switch (choice) {
                case '1':
                    addPay();
                    break;
                case '2':
                    deletePay();
                    break;
                case '3':
                    updatePay();
                    break;
                case '4':
                    searchPayById();
                    break;
                case '5':
                    showPayAll();
                    break;
                case '6':
                    clearPayAll();
                    break;
                case '7':
                    enterPatientMenu();
                    break;
            }
        }

    }

    //******************************************************************************************************************
    //******************************************************************************************************************

    public void Adminlogin() {
        System.out.println("------------------------------------【登录】-------------------------------------------------");
        System.out.println("请输入你的个人账号：");
        String adminId = Tools.readString();
        System.out.println("请输入你的个人密码：");
        String password = Tools.readString();
        Admin login = adminService.login(adminId, password);
        if (login != null) {
            System.out.println("欢迎使用学生信息管理系统！,点击回车键开始使用学生信息管理系统");
            // 临时保存当前管理员的信息
            tempAdminId = login.getAdminId();
            tempAdminName = login.getAdminName();
            tempAdminCard = login.getAdminCard();
            tempAdminPhone = login.getAdminPhone();
            // 按回车键继续
            Tools.readReturn();
            // 登录成功进入系统管理员主菜单
            enterAdminMainMenu();
        } else {
            System.out.println("账号或密码有误");
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }


    public void Doctorlogin() {
        System.out.println("------------------------------------【登录】-------------------------------------------------");
        System.out.println("请输入你的个人账号：");
        String doctorId = Tools.readString();
        System.out.println("请输入你的个人密码：");
        String doctorPassword = Tools.readString();
        Doctor login = doctorService.login(doctorId, doctorPassword);
        if (login != null) {
            System.out.println("欢迎！,点击回车键开始进入医生信息管理菜单");
            // 临时保存当前管理员的信息
            tempDoctorId = login.getDoctorId();
            tempDoctorName = login.getDoctorName();
            tempDoctorSex = login.getDoctorSex();
            tempDoctorAge = login.getDoctorAge();
            tempDoctorPhone = login.getDoctorPhone();
            tempDoctorTitle = login.getDoctorTitle();
            tempDoctorDepartment = login.getDoctorDepartment();
            tempDepartmentId = login.getDepartmentId();
            // 按回车键继续
            Tools.readReturn();
            // 登录成功进入医生主菜单
            enterDoctorMainMenu();
        } else {
            System.out.println("账号或密码有误");
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }

    /**
     * @author Snoopy
     * @Description 注册
     * @Date
     * @Param
     * @Return
     */

    public void Adminregister() {
        System.out.println("------------------------------------【注册】------------------------------------------------");
        System.out.println("请输入id(注册号)");
        String id = Tools.readString();
        while (adminService.checkId(id)) {
            System.out.println("你输入的注册号已经存在，请另选一个：");
            id = Tools.readString();
            adminService.checkId(id);
        }
        System.out.println("请输入你的个人账号：");
        String adminId = Tools.readString();
        while (adminService.checkId(adminId)) {
            System.out.println("你输入的账号已经存在，请重新输入：");
            adminId = Tools.readString();
            adminService.checkId(adminId);
        }
        System.out.println("请输入你的个人密码：");
        String adminPassword = Tools.readString();
        System.out.println("请输入你的姓名：");
        String adminName = Tools.readString();
        System.out.println("请输入你的身份证号：");
        String adminCard = Tools.readString();
        while (adminService.checkCard(adminCard)) {
            System.out.println("你输入的身份证号已经存在，请重新输入：");
            adminCard = Tools.readString();
            adminService.checkId(adminCard);
        }
        System.out.println("请输入你的手机号：");
        String adminPhone = Tools.readString();
        while (adminService.checkPhone(adminPhone)) {
            System.out.println("你输入的手机号已经存在，请重新输入：");
            adminPhone = Tools.readString();
            adminService.checkId(adminPhone);
        }
        Admin admin = new Admin(id,adminId, adminPassword, adminName, adminCard, adminPhone);
        int register = adminService.register(admin);
        if (register > 0) {
            System.out.println("注册成功");
        } else {
            System.out.println("注册失败，请重新试一下！");
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }



    public void Doctorregister() {
        System.out.println("------------------------------------【注册】------------------------------------------------");
        System.out.println("请输入你的个人账号：");
        String doctorId = Tools.readString();
        while (doctorService.checkId(doctorId)) {
            System.out.println("你输入的账号已经存在，请重新输入：");
            doctorId = Tools.readString();
            adminService.checkId(doctorId);
        }
        System.out.println("请输入你的个人密码：");
        String doctorPassword = Tools.readString();
        System.out.println("请输入你的姓名：");
        String doctorName = Tools.readString();
        System.out.println("请输入你的性别：");
        String doctorSex = Tools.readString();
        System.out.println("请输入你的年龄：");
        String doctorAge = Tools.readString();
        System.out.println("请输入你的电话号：");
        String doctorPhone = Tools.readString();
        while (adminService.checkCard(doctorPhone)) {
            System.out.println("你输入的电话号已经存在，请重新输入：");
            doctorPhone = Tools.readString();
            adminService.checkId(doctorPhone);
        }
        System.out.println("请输入医生类别：");
        String doctorTitle = Tools.readString();
        System.out.println("请输入所属部门：");
        String doctorDepartment = Tools.readString();
        System.out.println("请输入所属科室id：");
        String departmentId = Tools.readString();
        Doctor doctor = new Doctor(doctorId,doctorPassword, doctorName, doctorSex, doctorAge,doctorPhone, doctorTitle,doctorDepartment,departmentId);
        int register = doctorService.register(doctor);
        if (register > 0) {
            System.out.println("注册成功");
        } else {
            System.out.println("注册失败，请重新试一下！");
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }


    /**
     * @author Snoopy
     * @Description 找回密码
     * @Date
     * @Return
     */

    public void Adminrecover() {
        System.out.println("------------------------------------【找回密码】---------------------------------------------");
        System.out.println("请输入你的身份证号：");
        String card = Tools.readString();
        System.out.println("请输入你的手机号：");
        String phone = Tools.readString();
        String recover = adminService.recover(card, phone);
        if (recover != null) {
            System.out.println("密码找回成功，你的密码为：" + recover);
        } else {
            System.out.println("你输入的身份证号或手机号有误！");
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();

    }

    public void Doctorrecover() {
        System.out.println("------------------------------------【找回密码】---------------------------------------------");
        System.out.println("请输入你的账号：");
        String doctorId = Tools.readString();
        System.out.println("请输入你的手机号：");
        String doctorPhone = Tools.readString();
        String recover = doctorService.recover(doctorId, doctorPhone);
        if (recover != null) {
            System.out.println("密码找回成功，你的密码为：" + recover);
        } else {
            System.out.println("你输入的身份证号或手机号有误！");
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();

    }

    //******************************************************************************************************************
    //******************************************************************************************************************

    public void addAdmin() {
        System.out.println("------------------------------------【添加管理员信息】------------------------------------------");
        boolean flag = true; // 继续添加管理员标识
        int count = 0; // 当前已成功添加的管理员数量
        while (flag) {
            System.out.println("请输入需要添加的管理员id：");
            String id = Tools.readString();
            System.out.println("请输入需要添加的管理员账号：");
            String adminId = Tools.readString();
            while (adminService.checkId(adminId)) {
                System.out.println("你输入的账号已经存在，请重新输入：");
                adminId = Tools.readString();
                adminService.checkId(adminId);
            }
            System.out.println("请输入需要添加的管理员密码：");
            String adminPassword = Tools.readString();
            System.out.println("请输入需要添加的管理员姓名：");
            String adminName = Tools.readString();
            System.out.println("请输入需要添加的管理员身份证号：");
            String adminCard = Tools.readString();
            System.out.println("请输入需要添加的管理员电话号：");
            String adminPhone = Tools.readString();

            Admin admin = new Admin(id,adminId,adminPassword,adminName,adminCard,adminPhone);
            int register = adminService.register(admin);
            if (register > 0) {
                count++;
                System.out.println("已成功添加" + count + "条管理员信息");
                System.out.println("是否继续添加(Y/N)？");
                char choice = Tools.readConfirmSelection();
                if (choice == 'Y' || choice == 'y') {
                    System.out.println("-------------------------------------------------------------------------------------------");
                } else {
                    System.out.println("成功添加" + count + "条管理员信息");
                    break;
                }
            } else {
                System.out.println("添加失败");
            }
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }



    public void deleteAdmin() {
        System.out.println("------------------------------------【删除管理员信息】------------------------------------------");
        System.out.println("请输入要删除管理员的id：");
        String adminId = Tools.readString();
        // 先查询一下该学号的学生信息
        Object search = adminService.getAdminById(adminId);
        if (search != null) {
            System.out.println(search);
            System.out.println("确定要删除吗(Y/N)：");
            char choice = Tools.readConfirmSelection();
            if (choice == 'y' || choice == 'Y') {
                int delete = adminService.deleteAdmin(adminId);
                if (delete > 0) {
                    System.out.println("删除成功");
                } else {
                    System.out.println("删除失败");
                }
            }
        } else {
            System.out.println("要删除的学生不存在");
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }



    public void updateAdmin() {
        System.out.println("------------------------------------【修改管理员信息】------------------------------------------");
        System.out.println("请输入管理员账号：");
        String oldAdminId = Tools.readString();
        // 先查询一下该学号的学生信息
        Object search = adminService.getAdminById(oldAdminId);
        if (search != null) {
            System.out.println(search);
            System.out.println("确定要修改该管理员的信息(Y/N)？");
            char choice = Tools.readConfirmSelection();
            if (choice == 'Y' || choice == 'y') {
                System.out.println("请输入修改后的管理员id：");
                String id = Tools.readString();
                System.out.println("请输入修改后的管理员账号：");
                String adminID = Tools.readString();
                System.out.println("请输入修改后的管理员密码：");
                String adminPassword = Tools.readString();
                System.out.println("请输入修改后的管理员姓名：");
                String adminName = Tools.readString();
                System.out.println("请输入修改后的管理员身份证号：");
                String adminCard = Tools.readString();
                System.out.println("请输入修改后的管理员电话号：");
                String adminPhone = Tools.readString();
                Admin admin = new Admin(id, adminID, adminPassword, adminName, adminCard, adminPhone);
                int updateAll = adminService.updateAll(oldAdminId,admin);
                if (updateAll > 0) {
                    System.out.println("修改成功");
                } else {
                    System.out.println("修改失败");
                }
            }
        } else {
            System.out.println("你要修改的学生不存在！");
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }



    public void searchAdminById() {
        System.out.println("------------------------------------【查询管理员信息】------------------------------------------");
        System.out.println("请输入要查找的管理员账号：");
        String adminId = Tools.readString();
        Object admin = adminService.getAdminById(adminId);
        if (admin != null) {
            System.out.println("查询结果如下：");
            System.out.println(admin);
        } else {
            System.out.println("该学生信息不存在");
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        Tools.readReturn();
    }


    /**
     * @author Snoopy
     * @Description 显示全部管理信息
     * @Date
     * @Param
     * @Return
     */

    public void showAdminAll() {
        System.out.println("------------------------------------【显示全体管理员信息】---------------------------------------");
        List<Admin> admins = adminService.searchAll();
        for (Admin key : admins) {
            System.out.println(key);
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }

    public void clearAdminAll() {
        System.out.println("------------------------------------【清空所有管理员信息】---------------------------------------");
        System.out.println("确定要清空所有学生信息(Y/N),执行此操作将丢失所有管理员的信息：");
        char choice = Tools.readConfirmSelection();
        if (choice == 'Y' || choice == 'y') {
            List<Admin> admins = adminService.searchAll();
            if (admins.size() > 0 && null != admins) {
                System.out.println("请输入你的身份证号：");
                String card = Tools.readString();
                Admin admin = adminService.getAdminByCard(card);
                if (admin != null) {
                    int clearAll = adminService.clearAll();
                    if (clearAll > 0) {
                        System.out.println("学生信息清空成功");
                    } else {
                        System.out.printf("学生信息清空失败");
                    }
                } else {
                    System.out.println("你输入的身份证号不存在");
                }
            }else {
                System.out.println("尚未存入学生信息，无法进行清空操作");
            }
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }

    public int unsubscribe() {
        System.out.println("------------------------------------【注销账户】----------------------------------------------");
        System.out.println("当前登录的管理员账号：" + tempAdminId);
        System.out.println("当前登录的管理员姓名：" + tempAdminName);
        System.out.println("确定要注销账户(Y/N)?");
        char choice = Tools.readConfirmSelection();
        if (choice == 'Y' || choice == 'y') {
            System.out.println("请输入你的身份证号：");
            String card = Tools.readString();
            System.out.println("请输入你的手机号：");
            String phone = Tools.readString();
            Admin adminByCard = adminService.getAdminByCard(card);
            Admin adminByPhone = adminService.getAdminByPhone(phone);
            if (adminByCard != null && adminByPhone != null) {
                if (tempAdminCard.equals(adminByCard.getAdminCard()) && tempAdminPhone.equals(adminByPhone.getAdminPhone())) {
                    int unsubscribe = adminService.unsubscribe(card, phone);
                    if (unsubscribe > 0) {
                        System.out.println("账户注销成功,已成功退出登录状态");
                        return unsubscribe;
                    } else {
                        System.out.println("账户注销失败");
                    }
                } else {
                    System.out.println("你输入的身份证号或手机号有误");
                }
            } else {
                System.out.println("你的身份证号或手机号不存在");
            }
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
        return 0;
    }

    //******************************************************************************************************************
    //******************************************************************************************************************

    public void addDoctor() {
        System.out.println("------------------------------------【添加医生信息】------------------------------------------");
        boolean flag = true; // 继续添加医生标识
        int count = 0; // 当前已成功添加的医生数量
        while (flag) {
            System.out.println("请输入医生账号：");
            String id = Tools.readString();
            System.out.println("请输入医生密码：");
            String password = Tools.readString();
            System.out.println("请输入医生姓名：");
            String name = Tools.readString();
            System.out.println("请输入医生性别：");
            String sex = Tools.readString();
            System.out.println("请输入医生年龄：");
            String age = Tools.readString();
            System.out.println("请输入医生电话：");
            String phone = Tools.readString();
            System.out.println("请输入医生类型：");
            String title = Tools.readString();
            System.out.println("请输入医生部门：");
            String department = Tools.readString();
            System.out.println("请输入医生所属科室id：");
            String departmentId = Tools.readString();


            Doctor doctor = new Doctor(id,password,name,sex,age,phone,title,department,departmentId);
            int register = doctorService.register(doctor);
            if (register > 0) {
                count++;
                System.out.println("已成功添加" + count + "条医生信息");
                System.out.println("是否继续添加(Y/N)？");
                char choice = Tools.readConfirmSelection();
                if (choice == 'Y' || choice == 'y') {
                    System.out.println("-------------------------------------------------------------------------------------------");
                } else {
                    System.out.println("成功添加" + count + "条医生信息");
                    break;
                }
            } else {
                System.out.println("添加失败");
            }
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }



    public void deleteDoctor() {
        System.out.println("------------------------------------【删除医生信息】------------------------------------------");
        System.out.println("请输入要删除医生的账号：");
        String doctorId = Tools.readString();
        // 先查询一下该学号的学生信息
        Object search = doctorService.getDoctorById(doctorId);
        if (search != null) {
            System.out.println(search);
            System.out.println("确定要删除吗(Y/N)：");
            char choice = Tools.readConfirmSelection();
            if (choice == 'y' || choice == 'Y') {
                int delete = doctorService.deleteDoctor(doctorId);
                if (delete > 0) {
                    System.out.println("删除成功");
                } else {
                    System.out.println("删除失败");
                }
            }
        } else {
            System.out.println("要删除的学生不存在");
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }



    public void updateDoctor() {
        System.out.println("------------------------------------【修改医生信息】------------------------------------------");
        System.out.println("请输入医生账号：");
        String oldDoctorId = Tools.readString();
        // 先查询一下该账号的医生信息
        Object search = doctorService.getDoctorById(oldDoctorId);
        if (search != null) {
            System.out.println(search);
            System.out.println("确定要修改该医生的信息(Y/N)？");
            char choice = Tools.readConfirmSelection();
            if (choice == 'Y' || choice == 'y') {
                System.out.println("请输入修改后的医生账号：");
                String id = Tools.readString();
                System.out.println("请输入修改后的医生密码：");
                String password = Tools.readString();
                System.out.println("请输入修改后的医生姓名：");
                String name = Tools.readString();
                System.out.println("请输入修改后的医生性别：");
                String sex = Tools.readString();
                System.out.println("请输入修改后的医生年龄：");
                String age = Tools.readString();
                System.out.println("请输入修改后的医生电话：");
                String phone = Tools.readString();
                System.out.println("请输入修改后的医生类型：");
                String title = Tools.readString();
                System.out.println("请输入修改后的医生部门：");
                String department = Tools.readString();
                System.out.println("请输入修改后的医生所属科室id：");
                String departmentId = Tools.readString();
                Doctor doctor = new Doctor(id,password,name,sex,age,phone,title,department,departmentId);
                int updateAll = doctorService.updateAll(oldDoctorId,doctor);
                if (updateAll > 0) {
                    System.out.println("修改成功");
                } else {
                    System.out.println("修改失败");
                }
            }
        } else {
            System.out.println("你要修改的医生信息不存在！");
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }



    public void searchDoctorById() {
        System.out.println("------------------------------------【查询医生信息】------------------------------------------");
        System.out.println("请输入要查找的医生账号：");
        String doctorId = Tools.readString();
        Object doctor = doctorService.getDoctorById(doctorId);
        if (doctor != null) {
            System.out.println("查询结果如下：");
            System.out.println(doctor);
        } else {
            System.out.println("该医生信息不存在");
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        Tools.readReturn();
    }


    /**
     * @author Snoopy
     * @Description 显示全部医生信息
     * @Date
     * @Param
     * @Return
     */

    public void showDoctorAll() {
        System.out.println("------------------------------------【显示全体医生信息】---------------------------------------");
        List<Doctor> doctors = doctorService.searchAll();
        for (Doctor key : doctors) {
            System.out.println(key);
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }

    public void clearDoctorAll() {
        System.out.println("------------------------------------【清空所有医生信息】---------------------------------------");
        System.out.println("确定要清空所有医生信息(Y/N),执行此操作将丢失所有医生的信息：");
        char choice = Tools.readConfirmSelection();
        if (choice == 'Y' || choice == 'y') {
            List<Doctor> doctors = doctorService.searchAll();
            if (doctors.size() > 0 && null != doctors) {
                System.out.println("请输入你的账号：");
                String id = Tools.readString();
                Doctor doctor = doctorService.getDoctorById(id);
                if (doctor != null) {
                    int clearAll = doctorService.clearAll();
                    if (clearAll > 0) {
                        System.out.println("医生信息清空成功");
                    } else {
                        System.out.printf("医生信息清空失败");
                    }
                } else {
                    System.out.println("你输入的账号不存在");
                }
            }else {
                System.out.println("尚未存入医生信息，无法进行清空操作");
            }
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }

    //******************************************************************************************************************
    //******************************************************************************************************************

    public void addDepartment() {
        System.out.println("------------------------------------【添加科室信息】------------------------------------------");
        boolean flag = true; // 继续添加科室标识
        int count = 0; // 当前已成功添加的科室数量
        while (flag) {
            System.out.println("请输入需要添加的科室id：");
            String id = Tools.readString();
            while (departmentService.checkExist(id)) {
                System.out.println("你输入的账号已经存在，请重新输入：");
                id = Tools.readString();
                adminService.checkId(id);
            }
            System.out.println("请输入需要添加的科室名称：");
            String name = Tools.readString();
            System.out.println("请输入需要添加的科室地址：");
            String address = Tools.readString();
            System.out.println("请输入需要添加的科室电话：");
            String phone = Tools.readString();
            System.out.println("请输入需要添加的科室主任：");
            String chairman = Tools.readString();

            Department department = new Department(id,name,address,phone,chairman);
            int register = departmentService.addDepartment(department);
            if (register > 0) {
                count++;
                System.out.println("已成功添加" + count + "条科室信息");
                System.out.println("是否继续添加(Y/N)？");
                char choice = Tools.readConfirmSelection();
                if (choice == 'Y' || choice == 'y') {
                    System.out.println("-------------------------------------------------------------------------------------------");
                } else {
                    System.out.println("成功添加" + count + "条科室信息");
                    break;
                }
            } else {
                System.out.println("添加失败");
            }
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }



    public void deleteDepartment() {
        System.out.println("------------------------------------【删除科室信息】------------------------------------------");
        System.out.println("请输入要删除科室的id：");
        String departmentId = Tools.readString();
        // 先查询一下该学号的学生信息
        Object search = departmentService.search(departmentId);
        if (search != null) {
            System.out.println(search);
            System.out.println("确定要删除吗(Y/N)：");
            char choice = Tools.readConfirmSelection();
            if (choice == 'y' || choice == 'Y') {
                int delete = departmentService.deleteDepartment(departmentId);
                if (delete > 0) {
                    System.out.println("删除成功");
                } else {
                    System.out.println("删除失败");
                }
            }
        } else {
            System.out.println("要删除的科室不存在");
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }



    public void updateDepartment() {
        System.out.println("------------------------------------【修改科室信息】------------------------------------------");
        System.out.println("请输入科室id：");
        String oldDepartmentId = Tools.readString();
        // 先查询一下该学号的学生信息
        Object search = departmentService.search(oldDepartmentId);
        if (search != null) {
            System.out.println(search);
            System.out.println("确定要修改该科室的信息(Y/N)？");
            char choice = Tools.readConfirmSelection();
            if (choice == 'Y' || choice == 'y') {
                System.out.println("请输入修改后的科室id：");
                String id = Tools.readString();
                System.out.println("请输入修改后的科室名称：");
                String name = Tools.readString();
                System.out.println("请输入修改后的科室地址：");
                String address = Tools.readString();
                System.out.println("请输入修改后的科室电话：");
                String phone = Tools.readString();
                System.out.println("请输入修改后的科室主任：");
                String chairman = Tools.readString();
                Department department = new Department(id,name,address,phone,chairman);
                int updateAll = departmentService.updateAll(oldDepartmentId,department);
                if (updateAll > 0) {
                    System.out.println("修改成功");
                } else {
                    System.out.println("修改失败");
                }
            }
        } else {
            System.out.println("你要修改的科室不存在！");
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }



    public void searchDepartmentById() {
        System.out.println("------------------------------------【查询科室信息】------------------------------------------");
        System.out.println("请输入要查找的科室id：");
        String id = Tools.readString();
        Object department = departmentService.search(id);
        if (department != null) {
            System.out.println("查询结果如下：");
            System.out.println(department);
        } else {
            System.out.println("该科室信息不存在");
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        Tools.readReturn();
    }


    /**
     * @author Snoopy
     * @Description 显示全部管理信息
     * @Date
     * @Param
     * @Return
     */

    public void showDepartmentAll() {
        System.out.println("------------------------------------【显示所有科室信息】---------------------------------------");
        List<Department> departments = departmentService.searchAll();
        for (Department key : departments) {
            System.out.println(key);
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }

    public void clearDepartmentAll() {
        System.out.println("------------------------------------【清空所有科室信息】---------------------------------------");
        System.out.println("确定要清空所有科室信息(Y/N),执行此操作将丢失所有科室的信息：");
        char choice = Tools.readConfirmSelection();
        if (choice == 'Y' || choice == 'y') {
            List<Department> departments = departmentService.searchAll();
            if (departments.size() > 0 && null != departments) {
                    int clearAll = departmentService.clearAll();
                    if (clearAll > 0) {
                        System.out.println("科室信息清空成功");
                    } else {
                        System.out.printf("科室信息清空失败");
                    }
            }else {
                System.out.println("尚未存入科室信息，无法进行清空操作");
            }
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }


    //******************************************************************************************************************
    //******************************************************************************************************************

    public void addPatient() {
        System.out.println("------------------------------------【添加患者信息】------------------------------------------");
        boolean flag = true; // 继续添加患者标识
        int count = 0; // 当前已成功添加的患者数量
        while (flag) {
            System.out.println("请输入你的个人账号：");
            String patientId = Tools.readString();
            while (doctorService.checkId(patientId)) {
                System.out.println("你输入的账号已经存在，请重新输入：");
                patientId = Tools.readString();
                adminService.checkId(patientId);
            }
            System.out.println("请输入你的姓名：");
            String patientName = Tools.readString();
            System.out.println("请输入你的性别：");
            String patientSex = Tools.readString();
            System.out.println("请输入你的年龄：");
            String patientAge = Tools.readString();
            System.out.println("请输入家庭地址：");
            String patientRoom = Tools.readString();
            System.out.println("请输入就诊医生：");
            String patientDoctor = Tools.readString();
            System.out.println("请输入你的血型：");
            String patientBlood = Tools.readString();
            System.out.println("请输入就诊结果：");
            String patientResults = Tools.readString();
            System.out.println("请输入科室号：");
            String departmentId = Tools.readString();
            System.out.println("请输入病房号：");
            String sickroomId = Tools.readString();

            Patient patient = new Patient(patientId,patientName, patientSex, patientAge, patientRoom,patientDoctor, patientBlood,patientResults,departmentId,sickroomId);
            int register = patientService.register(patient);
            if (register > 0) {
                count++;
                System.out.println("已成功添加" + count + "条患者信息");
                System.out.println("是否继续添加(Y/N)？");
                char choice = Tools.readConfirmSelection();
                if (choice == 'Y' || choice == 'y') {
                    System.out.println("-------------------------------------------------------------------------------------------");
                } else {
                    System.out.println("成功添加" + count + "条患者信息");
                    break;
                }
            } else {
                System.out.println("添加失败");
            }
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }



    public void deletePatient() {
        System.out.println("------------------------------------【删除患者信息】------------------------------------------");
        System.out.println("请输入要删除的患者id：");
        String patientId = Tools.readString();
        System.out.println("请输入要删除的患者姓名：");
        String patientName = Tools.readString();
        // 先查询一下该学号的学生信息
        Object search1 = patientService.searchById(patientId);
        Object search2 = patientService.searchByName(patientName);
        if (search1 != null && search2 != null) {
            System.out.println(search1);
            System.out.println("确定要删除吗(Y/N)：");
            char choice = Tools.readConfirmSelection();
            if (choice == 'y' || choice == 'Y') {
                int delete = patientService.deletePatient(patientId,patientName);
                if (delete > 0) {
                    System.out.println("删除成功");
                } else {
                    System.out.println("删除失败");
                }
            }
        } else {
            System.out.println("要删除的患者不存在");
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }



    public void updatePatient() {
        System.out.println("------------------------------------【修改患者信息】------------------------------------------");
        System.out.println("请输入患者id：");
        String oldPatientId = Tools.readString();
        // 先查询一下该账号的患者信息
        Object search = patientService.searchById(oldPatientId);
        if (search != null) {
            System.out.println(search);
            System.out.println("确定要修改该患者的信息(Y/N)？");
            char choice = Tools.readConfirmSelection();
            if (choice == 'Y' || choice == 'y') {
                System.out.println("请输入修改后的患者id：");
                String id = Tools.readString();
                System.out.println("请输入修改后的患者姓名：");
                String name = Tools.readString();
                System.out.println("请输入修改后的患者性别：");
                String sex = Tools.readString();
                System.out.println("请输入修改后的患者年龄：");
                String age = Tools.readString();
                System.out.println("请输入修改后的患者家庭地址：");
                String room = Tools.readString();
                System.out.println("请输入修改后的患者的主治医生：");
                String doctor = Tools.readString();
                System.out.println("请输入修改后的患者的血型：");
                String blood = Tools.readString();
                System.out.println("请输入修改后的患者的诊断结果：");
                String result = Tools.readString();
                System.out.println("请输入修改后的患者的所属科室：");
                String departmentId = Tools.readString();
                System.out.println("请输入修改后的患者的病房id：");
                String sickroomId = Tools.readString();

                Patient patient = new Patient(id,name,sex,age,room,doctor,blood,result,departmentId,sickroomId);
                int updateAll = patientService.updateAll(oldPatientId,patient);
                if (updateAll > 0) {
                    System.out.println("修改成功");
                } else {
                    System.out.println("修改失败");
                }
            }
        } else {
            System.out.println("你要修改的患者不存在！");
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }



    public void searchPatientById() {
        System.out.println("------------------------------------【查询患者信息】------------------------------------------");
        System.out.println("请输入要查找的患者账号：");
        String patientId = Tools.readString();
        Object patient = patientService.searchById(patientId);
        if (patient != null) {
            System.out.println("查询结果如下：");
            System.out.println(patient);
        } else {
            System.out.println("该患者信息不存在");
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        Tools.readReturn();
    }


    /**
     * @author Snoopy
     * @Description 显示全部患者信息
     * @Date
     * @Param
     * @Return
     */

    public void showPatientAll() {
        System.out.println("------------------------------------【显示全体患者信息】---------------------------------------");
        List<Patient> patients = patientService.searchAll();
        for (Patient key : patients) {
            System.out.println(key);
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }

    public void clearPatientAll() {
        System.out.println("------------------------------------【清空所有患者信息】---------------------------------------");
        System.out.println("确定要清空所有患者信息(Y/N),执行此操作将丢失所有患者的信息：");
        char choice = Tools.readConfirmSelection();
        if (choice == 'Y' || choice == 'y') {
            List<Patient> patients = patientService.searchAll();
            if (patients.size() > 0 && null != patients) {
                    int clearAll = patientService.clearAll();
                    if (clearAll > 0) {
                        System.out.println("患者信息清空成功");
                    } else {
                        System.out.printf("患者信息清空失败");
                    }
            }else {
                System.out.println("尚未存入患者信息，无法进行清空操作");
            }
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }



    public void addSickroom() {
        System.out.println("------------------------------------【添加病房信息】------------------------------------------");
        boolean flag = true; // 继续添加病房标识
        int count = 0; // 当前已成功添加的病房数量
        while (flag) {
            System.out.println("请输入需要添加的病房id：");
            String id = Tools.readString();
            System.out.println("请输入需要添加的病床号：");
            String bed = Tools.readString();
            System.out.println("请输入需要添加的病房地址");
            String address = Tools.readString();
            System.out.println("请输入需要添加的病房状态：");
            String state = Tools.readString();
            System.out.println("请输入需要添加的病房的所属科室的id：");
            String departmentId = Tools.readString();
            Sickroom sickroom = new Sickroom(id,bed,address,state,departmentId);
            int register = sickroomService.addSickroom(sickroom);
            if (register > 0) {
                count++;
                System.out.println("已成功添加" + count + "条病房信息");
                System.out.println("是否继续添加(Y/N)？");
                char choice = Tools.readConfirmSelection();
                if (choice == 'Y' || choice == 'y') {
                    System.out.println("-------------------------------------------------------------------------------------------");
                } else {
                    System.out.println("成功添加" + count + "条病房信息");
                    break;
                }
            } else {
                System.out.println("添加失败");
            }
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }



    public void deleteSickroom() {
        System.out.println("------------------------------------【删除病房信息】------------------------------------------");
        System.out.println("请输入要删除的病房的id：");
        String sickroomId = Tools.readString();
        // 先查询一下该id的病房信息
        Object search = sickroomService.search(sickroomId);
        if (search != null) {
            System.out.println(search);
            System.out.println("确定要删除吗(Y/N)：");
            char choice = Tools.readConfirmSelection();
            if (choice == 'y' || choice == 'Y') {
                int delete = sickroomService.deleteSickroom(sickroomId);
                if (delete > 0) {
                    System.out.println("删除成功");
                } else {
                    System.out.println("删除失败");
                }
            }
        } else {
            System.out.println("要删除的病房不存在");
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }



    public void updateSickroom() {
        System.out.println("------------------------------------【修改病房信息】------------------------------------------");
        System.out.println("请输入病房id：");
        String oldSickroomId = Tools.readString();
        // 先查询一下该学号的学生信息
        Object search = sickroomService.search(oldSickroomId);
        if (search != null) {
            System.out.println(search);
            System.out.println("确定要修改该管理员的信息(Y/N)？");
            char choice = Tools.readConfirmSelection();
            if (choice == 'Y' || choice == 'y') {
                System.out.println("请输入修改后的病房id：");
                String id = Tools.readString();
                System.out.println("请输入修改后的病床号：");
                String bed = Tools.readString();
                System.out.println("请请输入修改后的病房地址");
                String address = Tools.readString();
                System.out.println("请输入修改后的病房状态：");
                String state = Tools.readString();
                System.out.println("请输入修改后的病房的所属科室的id：");
                String departmentId = Tools.readString();
                Sickroom sickroom = new Sickroom(id,bed,address,state,departmentId);
                int updateAll = sickroomService.updateAll(oldSickroomId,sickroom);
                if (updateAll > 0) {
                    System.out.println("修改成功");
                } else {
                    System.out.println("修改失败");
                }
            }
        } else {
            System.out.println("你要修改的病房不存在！");
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }



    public void searchSickroomById() {
        System.out.println("------------------------------------【查询病房信息】------------------------------------------");
        System.out.println("请输入要查找的病房账号：");
        String sickroomId = Tools.readString();
        Object sickroom = sickroomService.search(sickroomId);
        if (sickroom != null) {
            System.out.println("查询结果如下：");
            System.out.println(sickroom);
        } else {
            System.out.println("该病房信息不存在");
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        Tools.readReturn();
    }



    public void showSickroomAll() {
        System.out.println("------------------------------------【显示所有病房信息】---------------------------------------");
        List<Sickroom> sickrooms = sickroomService.searchAll();
        for (Sickroom key : sickrooms) {
            System.out.println(key);
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }

    //******************************************************************************************************************
    //******************************************************************************************************************


    public void addMedicine() {
        System.out.println("------------------------------------【添加药品信息】------------------------------------------");
        boolean flag = true; // 继续添加药品标识
        int count = 0; // 当前已成功添加的药品数量
        while (flag) {
            System.out.println("请输入需要添加的药品id：");
            String id = Tools.readString();
            System.out.println("请输入需要添加的药品名称：");
            String name = Tools.readString();
            System.out.println("请输入需要添加的药品价格：");
            String price = Tools.readString();
            System.out.println("请输入需要添加的药品适用病症：");
            String drugs = Tools.readString();
            System.out.println("请输入需要添加的药品注意事项：");
            String precautions = Tools.readString();
            System.out.println("请输入需要添加的药品使用说明：");
            String use = Tools.readString();
            System.out.println("请输入需要添加的患者id：");
            String patientId = Tools.readString();
            Medicine medicine = new Medicine(id,name,price,drugs,precautions,use,patientId);
            int register = medicineService.addMedicine(medicine);
            if (register > 0) {
                count++;
                System.out.println("已成功添加" + count + "条药品信息");
                System.out.println("是否继续添加(Y/N)？");
                char choice = Tools.readConfirmSelection();
                if (choice == 'Y' || choice == 'y') {
                    System.out.println("-------------------------------------------------------------------------------------------");
                } else {
                    System.out.println("成功添加" + count + "条药品信息");
                    break;
                }
            } else {
                System.out.println("添加失败");
            }
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }



    public void deleteMedicine() {
        System.out.println("------------------------------------【删除药品信息】------------------------------------------");
        System.out.println("请输入要删除的药品的id：");
        String medicineId = Tools.readString();
        // 先查询一下该id的病房信息
        Object search = medicineService.search(medicineId);
        if (search != null) {
            System.out.println(search);
            System.out.println("确定要删除吗(Y/N)：");
            char choice = Tools.readConfirmSelection();
            if (choice == 'y' || choice == 'Y') {
                int delete = medicineService.deleteMedicine(medicineId);
                if (delete > 0) {
                    System.out.println("删除成功");
                } else {
                    System.out.println("删除失败");
                }
            }
        } else {
            System.out.println("要删除的药品不存在");
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }



    public void updateMedicine() {
        System.out.println("------------------------------------【修改药品信息】------------------------------------------");
        System.out.println("请输入药品id：");
        String oldMedicineId = Tools.readString();
        // 先查询一下该学号的学生信息
        Object search = medicineService.search(oldMedicineId);
        if (search != null) {
            System.out.println(search);
            System.out.println("确定要修改该药品的信息(Y/N)？");
            char choice = Tools.readConfirmSelection();
            if (choice == 'Y' || choice == 'y') {
                System.out.println("请输入需要修改的药品id：");
                String id = Tools.readString();
                System.out.println("请输入需要修改的药品名称：");
                String name = Tools.readString();
                System.out.println("请输入需要修改的药品价格：");
                String price = Tools.readString();
                System.out.println("请输入需要修改的药品适用病症：");
                String drugs = Tools.readString();
                System.out.println("请输入需要修改的药品注意事项：");
                String precautions = Tools.readString();
                System.out.println("请输入需要修改的药品使用说明：");
                String use = Tools.readString();
                System.out.println("请输入需要修改的患者id：");
                String patientId = Tools.readString();
                Medicine medicine = new Medicine(id,name,price,drugs,precautions,use,patientId);
                int updateAll = medicineService.updateAll(oldMedicineId,medicine);
                if (updateAll > 0) {
                    System.out.println("修改成功");
                } else {
                    System.out.println("修改失败");
                }
            }
        } else {
            System.out.println("你要修改的药品不存在！");
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }



    public void searchMedicineById() {
        System.out.println("------------------------------------【查询药品信息】------------------------------------------");
        System.out.println("请输入要查找的药品账号：");
        String medicineId = Tools.readString();
        Object medicine = medicineService.search(medicineId);
        if (medicine != null) {
            System.out.println("查询结果如下：");
            System.out.println(medicine);
        } else {
            System.out.println("该药品信息不存在");
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        Tools.readReturn();
    }


    /**
     * @author Snoopy
     * @Description 显示全部药品信息
     * @Date
     * @Param
     * @Return
     */

    public void showMedicineAll() {
        System.out.println("------------------------------------【显示所有药品信息】---------------------------------------");
        List<Medicine> medicines = medicineService.searchAll();
        for (Medicine key : medicines) {
            System.out.println(key);
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }




    //******************************************************************************************************************
    //******************************************************************************************************************


    public void addPay() {
        System.out.println("------------------------------------【添加缴费记录】------------------------------------------");
        boolean flag = true; // 继续添加缴费标识
        int count = 0; // 当前已成功添加的缴费记录数量
        while (flag) {
            System.out.println("请输入需要添加的缴费id：");
            String id = Tools.readString();
            System.out.println("请输入需要添加的缴费项目：");
            String name = Tools.readString();
            System.out.println("请输入需要添加的缴费应收金额");
            String standard = Tools.readString();
            System.out.println("请输入需要添加的缴费实收金额：");
            String actual = Tools.readString();
            System.out.println("请输入需要添加的缴费支付方式：");
            String type = Tools.readString();
            System.out.println("请输入需要添加的缴费科室id：");
            String departmentId = Tools.readString();
            Pay pay = new Pay(id,name,standard,actual,type,departmentId);
            int register = payService.addPay(pay);
            if (register > 0) {
                count++;
                System.out.println("已成功添加" + count + "条缴费记录");
                System.out.println("是否继续添加(Y/N)？");
                char choice = Tools.readConfirmSelection();
                if (choice == 'Y' || choice == 'y') {
                    System.out.println("-------------------------------------------------------------------------------------------");
                } else {
                    System.out.println("成功添加" + count + "条缴费记录");
                    break;
                }
            } else {
                System.out.println("添加失败");
            }
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }



    public void deletePay() {
        System.out.println("------------------------------------【删除缴费记录】------------------------------------------");
        System.out.println("请输入要删除的缴费记录的id：");
        String payId = Tools.readString();
        // 先查询一下该id的病房信息
        Object search = payService.search(payId);
        if (search != null) {
            System.out.println(search);
            System.out.println("确定要删除吗(Y/N)：");
            char choice = Tools.readConfirmSelection();
            if (choice == 'y' || choice == 'Y') {
                int delete = payService.deletePay(payId);
                if (delete > 0) {
                    System.out.println("删除成功");
                } else {
                    System.out.println("删除失败");
                }
            }
        } else {
            System.out.println("要删除的缴费记录不存在");
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }



    public void updatePay() {
        System.out.println("------------------------------------【修改缴费记录】------------------------------------------");
        System.out.println("请输入缴费记录id：");
        String oldPayId = Tools.readString();
        // 先查询一下该id的缴费记录
        Object search = payService.search(oldPayId);
        if (search != null) {
            System.out.println(search);
            System.out.println("确定要修改该缴费记录的信息(Y/N)？");
            char choice = Tools.readConfirmSelection();
            if (choice == 'Y' || choice == 'y') {
                System.out.println("请输入修改后的缴费id：");
                String id = Tools.readString();
                System.out.println("请输入修改后的缴费项目：");
                String name = Tools.readString();
                System.out.println("请输入修改后的缴费应收金额");
                String standard = Tools.readString();
                System.out.println("请输入修改后的缴费实收金额：");
                String actual = Tools.readString();
                System.out.println("请输入修改后的缴费支付方式：");
                String type = Tools.readString();
                System.out.println("请输入修改后的缴费科室id：");
                String departmentId = Tools.readString();
                Pay pay = new Pay(id,name,standard,actual,type,departmentId);
                int updateAll = payService.updateAll(oldPayId,pay);
                if (updateAll > 0) {
                    System.out.println("修改成功");
                } else {
                    System.out.println("修改失败");
                }
            }
        } else {
            System.out.println("你要修改的缴费记录不存在！");
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }



    public void searchPayById() {
        System.out.println("------------------------------------【查询缴费记录】------------------------------------------");
        System.out.println("请输入要查找的缴费记录id：");
        String payId = Tools.readString();
        Object pay = payService.search(payId);
        if (pay != null) {
            System.out.println("查询结果如下：");
            System.out.println(pay);
        } else {
            System.out.println("该缴费记录不存在");
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        Tools.readReturn();
    }


    /**
     * @author Snoopy
     * @Description 显示全部管理信息
     * @Date
     * @Param
     * @Return
     */

    public void showPayAll() {
        System.out.println("------------------------------------【显示所有缴费记录】---------------------------------------");
            List<Pay> pays = payService.searchAll();
        for (Pay key : pays) {
            System.out.println(key);
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }

    public void clearPayAll() {
        System.out.println("------------------------------------【清空所有缴费记录】---------------------------------------");
        System.out.println("确定要清空所有缴费记录(Y/N),执行此操作将丢失所有缴费记录：");
        char choice = Tools.readConfirmSelection();
        if (choice == 'Y' || choice == 'y') {
            List<Pay> pays = payService.searchAll();
            if (pays.size() > 0 && null != pays) {
                    int clearAll = payService.clearAll();
                    if (clearAll > 0) {
                        System.out.println("缴费记录清空成功");
                    } else {
                        System.out.printf("缴费记录清空失败");
                    }
             } else {
                    System.out.println("尚未存入缴费记录，无法进行清空操作");
             }
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }


    public static void main(String[] args) {
    HospitalView hospitalView = new HospitalView();
    hospitalView.enterRegisterMenu();
}
}

