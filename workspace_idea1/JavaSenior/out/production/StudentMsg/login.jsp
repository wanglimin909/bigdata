<%--
  Created by IntelliJ IDEA.
  User: HUAWEI
  Date: 2023/11/1
  Time: 15:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>登录页面</title>
    <%--引入jQuery的js文件--%>
    <script src="../js/jquery-3.4.1.js"></script>
    <style>
        #d1{
            font-size: 20px;
            color: red;
        }
        #d2{
            width: 90px;
        }
        #t1{
            background-color:darkgray;
            margin-top: 170px;
        }
        #b1{
            background-image: url(../img/boy.jpg);
        }
        #code{
            cursor: pointer;
        }
    </style>
</head>
<body id="b1">
    <!-- 设计表单 -->
    <form action="${pageContext.request.contextPath}/login" method="post">
        <!-- 设计表格 -->
        <table  width="500px" align="center" height="300px" id="t1">
            <tr>
                <td></td>
                <td id="d1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;登录页面</td>
            </tr>
            <tr>
                <td align="right">账号&nbsp;:&nbsp;</td>
                <td>&nbsp;<input type="text" name="username" placeholder="输入账号"></td>
            </tr>
            <tr>
                <td align="right">密码&nbsp;:&nbsp;</td>
                <td>&nbsp;<input type="password" name="password" placeholder="输入密码"></td>
            </tr>
            <tr>
                <td align="right">验证码&nbsp;:&nbsp;</td>
                <td>&nbsp;
                    <input type="text" name="code" id="d2" placeholder="输入验证码">
                    <img src="${pageContext.request.contextPath}/authImage" width="70px" height="22px" id="code">
                </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <span>
                        <font color="red" size="4px">${msg}</font>
                    </span>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" value="登 录"></td>
            </tr>
        </table>
    </form>
</body>
</html>
<script>
    /*点击切换验证码*/
    /*为图片验证码绑定一个点击事件*/
    $("#code").click(function(){
        /*切换验证码  加上一个时间戳*/
        this.src="${pageContext.request.contextPath}/authImage?date="+new Date();
    })
</script>
