<%--
  Created by IntelliJ IDEA.
  User: HUAWEI
  Date: 2023/11/1
  Time: 15:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>注册页面</title>
    <style>
        #d1{
            font-size: 20px;
            color: red;
        }
        #d2{
            width: 200px;
        }
        #t1{
            background-color:darkgray;
            margin-top: 170px;
        }
        #b1{
            background-image: url(../img/boy.jpg);
        }
    </style>
</head>
<body id="b1">
    <!-- 设计表单 -->
    <form action="${pageContext.request.contextPath}/register" method="post">
        <!-- 设计表格 -->
        <table  width="500px" align="center" height="350px" id="t1">
            <tr>
                <td></td>
                <td id="d1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;注册用户</td>
            </tr>
            <tr>
                <td align="right">账号&nbsp;:&nbsp;</td>
                <td>&nbsp;<input type="text" name="username" placeholder="请输入账号"></td>
            </tr>
            <tr>
                <td align="right">密码&nbsp;:&nbsp;</td>
                <td>&nbsp;<input type="password" name="password" placeholder="请输入密码"></td>
            </tr>
            <tr>
                <td align="right">姓名&nbsp;:&nbsp;</td>
                <td>&nbsp;<input type="text" name="ufname" placeholder="请输入姓名"></td>
            </tr>
            <tr>
                <td align="right">性别&nbsp;:&nbsp;</td>
                <td>&nbsp;
                    <input type="radio" name="usex" value="男">男
                    <input type="radio" name="usex" value="女">女
                </td>
            </tr>
            <tr>
                <td align="right">学历&nbsp;:&nbsp;</td>
                <td>&nbsp;
                    <select name="udegree">
                        <option>==请选择==</option>
                        <option value="博士">博士</option>
                        <option value="硕士">硕士</option>
                        <option value="本科">本科</option>
                        <option value="专科">专科</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td align="right">家庭住址&nbsp;:&nbsp;</td>
                <td>&nbsp;<input type="text" name="uaddr" id="d2" placeholder="请输入家庭住址"></td>
            </tr>
            <tr>
                <td></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" value="注 册"></td>
            </tr>
        </table>
    </form>
</body>
</html>
