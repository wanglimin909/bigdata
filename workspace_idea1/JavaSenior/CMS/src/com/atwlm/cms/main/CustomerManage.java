package com.atwlm.cms.main;

import com.atwlm.cms.view.CustomerView;

/**
 * @author wlm.java
 * @create 2023-06-22 19:31
 */
public class CustomerManage {
    public static void main(String[] args) {
        CustomerView view = new CustomerView();
        view.enterMainMenu();
    }
}
