package com.tedu.c3p0;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.tedu.pojo.Student;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.junit.Test;

import java.util.List;

/**
 * 使用步骤:
 * 	1. 导入C3P0和DBUtils的依赖包(c3p0-0.9.5.2.jar, mchange-commons-java-0.2.11.jar, commons-dbutils-1.7.jar)
 *  2. 引入C3P0提供的配置文件, 作用是与数据库交互的
 *  3. 配置文件放置的位置, 必须放在src目录下
 *  4. 配置文件的名称是c3p0-config.xml, 一定不要改名字
 *  5. 测试 实现CRUD
 */
public class C3P0Demo {

    /**
     * 新增学生信息
     */
    @Test
    public void insertStudent() throws Exception {
        //1. 创建核心类  传递参数: 数据源
        QueryRunner qr = new QueryRunner(new ComboPooledDataSource());
        //2. 定义SQL语句
        String sql = "INSERT INTO student(sno, sname, ssex, sbirth, saddr, sclass) VALUES(?,?,?,?,?,?)";
        //赋值
        Object[] param = {"20211002208", "柳岩", "女", "2023-02-02", "湖南", "大数据2班"};
        //3. 执行SQL语句
        int i = qr.update(sql, param);
        //4. 判断
        if (i > 0){
            System.out.println("更新成功");
        }
    }

    /**
     * 修改学生信息
     */
    @Test
    public void updateStudent() throws Exception {
        //1. 创建核心类  传递参数: 数据源
        QueryRunner qr = new QueryRunner(new ComboPooledDataSource());
        //2. 定义SQL语句
        String sql = "UPDATE student SET sname=? WHERE sno=?";
        //赋值
        Object[] param = {"刘涛", "20211002208"};
        //3. 执行SQL语句
        int i = qr.update(sql, param);
        //4. 判断
        if (i > 0){
            System.out.println("更新成功");
        }
    }

    /**
     * 删除学生信息
     */
    @Test
    public void deleteStudent() throws Exception {
        //1. 创建核心类  传递参数: 数据源
        QueryRunner qr = new QueryRunner(new ComboPooledDataSource());
        //2. 定义SQL语句
        String sql = "DELETE FROM student WHERE sno=?";
        //3. 执行SQL语句
        int i = qr.update(sql, "20211002208");
        //4. 判断
        if (i > 0){
            System.out.println("更新成功");
        }
    }

    /**
     * 查询全部学生信息
     */
    @Test
    public void findAllStudent() throws Exception {
        //1. 创建核心类  传递参数: 数据源
        QueryRunner qr = new QueryRunner(new ComboPooledDataSource());
        //2. 定义SQL语句
        String sql = "select * from student";
        //3. 执行SQL语句  返回值类型是多个用BeanListHandler  <>泛型  ()写字节码
        List<Student> studentList = qr.query(sql, new BeanListHandler<Student>(Student.class));
        for (Student student : studentList) {
            System.out.println(student);
        }
    }


    /**
     * 查询某一个学生信息
     */
    @Test
    public void findStudentBySno() throws Exception {
        //1. 创建核心类  传递参数: 数据源
        QueryRunner qr = new QueryRunner(new ComboPooledDataSource());
        //2. 定义SQL语句
        String sql = "select * from student where sno=?";
        //3. 执行SQL语句  返回值类型是单个用BeanHandler  <>泛型  ()写字节码
        Student student = qr.query(sql, new BeanHandler<Student>(Student.class), "20211002207");
        System.out.println(student);
    }

}
