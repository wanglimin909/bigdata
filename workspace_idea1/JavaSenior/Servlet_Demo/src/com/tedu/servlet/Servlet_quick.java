package com.tedu.servlet;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * servlet入门案例:
 * 步骤:
 *  1. 创建一个普通类(Servlet_quick), 将其普通类继承一个抽象类HttpServlet
 *  2. 重写service()方法, 再其中写主要程序
 *  3. 配置访问路径(配置文件形式 | 注解形式)
 *  4. 在页面中配置访问路径, 将页面与程序进行交互
 *  5. 测试
 */
//1. 创建一个普通类(Servlet_quick), 将其普通类继承一个抽象类HttpServlet
@WebServlet("/servlet_quick")   //3. 配置访问路径(配置文件形式 | 注解形式@WebServlet("/")) 斜杠不能缺少
public class Servlet_quick extends HttpServlet {

    /**
     * 2. 重写service()方法, 再其中写主要程序
     * @param request   请求  页面向服务端发送的是请求
     * @param response  响应  把服务端的数据给页面进行展示, 叫响应
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /**
         * 接收页面发送的请求, 获取用户账号和密码
         * getParameter(String name): 根据input框中name属性的属性值 获取页面输入框手动输入的内容
         */
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        System.out.println("账号为: " + username + " ==> 密码为: " + password);
        //响应给浏览器, 告诉浏览器接收到了数据
        response.getWriter().write("success...");
    }
}
