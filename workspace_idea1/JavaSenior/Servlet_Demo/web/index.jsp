<%--
  Created by IntelliJ IDEA.
  User: HUAWEI
  Date: 2023/10/31
  Time: 16:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>$Title$</title>
    <style>
      #d1{
        font-size: 20px;
        color: red;
      }
      #d2{
        width: 80px;
      }
      #t1{
        background-color:darkgray;
        margin-top: 170px;
      }
      #b1{
        background-image: url(./img/boy.jpg);
      }
    </style>
  </head>
  <body id="b1">
    <!-- 设计表单 -->
    <%-- ${pageContext.request.contextPath}/servlet_quick
          动态获取项目名 Servlet_Demo       / 访问路径
     --%>
    <form action="${pageContext.request.contextPath}/servlet_quick" method="post">
      <!-- 设计表格 -->
      <table  width="500px" align="center" height="300px" id="t1">
        <tr>
          <td></td>
          <td id="d1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;登录页面</td>
        </tr>
        <tr>
          <td align="right">账号&nbsp;:&nbsp;</td>
          <td>&nbsp;<input type="text" name="username" placeholder="输入账号"></td>
        </tr>
        <tr>
          <td align="right">密码&nbsp;:&nbsp;</td>
          <td>&nbsp;<input type="password" name="password" placeholder="输入密码"></td>
        </tr>
        <tr>
          <td align="right">验证码&nbsp;:&nbsp;</td>
          <td>&nbsp;
            <input type="text" name="code" id="d2" placeholder="输入验证码">
            <img src="./img/code.png" width="70px" height="22px">
          </td>
        </tr>
        <tr>
          <td></td>
          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" value="登 录"></td>
        </tr>
      </table>
    </form>
  </body>
</html>
