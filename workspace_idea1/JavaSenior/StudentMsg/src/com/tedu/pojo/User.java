package com.tedu.pojo;

public class User {
    private int uid;
    private String username;
    private String password;
    private String ufname;
    private String usex;
    private String udegree;
    private String uaddr;

    public User() {
    }

    public User(int uid, String username, String password, String ufname, String usex, String udegree, String uaddr) {
        this.uid = uid;
        this.username = username;
        this.password = password;
        this.ufname = ufname;
        this.usex = usex;
        this.udegree = udegree;
        this.uaddr = uaddr;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUfname() {
        return ufname;
    }

    public void setUfname(String ufname) {
        this.ufname = ufname;
    }

    public String getUsex() {
        return usex;
    }

    public void setUsex(String usex) {
        this.usex = usex;
    }

    public String getUdegree() {
        return udegree;
    }

    public void setUdegree(String udegree) {
        this.udegree = udegree;
    }

    public String getUaddr() {
        return uaddr;
    }

    public void setUaddr(String uaddr) {
        this.uaddr = uaddr;
    }

    @Override
    public String toString() {
        return "User{" +
                "uid=" + uid +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", ufname='" + ufname + '\'' +
                ", usex='" + usex + '\'' +
                ", udegree='" + udegree + '\'' +
                ", uaddr='" + uaddr + '\'' +
                '}';
    }
}
