package com.tedu.servlet;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.tedu.pojo.User;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // 获取账号和秘密
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            // 通过账号和密码查询数据库进行登录
            // 创建核心类
            QueryRunner qr = new QueryRunner(new ComboPooledDataSource());
            // 定义sql语句
            String sql = "SELECT * FROM `user` WHERE username=? AND password=?";
            // 赋值
            Object[] param = {username, password};
            // 执行sql语句
            User user = qr.query(sql, new BeanHandler<User>(User.class), param);
            //判断user是否为空
            if (user != null){
                //保存下用户信息到session作用域中
                request.getSession().setAttribute("user", user);
                //登录成功
                response.sendRedirect(request.getContextPath() + "/page/index.jsp");
            }else{
                //登录失败
                request.getSession().setAttribute("msg", "账号或者密码错误...");
                //跳转登录页面
                response.sendRedirect(request.getContextPath() + "/page/login.jsp");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
