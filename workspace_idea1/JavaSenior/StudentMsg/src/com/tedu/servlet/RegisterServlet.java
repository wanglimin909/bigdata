package com.tedu.servlet;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.apache.commons.dbutils.QueryRunner;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * 注册功能
 */
@WebServlet("/register")
public class RegisterServlet extends HttpServlet {

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            /**
             * 处理乱码问题:
             * 1. 页面发送请求到达服务端接收的数据 出现了乱码问题
             * 解决方案:
             *      request.setCharacterEncoding("utf-8");
             * 2. 服务端响应页面需要展示的内容  出现乱码问题
             * 解决方案:
             *      response.setContentType("text/html;charset=utf-8");
             */
            request.setCharacterEncoding("utf-8");
            response.setContentType("text/html;charset=utf-8");
            //获取页面传递用户信息
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            String ufname = request.getParameter("ufname");
            String usex = request.getParameter("usex");
            String udegree = request.getParameter("udegree");
            String uaddr = request.getParameter("uaddr");
            //将用户信息保存到数据库
            //1. 创建核心类  传递参数: 数据源
            QueryRunner qr = new QueryRunner(new ComboPooledDataSource());
            //2. 定义SQL语句
            String sql = "INSERT INTO user(username, password, ufname, usex, udegree, uaddr) VALUES(?,?,?,?,?,?)";
            //赋值
            Object[] param = {username, password, ufname, usex, udegree, uaddr};
            //3. 执行SQL语句
            int i = qr.update(sql, param);
            //4. 判断
            if (i > 0){
                //跳转到登录页面   request.getContextPath()动态获取项目名
                //资源跳转之一: response.sendRedirect()  重定向到另一个页面
                response.sendRedirect(request.getContextPath() + "/page/login.jsp");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
