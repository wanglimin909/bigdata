package com.atwlm.api.utils;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * 基于工具类v1.0的curd
 *
 * @author wlm.java
 * @create 2023-06-22 15:25
 */
public class JdbcCurdPart {

    public void testInsert() throws SQLException {

        Connection connection = JdbcUtils.getConnection();

        //数据库的curd动作

        JdbcUtils.freeConnection(connection);
    }


}
