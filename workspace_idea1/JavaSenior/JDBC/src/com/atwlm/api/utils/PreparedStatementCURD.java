package com.atwlm.api.utils;

import org.junit.Test;

import java.sql.SQLException;

/**
 *
 * 使用preparedStatement进行t_user的curd操作
 *
 * @author wlm.java
 * @create 2023-06-21 16:16
 */
public class PreparedStatementCURD extends BaseDao{

    @Test
    public void testInsert() throws ClassNotFoundException, SQLException {
        /**
         * t_user表插入一条数据！
         *      account     test
         *      password    test
         *      nickname    张三
         */
        //3.编写SQL语句结构，动态值部分使用?代替
        String sql = "INSERT INTO t_user(account,PASSWORD,nickname)VALUES(?,?,?);";

        int rows = executeUpdate(sql, "test", "test", "张三");
        System.out.println("rows = " + rows);

    }


    @Test
    public void testUpdate() throws ClassNotFoundException, SQLException {
        /**
         * 修改 id=3 的用户 nickname = 李四
         */
        //3.编写SQL语句结构，动态值部分使用?代替
        String sql = "UPDATE t_user SET nickname = ? WHERE id = ?";

        int rows = executeUpdate(sql, "李四", 3);
        System.out.println("rows = " + rows);
    }


    @Test
    public void testDelete() throws ClassNotFoundException, SQLException {
        /**
         * 删除 id=3 的用户数据
         */
        //3.编写SQL语句结构，动态值部分使用?代替
        String sql = "DELETE FROM t_user WHERE id = ? ";

        int rows = executeUpdate(sql, 3);
        System.out.println("rows = " + rows);
    }

    /**
     * 目标：查询所有用户数据，并且封装到一个List<Map> list集合中
     *
     */
    @Test
    public void testSelect() throws Exception {
        //3.编写SQL语句结构，动态值部分使用?代替
        String sql = "SELECT id,account,PASSWORD,nickname FROM t_user";

    }
}
