package com.atwlm.api.statement;

import java.sql.*;
import java.util.Scanner;

/**
 *
 * 模拟用户登录
 * TODO：
 *  1.明确JDBC的使用流程和内部涉及的api方法
 *  2.发现问题，引出preparedStatement
 *
 * TODO:
 *  输入账号和密码
 *  进行数据库信息查询（t_user）
 *  反馈登录成功还是登录失败
 *
 * TODO：
 *  1.键盘输入事件，收集账号和密码信息
 *  2.注册驱动
 *  3.获取连接
 *  4.创建statement
 *  5.发送查询sql语句并获取查询结果
 *  6.结果判断，显示登录成功还是失败
 *  7.关闭资源
 *
 * @author wlm.java
 * @create 2023-06-20 16:05
 */
public class StatementUserLoginPart {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {

        //1.获取用户输入信息
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入账号：");
        String account = scanner.nextLine();
        System.out.println("请输入密码：");
        String password = scanner.nextLine();

        //2.注册驱动
        /**
         *方法一：
         *      DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
         *      注:8+  com.mysql.cj.jdbc.Driver()
         *         5+  com.mysql.jdbc.Driver()
         *      问题:会注册两次驱动
         *              1.DriverManager.registerDriver()方法本身会注册一次
         *              2.Driver.static{DriverManager.registerDriver()}静态代码块，也会注册一次
         *      解决：只想注册一次驱动
         *              只触发静态代码块即可！Driver.static{DriverManager.registerDriver()}
         *      触发静态代码块：
         *          类加载机制：类加载的时刻，会触发静态代码块！
         *              加载【class文件 ---> jvm虚拟机的class对象】
         *              连接【验证（检查文件类型） ---> 准备（静态变量默认值） ---> 解析（触发静态代码块）】
         *              初始化【静态属性附真实值】
         *      触发类加载：
         *          1.new 关键字
         *          2.调用静态方法
         *          3.调用静态属性
         *          4.接口 1.8 default默认实现
         *          5.发射
         *          6.子类触发父类
         *          7.程序的入口main
         */
        //2.注册驱动
        //方案1：会注册两次驱动，淘汰
//        DriverManager.registerDriver(new Driver());

        //方案2：mysql新版本的驱动 | 换成Oracle
//        new Driver();

        //方案3：字符串 ---> 提取到外部的配置文件 ---> 可以在不改变代码的情况下，完成数据库驱动的切换！
        Class.forName("com.mysql.cj.jdbc.Driver");//触发类加载，触发静态代码块的调用

        //3.获取数据库连接
        /**
         * getConnection(1,2,3)方法，是一个重载方法！
         * 允许开发者，用不同的形式传入数据库连接的核心参数！
         *
         * 核心属性：
         *      1.数据库软件所在的主机的ip地址：localhost | 127.0.0.1
         *      2.数据库软件所在主机的端口号：3306（默认）
         *      3.连接的具体库：atwlm
         *      4.连接的账号：root
         *      5.连接的密码：123456
         *      6.可选的信息：...
         *
         * 三个参数：
         *      String url          数据库软件所在的信息，连接的具体库，以及其他可选信息！
         *                          语法：jdbc:数据库管理软件名称[mysql,oracle]://ip地址|主机名：part端口号/数据库名?key=value&key=value 可选信息！
         *                          具体: jdbc:mysql://127.0.0.1:3306/atwlm
         *                                jdbc:mysql://localhost:3306/atwlm
         *                          本机的省略写法：如果你的数据库软件安装到本机，可以进行一些省略
         *                          jdbc:mysql://127.0.0.1:3306/atwlm  =  jdbc:mysql:///atwlm
         *                          省略了本机地址和3306默认端口号
         *                          强调：必须是本机，并且端口号是3306，才能省略
         *      String user         数据库账号   root
         *      String password     数据库密码   123456
         * 二个参数：
         *      String url          数据库ip，端口号，具体的数据库和可选信息
         *      Properties info     存储账号和密码
         *                          Properties 类似于 Map 只不过 key = value 都是字符串类型的！
         *                          key user : 账号信息
         *                          key password : 密码信息
         *
         * 一个参数：
         *      String url          数据库ip，端口号，具体的数据库  可选信息（）
         *                          jdbc:数据库软件名：//ip:part/数据库?key=value&key=value&key=value
         *
         *                          jdbc:mysql://localhost:3306/atwlm?user=root&password=123456
         *                          携带固定的参数名:user password 传递账号密码信息
         *
         */
        Connection connection = DriverManager.getConnection("jdbc:mysql:///atwlm", "root", "123456");

//        Properties info = new Properties();
//        info.put("user","root");
//        info.put("password","123456");
//        Connection connection1 = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/atwlm", info);
//
//        Connection connection2 = DriverManager.getConnection("jdbc:mysql://localhost:3306/atwlm?user=root&password=123456");

        //4.创建发送sql语句的statement对象
        //statement可以发送sql语句到数据库，并且获取返回结果！
        Statement statement = connection.createStatement();

        //5.发送sql语句（1.编写sql语句  2.发送sql语句）
        String sql = "SELECT * FROM t_user WHERE account = '"+account+"'AND PASSWORD = '"+password+"'";
        /**
         * SQL分类：DDL(容器创建，修改，删除)  DML(插入，修改，删除)  DQL(查询)  DCL(权限控制)  TPL(事物控制语言)
         *
         * 参数：sql 【非DQL】
         * 返回：int
         *          情况一：DML 返回影响的行数
         *                          例如删除了三条数据 return 3;插入了两条数据 return 2;修改了0条数据 return 0
         *          情况二：非DML return 0;
         * int row = executeUpdate(sql)
         *
         * 参数：sql 【DQL】
         * 返回：ResultSet 结果封装对象
         * ResultSet resultSet = executeQuery(sql);
         *
         */

//        int i = statement.executeUpdate(sql);

        ResultSet resultSet = statement.executeQuery(sql);

        //6.查询结果集解析 resultSet
        /**
         * java是一种面向对象的思维，将查询结果封装成了resultSet对象，我们应该理解，内部一定也是有行有列的，和Navicat的数据一样。
         *
         * 1.游标移动问题
         * resultSet ---> 逐行获取数据，行 ---> 行中列的数据
         *
         * boolean = next()  【默认初始指向的是第一行数据之前】
         *          true:有更多行数据并向下移动一行
         *          false:没有更多行数据，不移动
         *
         * 2.获取列的数据问题(获取光标指定的行的列的数据)
         *
         *      resultSet.get类型(String columnLabel | columnIndex);
         *              columnLabel:列名  如果有别名 写别名   select * | (id,account,password,nickname)
         *                                                   select id as aid,account as ac from
         *              columnIndex:列的下角标获取  从左向右 从1开始
         *
         */
//        while(resultSet.next()){
//            //指向当前行了
//            int id = resultSet.getInt(1);
//            String account1 = resultSet.getString("account");
//            String password1 = resultSet.getString(3);
//            String nickname = resultSet.getString("nickname");
//            System.out.println(id + "--" + account1 + "--" + password1 + "--" + nickname);
//        }

        //移动一次光标，只要有数据，就代表登录成功
        if(resultSet.next()){
            System.out.println("登录成功！");
        }else{
            System.out.println("登录失败！");
        }

        //7.关闭资源
        resultSet.close();
        statement.close();
        connection.close();
    }
}
