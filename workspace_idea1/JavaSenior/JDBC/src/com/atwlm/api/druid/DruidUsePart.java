package com.atwlm.api.druid;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.pool.DruidDataSourceFactory;
import org.junit.Test;

import javax.sql.DataSource;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
 * druid连接池使用类
 *
 * @author wlm.java
 * @create 2023-06-22 14:17
 */
public class DruidUsePart {

    /**
     * 直接使用代码设置连接池连接参数方式！
     *      1.创建一个Druid连接池对象
     *      2.设置连接池参数【必须】【非必须】
     *      3.获取连接【通用方法，所有连接池都一样】
     *      4.回收连接【通用方法，所有连接池都一样】
     */
    @Test
    public void testHard() throws SQLException {

        //连接池对象
        DruidDataSource druidDataSource = new DruidDataSource();

        //设置参数
            //必须        连接数据库驱动类的全限定符【注册驱动】 | url | user | password
        druidDataSource.setUrl("jdbc:mysql://localhost:3306/atwlm");
        druidDataSource.setUsername("root");
        druidDataSource.setPassword("123456");
        druidDataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");//帮助我们进行驱动注册和获取连接
            //非必须      初始化连接数量,最大的连接数量......
        druidDataSource.setInitialSize(5);//初始化的数量
        druidDataSource.setMaxActive(10);//最大的数量

        //获取连接
        Connection connection = druidDataSource.getConnection();

        //数据库curd

        //回收连接
        connection.close();//连接池提供的连接，close，就是回收连接

    }

    /**
     * 通过读取外部配置文件的方式，实例化druid连接池对象
     *
     */
    @Test
    public void testSoft() throws Exception {

        //1.读取外部配置文件 Properties
        Properties properties = new Properties();

        //src下的文件，可以使用类加载器提供的方法
        InputStream ips = DruidUsePart.class.getClassLoader().getResourceAsStream("druid.properties");

        properties.load(ips);

        //2.使用连接池的工具类的工厂模式，创建连接池
        DataSource dataSource = DruidDataSourceFactory.createDataSource(properties);

        Connection connection = dataSource.getConnection();

        //数据库的curd

        connection.close();

    }




}
