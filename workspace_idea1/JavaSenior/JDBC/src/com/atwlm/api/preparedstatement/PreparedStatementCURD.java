package com.atwlm.api.preparedstatement;

import org.junit.Test;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * 使用preparedStatement进行t_user的curd操作
 *
 * @author wlm.java
 * @create 2023-06-21 16:16
 */
public class PreparedStatementCURD {


    @Test
    public void testInsert() throws ClassNotFoundException, SQLException {
        /**
         * t_user表插入一条数据！
         *      account     test
         *      password    test
         *      nickname    张三
         */
        //1.注册驱动
        Class.forName("com.mysql.cj.jdbc.Driver");
        //2.获取连接
        Connection connection = DriverManager.getConnection("jdbc:mysql:///atwlm", "root", "123456");
        //3.编写SQL语句结构，动态值部分使用?代替
        String sql = "INSERT INTO t_user(account,PASSWORD,nickname)VALUES(?,?,?);";
        //4.创建preparedStatement，并且传入sql语句结构
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        //5.占位符赋值
        preparedStatement.setObject(1,"test");
        preparedStatement.setObject(2,"test");
        preparedStatement.setObject(3,"张三");
        //6.发送sql语句
        /**
         * DML类型
         */
        int rows = preparedStatement.executeUpdate();
        //7.输出结果
        if(rows > 0){
            System.out.println("数据插入成功");
        }else{
            System.out.println("数据插入失败");
        }
        //8.关闭资源
        preparedStatement.close();
        connection.close();
    }


    @Test
    public void testUpdate() throws ClassNotFoundException, SQLException {
        /**
         * 修改 id=3 的用户 nickname = 李四
         */
        //1.注册驱动
        Class.forName("com.mysql.cj.jdbc.Driver");
        //2.获取连接
        Connection connection = DriverManager.getConnection("jdbc:mysql:///atwlm", "root", "123456");
        //3.编写SQL语句结构，动态值部分使用?代替
        String sql = "UPDATE t_user SET nickname = ? WHERE id = ?";
        //4.创建preparedStatement，并且传入sql语句结构
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        //5.占位符赋值
        preparedStatement.setObject(1,"李四");
        preparedStatement.setObject(2,"3");
        //6.发送sql语句
        int i = preparedStatement.executeUpdate();
        //7.输出结果
        if(i > 0){
            System.out.println("修改成功");
        }else{
            System.out.println("修改失败");
        }
        //8.关闭资源
        preparedStatement.close();
        connection.close();
    }


    @Test
    public void testDelete() throws ClassNotFoundException, SQLException {
        /**
         * 删除 id=3 的用户数据
         */
        //1.注册驱动
        Class.forName("com.mysql.cj.jdbc.Driver");
        //2.获取连接
        Connection connection = DriverManager.getConnection("jdbc:mysql:///atwlm", "root", "123456");
        //3.编写SQL语句结构，动态值部分使用?代替
        String sql = "DELETE FROM t_user WHERE id = ? ";
        //4.创建preparedStatement，并且传入sql语句结构
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        //5.占位符赋值
        preparedStatement.setObject(1,3);
        //6.发送sql语句
        int i = preparedStatement.executeUpdate();
        //7.输出结果
        if(i > 0){
            System.out.println("删除成功");
        }else{
            System.out.println("删除失败");
        }
        //8.关闭资源
        preparedStatement.close();
        connection.close();
    }

    /**
     * 目标：查询所有用户数据，并且封装到一个List<Map> list集合中
     *
     * 解释：
     *      行   id    account   password    nickname
     *      行   id    account   password    nickname
     *      行   id    account   password    nickname
     *
     * 数据库 ---> resultSet ---> java ---> 一行 - map（key=列名，value=列的内容） ---> List<Map> list
     *
     * 实现思路：
     *      遍历行数据，一行对应一个map！获取一行的列名和对应的列的属性，装配即可！
     *      将map装到一个集合！
     *
     * 难点：
     *      如何获取列的名称？
     *
     */
    @Test
    public void testSelect() throws Exception {
        //1.注册驱动
        Class.forName("com.mysql.cj.jdbc.Driver");
        //2.获取连接
        Connection connection = DriverManager.getConnection("jdbc:mysql:///atwlm", "root", "123456");
        //3.编写SQL语句结构，动态值部分使用?代替
        String sql = "SELECT id,account,PASSWORD,nickname FROM t_user";
        //4.创建preparedStatement，并且传入sql语句结构
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        //5.占位符赋值
            //省略占位符赋值
        //6.发送sql语句
        ResultSet resultSet = preparedStatement.executeQuery();
        //7.结果集解析
        /**
         * TODO:
         *      resultSet:有行和有列！ 获取数据的时候是一行一行获取的！
         *                内部有一个游标，默认指向数据的第一行之前！
         *                我们可以利用next()方法移动游标，指向数据行！
         *
         *                获取行中的列的数据
         */
        List<Map> list = new ArrayList<>();

        //获取列的信息对象
        //TODO: metaData 装的当前结果集列的信息对象！（可以获取列的名称根据下角标，也可以获取列的数量）
        ResultSetMetaData metaData = resultSet.getMetaData();
        //有了它以后，就可以水平遍历列！
        int columnCount = metaData.getColumnCount();

        while (resultSet.next()){
            Map map = new HashMap();
            //一行数据对应一个map

            //纯手动取值！
//            map.put("id",resultSet.getInt("id"));
//            map.put("account",resultSet.getString("account"));
//            map.put("password",resultSet.getString("PASSWORD"));
//            map.put("nickname",resultSet.getString("nickname"));

            //自动遍历列
                //注意：要从1开始，并且小于等于总列数！
            for (int i = 1; i <= columnCount; i++) {

                //获取指定列下角标的值【resultSet】
                Object object = resultSet.getObject(i);

                //获取指定列下角标的列的名称【ResultSetMetaData】
                    //getColumnLabel:会获取别名，如果没有写别名才是列的名称 而不要使用 getColumnName:只会获取列的名称
                String columnLabel = metaData.getColumnLabel(i);

                map.put(columnLabel,object);
            }
            //一行数据的所有列全部存到了map中！

            //将map存储到集合中即可。
            list.add(map);
        }

        System.out.println("list = " + list);
        //8.关闭资源
        resultSet.close();
        preparedStatement.close();
        connection.close();
    }
}
