package com.atwlm.api.preparedstatement;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Scanner;

/**
 *
 * 使用预编译的statement完成用户登录
 *
 * TODO: 防止注入攻击  |  演示PS的使用流程
 *
 *
 * @author wlm.java
 * @create 2023-06-20 19:00
 */
public class PreparedStatementUserLoginPart {
    public static void main(String[] args) throws Exception {

        //1.获取用户输入信息
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入账号：");
        String account = scanner.nextLine();
        System.out.println("请输入密码：");
        String password = scanner.nextLine();

        //2.PS的数据库流程
            //1.注册驱动
            Class.forName("com.mysql.cj.jdbc.Driver");
            //2.获取连接
            Connection connection = DriverManager.getConnection("jdbc:mysql:///atwlm?user=root&password=123456");
            /**
             * statement
             *      1.创建statement
             *      2.拼接SQL语句
             *      3.发送SQL语句，并获取返回结果
             * preparedstatement
             *      1.编写SQL语句结构【不包含动态值部分的语句，动态值部分使用占位符?替代】注：?只能替代动态值
             *      2.创建preparedstatement，并且传入动态值
             *      3.动态值  占位符  赋值  ?  单独赋值即可
             *      4.发送SQL语句即可，并获取返回结果
             */
            //3.编写SQL语句结构
            String sql = "SELECT * FROM t_user WHERE account = ? AND password = ?";
            //4.创建预编译的statement并且设置SQL语句的结果
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            //5.单独的占位符进行赋值
            /**
             * 参数一：index 占位符的位置从左向右数【从1开始】
             * 参数二：object 占位符的值 可以设置任何类型的数据，避免了我们拼接和类型更加丰富
             */
            preparedStatement.setObject(1,account);
            preparedStatement.setObject(2,password);
            //6.发送SQL语句并返回结果
            //statement.executeUpdate | executeQuery(String sql);
            //preparedStatement.executeUpdate | executeQuery(); TODO:因为它已经知道语句，知道语句动态值！
            ResultSet resultSet = preparedStatement.executeQuery();
            //7.结果集解析
            if(resultSet.next()){
                System.out.println("登录成功！");
            }else{
                System.out.println("登录失败！");
            }
            //8.关闭资源
            resultSet.close();
            preparedStatement.close();
            connection.close();
    }
}
