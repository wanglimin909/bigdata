package com.atwlm.api.preparedstatement;
import org.junit.Test;
import java.sql.*;
/**
 *
 * 练习 preparedStatement 的特殊使用情况
 *
 * @author wlm.java
 * @create 2023-06-21 19:09
 */
public class PreparedStatementOtherPart {

    /**
     * TODO:
     *      t_user表插入一条数据！并且获取数据库自增长的主键！
     *
     * TODO:使用总结
     *      1.创建preparedStatement的时候，告知，携带回数据库自增长的主键(sql, Statement.RETURN_GENERATED_KEYS)
     *      2.获取装主键结果集的对象，一行一列，获取对应的数据即可
     */
    @Test
    public void returnPrimaryKey() throws Exception {
        //1.注册驱动
        Class.forName("com.mysql.cj.jdbc.Driver");
        //2.获取连接
        Connection connection = DriverManager.getConnection("jdbc:mysql:///atwlm", "root", "123456");
        //3.编写sql语句
        String sql = "insert into t_user (account,PASSWORD,nickname) values (?,?,?)";
        //4.创建statement
        PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        //5.占位符赋值
        preparedStatement.setObject(1,"test1");
        preparedStatement.setObject(2,"123456");
        preparedStatement.setObject(3,"王五");
        //6.发送sql语句，并获取结果
        int i = preparedStatement.executeUpdate();
        //7.结果解析
        if(i > 0){
            System.out.println("插入成功");

            //可以获取回显的主键
                //获取装主键的结果集对象，一行一列  id = 值
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            generatedKeys.next();//移动下光标！
            int id = generatedKeys.getInt(1);
            System.out.println("id " + id);
        }else{
            System.out.println("插入失败");
        }
        //8.关闭资源
        preparedStatement.close();
        connection.close();
    }

    /**
     * 使用普通的方式插入10000条数据
     * @throws Exception
     */
    @Test
    public void testInsert() throws Exception {
        //1.注册驱动
        Class.forName("com.mysql.cj.jdbc.Driver");
        //2.获取连接
        Connection connection = DriverManager.getConnection("jdbc:mysql:///atwlm", "root", "123456");
        //3.编写sql语句
        String sql = "insert into t_user (account,PASSWORD,nickname) values (?,?,?)";
        //4.创建statement
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        //5.占位符赋值

        long start = System.currentTimeMillis();

        for (int i = 0; i < 10000; i++) {
            preparedStatement.setObject(1,"dd"+i);
            preparedStatement.setObject(2,"dd"+i);
            preparedStatement.setObject(3,"王五"+i);
            //6.发送sql语句，并获取结果
            preparedStatement.executeUpdate();
        }

        long end = System.currentTimeMillis();

        //7.结果解析
        System.out.println("执行10000次数据插入消耗的时间：" + (end-start));//14433
        //8.关闭资源
        preparedStatement.close();
        connection.close();
    }

    /**
     * 使用批量插入的方式插入10000条数据
     * @throws Exception
     * TODO:总结批量插入
     *      1.路径后面添加 ?rewriteBatchedStatements=true 允许批量插入
     *      2.insert into values【必须写values】,语句不能以;结束
     *      3.不是执行语句每条，是批量添加 addBatch() ;
     *      4.遍历添加完毕以后，统一批量执行 executeBatch()
     */
    @Test
    public void testBatchInsert() throws Exception {
        //1.注册驱动
        Class.forName("com.mysql.cj.jdbc.Driver");
        //2.获取连接
        Connection connection = DriverManager.getConnection("jdbc:mysql:///atwlm?rewriteBatchedStatements=true", "root", "123456");
        //3.编写sql语句
        String sql = "insert into t_user (account,PASSWORD,nickname) values (?,?,?)";
        //4.创建statement
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        //5.占位符赋值

        long start = System.currentTimeMillis();

        for (int i = 0; i < 10000; i++) {
            preparedStatement.setObject(1,"ddd"+i);
            preparedStatement.setObject(2,"ddd"+i);
            preparedStatement.setObject(3,"王五d"+i);

            preparedStatement.addBatch();//不执行，追加到values的后面！
        }

        preparedStatement.executeBatch();//执行批量操作

        long end = System.currentTimeMillis();

        //7.结果解析
        System.out.println("执行10000次数据插入消耗的时间：" + (end-start));//173
        //8.关闭资源
        preparedStatement.close();
        connection.close();
    }
}
