package com.atwlm.api.transactionnew;

import com.atwlm.api.utils.JdbcUtilsV2;
import org.junit.Test;

import java.sql.Connection;

/**
 * 银行卡业务方法，调用dao方法
 *
 * @author wlm.java
 * @create 2023-06-21 20:24
 */
public class BankService {

    @Test
    public void start() throws Exception {
        transfer("wlm","mmm",500);
    }

    /**
     * TODO:
     *      事物添加是在业务方法中！
     *      利用try-catch代码块开启事物和提交事物，和事物回滚
     *      将connection传入到dao层即可！dao只负责使用，不要close()
     * @param addAccount
     * @param subAccount
     * @param money
     * @throws Exception
     */
    public void transfer(String addAccount,String subAccount,int money) throws Exception {

        BankDao bankDao = new BankDao();

        Connection connection = JdbcUtilsV2.getConnection();

        try {
            //开启事物
            //关闭事物提交！
            connection.setAutoCommit(false);

            //执行数据库动作
            //执行数据库动作
            bankDao.add(addAccount,money);
            System.out.println("-------");
            bankDao.sub(subAccount,money);

            //事物提交
            connection.commit();

        }catch (Exception e){
            //事物回滚
            connection.rollback();

            //抛出
            throw e;
        }finally {
            JdbcUtilsV2.freeConnection();
        }

    }
}
