package com.atwlm.api.transactionnew;

import com.atwlm.api.utils.JdbcUtilsV2;

import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 * bank表的数据库操作方法存储类
 *
 * @author wlm.java
 * @create 2023-06-21 20:20
 */
public class BankDao {

    /**
     * 加钱的数据库操作方法（jdbc）
     *
     * @param account   加钱的账号
     * @param money     加钱的金额
     */
    public void add(String account,int money) throws Exception {

        Connection connection = JdbcUtilsV2.getConnection();

        //3.编写sql语句结构
        String sql = "update t_bank set money = money + ? where account = ? ;";
        //4.创建statement
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        //5.占位符赋值
        preparedStatement.setObject(1,money);
        preparedStatement.setObject(2,account);
        //6.发送sql语句
        preparedStatement.executeUpdate();
        //7.关闭资源
        preparedStatement.close();

        System.out.println("加钱成功！");

    }


    /**
     * 减钱的数据库操作方法（jdbc）
     *
     * @param account   减钱的账号
     * @param money     减钱的金额
     */
    public void sub(String account,int money) throws Exception {

        Connection connection = JdbcUtilsV2.getConnection();

        //3.编写sql语句结构
        String sql = "update t_bank set money = money - ? where account = ? ;";
        //4.创建statement
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        //5.占位符赋值
        preparedStatement.setObject(1,money);
        preparedStatement.setObject(2,account);
        //6.发送sql语句
        preparedStatement.executeUpdate();
        //7.关闭资源
        preparedStatement.close();

        System.out.println("减钱成功！");

    }
}
