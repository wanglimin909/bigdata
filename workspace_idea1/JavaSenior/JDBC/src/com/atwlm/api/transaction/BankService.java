package com.atwlm.api.transaction;

import org.junit.Test;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * 银行卡业务方法，调用dao方法
 *
 * @author wlm.java
 * @create 2023-06-21 20:24
 */
public class BankService {

    @Test
    public void start() throws Exception {
        transfer("wlm","mmm",500);
    }

    /**
     * TODO:
     *      事物添加是在业务方法中！
     *      利用try-catch代码块开启事物和提交事物，和事物回滚
     *      将connection传入到dao层即可！dao只负责使用，不要close()
     * @param addAccount
     * @param subAccount
     * @param money
     * @throws Exception
     */
    public void transfer(String addAccount,String subAccount,int money) throws Exception {

        BankDao bankDao = new BankDao();

        //一个事物的最基本要求，必须是同一个连接对象 connection

        //一个转账方法，属于一个事物  （加钱  减钱）

        //1.注册驱动
        Class.forName("com.mysql.cj.jdbc.Driver");
        //2.获取连接
        Connection connection = DriverManager.getConnection("jdbc:mysql:///atwlm", "root", "123456");

        try {
            //开启事物
            //关闭事物提交！
            connection.setAutoCommit(false);

            //执行数据库动作
            //执行数据库动作
            bankDao.add(addAccount,money,connection);
            System.out.println("-------");
            bankDao.sub(subAccount,money,connection);

            //事物提交
            connection.commit();

        }catch (Exception e){
            //事物回滚
            connection.rollback();

            //抛出
            throw e;
        }finally {
            connection.close();
        }

    }
}
