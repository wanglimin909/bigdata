package com.atwlm.team.service;
/**
 * @Description 自定义异常类
 * @author atwlm Email:3478344074@qq.com 
 * @version 
 * @date 2023年2月13日下午4:14:09 
 */
public class TeamException extends Exception{
	 static final long serialVersionUID = -3387516483124229948L;
	 public TeamException(){
		 super();
	 }
	 public TeamException(String message){
		 super(message);
	 }
}
