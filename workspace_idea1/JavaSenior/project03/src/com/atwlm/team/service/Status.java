package com.atwlm.team.service;
/**
 * 
 * @Description 表示员工的状态
 * @author atwlm Email:3478344074@qq.com 
 * @version 
 * @date 2023年2月13日下午2:34:10 
 *
 */
//public class Status {
//	private final String NAME;
//
//	private Status(String name){
//		this.NAME = name;
//	}
//
//	public static final Status FREE = new Status("FREE");
//	public static final Status BUSY = new Status("BUSY");
//	public static final Status VOCATION = new Status("VOCATION");
//
//	public String toString(){
//		return NAME;
//	}
//
//	public String getNAME() {
//		return NAME;
//	}
//
//}
public enum Status{
    FREE,BUSY,VOCATION;
}
