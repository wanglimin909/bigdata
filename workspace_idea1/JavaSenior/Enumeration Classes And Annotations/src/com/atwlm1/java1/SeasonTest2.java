package com.atwlm1.java1;

/**
 * 使用enum关键字定义枚举类
 *  说明：定义的枚举类默认继承于java.lang.Enum类
 *
 * @author wlm.java
 * @create 2023-04-14 19:05
 */
public class SeasonTest2 {
    public static void main(String[] args) {
        Season2 summer = Season2.SUMMER;
        //toString():
        System.out.println(summer.toString());
//        System.out.println(Season2.class.getSuperclass());//class java.lang.Enum

        System.out.println("*****************************");

        //values():
        Season2[] values = Season2.values();
        for (int i = 0; i < values.length; i++) {
            System.out.println(values[i]);
            values[i].show();
        }

        System.out.println("*****************************");

        Thread.State[] values1 = Thread.State.values();
        for (int i = 0; i < values1.length; i++) {
            System.out.println(values1[i]);
        }

        //valueOf(String objName):返回枚举类中对象名是objName的对象
        //如果没有objName的枚举类对象，则抛异常：IllegalArgumentException
        Season2 winter = Season2.valueOf("WINTER");
        System.out.println(winter);

        winter.show();

    }
}

interface Info{
    void show();
}

//使用enum关键字定义枚举类
enum Season2 implements Info{
    //1.提供当前枚举类的对象，多个对象之间用“,”隔开，末尾对象用“;”结束
    SPRING("春天","春暖花开"){
        @Override
        public void show() {
            System.out.println("春");
        }
    },
    SUMMER("夏天","夏日炎炎"){
        @Override
        public void show() {
            System.out.println("夏");
        }
    },
    AUTUMN("秋天","秋高气爽"){
        @Override
        public void show() {
            System.out.println("秋");
        }
    },
    WINTER("冬天","冰天雪地"){
        @Override
        public void show() {
            System.out.println("冬");
        }
    };

    //1.声明Season对象的属性:private final修饰
    private final String seasonName;
    private final String seasonDesc;

    //2.私有化类的构造器,并给对象属性赋值

    private Season2(String seasonName,String seasonDesc){
        this.seasonName = seasonName;
        this.seasonDesc = seasonDesc;
    }

    //4.其他诉求1：获取枚举类对象的属性
    public String getSeasonName() {
        return seasonName;
    }

    public String getSeasonDesc() {
        return seasonDesc;
    }
//    //4.其他诉求2：提供toString()
//    @Override
//    public String toString() {
//        return "Season{" +
//                "seasonName='" + seasonName + '\'' +
//                ", seasonDesc='" + seasonDesc + '\'' +
//                '}';
//    }

//    @Override
//    public void show() {
//        System.out.println("这是一个季节");
//    }

}