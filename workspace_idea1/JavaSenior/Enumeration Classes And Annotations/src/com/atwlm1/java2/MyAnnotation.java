package com.atwlm1.java2;

import java.lang.annotation.*;

import static java.lang.annotation.ElementType.*;

/**
 * @author wlm.java
 * @create 2023-04-14 21:11
 */
@Inherited
@Repeatable(MyAnnotations.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({TYPE, FIELD, METHOD, PARAMETER, CONSTRUCTOR, LOCAL_VARIABLE,TYPE_PARAMETER,TYPE_USE})
public @interface MyAnnotation {

    String value() default "hello";//default指定默认值
}



