package com.student.dao;

import com.student.bean.Student;

import java.sql.Connection;
import java.util.List;

/**
 * 此接口用于规范针对于student表的常用操作
 */

public interface StudentDAO {

    /**
     * @author Snoopy
     * @Description 将 student 对象添加到数据库中
     * @Date
     * @Param
     * @Return
     */

    public abstract int insert(Connection connection, Student student);

    /**
     * @author Snoopy
     * @Description 根据学生的学号，删除学生信息
     * @Date
     * @Param
     * @Return
     */

    public abstract int delete(Connection connection, String studentId);

    /**
     * @author Snoopy
     * @Description 根据学号和字段修改指定学生的指定字段信息
     * @Date
     * @Param
     * @Return
     */

    public abstract int update(Connection connection, String studentId, String key, Object value);

    /**
     * @author Snoopy
     * @Description 针对内存中的 Student 对象，去修改数据库中指定的学生的全部数据
     * @Date
     * @Param
     * @Return
     */

    public abstract int updateAll(Connection connection, String oldStudentId, Student student);


    /**
     * @author Snoopy
     * @Description 根据学生学号，查询学生信息
     * @Date
     * @Param
     * @Return
     */

    public abstract Student getStudentByStudentId(Connection connection, String studentId);

    /**
     * @author Snoopy
     * @Description 根据手机号查询学生信息
     * @Date
     * @Param
     * @Return
     */

    public abstract Student getStudentByPhone(Connection connection, String phone);

    /**
     * @author Snoopy
     * @Description 根据身份证号查询学生信息
     * @Date
     * @Param
     * @Return
     */

    public abstract Student getStudentByCard(Connection connection, String card);


    /**
     * @author Snoopy
     * @Description 查询表中所有记录构成的集合
     * @Date
     * @Param
     * @Return
     */

    public abstract List<Student> getStudentAll(Connection connection);


    /**
     * @author Snoopy
     * @Description 查询数据库中 Student 数据总数目
     * @Date
     * @Param
     * @Return
     */

    public abstract Long getCount(Connection connection);

    /**
     * @author Snoopy
     * @Description 清空所有学生信息
     * @Date
     * @Param
     * @Return
     */

    public abstract int clearAll(Connection connection);

}
