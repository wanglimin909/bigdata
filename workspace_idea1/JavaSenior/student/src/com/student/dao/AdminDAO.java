package com.student.dao;

import com.student.bean.Admin;

import java.sql.Connection;

/**
 * 此接口是用来规范 针对于 admin 表的一些常用操作
 */
public interface AdminDAO {

    /**
     * @author Snoopy
     * @Description 注册功能（向数据库插入管理者信息）
     * @Date
     * @Param
     * @Return
     */

    public abstract int register(Connection connection, Admin admin);

    /**
     * @author Snoopy
     * @Description 登录功能 （验证密码和账号）
     * @Date
     * @Param
     * @Return
     */

    public abstract Admin login(Connection connection, String adminId, String password);


    /**
     * @author Snoopy
     * @Description 找回密码
     * @Date
     * @Param
     * @Return
     */

    public abstract String recover(Connection connection, String card, String phone);

    /**
     * @author Snoopy
     * @Description 注销账户
     * @Date
     * @Param
     * @Return
     */

    public abstract int unsubscribe(Connection connection, String card, String phone);


    /**
     * @author Snoopy
     * @Description 根据管理员id，获取管理员的信息
     * @Date
     * @Param
     * @Return
     */

    public abstract Admin getAdminById(Connection connection, String adminId);

    /**
     * @author Snoopy
     * @Description 根据管理员身份证号，获取管理员的信息
     * @Date
     * @Param
     * @Return
     */

    public abstract Admin getAdminByCard(Connection connection, String card);

    /**
     * @author Snoopy
     * @Description 根据管理员手机号，获取管理员的信息
     * @Date
     * @Param
     * @Return
     */

    public abstract Admin getAdminByPhone(Connection connection, String phone);

}
