package com.student.dao.junit;

import com.student.bean.Student;
import com.student.dao.StudentDAOImpl;
import com.student.dao.util.JDBCUtils;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

class StudentDAOImplTest {

    StudentDAOImpl studentDAO = new StudentDAOImpl();

    @Test
    void insert() {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Student student = new Student("1003", "Snoopy", "男", 12, "13978564811", "甘肃兰州", "4114789554", 100, 90, 90);
            studentDAO.insert(connection, student);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            // 关闭连接
            JDBCUtils.closeResource(connection, null, null);
        }
    }

    @Test
    void delete() {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            studentDAO.delete(connection,"1002");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection,null,null);
        }
    }

    @Test
    void update() {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            studentDAO.update(connection,"1001","student_name","李小龙");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection,null,null);
        }
    }

    @Test
    void updateAll() {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Student student = new Student("1001","王炸", "男", 12, "15139869514", "中国", "411456789", 89, 90, 90);
            studentDAO.updateAll(connection,"1001",student);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection,null,null);
        }
    }

    @Test
    void getStudentByStudentId() {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Student instance = studentDAO.getStudentByStudentId(connection, "1001");
            System.out.println(instance);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection,null,null);
        }
    }

    @Test
    void getAll() {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            List<Student> studentAll = studentDAO.getStudentAll(connection);
            System.out.println(studentAll);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection,null,null);
        }
    }

    @Test
    void getCount() {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Long count = studentDAO.getCount(connection);
            System.out.println(count);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection,null,null);
        }
    }
}