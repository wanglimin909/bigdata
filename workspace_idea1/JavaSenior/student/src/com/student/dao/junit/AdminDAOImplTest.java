package com.student.dao.junit;

import com.student.bean.Admin;
import com.student.dao.AdminDAOImpl;
import com.student.dao.util.JDBCUtils;
import org.junit.Test;


import java.sql.Connection;
import java.sql.SQLException;



public class AdminDAOImplTest {

    AdminDAOImpl adminDAO = new AdminDAOImpl();

    @Test
    public void register() {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Admin admin = new Admin("T001","123456","理工男","4113856987","123456789");
            adminDAO.register(connection,admin);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection,null,null);
        }
    }

    @Test
    public void login() {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Object t001 = adminDAO.login(connection, "T001", "123456");
            System.out.println(t001);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection,null,null);
        }
    }

    @Test
    public void recover(){
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            String recover = adminDAO.recover(connection, "4113856987", "123456789");
            System.out.println(recover);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection,null,null);
        }
    }
}