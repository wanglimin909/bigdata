package com.student.dao.util;

import com.alibaba.druid.pool.DruidDataSourceFactory;
import org.apache.commons.dbutils.DbUtils;
import javax.sql.DataSource;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;

/**
 * @ClassName JDBCUtils
 * @Description TODO
 * @Author Snoopy
 * @Date
 * @Version 1.0
 */
public class JDBCUtils {

    /**
    * @author Snoopy
    * @Description 使用Druid数据库连接池技术
    * @Date
    * @Param
    * @Return
    */
    private static DataSource source1;
    static{
        try {
            Properties properties = new Properties();
            InputStream resourceAsStream = ClassLoader.getSystemClassLoader().getResourceAsStream("student.properties");

            // 加载配置文件
            properties.load(resourceAsStream);

            source1 = DruidDataSourceFactory.createDataSource(properties);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static Connection getConnection() throws SQLException {
        Connection connection = source1.getConnection();
        return connection;
    }


    /**
     * @author Snoopy
     * @Description 获取数据库的连接
     * @Date
     * @Param
     * @Return
     */

    public static Connection getConnection1() throws Exception {
        // 获取数据库连接

        // 1. 读取配置文件中的四个基本信息
        InputStream resourceAsStream = ClassLoader.getSystemClassLoader().getResourceAsStream("jdbc.properties");
        Properties properties = new Properties();
        properties.load(resourceAsStream);

        String url = properties.getProperty("url");
        String user = properties.getProperty("user");
        String password = properties.getProperty("password");
        String driverClass = properties.getProperty("driverClass");

        // 2. 加载驱动
        Class.forName(driverClass);

        // 3. 获取连接
        Connection connection = DriverManager.getConnection(url, user, password);

        return connection;
    }

    /**
     * @author Snoopy
     * @Description 关闭连接 和 Statement的操作
     * @Date 20:39 2022/2/24
     * @Param
     * @Return
     */
    public static void closeResource(Connection connection, Statement ps, ResultSet resultSet) {
        // 7.关闭资源
        DbUtils.closeQuietly(connection);
        DbUtils.closeQuietly(ps);
        DbUtils.closeQuietly(resultSet);
    }

}
