package com.student.dao;

import com.student.bean.Student;

import java.sql.Connection;
import java.util.List;

/**
 * @ClassName StudentDAOImpl
 * @Description StudentDAO接口的实现类
 * @Author Snoopy
 * @Date
 * @Version 1.0
 */
public class StudentDAOImpl extends BaseDAO<Student> implements StudentDAO {

    /**
     * @author Snoopy
     * @Description 将 student 对象添加到数据库中
     * @Date
     * @Param
     * @Return
     */
    @Override
    public int insert(Connection connection, Student student) {
        String sql = "insert into student(student_id,student_name,student_sex,student_age,student_phone," +
                "student_location,student_card,student_english,student_math,student_java) values(?,?,?,?,?,?,?,?,?,?)";
        int insert = update(connection, sql, student.getStudentId(), student.getName(), student.getSex(),
                student.getAge(), student.getPhone(), student.getLocation(),
                student.getCard(), student.getEnglish(), student.getMath(), student.getJava());
        return insert;
    }

    /**
     * @author Snoopy
     * @Description 根据学生的学号，删除学生信息
     * @Date
     * @Param
     * @Return
     */

    @Override
    public int delete(Connection connection, String studentId) {
        String sql = "delete from student where student_id = ?";
        int delete = update(connection, sql, studentId);
        return delete;
    }

    /**
     * @author Snoopy
     * @Description 根据学号和字段修改指定学生的指定字段信息
     * @Date
     * @Param
     * @Return
     */

    @Override
    public int update(Connection connection, String studentId, String key, Object value) {
        String sql = "update student set " + key + "= ? where student_id = ?";
        int update = update(connection, sql, value, studentId);
        return update;
    }

    /**
     * @author Snoopy
     * @Description 针对内存中的 Student 对象，去修改数据库中指定的学生的全部数据
     * @Date
     * @Param
     * @Return
     */

    @Override
    public int updateAll(Connection connection, String oldStudentId, Student student) {
        String sql = "update student set student_id = ?,student_name = ?,student_sex = ?," +
                "student_age = ?,student_phone = ?,student_location = ?," +
                "student_card = ?,student_english = ?,student_math = ?,student_java = ? where student_id = ?";
        int updateAll = update(connection, sql, student.getStudentId(), student.getName(), student.getSex(), student.getAge(), student.getPhone(), student.getLocation(), student.getCard(),
                student.getEnglish(), student.getMath(), student.getJava(), oldStudentId);
        return updateAll;
    }


    /**
     * @author Snoopy
     * @Description 根据学生学号，查询学生信息
     * @Date
     * @Param
     * @Return
     */

    @Override
    public Student getStudentByStudentId(Connection connection, String studentId) {
        String sql = "select student_id studentId,student_name name,student_age age,student_sex sex,student_phone phone,student_card card,student_location location," +
                "student_english english,student_math math,student_java java from student where student_id = ?";
        Student student = getInstance(connection, sql, studentId);
        return student;
    }

    /**
    * @author Snoopy
    * @Description 通过手机号查询学生信息
    * @Date
    * @Param
    * @Return
    */

    @Override
    public Student getStudentByPhone(Connection connection, String phone) {
        String sql = "select student_id studentId,student_name name,student_age age,student_sex sex,student_phone phone,student_card card,student_location location," +
                "student_english english,student_math math,student_java java from student where student_phone = ?";
        Student student = getInstance(connection, sql, phone);
        return student;
    }

    /**
    * @author Snoopy
    * @Description 通过身份证号查询学生信息
    * @Date
    * @Param
    * @Return
    */

    @Override
    public Student getStudentByCard(Connection connection, String card) {
        String sql = "select student_id studentId,student_name name,student_age age,student_sex sex,student_phone phone,student_card card,student_location location," +
                "student_english english,student_math math,student_java java from student where student_card = ?";
        Student student = getInstance(connection, sql, card);
        return student;
    }

    /**
     * @author
     * @Description 查询表中所有记录构成的集合
     * @Date Snoopy
     * @Param
     * @Return
     */

    @Override
    public List<Student> getStudentAll(Connection connection) {
        String sql = "select student_id studentId,student_name name,student_age age,student_sex sex,student_phone phone,student_card card,student_location location," +
                "student_english english,student_math math,student_java java from student";
        List<Student> forList = getForList(connection, sql);
        return forList;
    }

    /**
     * @author Snoopy
     * @Description 查询数据库中 Student 数据总数目
     * @Date
     * @Param
     * @Return
     */

    @Override
    public Long getCount(Connection connection) {
        String sql = "select count(*) from student";
        Long value = getValue(connection, sql);
        return value;
    }

    /**
    * @author Snoopy
    * @Description 清空所有学生信息
    * @Date
    * @Param
    * @Return
    */

    @Override
    public int clearAll(Connection connection) {
        String sql = "delete from student";
        int clearAll = update(connection, sql);
        return clearAll;
    }
}
