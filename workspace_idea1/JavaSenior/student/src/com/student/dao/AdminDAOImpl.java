package com.student.dao;

import com.student.bean.Admin;

import java.sql.Connection;

/**
 * @ClassName AdminDAOImpl
 * @Description AdminDAOImpl接口的实现类
 * @Author Snoopy
 * @Date
 * @Version 1.0
 */
public class AdminDAOImpl extends BaseDAO<Admin> implements AdminDAO {

    @Override
    public int register(Connection connection, Admin admin) {
        String sql = "insert into admin(admin_id,admin_password,admin_name,admin_card,admin_phone) values(?,?,?,?,?)";
        int register = update(connection, sql, admin.getAdminId(), admin.getPassword(), admin.getName(), admin.getCard(), admin.getPhone());
        return register;
    }

    @Override
    public Admin login(Connection connection, String adminId, String password) {
        String sql = "select admin_id adminId,admin_password password,admin_name name,admin_card card,admin_phone phone from admin where admin_id = ? and admin_password = ?";
        Admin login = getInstance(connection, sql, adminId, password);
        return login;
    }

    @Override
    public String recover(Connection connection, String card, String phone) {
        String sql = "select admin_password password from admin where admin_card = ? and admin_phone = ?";
        String recover = getValue(connection, sql, card, phone);
        return recover;
    }

    @Override
    public int unsubscribe(Connection connection, String card, String phone) {
        String sql = "delete from admin where admin_card = ? and admin_phone = ?";
        int unsubscribe = update(connection, sql, card, phone);
        return unsubscribe;
    }

    @Override
    public Admin getAdminById(Connection connection, String adminId) {
        String sql = "select admin_id adminId,admin_password password,admin_name name,admin_card card,admin_phone phone from admin where admin_id = ?";
        Admin admin = getInstance(connection, sql, adminId);
        return admin;
    }

    @Override
    public Admin getAdminByCard(Connection connection, String card) {
        String sql = "select admin_id adminId,admin_password password,admin_name name,admin_card card,admin_phone phone from admin where admin_card = ?";
        Admin admin = getInstance(connection, sql, card);
        return admin;
    }

    @Override
    public Admin getAdminByPhone(Connection connection, String phone) {
        String sql = "select admin_id adminId,admin_password password,admin_name name,admin_card card,admin_phone phone from admin where admin_phone = ?";
        Admin admin = getInstance(connection, sql, phone);
        return admin;
    }
}
