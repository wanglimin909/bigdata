package com.student.view;

import com.student.bean.Admin;
import com.student.bean.Student;
import com.student.service.AdminService;
import com.student.service.StudentService;
import com.student.tools.Tools;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * @ClassName StudentView
 * @Description 学生信息管理系统的菜单界面
 * @Author Snoopy
 * @Date
 * @Version 1.0
 */
public class StudentView {

    private String tempAdminId = null;   // 临时保存当前登录的管理员的账号
    private String tempAdminName = null; // // 临时保存当前登录的管理员的姓名
    private String tempAdminCard = null; // 临时保存当前登录的管理员的身份证号
    private String tempAdminPhone = null; // 临时保存当前登录的管理员的手机号

    AdminService adminService = new AdminService();
    StudentService studentService = new StudentService();

    /**
     * @author Snoopy
     * @Description 登录主菜单
     * @Date
     * @Param
     * @Return
     */
    public void enterRegisterMenu() {
        // 是否退出系统的标识
        boolean flag = true;
        while (flag) {
            System.out.println("------------------------------------【学生信息管理系统】---------------------------------------");
            System.out.println("\t\t\t\t\t\t\t\t\t1.登录\t\t2.注册");
            System.out.println("\t\t\t\t\t\t\t\t\t3.忘记密码\t4.关于我的");
            System.out.println("\t\t\t\t\t\t\t\t\t5.关闭系统");
            System.out.println("-------------------------------------------------------------------------------------------");
            System.out.println("请输入你的选择：");
            char choice = Tools.readRegisterMenuSelection();
            switch (choice) {
                case '1':
                    login();
                    break;
                case '2':
                    register();
                    break;
                case '3':
                    recover();
                    break;
                case '4':
                    about();
                    break;
                case '5':
                    System.out.println("是否要关闭系统(Y/N)？");
                    char exit = Tools.readConfirmSelection();
                    if (exit == 'Y' || exit == 'y') {
                        flag = false;
                        System.out.println("成功关闭系统");
                    }
                    break;
            }
        }
    }

    /**
     * @author Snoopy
     * @Description 功能区主菜单
     * @Date
     * @Param
     * @Return
     */
    public void enterMainMenu() {
        boolean flag = true;
        while (flag) {
            System.out.println("------------------------------------【学生信息管理系统】---------------------------------------");
            System.out.println("\t\t\t\t\t\t\t\t\t1.添加学生信息");
            System.out.println("\t\t\t\t\t\t\t\t\t2.删除学生信息");
            System.out.println("\t\t\t\t\t\t\t\t\t3.修改学生信息(指定部分)");
            System.out.println("\t\t\t\t\t\t\t\t\t4.修改学生信息(所有信息)");
            System.out.println("\t\t\t\t\t\t\t\t\t5.查找学生信息");
            System.out.println("\t\t\t\t\t\t\t\t\t6.显示学生信息(全体学生)");
            System.out.println("\t\t\t\t\t\t\t\t\t7.清空学生信息");
            System.out.println("\t\t\t\t\t\t\t\t\t8.导出学生信息");
            System.out.println("\t\t\t\t\t\t\t\t\t9.注销账户");
            System.out.println("\t\t\t\t\t\t\t\t\t0.退出登录");
            System.out.println("-------------------------------------------------------------------------------------------");
            System.out.println("请输入你的选择：");
            char choice = Tools.readMainMenuSelection();
            switch (choice) {
                case '1':
                    //车辆管理 xxx写的
                    System.out.println("车辆管理，xxx");
                    addStudent();
                    break;
                case '2':
                    deleteStudent();
                    break;
                case '3':
                    updateStudent();
                    break;
                case '4':
                    updateStudentAll();
                    break;
                case '5':
                    searchStudentById();
                    break;
                case '6':
                    showStudentAll();
                    break;
                case '7':
                    clearStudentAll();
                    break;
                case '8':
                    export();
                    break;
                case '9':
                    int unsubscribe = unsubscribe();
                    if (unsubscribe > 0) {
                        return;
                    }
                    break;
                case '0':
                    System.out.println("是否要退出登录状态(Y/N)？");
                    char exit = Tools.readConfirmSelection();
                    if (exit == 'Y' || exit == 'y') {
                        flag = false;
                        System.out.println("成功退出登录");
                    }
                    break;
            }
        }

    }

    /**
     * @author Snoopy
     * @Description 登录
     * @Date
     * @Param
     * @Return
     */

    public void login() {
        System.out.println("------------------------------------【登录】-------------------------------------------------");
        System.out.println("请输入你的个人账号：");
        String adminId = Tools.readString();
        System.out.println("请输入你的个人密码：");
        String password = Tools.readString();
        Admin login = adminService.login(adminId, password);
        if (login != null) {
            System.out.println("欢迎使用学生信息管理系统！,点击回车键开始使用学生信息管理系统");
            // 临时保存当前管理员的信息
            tempAdminId = login.getAdminId();
            tempAdminName = login.getName();
            tempAdminCard = login.getCard();
            tempAdminPhone = login.getPhone();
            // 按回车键继续
            Tools.readReturn();
            // 登录成功进入主菜单
            enterMainMenu();
        } else {
            System.out.println("账号或密码有误");
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }

    /**
     * @author Snoopy
     * @Description 注册
     * @Date
     * @Param
     * @Return
     */

    public void register() {
        System.out.println("------------------------------------【注册】------------------------------------------------");
        System.out.println("请输入你的个人账号：");
        String adminId = Tools.readString();
        while (adminService.checkId(adminId)) {
            System.out.println("你输入的账号已经存在，请重新输入：");
            adminId = Tools.readString();
            adminService.checkId(adminId);
        }
        System.out.println("请输入你的个人密码：");
        String password = Tools.readString();
        System.out.println("请输入你的姓名：");
        String name = Tools.readString();
        System.out.println("请输入你的身份证号：");
        String card = Tools.readString();
        while (adminService.checkCard(card)) {
            System.out.println("你输入的身份证号已经存在，请重新输入：");
            card = Tools.readString();
            adminService.checkId(card);
        }
        System.out.println("请输入你的手机号：");
        String phone = Tools.readString();
        while (adminService.checkPhone(phone)) {
            System.out.println("你输入的手机号已经存在，请重新输入：");
            phone = Tools.readString();
            adminService.checkId(phone);
        }
        Admin admin = new Admin(adminId, password, name, card, phone);
        int register = adminService.register(admin);
        if (register > 0) {
            System.out.println("注册成功");
        } else {
            System.out.println("注册失败，请重新试一下！");
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }

    /**
     * @author Snoopy
     * @Description 找回密码
     * @Date
     * @Return
     */

    public void recover() {
        System.out.println("------------------------------------【找回密码】---------------------------------------------");
        System.out.println("请输入你的身份证号：");
        String card = Tools.readString();
        System.out.println("请输入你的手机号：");
        String phone = Tools.readString();
        String recover = adminService.recover(card, phone);
        if (recover != null) {
            System.out.println("密码找回成功，你的密码为：" + recover);
        } else {
            System.out.println("你输入的身份证号或手机号有误！");
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();

    }

    /**
     * @author Snoopy
     * @Description 关于我的
     * @Date
     * @Param
     * @Return
     */

    public void about() {
        try {
            Desktop desktop = Desktop.getDesktop();
            URI uri = new URI("https://www.baidu.com"); //创建URI统一资源标识符
            desktop.browse(uri);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            System.out.println("欢迎使用学生信息管理系统【作者：☆往事随風☆】");
        }

    }


    /**
     * @author Snoopy
     * @Description 显示学生信息列名
     * @Date
     * @Param
     * @Return
     */


    public void showLabel() {
        System.out.println("学号\t\t姓名\t\t\t性别\t\t年龄\t\t\t电话\t\t\t\t住址\t\t\t\t\t身份证号\t\t\t英语\t\t\t数学\t\t\tjava");
    }

    /**
     * @author Snoopy
     * @Description 添加学生信息
     * @Date
     * @Param
     * @Return
     */

    public void addStudent() {
        System.out.println("------------------------------------【添加学生信息】------------------------------------------");
        boolean flag = true; // 继续添加学生标识
        int count = 0; // 当前已成功添加的学生数量
        while (flag) {
            System.out.println("请输入学生学号：");
            String studentId = Tools.readString();
            while (studentService.checkStudentId(studentId)) {
                System.out.println("你要添加的学号已经存在，请重新输入：");
                studentId = Tools.readString();
                studentService.checkStudentId(studentId);
            }
            System.out.println("请输入学生姓名：");
            String name = Tools.readString();
            name = studentService.changeName(name);
            System.out.println("请输入学生性别：");
            String sex = Tools.readString();
            System.out.println("请输入学生年龄：");
            int age = Tools.readInt();
            System.out.println("请输入学生电话：");
            String phone = Tools.readString();
            while (studentService.checkPhone(phone)) {
                System.out.println("你要添加的手机号已经存在，请重新输入：");
                phone = Tools.readString();
                studentService.checkPhone(phone);
            }
            System.out.println("请输入学生身份证号码：");
            String card = Tools.readString();
            while (studentService.checkCard(card)) {
                System.out.println("你要添加的身份证号已经存在，请重新输入：");
                card = Tools.readString();
                studentService.checkPhone(card);
            }
            System.out.println("请输入如家庭住址：");
            String location = Tools.readString();
            System.out.println("请输入英语成绩：");
            double english = Tools.readDouble();
            System.out.println("请输入数学成绩：");
            double math = Tools.readDouble();
            System.out.println("请输入Java成绩：");
            double java = Tools.readDouble();
            Student student = new Student(studentId, name, sex, age, phone, location, card, english, math, java);
            int i = studentService.addStudent(student);
            if (i > 0) {
                count++;
                System.out.println("已成功添加" + count + "条学生信息");
                System.out.println("是否继续添加(Y/N)？");
                char choice = Tools.readConfirmSelection();
                if (choice == 'Y' || choice == 'y') {
                    System.out.println("-------------------------------------------------------------------------------------------");
                } else {
                    System.out.println("成功添加" + count + "条学生信息");
                    break;
                }
            } else {
                System.out.println("添加失败");
            }
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }


    /**
     * @author Snoopy
     * @Description 删除学生信息
     * @Date
     * @Param
     * @Return
     */

    public void deleteStudent() {
        System.out.println("------------------------------------【删除学生信息】------------------------------------------");
        System.out.println("请输入要删除学生的学号：");
        String studentId = Tools.readString();
        // 先查询一下该学号的学生信息
        Object search = studentService.search(studentId);
        if (search != null) {
            // 输出一下要删除的学生信息
            showLabel();
            System.out.println(search);
            System.out.println("确定要删除吗(Y/N)：");
            char choice = Tools.readConfirmSelection();
            if (choice == 'y' || choice == 'Y') {
                int delete = studentService.deleteStudent(studentId);
                if (delete > 0) {
                    System.out.println("删除成功");
                } else {
                    System.out.println("删除失败");
                }
            }
        } else {
            System.out.println("要删除的学生不存在");
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }


    /**
     * @author Snoopy
     * @Description 修改学生信息 （指定部分的信息）
     * @Date
     * @Param
     * @Return
     */

    public void updateStudent() {
        System.out.println("------------------------------------【修改学生信息】------------------------------------------");
        System.out.println("请输入学生学号：");
        String studentId = Tools.readString();
        // 先查询一下该学号的学生信息
        Object search = studentService.search(studentId);
        if (search != null) {
            // 输出一下要删除的学生信息
            showLabel();
            System.out.println(search);
            System.out.println("确定要修改该学生的部分信息(Y/N)？");
            char choice = Tools.readConfirmSelection();
            if (choice == 'Y' || choice == 'y') {
                System.out.println("你可在以下字段中选择一个进行修改：");
                System.out.println("[student_id，student_name，student_sex，student_age，student_phone，student_location，student_card，student_english，student_math，student_java]");
                System.out.println("请输入要修改的字段：");
                String key = Tools.readString();
                while (!studentService.checkExist(key)) {
                    System.out.println("你输入的字段不存在，请重新输入：");
                    key = Tools.readString();
                    studentService.checkExist(key);
                }
                System.out.println("请输入修改后的信息：");
                String value = Tools.readString();
                if (key == "student_name") {
                    value = studentService.changeName(value);
                }
                // 修改指定信息
                int update = studentService.update(studentId, key, value);
                if (update > 0) {
                    System.out.println("修改成功");
                } else {
                    System.out.println("修改失败，失败原因：新修改的学号或手机号或身份证号与其他学生相同");
                }
            }
        } else {
            System.out.println("你要修改的学生不存在！");
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }


    /**
     * @author Snoopy
     * @Description 修改学生信息 (所有信息)
     * @Date
     * @Param
     * @Return
     */

    public void updateStudentAll() {
        System.out.println("------------------------------------【修改学生全部信息】---------------------------------------");
        System.out.println("请输入学生学号：");
        String oldStudentId = Tools.readString();
        // 先查询一下该学号的学生信息
        Object search = studentService.search(oldStudentId);
        if (search != null) {
            // 输出一下要删除的学生信息
            showLabel();
            System.out.println(search);
            System.out.println("确定要修改该学生的全部信息(Y/N)？");
            char choice = Tools.readConfirmSelection();
            if (choice == 'Y' || choice == 'y') {
                System.out.println("请输入修改后的学生学号：");
                String studentId = Tools.readString();
                System.out.println("请输入修改后的学生姓名：");
                String name = Tools.readString();
                name = studentService.changeName(name);
                System.out.println("请输入修改后的学生性别：");
                String sex = Tools.readString();
                System.out.println("请输入修改后的学生年龄：");
                int age = Tools.readInt();
                System.out.println("请输入修改后的学生电话：");
                String phone = Tools.readString();
                System.out.println("请输入修改后的学生身份证号码：");
                String card = Tools.readString();
                System.out.println("请输入修改后的如家庭住址：");
                String location = Tools.readString();
                System.out.println("请输入修改后的英语成绩：");
                double english = Tools.readDouble();
                System.out.println("请输入修改后的数学成绩：");
                double math = Tools.readDouble();
                System.out.println("请输入修改后的Java成绩：");
                double java = Tools.readDouble();
                Student student = new Student(studentId, name, sex, age, phone, location, card, english, math, java);
                int updateAll = studentService.updateAll(oldStudentId, student);
                if (updateAll > 0) {
                    System.out.println("修改成功");
                } else {
                    System.out.println("修改失败，失败原因：新修改的学号或手机号或身份证号与其他学生相同");
                }
            }
        } else {
            System.out.println("你要修改的学生不存在！");
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }


    public void searchStudentById() {
        System.out.println("------------------------------------【查询学生信息】------------------------------------------");
        System.out.println("请输入要查找的学生的学号：");
        String studentId = Tools.readString();
        Object student = studentService.search(studentId);
        if (student != null) {
            System.out.println("查询结果如下：");
            showLabel();
            System.out.println(student);
        } else {
            System.out.println("该学生信息不存在");
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        Tools.readReturn();
    }


    /**
     * @author Snoopy
     * @Description 显示全部学生信息
     * @Date
     * @Param
     * @Return
     */

    public void showStudentAll() {
        System.out.println("------------------------------------【显示全体学生信息】---------------------------------------");
        Long count = studentService.getCount();
        System.out.println("一共查询到" + count + "条记录");
        showLabel();
        List<Student> students = studentService.searchAll();
        for (Student key : students) {
            System.out.println(key);
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }

    /**
     * @author Snoopy
     * @Description 导出学生信息
     * @Date
     * @Param
     * @Return
     */

    public void export() {
        RandomAccessFile randomAccessFile = null;
        try {
            System.out.println("------------------------------------【导出学生信息】------------------------------------------");
            System.out.println("1.自定义路径文件名\t\t2.默认保存路径文件名(D:\\\\student.csv)");
            System.out.println("3.暂不导出");
            String fileURL = null;
            System.out.println("请输入你的选择：");
            char choice = Tools.readRegisterMenuSelection();
            switch (choice) {
                case '1':
                    System.out.println("请输入要保存的文件路径(例如：D:\\\\student.csv)：");
                    fileURL = Tools.readString();
                    break;
                case '2':
                    fileURL = "D:\\student.csv";
                    break;
                case '3':
                    System.out.println("-------------------------------------------------------------------------------------------");
                    Tools.readReturn();
                    return;
            }
            randomAccessFile = new RandomAccessFile(new File(fileURL), "rw");
            List<Student> students = studentService.searchAll();
            if (students.size() > 0 && null != students) {
                // 读入表头
                String title[] = new String[]{"学号,", "姓名,", "性别,", "年龄,", "手机号,", "身份证号,", "家庭住址,", "英语,", "数学,", "java,\n"};
                for (String key : title) {
                    randomAccessFile.write(("\uFEFF" + key).getBytes());
                }
                for (Student student : students) {
                    randomAccessFile.write((student.getStudentId() + ",").getBytes());
                    randomAccessFile.write((student.getName() + ",").getBytes());
                    randomAccessFile.write((student.getSex() + ",").getBytes());
                    randomAccessFile.write((student.getAge() + ",").getBytes());
                    randomAccessFile.write((student.getPhone() + ",").getBytes());
                    randomAccessFile.write((student.getCard() + ",").getBytes());
                    randomAccessFile.write((student.getLocation() + ",").getBytes());
                    randomAccessFile.write((student.getEnglish() + ",").getBytes());
                    randomAccessFile.write((student.getMath() + ",").getBytes());
                    randomAccessFile.write((student.getJava() + ",").getBytes());
                    randomAccessFile.write("\n".getBytes());
                }
                System.out.println("学生信息导出完毕");
            } else {
                System.out.println("尚未查询到学生信息，无法进行导出操作");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (randomAccessFile != null) {
                    randomAccessFile.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }

    /**
     * @author Snoopy
     * @Description 清空所有学生信息
     * @Date
     * @Param
     * @Return
     */

    public void clearStudentAll() {
        System.out.println("------------------------------------【清空所有学生信息】---------------------------------------");
        System.out.println("确定要清空所有学生信息(Y/N),执行此操作将丢失所有学生的信息：");
        char choice = Tools.readConfirmSelection();
        if (choice == 'Y' || choice == 'y') {
            List<Student> students = studentService.searchAll();
            if (students.size() > 0 && null != students) {
                System.out.println("请输入你的身份证号：");
                String card = Tools.readString();
                Admin admin = adminService.getAdminByCard(card);
                if (admin != null) {
                    int clearAll = studentService.clearAll();
                    if (clearAll > 0) {
                        System.out.println("学生信息清空成功");
                    } else {
                        System.out.printf("学生信息清空失败");
                    }
                } else {
                    System.out.println("你输入的身份证号不存在");
                }
            }else {
                System.out.println("尚未存入学生信息，无法进行清空操作");
            }
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
    }

    /**
     * @author Snoopy
     * @Description 注销账户
     * @Date
     * @Param
     * @Return
     */

    public int unsubscribe() {
        System.out.println("------------------------------------【注销账户】----------------------------------------------");
        System.out.println("当前登录的管理员账号：" + tempAdminId);
        System.out.println("当前登录的管理员姓名：" + tempAdminName);
        System.out.println("确定要注销账户(Y/N)?");
        char choice = Tools.readConfirmSelection();
        if (choice == 'Y' || choice == 'y') {
            System.out.println("请输入你的身份证号：");
            String card = Tools.readString();
            System.out.println("请输入你的手机号：");
            String phone = Tools.readString();
            Admin adminByCard = adminService.getAdminByCard(card);
            Admin adminByPhone = adminService.getAdminByPhone(phone);
            if (adminByCard != null && adminByPhone != null) {
                if (tempAdminCard.equals(adminByCard.getCard()) && tempAdminPhone.equals(adminByPhone.getPhone())) {
                    int unsubscribe = adminService.unsubscribe(card, phone);
                    if (unsubscribe > 0) {
                        System.out.println("账户注销成功,已成功退出登录状态");
                        return unsubscribe;
                    } else {
                        System.out.println("账户注销失败");
                    }
                } else {
                    System.out.println("你输入的身份证号或手机号有误");
                }
            } else {
                System.out.println("你的身份证号或手机号不存在");
            }
        }
        System.out.println("-------------------------------------------------------------------------------------------");
        // 按回车键继续
        Tools.readReturn();
        return 0;
    }

    public static void main(String[] args) {
        StudentView studentView = new StudentView();
        studentView.enterRegisterMenu();
    }
}
