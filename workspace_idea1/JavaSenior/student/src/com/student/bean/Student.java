package com.student.bean;

import com.student.tools.Tools;

import javax.tools.Tool;
import java.math.BigDecimal;

/**
 * @ClassName Student
 * @Description 学生信息
 * @Author Snoopy
 * @Date
 * @Version 1.0
 */
public class Student {
    private String studentId;
    private String name;
    private String sex;
    private int age;
    private String phone;
    private String location;
    private String card;
    private double english;
    private double math;
    private double java;

    public Student() {
    }

    public Student(String studentId, String name, String sex, int age, String phone, String location, String card, double english, double math, double java) {
        this.studentId = studentId;
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.phone = phone;
        this.location = location;
        this.card = card;
        this.english = english;
        this.math = math;
        this.java = java;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public double getEnglish() {
        return english;
    }

    public void setEnglish(double english) {
        this.english = english;
    }

    public double getMath() {
        return math;
    }

    public void setMath(double math) {
        this.math = math;
    }

    public double getJava() {
        return java;
    }

    public void setJava(double java) {
        this.java = java;
    }

    @Override
    public String toString() {
        return studentId + '\t' + name + "\t\t" + sex + "\t\t" + age + "\t\t" + Tools.alignment(phone) + Tools.alignment(location) + Tools.alignment(card) + english + "\t\t" + math + "\t\t" + java;
    }
}
