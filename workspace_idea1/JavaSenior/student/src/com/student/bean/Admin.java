package com.student.bean;

/**
 * @ClassName Admin
 * @Description 管理员信息
 * @Author Snoopy
 * @Date
 * @Version 1.0
 */
public class Admin {
    private String adminId;
    private String password;
    private String name;
    private String card;
    private String phone;

    public Admin() {
    }

    public Admin(String adminId, String password, String name, String card, String phone) {
        this.adminId = adminId;
        this.password = password;
        this.name = name;
        this.card = card;
        this.phone = phone;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Admin{" +
                "adminId='" + adminId + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", card='" + card + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
