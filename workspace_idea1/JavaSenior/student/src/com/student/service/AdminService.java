package com.student.service;

import com.student.bean.Admin;
import com.student.bean.Student;
import com.student.dao.AdminDAOImpl;
import com.student.dao.util.JDBCUtils;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @ClassName AdminService
 * @Description TODO
 * @Author Snoopy
 * @Date
 * @Version 1.0
 */
public class AdminService { //service就相当于controller
    AdminDAOImpl adminDAO = new AdminDAOImpl();

    /**
     * @author Snoopy
     * @Description 注册功能
     * @Date
     * @Param
     * @Return
     */

    public int register(Admin admin) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int register = adminDAO.register(connection, admin);
            return register;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }

    /**
     * @author Snoopy
     * @Description 登录功能
     * @Date
     * @Param
     * @Return
     */

    public Admin login(String adminId, String password) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Admin login = adminDAO.login(connection, adminId, password);
            return login;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return null;
    }


    /**
     * @author
     * @Description 找回密码
     * @Date Snoopy
     * @Param
     * @Return
     */

    public String recover(String card, String phone) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            String recover = adminDAO.recover(connection, card, phone);
            return recover;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return null;
    }

    /**
     * @author Snoopy
     * @Description 注销账户
     * @Date
     * @Param
     * @Return
     */

    public int unsubscribe(String card, String phone) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int unsubscribe = adminDAO.unsubscribe(connection, card, phone);
            return unsubscribe;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }

    /**
     * @author
     * @Description 通过身份证号，获取管理员信息
     * @Date Snoopy
     * @Param
     * @Return
     */

    public Admin getAdminByCard(String card) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Admin admin = adminDAO.getAdminByCard(connection, card);
            return admin;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return null;
    }

    /**
     * @author Snoopy
     * @Description 根据手机号，获取管理员信息
     * @Date
     * @Param
     * @Return
     */

    public Admin getAdminByPhone(String phone) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Admin admin = adminDAO.getAdminByPhone(connection, phone);
            return admin;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return null;
    }

    /**
     * @author Snoopy
     * @Description 检查管理员账号是否重复
     * @Date
     * @Param
     * @Return
     */

    public boolean checkId(String adminId) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Admin adminById = adminDAO.getAdminById(connection, adminId);
            if (adminById != null) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return false;
    }


    /**
     * @author Snoopy
     * @Description 检查身份证号是否重复
     * @Date
     * @Param
     * @Return
     */

    public boolean checkCard(String card) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Admin admin = adminDAO.getAdminById(connection, card);
            if (admin != null) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return false;
    }

    /**
     * @author Snoopy
     * @Description 检查手机号是否有重复
     * @Date
     * @Param
     * @Return
     */

    public boolean checkPhone(String phone) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Admin admin = adminDAO.getAdminById(connection, phone);
            if (admin != null) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return false;
    }

}
