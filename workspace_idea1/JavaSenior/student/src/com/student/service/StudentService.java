package com.student.service;

import com.student.bean.Student;
import com.student.dao.StudentDAOImpl;
import com.student.dao.util.JDBCUtils;

import java.sql.*;
import java.util.List;


public class StudentService {

    StudentDAOImpl studentDAO = new StudentDAOImpl();

    /**
     * @author Snoopy
     * @Description 添加学生信息
     * @Date
     * @Param
     * @Return
     */
    public int addStudent(Student student) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int insert = studentDAO.insert(connection, student);
            return insert;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }

    /**
     * @author Snoopy
     * @Description 删除学生信息
     * @Date
     * @Param
     * @Return
     */
    public int deleteStudent(String studentId) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int delete = studentDAO.delete(connection, studentId);
            return delete;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }

    /**
     * @author Snoopy
     * @Description 根据学号，修改学生部分信息
     * @Date
     * @Param
     * @Return
     */

    public int update(String studentId, String key, Object value) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int update = studentDAO.update(connection, studentId, key, value);
            return update;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }

    /**
     * @author Snoopy
     * @Description 根据修改全部信息
     * @Date
     * @Param
     * @Return
     */

    public int updateAll(String oldStudentId, Student student) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int updateAll = studentDAO.updateAll(connection, oldStudentId, student);
            return updateAll;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }

    /**
     * @author Snoopy
     * @Description 根据学号查询学生信息
     * @Date
     * @Param
     * @Return
     */

    public Object search(String studentId) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Student student = studentDAO.getStudentByStudentId(connection, studentId);
            return student;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return null;
    }

    /**
     * @author Snoopy
     * @Description 查找全部学生信息
     * @Date
     * @Param
     * @Return
     */

    public List<Student> searchAll() {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            List<Student> studentAll = studentDAO.getStudentAll(connection);
            return studentAll;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return null;
    }

    /**
     * @author Snoopy
     * @Description 获取学生总数
     * @Date
     * @Param
     * @Return
     */

    public Long getCount() {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Long count = studentDAO.getCount(connection);
            return count;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return null;
    }


    /**
     * @author Snoopy
     * @Description 清空所有学生的信息
     * @Date
     * @Param
     * @Return
     */

    public int clearAll() {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            int clearAll = studentDAO.clearAll(connection);
            return clearAll;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return 0;
    }

    /**
     * @author Snoopy
     * @Description 检查添加的学生学号是否重复
     * @Date
     * @Param
     * @Return
     */

    public boolean checkStudentId(String studentId) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Student studentByStudentId = studentDAO.getStudentByStudentId(connection, studentId);
            if (studentByStudentId != null) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return false;
    }

    /**
     * @author Snoopy
     * @Description 检查学生电话号码是否有重复
     * @Date
     * @Param
     * @Return
     */

    public boolean checkPhone(String phone) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Student studentByPhone = studentDAO.getStudentByPhone(connection, phone);
            if (studentByPhone != null) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return false;
    }

    /**
     * @author Snoopy
     * @Description 检查学生身份证号码是否有重复
     * @Date
     * @Param
     * @Return
     */

    public boolean checkCard(String card) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Student studentByCard = studentDAO.getStudentByCard(connection, card);
            if (studentByCard != null) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return false;
    }

    /**
     * @author Snoopy
     * @Description 检查用户输入的字段是否存在
     * @Date
     * @Param
     * @Return
     */

    public boolean checkExist(String value) {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            String sql = "select student_id,student_name,student_age,student_sex,student_phone,student_card,student_location," +
                    "student_english,student_math,student_java from student";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            // 获取结果集
            ResultSet resultSet = preparedStatement.executeQuery();
            // 获取结果集的元数据
            ResultSetMetaData metaData = resultSet.getMetaData();
            // 获取每一行的列数
            int columnCount = metaData.getColumnCount();
            String columnLabel = "";
            for (int i = 0; i < columnCount; i++) {
                // 获取每一行的每一列的别名
                columnLabel = metaData.getColumnLabel(i + 1);
                if (value.equals(columnLabel)) {
                    return true;
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtils.closeResource(connection, null, null);
        }
        return false;
    }

    /**
     * @author Snoopy
     * @Description 如果名字为两个字，则在名字中间添加空格，使其长度为3
     * @Date
     * @Param
     * @Return
     */

    public String changeName(String name) {
        if (name.length() == 2) {
            name = name.substring(0, 1) + " " + name.substring(name.length() - 1);
        }
        return name;
    }
}
