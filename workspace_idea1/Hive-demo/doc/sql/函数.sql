-- 8 函数

-- 8.2 常用内置函数
-- 8.2.1 空字段赋值---NVL
-- 查询emp_nvl表中id、sal、comm 如果comm为null，用0代替

-- 查询emp表中员工的总收入

-- 8.2.2 CASE when then else end

-- 案例需求：求出每个部门男女各多少人？
-- 结果示例：
-- dept_id man_num woman_num
-- A       2       1
-- B       1       2
-- 创建表

-- 查询


-- 8.2.3 行转列

-- concat、concat_ws、collect_set
-- 案例需求：把星座和血型一样的人归类到一起
-- 结果示例：
-- 射手座,A            大海|凤姐
-- 白羊座,A            孙悟空|猪八戒
-- 白羊座,B            宋宋|苍老师
-- 创建表

-- 查询


-- 8.2.4 列转行

-- explode、split、lateral view

-- 案例需求：将电影分类中的category列的数据展开
-- 结果示例：
-- 《疑犯追踪》      悬疑
-- 《疑犯追踪》      动作
-- 《疑犯追踪》      科幻
-- 《疑犯追踪》      剧情
-- 《Lie to me》   悬疑
-- 《Lie to me》   警匪
-- 《Lie to me》   动作
-- 《Lie to me》   心理
-- 《Lie to me》   剧情
-- 《战狼2》        战争
-- 《战狼2》        动作
-- 《战狼2》        灾难
-- 创建表

-- 查询


-- 8.2.5 窗口函数

-- over()、lag()、 lead()、 ntile()

-- 数据准备

-- （1）查询在2017年4月份购买过的顾客及总人数
-- （2）查询顾客的购买明细及月购买总额
-- （3）上述的场景, 将每个顾客的cost按照日期进行累加
-- 3.1 所有行进行累加
-- 3.2 按照name 分组，组内数据累加
-- 3.3 按照name分区，组内数据按照日其有序累加
-- 3.4 按照name分区，组内数据按照日期排序，由起点到当前行进行累加
-- 3.5 按照name分区，组内数据按照日期排序，由前一行和当前行进行做聚合
-- 3.6 按照name分区，组内数据按照日期排序，由当前行和前一行和后一行做聚合
-- 3.6 按照name分区，组内数据按照日期排序，由当前行和后面所有行作聚合
-- （4）查询每个顾客上次的购买时间
-- （5）查询前20%时间的订单信息

-- 8.2.6 Rank
-- Rank()、Dense_rank()、row_number()
--数据准备
-- 计算每门学科成绩排名
