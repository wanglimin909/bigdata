-- 创建student表内部表
create table student_in(
    id int,
    name string
)
row format delimited
fields terminated by '\t';

-- 加载数据
load data local inpath '/opt/module/hive-3.1.2/datas/student.txt' into table student_in;

-- 创建student2 要求：仅包含id为偶数的
create table student2 as select id,name from student_in where id % 2 =0;

-- 创建student3 要求：仅要结构相同
create table student3 like student_in;

-- -----------------------------------------------------------------------------------------------------

-- 创建teacher表外部表
create external table teacher(
    id int,
    name string
)
row format delimited
fields terminated by '\t';

-- 加载数据
load data local inpath '/opt/module/hive-3.1.2/datas/teacher.txt' into table teacher;