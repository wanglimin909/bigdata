package com.atwlm.hive.demo.function;

import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.exec.UDFArgumentLengthException;
import org.apache.hadoop.hive.ql.exec.UDFArgumentTypeException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDF;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;

/**
 * ClassName: MyStringLength
 * Package: com.atwlm.hive.demo.function
 * Description:
 *
 * @Author wlm.java
 * @Create 2023-08-10 - 19:33
 * @Version: v1.0
 */
public class MyStringLength extends GenericUDF {
    /**
     * 初始化方法：检查函数参数个数、类型并返回函数的返回值的类型检查器
     * @param objectInspectors
     * @return 返回函数的返回值的类型检查器
     * @throws UDFArgumentException
     */
    @Override
    public ObjectInspector initialize(ObjectInspector[] objectInspectors) throws UDFArgumentException {
        //1.判断参数的个数
        if(objectInspectors.length != 1) {
            throw new UDFArgumentLengthException("参数个数异常！！！");
        }
        //2.判断参数的类型
        if(! objectInspectors[0].getCategory().equals(ObjectInspector.Category.PRIMITIVE)) {
            throw new UDFArgumentTypeException(0, "参数类型异常！！！");
        }
        //3.返回函数返回值的类型检查器
        return PrimitiveObjectInspectorFactory.javaIntObjectInspector;
    }

    /**
     * 函数的核心代码
     * @param deferredObjects
     * @return 返回参数的长度
     * @throws HiveException
     */
    @Override
    public Object evaluate(DeferredObject[] deferredObjects) throws HiveException {
        //1.null值问题
        if(deferredObjects[0].get() == null){
            return 0;
        }
        //2.返回参数的长度
        return deferredObjects[0].get().toString().length();
    }

    @Override
    public String getDisplayString(String[] strings) {
        return null;
    }
}
