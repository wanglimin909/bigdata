package com.atwlm.mapreduce.combineTextinputformat;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.CombineTextInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

/**
 * ClassName: WordCountDriver
 * Package: com.atwlm.mapreduce.wordcount
 * Description:
 *
 * @Author wlm.java
 * @Create 2023/7/29 - 9:30
 * @Version: v1.0
 */
public class WordCountDriver {
    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
        //1.获取 job
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf);
        //2.设置 jar包路径
        job.setJarByClass(WordCountDriver.class);
        //3.关联 mapper和 reducer
        job.setMapperClass(WordCountMapper.class);
        job.setReducerClass(WordCountReducer.class);
        //4.设置 map输出的 kv类型
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);
        //5.设置最终输出的 kv类型
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        // 如果不设置InputFormat，它默认用的是TextInputFormat.class
        job.setInputFormatClass(CombineTextInputFormat.class);

        //虚拟存储切片最大值设置4m
        CombineTextInputFormat.setMaxInputSplitSize(job, 20971520);

        //6.设置输入路径和输出路径
        FileInputFormat.setInputPaths(job, new Path("F:\\developer_tools\\hadoop3.x\\Hadoop-notes\\input\\inputcombine"));
        FileOutputFormat.setOutputPath(job, new Path("F:\\developer_tools\\hadoop3.x\\Hadoop-notes\\input\\outputcombine"));
        //7.提交job
        boolean result = job.waitForCompletion(true);

        System.exit(result ? 0 : 1);

    }
}
