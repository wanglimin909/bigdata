package com.atwlm.mapreduce.reduceJoin;

import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * ClassName: TableBean
 * Package: com.atwlm.mapreduce.reduceJoin
 * Description:
 *
 * @Author wlm.java
 * @Create 2023-07-31 - 10:11
 * @Version: v1.0
 */
public class TableBean implements Writable {

    //  id	   pid	    amount
    //  pid	   pname
    private String id;//订单id
    private String pid;//商品id
    private int amount;//商品数量
    private String pname;//商品名称
    private String flag;//标记是什么表  order 和 pd

    //空参构造
    public TableBean() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeUTF(id);
        dataOutput.writeUTF(pid);
        dataOutput.writeInt(amount);
        dataOutput.writeUTF(pname);
        dataOutput.writeUTF(flag);

    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        this.id = dataInput.readUTF();
        this.pid = dataInput.readUTF();
        this.amount = dataInput.readInt();
        this.pname = dataInput.readUTF();
        this.flag = dataInput.readUTF();

    }

    //  id	    pname	    amount
    @Override
    public String toString() {
        return id + "\t" + pname + "\t" + amount;
    }

}
