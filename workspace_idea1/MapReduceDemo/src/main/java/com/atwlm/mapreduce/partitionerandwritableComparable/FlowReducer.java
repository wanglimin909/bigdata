package com.atwlm.mapreduce.partitionerandwritableComparable;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * ClassName: FlowReducer
 * Package: com.atwlm.mapreduce.writable
 * Description:
 *
 * @Author wlm.java
 * @Create 2023/7/29 - 13:04
 * @Version: v1.0
 */
public class FlowReducer extends Reducer<FlowBean, Text,Text, FlowBean> {

    @Override
    protected void reduce(FlowBean key, Iterable<Text> values, Reducer<FlowBean, Text, Text, FlowBean>.Context context) throws IOException, InterruptedException {

        for (Text value : values) {
            context.write(value,key);
        }

    }

}
