package com.atwlm.mapreduce.reduceJoin;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

/**
 * ClassName: TableDriver
 * Package: com.atwlm.mapreduce.reduceJoin
 * Description:
 *
 * @Author wlm.java
 * @Create 2023-07-31 - 11:51
 * @Version: v1.0
 */
public class TableDriver {
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        Job job = Job.getInstance(new Configuration());

        job.setJarByClass(TableDriver.class);
        job.setMapperClass(TableMapper.class);
        job.setReducerClass(TableReducer.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(TableBean.class);

        job.setOutputKeyClass(TableBean.class);
        job.setOutputValueClass(NullWritable.class);

        FileInputFormat.setInputPaths(job, new Path("F:\\developer_tools\\hadoop3.x\\Hadoop-notes\\input\\inputreduceJoin"));
        FileOutputFormat.setOutputPath(job, new Path("F:\\developer_tools\\hadoop3.x\\Hadoop-notes\\input\\outputreduceJoin"));

        boolean b = job.waitForCompletion(true);
        System.exit(b ? 0 : 1);
    }
}
