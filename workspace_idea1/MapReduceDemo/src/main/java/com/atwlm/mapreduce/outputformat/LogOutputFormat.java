package com.atwlm.mapreduce.outputformat;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;


/**
 * ClassName: LogOutputFormat
 * Package: com.atwlm.mapreduce.outputformat
 * Description:
 *
 * @Author wlm.java
 * @Create 2023-07-30 - 14:41
 * @Version: v1.0
 */
public class LogOutputFormat extends FileOutputFormat<Text, NullWritable> {

    @Override
    public RecordWriter<Text, NullWritable> getRecordWriter(TaskAttemptContext taskAttemptContext) throws IOException, InterruptedException {

        LogRecordWriter lrw = new LogRecordWriter(taskAttemptContext);

        return lrw;
    }
}
