package com.atwlm.mapreduce.partitioner;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;

/**
 * ClassName: ProvincePartitioner
 * Package: com.atwlm.mapreduce.partitioner
 * Description:
 *
 * @Author wlm.java
 * @Create 2023-07-30 - 12:03
 * @Version: v1.0
 */
public class ProvincePartitioner extends Partitioner<Text, FlowBean> {


    @Override
    public int getPartition(Text text, FlowBean flowBean, int i) {
        //o 是手机号
        String phone = text.toString();
        String prePhone = phone.substring(0, 3);
        int partition;
        if ("136".equals(prePhone)) {
            partition = 0;
        } else if ("137".equals(prePhone)) {
            partition = 1;
        } else if ("138".equals(prePhone)) {
            partition = 2;
        } else if ("139".equals(prePhone)) {
            partition = 3;
        } else {
            partition = 4;
        }

        return partition;

    }
}
