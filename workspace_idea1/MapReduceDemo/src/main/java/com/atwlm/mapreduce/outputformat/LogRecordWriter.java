package com.atwlm.mapreduce.outputformat;

import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

import java.io.IOException;

/**
 * ClassName: LogRecordWriter
 * Package: com.atwlm.mapreduce.outputformat
 * Description:
 *
 * @Author wlm.java
 * @Create 2023-07-30 - 14:44
 * @Version: v1.0
 */
public class LogRecordWriter extends RecordWriter<Text, NullWritable> {

    private FSDataOutputStream atguiguOut;
    private FSDataOutputStream otherOut;

    public LogRecordWriter(TaskAttemptContext taskAttemptContext) {
        //创建两条流
        try {
            FileSystem fs = FileSystem.get(taskAttemptContext.getConfiguration());

            atguiguOut = fs.create(new Path("F:\\developer_tools\\hadoop3.x\\Hadoop-notes\\input\\outputLogOutputFormat\\atguigu.log"));
            otherOut = fs.create(new Path("F:\\developer_tools\\hadoop3.x\\Hadoop-notes\\input\\outputLogOutputFormat\\other.log"));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void write(Text text, NullWritable nullWritable) throws IOException, InterruptedException {

        String log = text.toString();

        //具体写
        if (log.contains("atguigu")) {
            atguiguOut.writeBytes(log + "\n");
        } else {
            otherOut.writeBytes(log + "\n");
        }
    }

    @Override
    public void close(TaskAttemptContext taskAttemptContext) throws IOException, InterruptedException {
        //关流
        IOUtils.closeStream(atguiguOut);
        IOUtils.closeStream(otherOut);
    }
}
