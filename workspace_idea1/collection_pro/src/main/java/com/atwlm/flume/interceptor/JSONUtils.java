package com.atwlm.flume.interceptor;
import org.mortbay.util.ajax.JSON;
/**
 * ClassName: JSONUtils
 * Package: com.atwlm.flume.interceptor
 * Description:
 *
 * @Author wlm.java
 * @Create 2023-11-29 - 16:55
 * @Version: v1.0
 */
public class JSONUtils {
    public static boolean isJSONValidate(String log){
        try {
            // 1. 解析JSON字符串
            JSON.parse(log);
            return true;
        } catch (Exception e) {
            // 2. 失败了，证明不是JSON字符串
            return false;
        }
    }
}
