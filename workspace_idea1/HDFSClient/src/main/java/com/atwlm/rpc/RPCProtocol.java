package com.atwlm.rpc;

/**
 * ClassName: RPCProtocol
 * Package: com.atwlm.rpc
 * Description:
 *
 * @Author wlm.java
 * @Create 2023-08-03 - 12:26
 * @Version: v1.0
 */
public interface RPCProtocol {

    long versionID = 666;

    void mkdirs(String path);

}
