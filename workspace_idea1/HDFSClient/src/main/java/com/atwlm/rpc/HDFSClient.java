package com.atwlm.rpc;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.ipc.RPC;

import java.io.IOException;
import java.net.InetSocketAddress;

/**
 * ClassName: HDFSClient
 * Package: com.atwlm.rpc
 * Description:
 *
 * @Author wlm.java
 * @Create 2023-08-03 - 12:32
 * @Version: v1.0
 */
public class HDFSClient {
    public static void main(String[] args) throws IOException {
        //获取客户端对象
        RPCProtocol client = RPC.getProxy(
                RPCProtocol.class,
                RPCProtocol.versionID,
                new InetSocketAddress("localhost", 8888),
                new Configuration());

        System.out.println("我是客户端");
        client.mkdirs("/input");

    }
}
