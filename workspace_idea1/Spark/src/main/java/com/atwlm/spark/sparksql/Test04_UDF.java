package com.atwlm.spark.sparksql;

import org.apache.spark.SparkConf;
import org.apache.spark.sql.*;
import org.apache.spark.sql.api.java.UDF0;
import org.apache.spark.sql.api.java.UDF3;
import org.apache.spark.sql.expressions.UserDefinedFunction;
import org.apache.spark.sql.types.DataTypes;

import static org.apache.spark.sql.functions.udf;
/**
 * ClassName: Test04_UDF
 * Package: com.atwlm.spark.sparksql
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-24 - 11:44
 * @Version: v1.0
 */
public class Test04_UDF {
    public static void main(String[] args) throws AnalysisException {
        //1 创建Spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkSql").setMaster("local[2]");
        //2 创建SparkSql的运行环境
        SparkSession ss = SparkSession.builder().config(sparkConf).getOrCreate();
        //3 执行代码
        Dataset<Row> dataset = ss.read().json("input/user.json");

        dataset.createTempView("user_info");

        //UDF0 : 没有传入参数
        UserDefinedFunction udf0 = udf(new UDF0<Long>() {
            @Override
            public Long call() throws Exception {
                return System.currentTimeMillis();
            }
        }, DataTypes.LongType);

        //UDF3 :
        // 第一个参数：传入的第一个字段
        // 第二个参数：传入的第二个字段
        // 第三个参数：传入字段分隔符
        UserDefinedFunction udf3 = udf(new UDF3<Object, Object, String, String>() {
            @Override
            public String call(Object o, Object o2, String s) throws Exception {
                return o + s + o2;
            }
        }, DataTypes.StringType);

        ss.udf().register("myNow",udf0);

        ss.udf().register("myConcat",udf3);

        ss.sql("select name,age,myNow(),myConcat(name,age,'~') from user_info").show();


        //4 关闭资源
        ss.stop();
    }
}
