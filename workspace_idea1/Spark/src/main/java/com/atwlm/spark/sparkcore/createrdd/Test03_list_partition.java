package com.atwlm.spark.sparkcore.createrdd;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.util.Arrays;
import java.util.List;

/**
 * ClassName: Test01_list
 * Package: com.atwlm.spark.sparkcore.createrdd
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-17 - 15:48
 * @Version: v1.0
 */
public class Test03_list_partition {

    public static void main(String[] args) {

        //1.创建spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkCore").setMaster("local[*]");
        //2.创建sparkContext
        JavaSparkContext jsc = new JavaSparkContext(sparkConf);
        //3.编写逻辑代码

        //从集合创建RDD
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);

        JavaRDD<Integer> javaRDD = jsc.parallelize(list,2);

        javaRDD.saveAsTextFile("output");

        //4.关闭资源
        jsc.stop();

    }

}
