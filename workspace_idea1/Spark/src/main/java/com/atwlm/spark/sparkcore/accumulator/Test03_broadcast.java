package com.atwlm.spark.sparkcore.accumulator;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.broadcast.Broadcast;

import java.util.Arrays;
import java.util.List;

/**
 * ClassName: Test03_broadcast
 * Package: com.atwlm.spark.sparkcore.accumulator
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-23 - 12:33
 * @Version: v1.0
 */
public class Test03_broadcast {
    public static void main(String[] args) {
        //1.创建spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkCore").setMaster("local[2]");
        //2.创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
        //3.编写逻辑代码

        //抽奖
        List<Integer> list = Arrays.asList(3, 4, 8);
        JavaRDD<Integer> javaRDD = sc.parallelize(Arrays.asList(3, 4, 2, 9, 5, 7, 8, 1, 6),4);

        Broadcast<List<Integer>> broadcast = sc.broadcast(list);//广播变量

        JavaRDD<Integer> filter = javaRDD.filter(new Function<Integer, Boolean>() {
            @Override
            public Boolean call(Integer v1) throws Exception {
                return broadcast.value().contains(v1);
            }
        });

        filter.collect().forEach(System.out::println);


        //4.关闭资源
        sc.stop();
    }
}
