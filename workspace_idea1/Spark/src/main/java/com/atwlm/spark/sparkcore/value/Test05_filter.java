package com.atwlm.spark.sparkcore.value;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;

import java.util.Arrays;
import java.util.Iterator;

/**
 * ClassName: Test05_filter
 * Package: com.atwlm.spark.sparkcore.value
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-20 - 14:08
 * @Version: v1.0
 */
public class Test05_filter {
    public static void main(String[] args) {
        //1.创建spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkCore").setMaster("local[2]");
        //2.创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
        //3.编写逻辑代码
        JavaRDD<String> lineRDD = sc.textFile("input/1.txt");
        JavaRDD<String> flatMap = lineRDD.flatMap(new FlatMapFunction<String, String>() {
            @Override
            public Iterator<String> call(String line) throws Exception {
                String[] split = line.split(" ");
                return Arrays.stream(split).iterator();
            }
        });
        //过滤操作
        //返回值类型 ： 如果返回true 数据保留，返回false 数据扔掉
        JavaRDD<String> filter = flatMap.filter(new Function<String, Boolean>() {
            @Override
            public Boolean call(String Word) throws Exception {
                return !"".equals(Word) && Word != null;
            }
        });
        filter.collect().forEach(System.out::println);

        //4.关闭资源
        sc.stop();
    }
}
