package com.atwlm.spark.sparksql;

import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;

/**
 * ClassName: Test07_CSV
 * Package: com.atwlm.spark.sparksql
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-24 - 13:10
 * @Version: v1.0
 */
public class Test07_CSV {
    public static void main(String[] args) {
        //1 创建Spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkSql").setMaster("local[2]");
        //2 创建SparkSql的运行环境
        SparkSession ss = SparkSession.builder().config(sparkConf).getOrCreate();
        //3 执行代码
        Dataset<Row> csv = ss
                .read()
                .option("header",true)  //第一行放成列名,默认false
//                .option("sep","_")  //默认 ,
                .csv("input/user.csv");

        csv.show();
        csv.printSchema();

        /**
         * SaveMode.Append : 往目标目录一直追加文件
         * SaveMode.Overwrite : 如果目标目录里不为空，就清空之后再往里写
         * SaveMode.ErrorIfExists : (默认)目标目录存在就报错
         * SaveMode.Ignore : 如果目标目录存在，就不执行了（相当于啥都不干 -》又不写又不报错）
         */
        csv
                .write()
//                .option("compression","gzip")
//                .mode(SaveMode.Append)
//                .mode(SaveMode.Overwrite)
//                .mode(SaveMode.ErrorIfExists)
//                .mode(SaveMode.Ignore)
                .csv("output");

        //4 关闭资源
        ss.stop();
    }
}
