package com.atwlm.spark.sparkcore.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * ClassName: UserVisitAction
 * Package: com.atwlm.spark.sparkcore.bean
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-23 - 13:15
 * @Version: v1.0
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserVisitAction implements Serializable {
    private String date;
    private String user_id;
    private String session_id;
    private String page_id;
    private String action_time;
    private String search_keyword;
    private String click_category_id;
    private String click_product_id;
    private String order_category_ids;
    private String order_product_ids;
    private String pay_category_ids;
    private String pay_product_ids;
    private String city_id;

}
