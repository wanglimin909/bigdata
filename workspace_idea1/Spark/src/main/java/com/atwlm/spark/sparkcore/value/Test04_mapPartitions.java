package com.atwlm.spark.sparkcore.value;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * ClassName: Test04
 * Package: com.atwlm.spark.sparkcore.value
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-20 - 13:29
 * @Version: v1.0
 */
public class Test04_mapPartitions {
    public static void main(String[] args) {
        //1.创建spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkCore").setMaster("local[2]");
        //2.创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
        //3.编写逻辑代码
        List<String> list = Arrays.asList("A", "B", "C", "D", "A");

        JavaRDD<String> javaRDD = sc.parallelize(list,2);
        //将RDD的数据循环写入mysql

        //如果需要循环执行某个创建资源的逻辑,用mapPartitions,可以减少创建的数量。
        //但是mapPartitions也要慎用！！！ 容易造成OOM！！！,因为他一次拿出来的是一整个分区数据。
        //输入参数 ：一整个分区所有的数据
        javaRDD.mapPartitions(new FlatMapFunction<Iterator<String>, String >() {
            @Override
            public Iterator<String> call(Iterator<String> stringIterator) throws Exception {
                System.out.println("1.创建连接");

                while (stringIterator.hasNext()){
                    String next = stringIterator.next();

                    System.out.println("2.执行sql语句，将数据" + next + "插入mysql");

                }
                return stringIterator;
            }
        }).collect();
        System.out.println("3.关闭连接");

        //4.关闭资源
        sc.stop();
    }
}
