package com.atwlm.spark.sparkcore.action;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * ClassName: Test01_action
 * Package: com.atwlm.spark.sparkcore.action
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-21 - 16:27
 * @Version: v1.0
 */
public class Test01_action {
    public static void main(String[] args) {

        System.setProperty("HADOOP_USER_NAME","atwlm");

        //1.创建spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkCore").setMaster("local[2]");
        //2.创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
        //3.编写逻辑代码

        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
        JavaRDD<Integer> javaRDD = sc.parallelize(list, 2);

        ArrayList<Tuple2<String, Integer>> list1 = new ArrayList<>();
        list1.add(new Tuple2<>("A",1));
        list1.add(new Tuple2<>("B",1));
        list1.add(new Tuple2<>("C",1));
        list1.add(new Tuple2<>("B",1));
        list1.add(new Tuple2<>("B",1));
        list1.add(new Tuple2<>("D",1));
        list1.add(new Tuple2<>("D",1));
        JavaPairRDD<String, Integer> pairRDD = sc.parallelizePairs(list1,2);

        //collect 将所有executor的数据收集到driver端
        System.out.println("---------------collect--------------------");
        List<Integer> collect = javaRDD.collect();
        System.out.println(collect);

        System.out.println("---------------count--------------------");
        //count 返回RDD当中的数据条数，可以预估出整个RDD的数据量
        long count = javaRDD.count();
        System.out.println(count);

        System.out.println("---------------first--------------------");
        //first 返回RDD当中的第一个元素
        Integer first = javaRDD.first();
        System.out.println(first);

        System.out.println("---------------take--------------------");
        //take 相当于 hive 的 limit
        List<Integer> take = javaRDD.take(3);
        System.out.println(take);

        System.out.println("---------------countByKey--------------------");
        // wordcount之后，如果还要排序等操作，就没法用spark算了，只能对Map排序了，Map已经在Driver了，不是用Executor分布式排序了
        // 当然也可以再给Map转化成RDD继续计算，麻烦
        //countByKey作用：发现数据倾斜的key是哪一个
        Map<String, Long> countByKey = pairRDD.countByKey();
        System.out.println(countByKey);

        System.out.println("---------------saveAsTextFile--------------------");
        //如何写入HDFS
        //1.在Windows执行
            //     hdfs://hadoop:8020/output
            //     把core-site.xml 和 hdfs-site.xml 拿到 resources 目录下
        //2.在Linux执行
            //     hdfs://hadoop:8020/output
            //     /output     找到环境变量当中的HADOOP_HOME,进而找到HDFS URL
        //在linux上如何写到linux本地磁盘？
            //     file:///opt/module/output

        javaRDD.saveAsTextFile("output");

        System.out.println("---------------saveAsObjectFile--------------------");
        javaRDD.saveAsObjectFile("output1");

        //4.关闭资源
        sc.stop();
    }
}
