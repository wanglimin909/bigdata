package com.atwlm.spark.sparksql;

import com.atwlm.spark.sparksql.bean.User;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.*;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * ClassName: Test01_DatasetAPI
 * Package: com.atwlm.spark.sparksql
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-23 - 16:12
 * @Version: v1.0
 */
public class Test01_DatasetAPI {
    public static void main(String[] args) {
        //1 创建Spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkSql").setMaster("local[2]");
        //2 创建SparkSql的运行环境
        SparkSession ss = SparkSession.builder().config(sparkConf).getOrCreate();
        //3 执行代码
        DataFrameReader read = ss.read();
        Dataset<Row> dataset = read.json("input/user.json");

        Dataset<User> map = dataset.map(new MapFunction<Row, User>() {
            @Override
            public User call(Row value) throws Exception {
                long age = value.getLong(0);
                String name = value.getString(1);
                return new User(age, name);
            }
        }, Encoders.bean(User.class));

        map.show();
        map.printSchema();

        Dataset<User> userDataset = dataset.as(Encoders.bean(User.class));

        userDataset.show();
        userDataset.printSchema();

        Dataset<User> flatMap = map.flatMap(new FlatMapFunction<User, User>() {
            @Override
            public Iterator<User> call(User user) throws Exception {
                ArrayList<User> users = new ArrayList<>();
                users.add(user);
                users.add(user);
                users.add(user);
                users.add(user);
                return users.iterator();
            }
        }, Encoders.bean(User.class));

        flatMap.show();
        flatMap.printSchema();

        //4 关闭资源
        ss.stop();
    }
}
