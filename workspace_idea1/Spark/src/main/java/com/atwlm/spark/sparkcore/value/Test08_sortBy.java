package com.atwlm.spark.sparkcore.value;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;

import java.util.Arrays;
import java.util.List;

/**
 * ClassName: Test08_sortBy
 * Package: com.atwlm.spark.sparkcore.value
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-20 - 18:14
 * @Version: v1.0
 */
public class Test08_sortBy {
    public static void main(String[] args) {
        //1.创建spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkCore").setMaster("local[2]");
        //2.创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
        //3.编写逻辑代码
        List<Integer> list = Arrays.asList(4, 2, 7, 5, 9, 6, 8, 1, 0, 3);
        JavaRDD<Integer> javaRDD = sc.parallelize(list);

        //传入参数 ： RDD当中的数据
        //传出参数 ： 排序字段
        JavaRDD<Integer> sortBy = javaRDD.sortBy(new Function<Integer, Integer>() {
            @Override
            public Integer call(Integer integer) throws Exception {
                return integer;
            }
        }, true, 3);
        sortBy.collect().forEach(System.out::println);

        //4.关闭资源
        sc.stop();
    }
}
