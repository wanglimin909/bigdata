package com.atwlm.spark.sparkcore.accumulator;

import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.util.LongAccumulator;

import java.util.Arrays;
import java.util.List;

/**
 * ClassName: Test01_accumulator
 * Package: com.atwlm.spark.sparkcore.accumulator
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-23 - 11:56
 * @Version: v1.0
 */
public class Test01_accumulator_foreach {
    public static void main(String[] args) {
        //1.创建spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkCore").setMaster("local[2]");
        //2.创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(sparkConf);

        SparkContext sparkContext = JavaSparkContext.toSparkContext(sc);//sparkContext里面才有累加器

        LongAccumulator acc = sparkContext.longAccumulator();//累加器

        LongAccumulator acc1 = sparkContext.longAccumulator();//累加器

        //3.编写逻辑代码
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
        JavaRDD<Integer> javaRDD = sc.parallelize(list,2);

        javaRDD.foreach(new VoidFunction<Integer>() {
            @Override
            public void call(Integer integer) throws Exception {
                acc.add(integer);
            }
        });

        javaRDD.foreach(new VoidFunction<Integer>() {
            @Override
            public void call(Integer integer) throws Exception {
                acc1.add(integer);
            }
        });

        System.out.println(acc.value());//15
        System.out.println(acc1.value());//15

        //4.关闭资源
        sc.stop();
    }
}
