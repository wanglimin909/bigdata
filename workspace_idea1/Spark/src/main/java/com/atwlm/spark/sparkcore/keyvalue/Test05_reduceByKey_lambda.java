package com.atwlm.spark.sparkcore.keyvalue;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function2;
import scala.Tuple2;

import java.util.ArrayList;

/**
 * ClassName: Test05_reduceByKey_lambda
 * Package: com.atwlm.spark.sparkcore.keyvalue
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-21 - 13:18
 * @Version: v1.0
 */
public class Test05_reduceByKey_lambda {
    public static void main(String[] args) {
        //1.创建spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkCore").setMaster("local[2]");
        //2.创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
        //3.编写逻辑代码
        ArrayList<Tuple2<String, Integer>> list = new ArrayList<>();
        list.add(new Tuple2<>("A",1));
        list.add(new Tuple2<>("B",1));
        list.add(new Tuple2<>("B",1));
        list.add(new Tuple2<>("B",1));
        list.add(new Tuple2<>("C",1));
        list.add(new Tuple2<>("C",1));
        list.add(new Tuple2<>("D",1));
        list.add(new Tuple2<>("D",1));
        JavaPairRDD<String, Integer> pairRDD = sc.parallelizePairs(list,2);

        //第一个传入参数 ： sum [第一个值为value的初始值] 【分区之间的聚合也要符合聚合逻辑】
        //第二个传入参数 : elem
        //传出参数 ： 聚合之后的结果
        JavaPairRDD<String, Integer> reduceByKey = pairRDD.reduceByKey(new Function2<Integer, Integer, Integer>() {
            @Override
            public Integer call(Integer sum, Integer elem) throws Exception {
                return sum + elem;
            }
        });
        reduceByKey.collect().forEach(System.out::println);

        System.out.println("-------------------reduceByKey Lambda-----------------------");

        pairRDD
                .reduceByKey((Integer sum, Integer elem) -> {
                    return sum + elem;
                })
                .collect()
                .forEach(System.out::println);

        System.out.println("-------------------reduceByKey Lambda 化简1-----------------------");

        pairRDD
                .reduceByKey((sum, elem) -> {
                    return sum + elem;
                })
                .collect()
                .forEach(System.out::println);

        System.out.println("-------------------reduceByKey Lambda 化简2-----------------------");
        //当参数列表中有多个参数时，参数列表的括号就不能省略了
        pairRDD
                .reduceByKey((sum, elem) -> sum + elem)
                .collect()
                .forEach(System.out::println);

        System.out.println("-------------------reduceByKey Lambda 化简3-----------------------");
        //不建议写这种
        pairRDD
                .reduceByKey(Integer::sum)
                .collect()
                .forEach(System.out::println);

        //4.关闭资源
        sc.stop();
    }
}
