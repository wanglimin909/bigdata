package com.atwlm.spark.sparkcore.partition;

import org.apache.spark.Partitioner;

/**
 * ClassName: CustomPartitioner
 * Package: com.atwlm.spark.sparkcore.partition
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-22 - 17:23
 * @Version: v1.0
 */
public class CustomPartitioner extends Partitioner {

    private int numPartitions;
    public CustomPartitioner() {
    }
    public CustomPartitioner(int numPartitions) {
        this.numPartitions = numPartitions;
    }

    /**
     * 获取分区数量
     */
    @Override
    public int numPartitions() {
        return numPartitions;
    }

    /**
     * 根据key，自定义分区规则
     * return 的结果就是分区号
     */
    @Override
    public int getPartition(Object key) {
        int intKey = (int) key;
        if(intKey == 1){
            return 0;
        }else if (intKey == 2){
            return 1;
        }else {
            return intKey % numPartitions;
        }
    }

    public int getNumPartitions() {
        return numPartitions;
    }
    public void setNumPartitions(int numPartitions) {
        this.numPartitions = numPartitions;
    }
}
