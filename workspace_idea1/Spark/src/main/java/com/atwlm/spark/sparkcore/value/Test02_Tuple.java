package com.atwlm.spark.sparkcore.value;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import scala.Tuple2;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ClassName: Test02
 * Package: com.atwlm.spark.sparkcore.value
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-20 - 11:48
 * @Version: v1.0
 */
public class Test02_Tuple {
    public static void main(String[] args) {
        //1.创建spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkCore").setMaster("local[2]");
        //2.创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
        //3.编写逻辑代码

        List<String> list = Arrays.asList("A", "B", "C", "D", "A");

        JavaRDD<String> javaRDD = sc.parallelize(list,2);

        //尝试用HashMap装结果，但是将5个单词装成了5个HashMap，不好
//        JavaRDD<Map<String, Integer>> map = javaRDD.map(new Function<String, Map<String, Integer>>() {
//            @Override
//            public Map<String, Integer> call(String word) throws Exception {
//                HashMap<String, Integer> hashMap = new HashMap<>();
//                hashMap.put(word, 1);
//                return hashMap;
//            }
//        });
//
//        map.collect().forEach(System.out::println);

        //Tuple 元组（scala）
        JavaRDD<Tuple2<String, Integer>> map = javaRDD.map(new Function<String, Tuple2<String, Integer>>() {
            @Override
            public Tuple2<String, Integer> call(String word) throws Exception {
                return new Tuple2<>(word, 1);
            }
        });

        map.collect().forEach(System.out::println);

        System.out.println("--------------------Tuple map Lambda----------------------");
        javaRDD
                .map(word -> new Tuple2<>(word,1))
                .collect()
                .forEach(System.out::println)
        ;

        //4.关闭资源
        sc.stop();
    }
}
