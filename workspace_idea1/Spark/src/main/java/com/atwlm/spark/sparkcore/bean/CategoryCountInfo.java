package com.atwlm.spark.sparkcore.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * ClassName: CategoryCountInfo
 * Package: com.atwlm.spark.sparkcore.bean
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-23 - 13:38
 * @Version: v1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CategoryCountInfo implements Serializable,Comparable<CategoryCountInfo>{
    private String categoryId;
    private Long clickCount;
    private Long orderCount;
    private Long payCount;

    /**
     * 比较
     *      传入参数：和this比较的对象
     *      return：this > 传入   return 正数
     *      return：this < 传入   return 负数
     *      return：this = 传入   return 0
     */
    @Override
    public int compareTo(CategoryCountInfo o) {
        // 小于返回-1,等于返回0,大于返回1
        if (this.getClickCount().equals(o.getClickCount())) {
            if (this.getOrderCount().equals(o.getOrderCount())) {
                if (this.getPayCount().equals(o.getPayCount())) {
                    return 0;
                } else {
                    return this.getPayCount() < o.getPayCount() ? -1 : 1;
                }
            } else {
                return this.getOrderCount() < o.getOrderCount() ? -1 : 1;
            }
        } else {
            return this.getClickCount() < o.getClickCount() ? -1 : 1;
        }
    }

}

