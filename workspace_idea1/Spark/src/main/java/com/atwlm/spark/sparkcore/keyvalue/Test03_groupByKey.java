package com.atwlm.spark.sparkcore.keyvalue;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.util.ArrayList;

/**
 * ClassName: Test03_groupByKey
 * Package: com.atwlm.spark.sparkcore.keyvalue
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-21 - 12:28
 * @Version: v1.0
 */
public class Test03_groupByKey {
    public static void main(String[] args) {
        //1.创建spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkCore").setMaster("local[2]");
        //2.创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
        //3.编写逻辑代码
        ArrayList<Tuple2<String, Integer>> list = new ArrayList<>();
        list.add(new Tuple2<>("A",1));
        list.add(new Tuple2<>("B",1));
        list.add(new Tuple2<>("B",1));
        list.add(new Tuple2<>("B",1));
        list.add(new Tuple2<>("C",1));
        list.add(new Tuple2<>("C",1));
        list.add(new Tuple2<>("D",1));
        list.add(new Tuple2<>("D",1));
        JavaPairRDD<String, Integer> pairRDD = sc.parallelizePairs(list);

        JavaPairRDD<String, Iterable<Integer>> groupByKey = pairRDD.groupByKey();

        groupByKey.collect().forEach(System.out::println);

        System.out.println("----------------groupByKey wordcount---------------------");

        JavaRDD<Tuple2<String, Integer>> map = groupByKey.map(v1 -> {
            int sum = 0;
            for (Integer elem : v1._2) {
                sum += elem;
            }
            return new Tuple2<>(v1._1, sum);
        });

        map.collect().forEach(System.out::println);


        //4.关闭资源
        sc.stop();
    }
}
