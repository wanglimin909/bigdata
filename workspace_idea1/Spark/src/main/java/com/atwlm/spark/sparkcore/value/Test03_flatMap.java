package com.atwlm.spark.sparkcore.value;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

/**
 * ClassName: Test03
 * Package: com.atwlm.spark.sparkcore.value
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-20 - 12:55
 * @Version: v1.0
 */
public class Test03_flatMap {
    public static void main(String[] args) {
        //1.创建spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkCore").setMaster("local[2]");
        //2.创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
        //3.编写逻辑代码
        JavaRDD<String> lineRDD = sc.textFile("input/1.txt");

        // "hello world" -> "hello"  "world"
        JavaRDD<String> flatMap = lineRDD.flatMap(new FlatMapFunction<String, String>() {
            @Override
            public Iterator<String> call(String line) throws Exception {
                String[] split = line.split(" ");
                return Arrays.stream(split).iterator();
            }
        });
        JavaRDD<Tuple2<String, Integer>> map = flatMap.map(new Function<String, Tuple2<String, Integer>>() {
            @Override
            public Tuple2<String, Integer> call(String word) throws Exception {
                return new Tuple2<>(word, 1);
            }
        });
        map.collect().forEach(System.out::println);

        System.out.println("----------------化简--------------------");

        // "hello world" -> (hello,1)  (world,1)
        JavaRDD<Tuple2<String, Integer>> flatMap1 = lineRDD.flatMap(new FlatMapFunction<String, Tuple2<String, Integer>>() {
            @Override
            public Iterator<Tuple2<String, Integer>> call(String line) throws Exception {
                String[] split = line.split(" ");
                ArrayList<Tuple2<String, Integer>> result = new ArrayList<>();
                for (String word : split) {
                    result.add(new Tuple2<>(word, 1));
                }
                return result.iterator();
            }
        });
        flatMap1.collect().forEach(System.out::println);

        System.out.println("----------------flatMap lambda--------------------");

        lineRDD
                .flatMap(line -> {
                    String[] split = line.split(" ");
                    ArrayList<Tuple2<String, Integer>> result = new ArrayList<>();
                    for (String word : split) {
                        result.add(new Tuple2<>(word,1));
                    }
                    return result.iterator();
                })
                .collect()
                .forEach(System.out::println)
        ;

        //4.关闭资源
        sc.stop();
    }
}
