package com.atwlm.spark.sparkcore.keyvalue;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import scala.Tuple2;

import java.util.ArrayList;

/**
 * ClassName: Test02_mapValues
 * Package: com.atwlm.spark.sparkcore.keyvalue
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-21 - 12:19
 * @Version: v1.0
 */
public class Test02_mapValues {
    public static void main(String[] args) {
        //1.创建spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkCore").setMaster("local[2]");
        //2.创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
        //3.编写逻辑代码
        ArrayList<Tuple2<String, Integer>> list = new ArrayList<>();
        list.add(new Tuple2<>("A",1));
        list.add(new Tuple2<>("B",1));
        list.add(new Tuple2<>("C",1));
        list.add(new Tuple2<>("D",1));
        JavaPairRDD<String, Integer> pairRDD = sc.parallelizePairs(list);

        //修改 pairRDD 的 value * 2
        JavaPairRDD<String, Integer> mapToPair = pairRDD.mapToPair(new PairFunction<Tuple2<String, Integer>, String, Integer>() {
            @Override
            public Tuple2<String, Integer> call(Tuple2<String, Integer> v) throws Exception {
                return new Tuple2<>(v._1, v._2 * 2);
            }
        });
        mapToPair.collect().forEach(System.out::println);

        System.out.println("-------------------mapValues-------------------------");

        //传入参数：pairRDD的value值
        //传出参数：修改后的value值
        JavaPairRDD<String, Integer> mapValues = pairRDD.mapValues(new Function<Integer, Integer>() {
            @Override
            public Integer call(Integer v1) throws Exception {
                return v1 * 2;
            }
        });
        mapValues.collect().forEach(System.out::println);


        //4.关闭资源
        sc.stop();
    }
}
