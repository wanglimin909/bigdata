package com.atwlm.spark.sparkcore.keyvalue;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.util.ArrayList;

/**
 * ClassName: Test08_sortByKey
 * Package: com.atwlm.spark.sparkcore.keyvalue
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-21 - 16:10
 * @Version: v1.0
 */
public class Test08_sortByKey {
    public static void main(String[] args) {
        //1.创建spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkCore").setMaster("local[2]");
        //2.创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
        //3.编写逻辑代码
        ArrayList<Tuple2<Integer, Integer>> list = new ArrayList<>();
        list.add(new Tuple2<>(1,4));
        list.add(new Tuple2<>(4,1));
        list.add(new Tuple2<>(2,3));
        list.add(new Tuple2<>(3,2));
        JavaPairRDD<Integer, Integer> pairRDD = sc.parallelizePairs(list,2);

        pairRDD
                .sortByKey(true)
                .collect()
                .forEach(System.out::println);

        System.out.println("------------------根据value排序------------------");

        pairRDD
                .mapToPair(v1 -> new Tuple2<>(v1._2,v1._1))
                .sortByKey(true)
                .mapToPair(v1 -> new Tuple2<>(v1._2,v1._1))
                .collect()
                .forEach(System.out::println);

        pairRDD
                .map(v1 -> new Tuple2<>(v1._1,v1._2))
                .sortBy(v1 -> v1._2,true,2)
                .collect()
                .forEach(System.out::println);

        //4.关闭资源
        sc.stop();
    }
}
