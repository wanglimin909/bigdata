package com.atwlm.spark.sparkcore.keyvalue;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import scala.Tuple2;

import java.util.ArrayList;

/**
 * ClassName: Test07_avg_reduceByKey
 * Package: com.atwlm.spark.sparkcore.keyvalue
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-21 - 14:39
 * @Version: v1.0
 */
public class Test07_avg_reduceByKey {
    public static void main(String[] args) {
        //1.创建spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkCore").setMaster("local[2]");
        //2.创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
        //3.编写逻辑代码

        ArrayList<Tuple2<String ,Integer>> list = new ArrayList<>();
        list.add(new Tuple2<>("A",10));
        list.add(new Tuple2<>("B",15));
        list.add(new Tuple2<>("C",20));
        list.add(new Tuple2<>("A",20));
        list.add(new Tuple2<>("B",11));
        list.add(new Tuple2<>("A",14));
        list.add(new Tuple2<>("C",30));
        JavaPairRDD<String, Integer> pairRDD = sc.parallelizePairs(list,2);

        // avg = sum / count
        //如何通过一个reduceByKey聚合多个值
        //reduceByKey聚合是通过key对value聚合，如果value能装下两个值，就可以同时聚合两个值
        JavaPairRDD<String, Tuple2<Integer, Integer>> mapValues = pairRDD.mapValues(new Function<Integer, Tuple2<Integer, Integer>>() {
            @Override
            public Tuple2<Integer, Integer> call(Integer v1) throws Exception {
                return new Tuple2<>(v1, 1);
            }
        });
        JavaPairRDD<String, Tuple2<Integer, Integer>> reduceByKey = mapValues.reduceByKey(new Function2<Tuple2<Integer, Integer>, Tuple2<Integer, Integer>, Tuple2<Integer, Integer>>() {
            @Override
            public Tuple2<Integer, Integer> call(Tuple2<Integer, Integer> sum, Tuple2<Integer, Integer> elem) throws Exception {
                return new Tuple2<>(sum._1 + elem._1(), sum._2() + elem._2());
            }
        });
        JavaPairRDD<String, Double> avg = reduceByKey.mapValues(new Function<Tuple2<Integer, Integer>, Double>() {
            @Override
            public Double call(Tuple2<Integer, Integer> v1) throws Exception {
                String format = String.format("%.1f", v1._1.doubleValue() / v1._2);//保留1位小数
                return Double.valueOf(format);
            }
        });

        avg.collect().forEach(System.out::println);

        System.out.println("-----------------avg lambda---------------------");

        // avg = sum / count
        // ("A",30)
            //如何通过一个reduceByKey聚合多个值
            //reduceByKey聚合是通过key对value聚合，如果value能装下两个值，就可以同时聚合两个值
        // ("A",30)  ->  ("A",(30,1))
        // ("A",14)  ->  ("A",(14,1))
        // ("A",(30,1)) + ("A",(14,1))  ->  ("A",(44,2))
        // ("A",(44,2))  ->  ("A",22)
        pairRDD
                .mapValues(v1 -> new Tuple2<>(v1,1))
                .reduceByKey((sum,elem) -> new Tuple2<>(sum._1 + elem._1 , sum._2 + elem._2))
                .mapValues(v1 -> Double.valueOf(String.format("%.1f",v1._1.doubleValue() / v1._2)))
                .collect()
                .forEach(System.out::println);

        //4.关闭资源
        sc.stop();
    }
}
