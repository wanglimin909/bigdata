package com.atwlm.spark.sparksql;

import org.apache.spark.SparkConf;
import org.apache.spark.sql.*;

/**
 * ClassName: Test02_sql
 * Package: com.atwlm.spark.sparksql
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-23 - 17:28
 * @Version: v1.0
 */
public class Test02_sql {
    public static void main(String[] args) throws AnalysisException {
        //1 创建Spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkSql").setMaster("local[2]");
        //2 创建SparkSql的运行环境
        SparkSession ss = SparkSession.builder().config(sparkConf).getOrCreate();

        //一个Application可以对应多个SparkSession
        SparkSession ss2 = ss.newSession();

        //3 执行代码
        Dataset<Row> dataset = ss.read().json("input/user.json");

        //普通的临时视图只有当前session可用
        dataset.createTempView("user_info");
//        dataset.createOrReplaceGlobalTempView("user_info");

        //全局的临时视图允许当前Application的所有session使用
        dataset.createGlobalTempView("user_info");//查询时需要用 global_temp.user_info

        //普通的临时视图只有当前session可用
        Dataset<Row> sql = ss.sql("select * from user_info where age >=18");

        //全局的临时视图允许当前Application的所有session使用
        Dataset<Row> sql1 = ss2.sql("select * from global_temp.user_info where age >=18");
        Dataset<Row> sql2 = ss2.sql("select * from global_temp.user_info where age >=18");

        sql.show();
        sql1.show();
        sql2.show();

        //4 关闭资源
        ss.stop();
    }
}
