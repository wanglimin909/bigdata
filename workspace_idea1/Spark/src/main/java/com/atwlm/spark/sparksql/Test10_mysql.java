package com.atwlm.spark.sparksql;

import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;

import java.util.Properties;

/**
 * ClassName: Test10_mysql
 * Package: com.atwlm.spark.sparksql
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-24 - 13:50
 * @Version: v1.0
 */
public class Test10_mysql {
    public static void main(String[] args) {
        //1 创建Spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkSql").setMaster("local[2]");
        //2 创建SparkSql的运行环境
        SparkSession ss = SparkSession.builder().config(sparkConf).getOrCreate();
        //3 执行代码

        Properties properties = new Properties();
        properties.put("user","root");
        properties.put("password","123456");
        Dataset<Row> jdbc = ss
                .read()
                .jdbc("jdbc:mysql://hadoop102:3306/gmall?useSSL=false", "base_province", properties);

        //show 默认展示20行  truncate 打断
        jdbc.show();

        jdbc
                .write()
//                .mode(SaveMode.Append)
                //表不用提前创建
                .jdbc("jdbc:mysql://hadoop102:3306/gmall?useSSL=false","test_province",properties);


        //4 关闭资源
        ss.stop();
    }
}
