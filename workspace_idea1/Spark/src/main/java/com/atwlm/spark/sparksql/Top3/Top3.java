package com.atwlm.spark.sparksql.Top3;

import org.apache.spark.SparkConf;
import org.apache.spark.sql.AnalysisException;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.SparkSession;

import static org.apache.spark.sql.functions.udaf;
/**
 * ClassName: Top3
 * Package: com.atwlm.spark.sparksql.TopN
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-24 - 17:16
 * @Version: v1.0
 */
public class Top3 {
    public static void main(String[] args) throws AnalysisException {
        //1 创建Spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkSql").setMaster("local[2]");
        //2 创建SparkSql的运行环境
        SparkSession ss = SparkSession.builder().enableHiveSupport().config(sparkConf).getOrCreate();
        //3 执行代码

        ss.udf().register("cityMark",udaf(new CityMark(), Encoders.STRING()));

        ss
                .sql("select \n" +
                "\tc.city_name,\n" +
                "\tc.area,\n" +
                "\tp.product_name\n" +
                "from \n" +
                "\tuser_visit_action u\n" +
                "join\n" +
                "\tcity_info c \n" +
                "on\n" +
                "\tu.city_id = c.city_id\n" +
                "join\n" +
                "\tproduct_info p\n" +
                "on\n" +
                "\tu.click_product_id = p.product_id\n" +
                "where \n" +
                "\tu.click_product_id != -1")
                .createTempView("t1")
        ;

        ss
                .sql("select \n" +
                        "\tt1.area,\n" +
                        "\tt1.product_name,\n" +
                        "\tcount(*) click_count,\n" +
                        "\tcityMark(t1.city_name) city_mark\n" +
                        "from \n" +
                        "\tt1\n" +
                        "group by \n" +
                        "\tt1.area,\n" +
                        "\tt1.product_name")
                .createTempView("t2")
        ;

        ss
                .sql("select \n" +
                "\tt2.area,\n" +
                "\tt2.product_name,\n" +
                "\tt2.click_count,\n" +
                "\trank() over(partition by t2.area order by t2.click_count desc) rk,\n" +
                "\tt2.city_mark\n" +
                "from \n" +
                "\tt2")
                .createTempView("t3")
        ;

        ss
                .sql("select \n" +
                "\tt3.area,\n" +
                "\tt3.product_name,\n" +
                "\tt3.click_count,\n" +
                "\tt3.rk,\n" +
                "\tt3.city_mark\n" +
                "from \n" +
                "\tt3\n" +
                "where \n" +
                "\trk <= 3")
                .show(false)
        ;

        //4 关闭资源
        ss.stop();
    }
}
