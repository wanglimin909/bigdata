package com.atwlm.spark.sparkcore.serialize;

import com.atwlm.spark.sparkcore.bean.User;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * ClassName: Test01_Ser
 * Package: com.atwlm.spark.sparkcore.serialize
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-22 - 10:21
 * @Version: v1.0
 */
public class Test01_Ser {
    public static void main(String[] args) throws SQLException {
        //1.创建spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkCore").setMaster("local[2]");
        //2.创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(sparkConf);

        //3.编写逻辑代码
        JavaRDD<String> lineRDD = sc.textFile("input/user.txt",2);

        JavaRDD<User> map = lineRDD.map(new Function<String, User>() {
            @Override
            public User call(String v1) throws Exception {
                String[] split = v1.split(" ");
                return new User(
                        Integer.valueOf(split[0]),
                        split[1],
                        Integer.valueOf(split[2])
                );
            }
        });

        map.collect().forEach(System.out::println);

        //4.关闭资源
        sc.stop();
    }
}
