package com.atwlm.spark.sparkcore.value;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

/**
 * ClassName: Test01_map
 * Package: com.atwlm.spark.sparkcore.value
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-20 - 11:26
 * @Version: v1.0
 */
public class Test01_map {

    public static void main(String[] args) {
        //1.创建spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkCore").setMaster("local[2]");
        //2.创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
        //3.编写逻辑代码

        List<String> list = Arrays.asList("A", "B", "C", "D", "A");

        JavaRDD<String> javaRDD = sc.parallelize(list,2);

        // "A" -> "A,1"
        JavaRDD<String> map = javaRDD.map(new Function<String, String>() {
            @Override
            public String call(String word) throws Exception {
                return word + ",1";
            }
        });
        map.collect().forEach(System.out::println);

        //lambda,熟悉方法的输入和输出参数就好些了
        System.out.println("---------------map lambda-----------------");

        javaRDD
                .map( word ->  word + ",1")
                .collect()
                .forEach(System.out::println)
        ;

        //4.关闭资源
        sc.stop();
    }

}
