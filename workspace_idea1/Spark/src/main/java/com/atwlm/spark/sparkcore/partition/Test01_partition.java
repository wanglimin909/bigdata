package com.atwlm.spark.sparkcore.partition;

import org.apache.spark.Partitioner;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.Optional;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * ClassName: Test01_partition
 * Package: com.atwlm.spark.sparkcore.partition
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-22 - 15:58
 * @Version: v1.0
 */
public class Test01_partition {
    public static void main(String[] args) {
        //1.创建spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkCore").setMaster("local[2]");
        //2.创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
        //3.编写逻辑代码
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
        JavaRDD<Integer> javaRDD = sc.parallelize(list, 2);

        ArrayList<Tuple2<String, Integer>> list1 = new ArrayList<>();
        list1.add(new Tuple2<>("A",1));
        list1.add(new Tuple2<>("B",1));
        list1.add(new Tuple2<>("C",1));
        JavaPairRDD<String, Integer> pairRDD = sc.parallelizePairs(list1,2);

        //获取分区器
        Optional<Partitioner> javaRDDPartitioner = javaRDD.partitioner();
        Optional<Partitioner> pairRDDPartition = pairRDD.partitioner();
        System.out.println("parallelize分区器：" + javaRDDPartitioner);//parallelize分区器：Optional.empty
        System.out.println("parallelizePairs分区器：" + pairRDDPartition);//parallelizePairs分区器：Optional.empty

        System.out.println("map分区器：" + javaRDD.map(x -> x).partitioner());//map分区器：Optional.empty

        System.out.println("groupBy分区器：" + javaRDD.groupBy(v1 -> v1).partitioner());//groupBy分区器：Optional[org.apache.spark.HashPartitioner@2]

        System.out.println("sortBy分区器：" + javaRDD.sortBy(v1 -> v1,false,2).partitioner());//sortBy分区器：Optional.empty

        System.out.println("distinct分区器：" + javaRDD.distinct(3).partitioner());//distinct分区器：Optional.empty



        System.out.println("groupBy分区器：" + pairRDD.groupByKey().partitioner());//groupBy分区器：Optional[org.apache.spark.HashPartitioner@2]

        System.out.println("reduceByKey分区器：" + pairRDD.reduceByKey(Integer::sum).partitioner());//reduceByKey分区器：Optional[org.apache.spark.HashPartitioner@2]

        System.out.println("sortByKey分区器：" + pairRDD.sortByKey().partitioner());//sortByKey分区器：Optional[org.apache.spark.RangePartitioner@108e]

        System.out.println("distinct分区器：" + pairRDD.distinct(3).partitioner());//distinct分区器：Optional.empty

        //4.关闭资源
        sc.stop();
    }
}
