package com.atwlm.spark.sparksql.Top3;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.expressions.Aggregator;

import java.io.Serializable;
import java.util.*;
import java.util.function.BiConsumer;

/**
 * ClassName: CityMark
 * Package: com.atwlm.spark.sparksql.Top3
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-24 - 17:58
 * @Version: v1.0
 */

/**
 * 北京21.2%，天津13.2%，其他65.6%
 * 各地区城市商品点击量的比例 =  各地区城市商品点击量  /  各地区城市商品总点击量
 */
public class CityMark_TreeMap extends Aggregator<String, CityMark_TreeMap.Buffer, String> {

    //初始化buffer
    @Override
    public Buffer zero() {
        return new Buffer(0L, new HashMap<String, Long>());
    }

    //分区内预聚合
    //将城市名字累加到Buffer里
    @Override
    public Buffer reduce(Buffer b, String cityName) {
        //累加地区次数
        b.setAreaCount(b.getAreaCount() + 1);

        //累加城市次数
        Map<String, Long> citysCount = b.getCitysCount();
        citysCount.put(cityName, citysCount.getOrDefault(cityName, 0L) + 1L);

        //没必要执行，因为上面修改的是Map的堆内存的数据
        b.setCitysCount(citysCount);
        return b;
    }

    //分区间聚合
    @Override
    public Buffer merge(Buffer sum, Buffer elem) {
        //累加地区次数
        sum.setAreaCount(sum.getAreaCount() + elem.getAreaCount());
        //累加城市次数
        Map<String, Long> sumCitysCount = sum.getCitysCount();
        Map<String, Long> elemCitysCount = elem.getCitysCount();
        elemCitysCount.forEach(new BiConsumer<String, Long>() {
            @Override
            public void accept(String cityName, Long cityCount) {
                sumCitysCount.put(cityName, sumCitysCount.getOrDefault(cityName, 0L) + cityCount);
            }
        });

        //没必要执行，因为上面修改的是Map的堆内存的数据
        sum.setCitysCount(sumCitysCount);

        return sum;
    }

    //根据buffer拼接最终结果
    //北京21.2%，天津13.2%，其他65.6%
    @Override
    public String finish(Buffer reduction) {
        Long areaCount = reduction.getAreaCount();
        Map<String, Long> citysCount = reduction.citysCount;

        //Map集合排序,但是Map默认没有排序功能
        // 1 TreeSet  2 TreeMap  3 CollectionsAPI（最优解）
        TreeMap<Long, String> treeMap = new TreeMap<>();
        citysCount.forEach(new BiConsumer<String, Long>() {
            @Override
            public void accept(String cityName, Long cityCount) {
                if (!treeMap.containsKey(cityCount)) {
                    treeMap.put(cityCount, cityName);
                } else {
                    treeMap.put(cityCount, treeMap.get(cityCount) + "_" + cityName);
                }
            }
        });


        //取List集合前二
        ArrayList<String> result = new ArrayList<>();
        //北京21.2% 天津13.2%
        double sum = 0.0;
        while (treeMap.size() > 0 && result.size() < 2) {
            Map.Entry<Long, String> lastEntry = treeMap.lastEntry();
            Long cityCount = lastEntry.getKey();
            String citysName = lastEntry.getValue();

            String[] split = citysName.split("_");
            for (String cityName : split) {
                double ratio = cityCount.doubleValue() * 100 / areaCount;
                String format = String.format("%.1f", ratio);

                result.add(cityName + format + "%");
                sum += ratio;
            }
            treeMap.remove(cityCount);
        }

        //拼接其他城市
        double other = 100 - sum;
        String format = String.format("%.1f", other);
        result.add("其他" + format + "%");

        return result.toString().substring(1, result.toString().length() - 1);
    }

    @Override
    public Encoder<Buffer> bufferEncoder() {
        return Encoders.bean(Buffer.class);
    }

    @Override
    public Encoder<String> outputEncoder() {
        return Encoders.STRING();
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public static class Buffer implements Serializable {
        private Long areaCount;
        private Map<String, Long> citysCount;
    }
}
