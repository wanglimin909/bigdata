package com.atwlm.spark.sparksql;

import org.apache.spark.SparkConf;
import org.apache.spark.sql.*;

import static org.apache.spark.sql.functions.udaf;
/**
 * ClassName: Test05_UDAF
 * Package: com.atwlm.spark.sparksql
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-24 - 12:07
 * @Version: v1.0
 */
public class Test05_UDAF {
    public static void main(String[] args) throws AnalysisException {
        //1 创建Spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkSql").setMaster("local[2]");
        //2 创建SparkSql的运行环境
        SparkSession ss = SparkSession.builder().config(sparkConf).getOrCreate();
        //3 执行代码
        Dataset<Row> dataset = ss.read().json("input/user.json");
        dataset.createTempView("user_info");

        //注册 UDAF
        ss.udf().register("myAvg",udaf(new MyAvg(), Encoders.INT()));

        //求平均值
        ss.sql("select myAvg(age) from user_info").show();


        //4 关闭资源
        ss.stop();
    }
}
