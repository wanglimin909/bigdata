package com.atwlm.spark.sparkcore.accumulator;

import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.util.LongAccumulator;

import java.util.Arrays;
import java.util.List;

/**
 * ClassName: Test01_accumulator
 * Package: com.atwlm.spark.sparkcore.accumulator
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-23 - 11:56
 * @Version: v1.0
 */
public class Test01_accumulator {
    public static void main(String[] args) {
        //1.创建spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkCore").setMaster("local[2]");
        //2.创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(sparkConf);

        SparkContext sparkContext = JavaSparkContext.toSparkContext(sc);//sparkContext里面才有累加器

        LongAccumulator acc = sparkContext.longAccumulator();//累加器

        //3.编写逻辑代码
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
        JavaRDD<Integer> javaRDD = sc.parallelize(list,2);

        JavaRDD<Integer> map = javaRDD.map(new Function<Integer, Integer>() {
            @Override
            public Integer call(Integer v1) throws Exception {

                acc.add(v1);

                return v1;
            }
        });

        map.collect();//15
        map.first();//1
        //每个job执行一个累加器
        System.out.println(acc.value());//16

        //4.关闭资源
        sc.stop();
    }
}
