package com.atwlm.spark.sparkstreaming;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.Optional;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka010.ConsumerStrategies;
import org.apache.spark.streaming.kafka010.KafkaUtils;
import org.apache.spark.streaming.kafka010.LocationStrategies;
import scala.Tuple2;

import java.util.*;

/**
 * ClassName: Test01_kafka
 * Package: com.atwlm.spark.sparkstreaming
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-28 - 12:52
 * @Version: v1.0
 */
public class Test03_with_state {
    public static void main(String[] args) throws InterruptedException {
        //创建流环境
        SparkConf sparkConf = new SparkConf().setAppName("SparkStreaming").setMaster("local[2]");
        JavaStreamingContext jssc = new JavaStreamingContext(sparkConf, Duration.apply(3000));
        jssc.checkpoint("ck");

        List<String> topics = Arrays.asList("first");
        HashMap<String, Object> kafkaParams = new HashMap<>();
        kafkaParams.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,"hadoop102:9092,hadoop103:9092");
        kafkaParams.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        kafkaParams.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        kafkaParams.put(ConsumerConfig.GROUP_ID_CONFIG,"sparkStreaming");
        //写代码
        JavaInputDStream<ConsumerRecord<String , String>> directStream = KafkaUtils.createDirectStream(
                jssc,
                //PreferBrokers() : 将计算的节点运行在kafka的Topic的leader所在的节点
                //PreferConsistent() : 找集群上空闲的机器运行计算节点
                //PreferFixed() : 指定计算的节点在哪个机器上运行
                LocationStrategies.PreferBrokers(),
                ConsumerStrategies.Subscribe(topics, kafkaParams)
        );
        // ConsumerRecord 是消费者封装的一个类，消费者读出来的数据在ConsumerRecord里
        JavaDStream<String> map = directStream.map(new Function<ConsumerRecord<String, String>, String>() {
            @Override
            public String call(ConsumerRecord<String, String> stringStringConsumerRecord) throws Exception {
                return stringStringConsumerRecord.value();
            }
        });

        JavaDStream<String> flatMap = map.flatMap(new FlatMapFunction<String, String>() {
            @Override
            public Iterator<String> call(String s) throws Exception {
                ArrayList<String> result = new ArrayList<>();
                String[] split = s.split(" ");
                for (String word : split) {
                    result.add(word);
                }
                return result.iterator();
            }
        });

        JavaPairDStream<String, Integer> mapToPair = flatMap.mapToPair(new PairFunction<String, String, Integer>() {
            @Override
            public Tuple2<String, Integer> call(String s) throws Exception {
                return new Tuple2<>(s, 1);
            }
        });

        //第一个参数：List集合里存的是 一个统计周期相同key的value值 [1,1]
        //第二个参数：状态
        //第三个参数：返回值，先把状态里的数据读出来，然后将list集合的数据累加到状态里返回
        JavaPairDStream<String, Integer> updateStateByKey = mapToPair.updateStateByKey(new Function2<List<Integer>, Optional<Integer>, Optional<Integer>>() {

            @Override
            public Optional<Integer> call(List<Integer> elems, Optional<Integer> state) throws Exception {
                //获取当前key的状态：有就获取出来，没有就给一个默认值
                Integer newState = state.orElse(0);
                for (Integer elem : elems) {
                    newState += elem;
                }
                return Optional.of(newState);
            }
        });

        updateStateByKey.print();


        //执行流的任务
        jssc.start();
        jssc.awaitTermination();

    }
}
