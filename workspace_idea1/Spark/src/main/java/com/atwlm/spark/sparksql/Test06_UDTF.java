package com.atwlm.spark.sparksql;

import com.atwlm.spark.sparksql.bean.User;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.sql.*;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * ClassName: Test06_UDTF
 * Package: com.atwlm.spark.sparksql
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-24 - 12:46
 * @Version: v1.0
 */
public class Test06_UDTF {
    public static void main(String[] args) throws AnalysisException {
        //1 创建Spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkSql").setMaster("local[2]");
        //2 创建SparkSql的运行环境
        SparkSession ss = SparkSession.builder().config(sparkConf).getOrCreate();
        //3 执行代码
        Dataset<Row> dataset = ss.read().json("input/user.json");

        Dataset<User> flatMap = dataset.flatMap(new FlatMapFunction<Row, User>() {
            @Override
            public Iterator<User> call(Row row) throws Exception {
                ArrayList<User> result = new ArrayList<>();
                User user = new User(row.getLong(0),row.getString(1));
                result.add(user);
                result.add(user);
                result.add(user);
                result.add(user);
                return result.iterator();
            }
        }, Encoders.bean(User.class));

        flatMap.createTempView("user_info");

        ss.sql("select * from user_info").show();

        //4 关闭资源
        ss.stop();
    }
}
