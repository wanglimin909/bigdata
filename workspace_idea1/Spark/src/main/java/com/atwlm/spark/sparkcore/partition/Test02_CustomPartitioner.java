package com.atwlm.spark.sparkcore.partition;

import org.apache.spark.HashPartitioner;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function2;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * ClassName: Test02_CustomPartitioner
 * Package: com.atwlm.spark.sparkcore.partition
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-22 - 17:09
 * @Version: v1.0
 */
public class Test02_CustomPartitioner {
    public static void main(String[] args) {
        //1.创建spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkCore").setMaster("local[2]");
        //2.创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
        //3.编写逻辑代码
        ArrayList<Tuple2<Integer, Integer>> list = new ArrayList<>();
        list.add(new Tuple2<>(1, 1));list.add(new Tuple2<>(1, 1));list.add(new Tuple2<>(1, 1));
        list.add(new Tuple2<>(1, 1));list.add(new Tuple2<>(1, 1));list.add(new Tuple2<>(1, 1));
        list.add(new Tuple2<>(1, 1));list.add(new Tuple2<>(1, 1));list.add(new Tuple2<>(1, 1));
        list.add(new Tuple2<>(3, 3));
        list.add(new Tuple2<>(5, 5));
        list.add(new Tuple2<>(4, 4));
        list.add(new Tuple2<>(2, 2));list.add(new Tuple2<>(2, 2));list.add(new Tuple2<>(2, 2));
        list.add(new Tuple2<>(2, 2));list.add(new Tuple2<>(2, 2));list.add(new Tuple2<>(2, 2));
        list.add(new Tuple2<>(2, 2));list.add(new Tuple2<>(2, 2));list.add(new Tuple2<>(2, 2));

        JavaPairRDD<Integer, Integer> pairRDD = sc.parallelizePairs(list, 2);

        JavaPairRDD<Integer, Integer> sortByKey = pairRDD.sortByKey();

        //JavaPairRDD<Integer, Integer> partitionBy = sortByKey.partitionBy(new HashPartitioner(2));

        //自定义分区器
        JavaPairRDD<Integer, Integer> partitionBy = sortByKey.partitionBy(new CustomPartitioner(2));

        JavaRDD<String> withIndex = partitionBy.mapPartitionsWithIndex(new Function2<Integer, Iterator<Tuple2<Integer, Integer>>, Iterator<String>>() {
            @Override
            public Iterator<String> call(Integer numPartition, Iterator<Tuple2<Integer, Integer>> v2) throws Exception {
                ArrayList<String> result = new ArrayList<>();
                while (v2.hasNext()) {
                    Tuple2<Integer, Integer> next = v2.next();
                    result.add(numPartition + "----" + next);
                }
                return result.iterator();
            }
        }, false);

        withIndex.collect().forEach(System.out::println);

        //4.关闭资源
        sc.stop();
    }
}
