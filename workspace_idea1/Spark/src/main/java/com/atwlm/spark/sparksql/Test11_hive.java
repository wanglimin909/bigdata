package com.atwlm.spark.sparksql;

import org.apache.spark.SparkConf;
import org.apache.spark.sql.SparkSession;

/**
 * ClassName: Test11_hive
 * Package: com.atwlm.spark.sparksql
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-24 - 14:32
 * @Version: v1.0
 */
public class Test11_hive {
    public static void main(String[] args) {

        System.setProperty("HADOOP_USER_NAME","atwlm");

        //1 创建Spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkSql").setMaster("local[2]");
        //2 创建SparkSql的运行环境
        SparkSession ss = SparkSession.builder().enableHiveSupport().config(sparkConf).getOrCreate();
        //3 执行代码

        ss.sql("show databases").show();
        ss.sql("show tables").show();
//        ss.sql("create table user_info(name string,age bigint)");
//        ss.sql("show tables").show();
//        ss.sql("insert into table user_info values('zhangsan',19)");
//        ss.sql("select * from user_info").show();

        //4 关闭资源
        ss.stop();
    }
}
