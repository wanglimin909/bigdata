package com.atwlm.spark.sparkcore.wordcount;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.util.ArrayList;

/**
 * ClassName: Test04_WordCount_Lambda
 * Package: com.atwlm.spark.sparkcore.wordcount
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-21 - 20:04
 * @Version: v1.0
 */
public class Test04_WordCount_Lambda {
    public static void main(String[] args) {
        //1.创建spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkCore").setMaster("local[2]");
        //2.创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
        //3.编写逻辑代码
        sc
                .textFile("input/1.txt",2)
                .flatMapToPair(line -> {
                    String[] split = line.split(" ");
                    ArrayList<Tuple2<String, Integer>> result = new ArrayList<>();
                    for (String word : split) {
                        if(!"".equals(word) && word != null){
                            result.add(new Tuple2<>(word,1));
                        }
                    }
                    return result.iterator();
                })
                .reduceByKey((sum,elem) -> sum + elem)
                .collect()
                .forEach(System.out::println)
        ;

        //4.关闭资源
        sc.stop();
    }
}
