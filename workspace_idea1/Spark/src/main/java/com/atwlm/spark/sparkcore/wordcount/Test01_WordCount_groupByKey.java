package com.atwlm.spark.sparkcore.wordcount;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import scala.Tuple2;

import java.util.Arrays;
import java.util.Iterator;

/**
 * ClassName: Test01_WordCount_groupByKey
 * Package: com.atwlm.spark.sparkcore.wordcount
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-21 - 18:54
 * @Version: v1.0
 */
public class Test01_WordCount_groupByKey {
    public static void main(String[] args) {
        //1.创建spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkCore").setMaster("local[2]");
        //2.创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
        //3.编写逻辑代码

        JavaRDD<String> lineRDD = sc.textFile("input/1.txt",2);

        JavaRDD<String> filter = lineRDD.filter(new Function<String, Boolean>() {
            @Override
            public Boolean call(String line) throws Exception {
                return !"".equals(line) && line != null;
            }
        });

        //hello world spark flink hadoop kafka flume zookeeper  ->  "hello"  "world".........
        JavaRDD<String> flatMap = filter.flatMap(new FlatMapFunction<String, String>() {
            @Override
            public Iterator<String> call(String line) throws Exception {
                return Arrays.stream(line.split(" ")).iterator();
            }
        });
        
        //"hello"  "world"  ->  ("hello",1)  ("spark",1)
        JavaPairRDD<String, Integer> mapToPair = flatMap.mapToPair(new PairFunction<String, String, Integer>() {
            @Override
            public Tuple2<String, Integer> call(String word) throws Exception {
                return new Tuple2<>(word, 1);
            }
        });

        JavaPairRDD<String, Iterable<Integer>> groupByKey = mapToPair.groupByKey();

        JavaPairRDD<String, Integer> mapValues = groupByKey.mapValues(new Function<Iterable<Integer>, Integer>() {
            @Override
            public Integer call(Iterable<Integer> v1) throws Exception {
                int sum = 0;
                for (Integer elem : v1) {
                    sum += elem;
                }
                return sum;
            }
        });

        mapValues.collect().forEach(System.out::println);

        //4.关闭资源
        sc.stop();
    }
}
