package com.atwlm.spark.sparkcore.createrdd;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

/**
 * ClassName: Test02_file
 * Package: com.atwlm.spark.sparkcore.createrdd
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-18 - 10:54
 * @Version: v1.0
 */
public class Test04_file_partition {

    public static void main(String[] args) {

        //1.创建spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkCore").setMaster("local[2]");
        //2.创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
        //3.编写逻辑代码

        //从文件创建RDD
        JavaRDD<String> lineRDD = sc.textFile("input/1.txt",2);

        //long totalSize = 7 byte;  所有文件的总长度(windows一个换行占两个字符)

        //long goalSize = totalSize / (long)(numSplits == 0 ? 1 : numSplits);
                            //  7 / 2 = 3 byte
        //long minSize = Math.max(job.getLong("mapreduce.input.fileinputformat.split.minsize", 1L), this.minSplitSize);
                            //  1 byte
        //long blockSize = file.getBlockSize();
                            //  128M

        //long splitSize = Math.max(minSize, Math.min(goalSize, blockSize))
                            //  Math.max(1byte, Math.min(3byte, 128M))
                            //  3byte

        //按照剩余文件总大小 > splitSize的1.1倍, 就切割,但是切的时候还是按照splitSize切

        //注意：切割的时候，如果切到一行的中间，不能打断，必须把一整行全拿出来

        lineRDD.saveAsTextFile("output");

        //4.关闭资源
        sc.stop();

    }
}
