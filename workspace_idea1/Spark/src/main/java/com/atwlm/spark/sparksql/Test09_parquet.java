package com.atwlm.spark.sparksql;

import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;

/**
 * ClassName: Test09_parquet
 * Package: com.atwlm.spark.sparksql
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-24 - 13:39
 * @Version: v1.0
 */
public class Test09_parquet {
    public static void main(String[] args) {
        //1 创建Spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkSql").setMaster("local[2]");
        //2 创建SparkSql的运行环境
        SparkSession ss = SparkSession.builder().config(sparkConf).getOrCreate();
        //3 执行代码
//        Dataset<Row> json = ss
//                .read()
//                .json("input/user.json");
//
//        json
//                .write()
//                .mode(SaveMode.Overwrite)
//                .parquet("output")
//        ;

        ss.read().parquet("output").show();



        //4 关闭资源
        ss.stop();
    }
}
