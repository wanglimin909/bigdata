package com.atwlm.spark.sparkcore.cache;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.storage.StorageLevel;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * ClassName: Test02_WordCount_reduceByKey
 * Package: com.atwlm.spark.sparkcore.wordcount
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-21 - 19:13
 * @Version: v1.0
 */
public class Test01_cache {
    public static void main(String[] args) {
        //1.创建spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkCore").setMaster("local[2]");
        //2.创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
        //3.编写逻辑代码

        JavaRDD<String> lineRDD = sc.textFile("input/1.txt", 2);

        JavaRDD<Tuple2<String, Integer>> flatMap = lineRDD.flatMap(new FlatMapFunction<String, Tuple2<String, Integer>>() {
            @Override
            public Iterator<Tuple2<String, Integer>> call(String line) throws Exception {
                String[] split = line.split(" ");
                ArrayList<Tuple2<String, Integer>> result = new ArrayList<>();
                for (String word : split) {
                    if(!"".equals(word) && word != null){
                        result.add(new Tuple2<>(word, 1));
                    }
                }
                return result.iterator();
            }
        });

        JavaPairRDD<String, Integer> mapToPair = flatMap.mapToPair(new PairFunction<Tuple2<String, Integer>, String, Integer>() {
            @Override
            public Tuple2<String, Integer> call(Tuple2<String, Integer> stringIntegerTuple2) throws Exception {
                return stringIntegerTuple2;
            }
        });

        //后面reduceByKey和groupByKey各开启一个job，但Stage0逻辑完全一样，故在此开启cache缓存
        mapToPair.cache();//有的话直接拿，没有就计算完放进去。下一个job再判断，看有没有。

        //cache缓存是缓存在Spark维护的临时目录，随着Application结束，目录就清除

        // cache() = persist(StorageLevel.MEMORY_ONLY())
        //该缓存算子可设置存储位置（内存、磁盘、...........）
//        mapToPair.persist(StorageLevel.MEMORY_ONLY());

        JavaPairRDD<String, Integer> reduceByKey = mapToPair.reduceByKey(new Function2<Integer, Integer, Integer>() {
            @Override
            public Integer call(Integer sum, Integer elem) throws Exception {
                return sum + elem;
            }
        });

        JavaPairRDD<String, Iterable<Integer>> groupByKey = mapToPair.groupByKey();

        reduceByKey.collect().forEach(System.out::println);

        System.out.println(groupByKey.first());

        //4.关闭资源
        sc.stop();
    }
}
