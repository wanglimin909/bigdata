package com.atwlm.spark.sparkcore.cache;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * ClassName: Test02_WordCount_reduceByKey
 * Package: com.atwlm.spark.sparkcore.wordcount
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-21 - 19:13
 * @Version: v1.0
 */
public class Test02_checkpoint {
    public static void main(String[] args) {

//        System.setProperty("HADOOP_USER_NAME","atwlm");//给到忘hdfs写的权限，放到 new JavaSparkContext 的上面

        //1.创建spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkCore").setMaster("local[2]");
        //2.创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(sparkConf);

        sc.setCheckpointDir("ck");
        //放到hdfs上相对更安全
//        sc.setCheckpointDir("hdfs://hadoop102:8020/ck");

        //3.编写逻辑代码

        JavaRDD<String> lineRDD = sc.textFile("input/1.txt", 2);

        JavaRDD<Tuple2<String, Integer>> flatMap = lineRDD.flatMap(new FlatMapFunction<String, Tuple2<String, Integer>>() {
            @Override
            public Iterator<Tuple2<String, Integer>> call(String line) throws Exception {
                String[] split = line.split(" ");
                ArrayList<Tuple2<String, Integer>> result = new ArrayList<>();
                for (String word : split) {
                    if(!"".equals(word) && word != null){
                        result.add(new Tuple2<>(word, 1));
                    }
                }
                return result.iterator();
            }
        });

        JavaPairRDD<String, Integer> mapToPair = flatMap.mapToPair(new PairFunction<Tuple2<String, Integer>, String, Integer>() {
            @Override
            public Tuple2<String, Integer> call(Tuple2<String, Integer> stringIntegerTuple2) throws Exception {
                return stringIntegerTuple2;
            }
        });

        mapToPair.cache();//结合cache缓存对checkpoint进行优化

        //checkpoint需要开发人员自己指定有一个存储目录，Application不删除；但是checkpoint打断血缘关系，
        //如果ck目录丢了，就没法从头计算了，所以，ck可以选择放在相对来说更安全的文件系统上 HDFS。
        mapToPair.checkpoint();

        JavaPairRDD<String, Integer> reduceByKey = mapToPair.reduceByKey(new Function2<Integer, Integer, Integer>() {
            @Override
            public Integer call(Integer sum, Integer elem) throws Exception {
                return sum + elem;
            }
        });

        JavaPairRDD<String, Iterable<Integer>> groupByKey = mapToPair.groupByKey();


        System.out.println(groupByKey.first());

        reduceByKey.collect().forEach(System.out::println);
        
        //4.关闭资源
        sc.stop();
    }
}
