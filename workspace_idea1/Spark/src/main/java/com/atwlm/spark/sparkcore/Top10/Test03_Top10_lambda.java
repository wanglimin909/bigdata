package com.atwlm.spark.sparkcore.Top10;

import com.atwlm.spark.sparkcore.bean.CategoryCountInfo;
import com.atwlm.spark.sparkcore.bean.UserVisitAction;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.util.ArrayList;

/**
 * ClassName: Test03_Top10_lambda
 * Package: com.atwlm.spark.sparkcore.Top10
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-23 - 15:01
 * @Version: v1.0
 */
public class Test03_Top10_lambda {
    public static void main(String[] args) {
        //1.创建spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkCore").setMaster("local[2]");
        //2.创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
        //3.编写逻辑代码
        sc
                .textFile("input/user_visit_action.txt", 2)
                .map(line -> {
                    String[] split = line.split("_");
                    return new UserVisitAction(
                            split[0],
                            split[1],
                            split[2],
                            split[3],
                            split[4],
                            split[5],
                            split[6],
                            split[7],
                            split[8],
                            split[9],
                            split[10],
                            split[11],
                            split[12]
                    );
                })
                .flatMapToPair(userVisitAction -> {
                    ArrayList<Tuple2<String, CategoryCountInfo>> result = new ArrayList<>();

                    if (!"-1".equals(userVisitAction.getClick_category_id())) {
                        //点击数
                        result.add(new Tuple2<>(userVisitAction.getClick_category_id(), new CategoryCountInfo(userVisitAction.getClick_category_id(), 1L, 0L, 0L)));
                    } else if (!"null".equals(userVisitAction.getOrder_category_ids())) {
                        //下单数
                        String[] ids = userVisitAction.getOrder_category_ids().split(",");
                        for (String id : ids) {
                            result.add(new Tuple2<>(id, new CategoryCountInfo(id, 0L, 1L, 0L)));
                        }
                    } else if (!"null".equals(userVisitAction.getPay_category_ids())) {
                        //支付数
                        String[] ids = userVisitAction.getPage_id().split(",");
                        for (String id : ids) {
                            result.add(new Tuple2<>(id, new CategoryCountInfo(id, 0L, 0L, 1L)));
                        }
                    }
                    return result.iterator();
                })
                .reduceByKey((sum,elem) -> {
                    sum.setClickCount(sum.getClickCount() + elem.getClickCount());
                    sum.setOrderCount(sum.getOrderCount() + elem.getOrderCount());
                    sum.setPayCount(sum.getPayCount() + elem.getPayCount());
                    return sum;
                })
                .map(v1 -> v1._2)
                .sortBy(v1 -> v1,false,2)
                .take(10)
                .forEach(System.out::println)

        ;

        //4.关闭资源
        sc.stop();
    }
}
