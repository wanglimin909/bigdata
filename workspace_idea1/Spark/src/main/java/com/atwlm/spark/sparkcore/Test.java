package com.atwlm.spark.sparkcore;

import scala.Tuple2;

/**
 * ClassName: Test
 * Package: com.atwlm.spark.sparkcore
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-20 - 12:00
 * @Version: v1.0
 */
public class Test {
    public static void main(String[] args) {
        /**
         * Tuple 测试
         */
        Tuple2<String, Integer> tuple2 = new Tuple2<>("A", 1);
        System.out.println(tuple2);

        System.out.println(tuple2._1);
        System.out.println(tuple2._2);

    }
}
