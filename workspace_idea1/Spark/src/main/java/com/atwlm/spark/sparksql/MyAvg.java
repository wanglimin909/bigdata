package com.atwlm.spark.sparksql;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.expressions.Aggregator;

import java.io.Serializable;

/**
 * ClassName: MyAvg
 * Package: com.atwlm.spark.sparksql
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-24 - 12:12
 * @Version: v1.0
 */
public class MyAvg extends Aggregator<Integer, MyAvg.Buffer,Double> {

    /**
     * 初始化 buffer
     */
    @Override
    public Buffer zero() {
        return new Buffer(0,0);
    }

    /**
     * 分区内预聚合
     */
    @Override
    public Buffer reduce(Buffer b, Integer age) {
        b.setSum(b.getSum() + age);
        b.setCount(b.getCount() + 1);
        return b;
    }

    /**
     * 分区间聚合
     * 只能将b2累加到b1里
     */
    @Override
    public Buffer merge(Buffer sum, Buffer elem) {
        sum.setSum(sum.getSum() + elem.getSum());
        sum.setCount(sum.getCount() + elem.getCount());
        return sum;
    }

    /**
     * 根据 buffer 来计算最终结果
     */
    @Override
    public Double finish(Buffer reduction) {
        return reduction.getSum().doubleValue() / reduction.getCount();
    }

    /**
     * 指定 buffer 的类型
     */
    @Override
    public Encoder<Buffer> bufferEncoder() {
        return Encoders.bean(Buffer.class);
    }

    /**
     * 指定输出的类型
     */
    @Override
    public Encoder<Double> outputEncoder() {
        return Encoders.DOUBLE();
    }

    // 平均值 = sum / count
    // Buffer : 缓存计算最终值的中间结果
    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public static class Buffer implements Serializable {
        private Integer sum;
        private Integer count;
    }

}
