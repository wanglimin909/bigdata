package com.atwlm.spark.sparkcore.dependency;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * ClassName: Test02_WordCount_reduceByKey
 * Package: com.atwlm.spark.sparkcore.wordcount
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-21 - 19:13
 * @Version: v1.0
 */
public class Test01_dep {
    public static void main(String[] args) throws InterruptedException {
        //1.创建spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkCore").setMaster("local[2]");
        //2.创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
        //3.编写逻辑代码

        JavaRDD<String> lineRDD = sc.textFile("input/1.txt", 2);

        JavaRDD<Tuple2<String, Integer>> flatMap = lineRDD.flatMap(new FlatMapFunction<String, Tuple2<String, Integer>>() {
            @Override
            public Iterator<Tuple2<String, Integer>> call(String line) throws Exception {
                String[] split = line.split(" ");
                ArrayList<Tuple2<String, Integer>> result = new ArrayList<>();
                for (String word : split) {
                    if(!"".equals(word) && word != null){
                        result.add(new Tuple2<>(word, 1));
                    }
                }
                return result.iterator();
            }
        });

        /**
         * 如何改变分区数量 ：
         * 	分区改少 ：coalesce(1, false)窄依赖；coalesce(1, true) 宽依赖
         * 	分区改多 ：coalesce(4, true) 宽依赖；coalesce(4, false) 不生效
         *
         * 	还有一个算子：
         * 	    repartition(4) 底层就是 coalesce(4, true)
         */
        //窄依赖改分区数
        //JavaRDD<Tuple2<String, Integer>> coalesce = flatMap.coalesce(1, false);


        JavaPairRDD<String, Integer> mapToPair = flatMap.mapToPair(new PairFunction<Tuple2<String, Integer>, String, Integer>() {
            @Override
            public Tuple2<String, Integer> call(Tuple2<String, Integer> stringIntegerTuple2) throws Exception {
                return stringIntegerTuple2;
            }
        });

        JavaPairRDD<String, Integer> reduceByKey = mapToPair.reduceByKey(new Function2<Integer, Integer, Integer>() {
            @Override
            public Integer call(Integer sum, Integer elem) throws Exception {
                return sum + elem;
            }
        },3);
        
        reduceByKey.collect().forEach(System.out::println);

        //血缘关系
        System.out.println(reduceByKey.toDebugString());
        /**
         * (2) ShuffledRDD[4] at reduceByKey at Test02_dep.java:55 []
         *  +-(2) MapPartitionsRDD[3] at mapToPair at Test02_dep.java:48 []
         *     |  MapPartitionsRDD[2] at flatMap at Test02_dep.java:34 []
         *     |  input/1.txt MapPartitionsRDD[1] at textFile at Test02_dep.java:32 []
         *     |  input/1.txt HadoopRDD[0] at textFile at Test02_dep.java:32 []
         */

        //4.关闭资源
        sc.stop();

    }
}
