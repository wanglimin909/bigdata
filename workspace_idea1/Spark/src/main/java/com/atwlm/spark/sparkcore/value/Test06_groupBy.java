package com.atwlm.spark.sparkcore.value;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import scala.Tuple2;

import java.util.Arrays;
import java.util.List;

/**
 * ClassName: Test06
 * Package: com.atwlm.spark.sparkcore.value
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-20 - 14:18
 * @Version: v1.0
 */
public class Test06_groupBy {
    public static void main(String[] args) {
        //1.创建spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkCore").setMaster("local[2]");
        //2.创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
        //3.编写逻辑代码
        List<String> list = Arrays.asList("A", "B", "C", "D", "A","B","A");

        JavaRDD<String> javaRDD = sc.parallelize(list,2);

        // wc 第一步  标记
        // wc 第二步  将相同的一个key放到一个组
        // wc 第三步  将一个组当中的相同key的value值聚合

        JavaRDD<Tuple2<String, Integer>> map = javaRDD.map(new Function<String, Tuple2<String, Integer>>() {
            @Override
            public Tuple2<String, Integer> call(String word) throws Exception {
                return new Tuple2<>(word, 1);
            }
        });

        //分组  group By
        //传入  上一个RDD的数据
        //返回值  分组的字段
        JavaPairRDD<String, Iterable<Tuple2<String, Integer>>> groupBy = map.groupBy(new Function<Tuple2<String, Integer>, String>() {

            @Override
            public String call(Tuple2<String, Integer> v1) throws Exception {
                return v1._1;
            }
        });

        //聚合
        JavaRDD<Tuple2<String, Integer>> map1 = groupBy.map(new Function<Tuple2<String, Iterable<Tuple2<String, Integer>>>, Tuple2<String, Integer>>() {
            @Override
            public Tuple2<String, Integer> call(Tuple2<String, Iterable<Tuple2<String, Integer>>> v1) throws Exception {
                int sum = 0;
                for (Tuple2<String, Integer> elem : v1._2) {
                    sum += elem._2;
                }
                return new Tuple2<>(v1._1,sum);
            }
        });

        map1.collect().forEach(System.out::println);

        System.out.println("------------------groupBy lambda------------------------");

        javaRDD
                .map(word -> new Tuple2<>(word,1))
                .groupBy(v1 -> v1._1)
                .map(v1 -> {
                    int sum = 0;
                    for (Tuple2<String, Integer> elem : v1._2) {
                        sum += elem._2;
                    }
                    return new Tuple2<>(v1._1,sum);
                })
                .collect()
                .forEach(System.out::println);

        //4.关闭资源
        sc.stop();
    }
}
