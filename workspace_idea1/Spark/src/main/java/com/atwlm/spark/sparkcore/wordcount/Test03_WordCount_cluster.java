package com.atwlm.spark.sparkcore.wordcount;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * ClassName: Test02_WordCount_reduceByKey
 * Package: com.atwlm.spark.sparkcore.wordcount
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-21 - 19:13
 * @Version: v1.0
 */
public class Test03_WordCount_cluster {
    public static void main(String[] args) {
        //1.创建spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkCore").setMaster("yarn");
        //2.创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
        //3.编写逻辑代码

        JavaRDD<String> lineRDD = sc.textFile(args[0], 2);

        JavaRDD<Tuple2<String, Integer>> flatMap = lineRDD.flatMap(new FlatMapFunction<String, Tuple2<String, Integer>>() {
            @Override
            public Iterator<Tuple2<String, Integer>> call(String line) throws Exception {
                String[] split = line.split(" ");
                ArrayList<Tuple2<String, Integer>> result = new ArrayList<>();
                for (String word : split) {
                    if(!"".equals(word) && word != null){
                        result.add(new Tuple2<>(word, 1));
                    }
                }
                return result.iterator();
            }
        });

        JavaPairRDD<String, Integer> mapToPair = flatMap.mapToPair(new PairFunction<Tuple2<String, Integer>, String, Integer>() {
            @Override
            public Tuple2<String, Integer> call(Tuple2<String, Integer> stringIntegerTuple2) throws Exception {
                return stringIntegerTuple2;
            }
        });

        JavaPairRDD<String, Integer> reduceByKey = mapToPair.reduceByKey(new Function2<Integer, Integer, Integer>() {
            @Override
            public Integer call(Integer sum, Integer elem) throws Exception {
                return sum + elem;
            }
        });
        
        reduceByKey.saveAsTextFile(args[1]);
        
        //4.关闭资源
        sc.stop();
    }
}

/**
 * 提交命令：
 * 
           bin/spark-submit \
           --class com.atwlm.spark.sparkcore.wordcount.Test03_WordCount_cluster \
           --deploy-mode cluster \
           --master yarn \
           ./Spark-1.0-SNAPSHOT.jar \
           /input/1.txt \
           /output
 *
 */