package com.atwlm.spark.sparkstreaming;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.Optional;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function0;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka010.ConsumerStrategies;
import org.apache.spark.streaming.kafka010.KafkaUtils;
import org.apache.spark.streaming.kafka010.LocationStrategies;
import scala.Tuple2;

import java.util.*;

/**
 * ClassName: Test01_kafka
 * Package: com.atwlm.spark.sparkstreaming
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-28 - 12:52
 * @Version: v1.0
 */
public class Test04_state_with_checkpoint {
    public static void main(String[] args) throws InterruptedException {

        JavaStreamingContext jssc = JavaStreamingContext.getOrCreate("ck", new Function0<JavaStreamingContext>() {
            @Override
            public JavaStreamingContext call() throws Exception {
                SparkConf sparkConf = new SparkConf().setAppName("SparkStreaming").setMaster("local[2]");
                JavaSparkContext sc = new JavaSparkContext(sparkConf);
                JavaStreamingContext jssc = new JavaStreamingContext(sc, Duration.apply(3000));
                jssc.checkpoint("ck");
                //写代码***************************************************************************************
                List<String> topics = Collections.singletonList("first");
                HashMap<String, Object> kafkaParams = new HashMap<>();
                kafkaParams.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,"hadoop102:9092,hadoop103:9092");
                kafkaParams.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
                kafkaParams.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
                kafkaParams.put(ConsumerConfig.GROUP_ID_CONFIG,"sparkStreaming");

                JavaInputDStream<ConsumerRecord<String, String>> directStream = KafkaUtils.createDirectStream(
                        jssc,
                        LocationStrategies.PreferBrokers(),
                        ConsumerStrategies.Subscribe(topics, kafkaParams)
                );

                JavaDStream<String> map = directStream.map(new Function<ConsumerRecord<String, String>, String>() {
                    @Override
                    public String call(ConsumerRecord<String, String> stringStringConsumerRecord) throws Exception {
                        return stringStringConsumerRecord.value();
                    }
                });

                JavaPairDStream<String, Integer> flatMapToPair = map.flatMapToPair(new PairFlatMapFunction<String, String, Integer>() {
                    @Override
                    public Iterator<Tuple2<String, Integer>> call(String s) throws Exception {
                        ArrayList<Tuple2<String, Integer>> result = new ArrayList<>();
                        String[] split = s.split(" ");
                        for (String word : split) {
                            result.add(new Tuple2<>(word, 1));
                        }
                        return result.iterator();
                    }
                });

                JavaPairDStream<String, Integer> updateStateByKey = flatMapToPair.updateStateByKey(new Function2<List<Integer>, Optional<Integer>, Optional<Integer>>() {

                    @Override
                    public Optional<Integer> call(List<Integer> elems, Optional<Integer> state) throws Exception {
                        Integer newState = state.orElse(0);
                        for (Integer elem : elems) {
                            newState += elem;
                        }
                        return Optional.of(newState);
                    }
                });

                updateStateByKey.print();

                //写代码***************************************************************************************
                return jssc;
            }
        });

        jssc.start();
        jssc.awaitTermination();

    }
}
