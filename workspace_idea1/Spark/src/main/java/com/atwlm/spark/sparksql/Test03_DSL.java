package com.atwlm.spark.sparksql;

import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

/**
 * ClassName: Test03_DSL
 * Package: com.atwlm.spark.sparksql
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-23 - 17:52
 * @Version: v1.0
 */
public class Test03_DSL {
    public static void main(String[] args) {
        //1 创建Spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkSql").setMaster("local[2]");
        //2 创建SparkSql的运行环境
        SparkSession ss = SparkSession.builder().config(sparkConf).getOrCreate();
        //3 执行代码
        Dataset<Row> dataset = ss.read().json("input/user.json");

        // select age+1 newAge,name from user_info where age >= 18
        dataset.select(dataset.col("age").plus(-1).as("newAge"),dataset.col("name")).where("newAge >= 18").show();


        //4 关闭资源
        ss.stop();
    }
}
