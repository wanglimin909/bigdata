package com.atwlm.spark.sparkcore.action;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.VoidFunction;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * ClassName: Test02_foreach
 * Package: com.atwlm.spark.sparkcore.action
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-21 - 17:14
 * @Version: v1.0
 */
public class Test02_foreach {
    public static void main(String[] args) {
        //1.创建spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkCore").setMaster("local[2]");
        //2.创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
        //3.编写逻辑代码
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
        JavaRDD<Integer> javaRDD = sc.parallelize(list, 2);

        List<Integer> collect = javaRDD.collect();

        //单线程遍历，因为collect给所有Executor数据拉到driver了
        collect.forEach(System.out::println);

        //行动算子遍历是多线程遍历，在executor直接处理
        javaRDD.foreach(new VoidFunction<Integer>() {
            @Override
            public void call(Integer integer) throws Exception {
                System.out.println(integer);
            }
        });

        //一次处理一整个分区的数据，如果需要循环创建连接的话，foreachPartition可以创建更少的连接
        //也有坏处，因为他会一次拿出一整个分区的数据更容易OOM，所以内存不够的时候不要用，而foreach只是一条一条遍历处理
        javaRDD.foreachPartition(new VoidFunction<Iterator<Integer>>() {
            @Override
            public void call(Iterator<Integer> integerIterator) throws Exception {
                System.out.println(integerIterator);
            }
        });

        //4.关闭资源
        sc.stop();
    }
}
