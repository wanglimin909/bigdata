package com.atwlm.spark.sparkcore.createrdd;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

/**
 * ClassName: Test01_list
 * Package: com.atwlm.spark.sparkcore.createrdd
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-17 - 15:48
 * @Version: v1.0
 */
public class Test01_list {

    public static void main(String[] args) {

        //1.创建spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkCore").setMaster("local[2]");
        //2.创建sparkContext
        JavaSparkContext jsc = new JavaSparkContext(sparkConf);
        //3.编写逻辑代码

        //从集合创建RDD
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);

        JavaRDD<Integer> javaRDD = jsc.parallelize(list);

        List<Integer> collect = javaRDD.collect();
        for (Integer integer : collect) {
            System.out.println(integer);
        }

        System.out.println("-------------for lambda----------------");
        collect.forEach(new Consumer<Integer>() {
            @Override
            public void accept(Integer integer) {
                System.out.println(integer);
            }
        });

        System.out.println("-------------for lambda 简化1----------------");
        //用 -> 箭头操作符表示 lambda 表达式
        collect.forEach((Integer integer) -> {
            System.out.println(integer);
        });

        System.out.println("-------------for lambda 简化2----------------");
        //参数列表的参数类型可以省略
        collect.forEach((integer) -> {
            System.out.println(integer);
        });

        System.out.println("-------------for lambda 简化3----------------");
        //如果参数列表中只有一个参数，参数列表的括号也可以省略
        collect.forEach(integer -> {
            System.out.println(integer);
        });

        System.out.println("-------------for lambda 简化4----------------");
        //如果方法体中只有一行，可以省略方法体的 {}
        collect.forEach(integer -> System.out.println(integer));

        System.out.println("-------------for lambda 简化5----------------");
        //如果是 System.out.println,并且只输出数据，不对数据进行处理，可以优化成如下
        collect.forEach(System.out::println);


        //4.关闭资源
        jsc.stop();

    }

}
