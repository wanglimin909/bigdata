package com.atwlm.spark.sparkcore.keyvalue;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import scala.Tuple2;

import java.util.ArrayList;

/**
 * ClassName: Test06_avg
 * Package: com.atwlm.spark.sparkcore.keyvalue
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-21 - 14:08
 * @Version: v1.0
 */
public class Test06_avg_groupByKey {
    public static void main(String[] args) {
        //1.创建spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkCore").setMaster("local[2]");
        //2.创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
        //3.编写逻辑代码

        ArrayList<Tuple2<String ,Integer>> list = new ArrayList<>();
        list.add(new Tuple2<>("A",10));
        list.add(new Tuple2<>("B",15));
        list.add(new Tuple2<>("C",20));
        list.add(new Tuple2<>("A",20));
        list.add(new Tuple2<>("B",11));
        list.add(new Tuple2<>("A",14));
        list.add(new Tuple2<>("C",30));
        JavaPairRDD<String, Integer> pairRDD = sc.parallelizePairs(list,2);

        // avg = sum / count
        JavaPairRDD<String, Iterable<Integer>> groupByKey = pairRDD.groupByKey();
        JavaPairRDD<String, Double> mapValues = groupByKey.mapValues(new Function<Iterable<Integer>, Double>() {
            @Override
            public Double call(Iterable<Integer> v1) throws Exception {
                int sum = 0;
                int count = 0;
                for (Integer elem : v1) {
                    sum += elem;
                    count++;
                }
                return (double) (sum / count);
            }
        });
        mapValues.collect().forEach(System.out::println);

        System.out.println("-----------------avg lambda---------------------");

        pairRDD
                .groupByKey()
                .mapValues(v1 -> {
                    int sum = 0;
                    int count = 0;
                    for (Integer elem : v1) {
                        sum += elem;
                        count ++;
                    }
                    return (double)(sum /count);
                })
                .collect()
                .forEach(System.out::println)
        ;

        //4.关闭资源
        sc.stop();
    }
}
