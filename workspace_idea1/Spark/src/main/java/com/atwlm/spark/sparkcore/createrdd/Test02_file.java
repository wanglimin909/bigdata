package com.atwlm.spark.sparkcore.createrdd;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

/**
 * ClassName: Test02_file
 * Package: com.atwlm.spark.sparkcore.createrdd
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-18 - 10:54
 * @Version: v1.0
 */
public class Test02_file {

    public static void main(String[] args) {

        //1.创建spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkCore").setMaster("local[2]");
        //2.创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
        //3.编写逻辑代码

        //从文件创建RDD
        JavaRDD<String> lineRDD = sc.textFile("input/1.txt");

        lineRDD.collect().forEach(System.out::println);

        //4.关闭资源
        sc.stop();

    }
}
