package com.atwlm.spark.sparkcore.value;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import scala.Tuple2;

import java.util.Arrays;
import java.util.List;

/**
 * ClassName: Test07_distinct
 * Package: com.atwlm.spark.sparkcore.value
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-20 - 17:47
 * @Version: v1.0
 */
public class Test07_distinct {
    public static void main(String[] args) throws InterruptedException {
        //1.创建spark配置
        SparkConf sparkConf = new SparkConf().setAppName("SparkCore").setMaster("local[2]");
        //2.创建sparkContext
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
        //3.编写逻辑代码
        List<String> list = Arrays.asList("A", "B", "C", "D", "A");

        JavaRDD<String> javaRDD = sc.parallelize(list,2);

        JavaRDD<String> distinct = javaRDD.distinct(3);
        distinct.collect().forEach(System.out::println);

        System.out.println("------------------distinct 原理-----------------------");

        JavaPairRDD<String, Iterable<String>> groupBy = javaRDD.groupBy(new Function<String, String>() {
            @Override
            public String call(String v1) throws Exception {
                return v1;
            }
        });
        JavaRDD<String> map = groupBy.map(new Function<Tuple2<String, Iterable<String>>, String>() {
            @Override
            public String call(Tuple2<String, Iterable<String>> v1) throws Exception {
                return v1._1;
            }
        });
        map.collect().forEach(System.out::println);

        //4.关闭资源
        sc.stop();
    }
}
