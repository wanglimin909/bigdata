package com.atwlm.spark.sparksql.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * ClassName: user
 * Package: com.atwlm.spark.sparksql.bean
 * Description:
 *
 * @Author wlm.java
 * @Create 2024-01-23 - 16:29
 * @Version: v1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private long age;
    private String name;
}
