package com.atwlm.kafka.partitioner;

import org.apache.kafka.clients.producer.Partitioner;
import org.apache.kafka.common.Cluster;

import java.util.Map;

/**
 * ClassName: CustomPartitioner
 * Package: com.atwlm.kafka.partitioner
 * Description:
 *
 * @Author wlm.java
 * @Create 2023-08-16 - 15:05
 * @Version: v1.0
 */
public class CustomPartitioner implements Partitioner {
    @Override
    public int partition(String topic, Object key, byte[] keyBytes, Object value, byte[] valueBytes, Cluster cluster) {
        //1.获取解析key
        String keyStr = key.toString();
        //2.计算key的哈希值
        int keyHashCode = keyStr.hashCode();
        //3.获取分区个数
        Integer number = cluster.partitionCountForTopic(topic);
        //4.计算分区
        int partition = Math.abs(keyHashCode) % number;
        return partition;
    }

    @Override
    public void close() {

    }

    @Override
    public void configure(Map<String, ?> map) {

    }
}
