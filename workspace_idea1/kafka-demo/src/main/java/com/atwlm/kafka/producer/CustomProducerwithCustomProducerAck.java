package com.atwlm.kafka.producer;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

/**
 * ClassName: CustomProducer
 * Package: com.atwlm.kafka.producer
 * Description:
 *
 * @Author wlm.java
 * @Create 2023-08-15 - 17:04
 * @Version: v1.0
 */
public class CustomProducerwithCustomProducerAck {
    //1.main线程
    public static void main(String[] args) {
        //2.创建配置对象
        Properties properties = new Properties();
//        properties.put("bootstrap.servers","hadoop102:9092,hadoop103:9092,hadoop104:9092");
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,"hadoop102:9092,hadoop103:9092,hadoop104:9092");
        properties.put(ProducerConfig.PARTITIONER_CLASS_CONFIG,"com.atwlm.kafka.partitioner.CustomPartitioner");
        //序列化
//        properties.put("key.serializer", StringSerializer.class.getName());
//        properties.put("value.serializer", StringSerializer.class.getName());
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        // ack
        // 0,1,-1,all(等于-1)
        properties.put(ProducerConfig.ACKS_CONFIG,"all");
        //重试次数，默认int的最大值
        properties.put(ProducerConfig.RETRIES_CONFIG,3);

        //幂等性
        properties.put(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG,"true");

        //3.创建kafka生产者对象
        KafkaProducer<String, String> producer = new KafkaProducer<String, String>(properties);

        //4.发送数据
        for (int i = 1; i < 11; i++) {
            //5.造数据
            String message="你好，我是"+i+"号";
            //6.创建producerRecord
            final ProducerRecord<String, String> producerRecord = new ProducerRecord<String, String>(
                    "first",
                    i+"",
                    message
            );
            //7.发送数据
            producer.send(producerRecord, new Callback() {
                @Override
                public void onCompletion(RecordMetadata recordMetadata, Exception e) {
                    //7.1 当发送成功时输出原数据信息
                    if(e == null){
                        int partition = recordMetadata.partition();
                        long offset = recordMetadata.offset();
                        System.out.println(producerRecord.value()+",分区："+partition+",偏移量："+offset);
                    }
                }
            });
        }

        //8.关闭资源
        producer.close();

    }
}
